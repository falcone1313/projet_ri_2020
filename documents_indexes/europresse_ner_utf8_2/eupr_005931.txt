Les moineaux qui arpentent les trottoirs en se nourrissant de nos restes vont-ils rapetisser au fur et à mesure que le réchauffement climatique global fera sentir ses effets ? C'est en tout cas ce que semble indiquer une étude qui vient d'être publiée dans la revue spécialisée "The Auk : Ornithological Advances". La chaleur influence la croissance des oisillons Une équipe de scientifiques emmenée par <START:person> Samuel Andrews, <END> biologiste à l'université <START:person> Macquarie ( Australie <END> ), a étudié les populations de moineaux domestiques installées dans diverses régions d' <START:location> Australie <END> et de <START:location> Nouvelle-Zélande, <END> mesurant des représentants de l'espèce dans une trentaine de lieux différents. Le résultat de leurs observations est que le meilleur moyen de prédire la taille adulte de ces oiseaux est de la relier aux températures maximales enregistrées durant l'été, période de reproduction, plutôt qu'aux températures hivernales. Selon eux, cela démontre que des températures plus importantes pendant le développement des oisillons va ensuite influencer leur croissance, et que l'augmentation des températures estivales, liées au changement climatique, pourraient fort bien faire chuter la taille moyenne des moineaux adultes. Les moineaux ne sont pas les seuls concernés, bien que le fait que cette espèce soit très répandue dans le monde en fasse un cas plus emblématique. En 2016, une équipe emmenée par <START:person> Jan <END> A. <START:person> van Gils, <END> de l'université d'Utrecht (Pays-Bas) a mis en évidence la réduction de taille des bécasseaux maubèches liée au réchauffement des régions arctiques où ces migrateurs se reproduisent. L'oiseau qui chantait pour ses oeufs Le gobemouche à collier va lui aussi s'adapter en rétrécissant. Ce passereau a la particularité de chanter pour ses oeufs, et selon une étude menée par <START:person> Mylene Mariette <END> et <START:person> Katherine Buchanan, <END> de l'université Deakin ( <START:location> Australie <END> ), leur chant est différent lorsque les températures maximales dépassent les 26 degrés. Les poussins nés d'oeufs exposés à ce chant particulier sont non seulement plus bavards que la normale, mais ils sont aussi plus petits par rapport aux oisillons de la même espèce nés dans des endroits où la température reste au-dessous des 26°C. Les gobemouches à collier prépareraient donc leurs petits à affronter des climats plus chauds... en devenant plus petits, mais aussi en produisant ensuite davantage d'oiseaux que ceux de poids "normal" quand, à leur tour, ils sont en état de procréer. La taille, ça compte Que les oiseaux "rétrécissent" pour s'adapter, n'est pas sans conséquences. Ainsi, pour les bécasseaux maubèches, les oiseaux plus petits "ont des difficultés à atteindre leur source principale de nourriture, des mollusques enterrés profondément, ce qui fait décroître la survie des oiseaux nés durant les années particulièrement chaudes", note l'équipe de <START:person> Jan van Gils. Samuel Andrews, <END> de son côté, souligne que la réduction de taille pourrait avoir des effets négatifs sur leur condition physique des moineaux. Mais la taille pourrait devenir un indicateur d'adaptation d'une espèce au réchauffement planétaire. "Si la variation de taille corporelle est liée directement ou indirectement à l'adaptation aux différents climats, alors elle est utile pour surveiller le degré auquel les populations d'oiseaux sont capables de s'adapter rapidement aux climats changeants," explique <START:person> Samuel Andrew. <END> Cela pourrait même aider à "potentiellement identifier les espèces les plus à risque" dans les nouvelles conditions climatiques. Plus gros quand il fait froid ? Mais pourquoi la taille serait-elle liée à la température ? Une vieille règle empirique toujours citée en biologie explique que plus il fait froid, plus les animaux ont tendance à être gros. L'une des explications avancées serait que le rapport entre la surface exposée à l'air et leur masse corporelle devient alors plus petit, ce qui leur permet de conserver davantage de chaleur. Une autre raison pour avoir des corps plus gros serait d'accumuler davantage de graisses pour apporter du "carburant" à l'organisme durant les rudes hivers. A l'inverse, lorsque les températures sont élevées, il faut que la chaleur se diffuse et des corps plus petits y aideraient pour des raisons exactement inverses. Ces éléments sont d'autant plus importants pour les animaux à sang chaud (mammifères, oiseaux...) qui ont besoin de maintenir une température corporelle stable. Cette règle a des exceptions et est contestée par certains, mais elle semble s'appliquer à nos bons vieux moineaux domestiques. Il faudra sans doute davantage d'études pour démontrer si la plupart des oiseaux (ou même les mammifères) y seront sujets. Pour <START:person> Tim Parker, <END> un expert du Whitman <START:location> College <END> (USA), l'étude de l'équipe de <START:person> Samuel Andrew <END> "est un ajout important à un ensemble grandissant de travaux qui changent notre compréhension des relations entre le climat et la taille du corps". Dans un monde qui se réchauffe, comment les animaux qui survivront aux influences combinées de l'humanité et du climat vont-ils s'adapter ? Leur taille ne sera pas être qu'un début. Cet article est paru dans L'Obs (site web)
