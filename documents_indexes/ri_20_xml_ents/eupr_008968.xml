<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">La Vallée heureuse</field>
    <field name="author">Jules ROY</field>
    <field name="id">eupr_008968.xml</field>
    <field name="description"><![CDATA[


Il y avait six heures à peine que le lourd quadrimoteur marqué d'un B peint en rouge sur son fuselage, derrière l'Indicatif de l'escadrille, s'était glissé dans la file des bombardiers qui roulaient lentement vers la tour de vigie en gémissant sur leurs freins.


Chevrier revoyait tout, avec une précision bouleversante.


Debout derrière eux, le mécanicien surveillait le tableau des moteurs. Installés aux postes de sécurité du décollage, près des trappes d'évacuation, le navigateur, le radio et le mitrailleur supérieur répondaient de la main au salut des gens du sol. Isolé dans l'étroite tourelle vitrée qui surplombait le vide, à l'extrême pointe de l'empennage, le mitrailleur arrière commençait déjà à mâcher de la gomme.


L'angoisse creusait la poitrine et le ventre de Chevrier, mais il n'aurait cédé sa place pour rien au monde. Ce n'était pas le premier vol qu'il allait faire avec son équipage. Il connaissait les qualités et les défauts de chacun depuis un an que l'exil les avait rassemblés et qu'ils peinaient ensemble. Il avait souvent interrogé les instructeurs et ceux qu'il rencontrait au hasard des camps et des voyages. Les langues restaient liées par quelque chose qui n'était pas seulement de la pudeur. On lui avait tout enseigné, mais personne ne lui avait jamais parlé de l'aspect que prend dans la nuit une ville attaquée par mille avions, et cette confrontation l'intimidait. Une fois seulement, il s'en souvenait tout à coup, un moniteur avait tenu un discours dont la violence et l'amertume l'avaient profondément remué. Il repoussa cette pensée. Ce type-là passait pour un peu fou, et on l'avait presque aussitôt envoyé sur une autre base.


Chevrier était habitué à décoller à la minute imposée avec des ciels bas et d'épais nuages de givre à percer. Mais rien de tout cela ne comptait ce soir-là. L'inquiétude qui frayait en lui son chemin touchait un ordre nouveau de son propre destin, comme s'il avait atteint le saint des saints avant l'apparition de l'ange.


Le pilote n'avait jamais encore arraché un avion aussi lourd. Il pouvait rater son départ ou bien accrocher un arbre, et l'équipage s'écraserait avec les bombes. Cela arrivait quelquefois, et c'était peut-être la pensée maîtresse de l'équipage en cet instant, mais chacun la tenait bien serrée en soi, comme un écolier cache un objet dérobé dans son poing.


Quand Chevrier eut devant lui le large boulevard cimenté de la piste d'envol et que le signal vert fut braqué sur le " B ", une farouche résolution s'empara de lui. Il ressemblait à un enfant pusillanime provoqué par de mauvais garçons, qui, soudain, leur faisait face et les forçait à reculer devant la colère qui l'enflammait. Ainsi les vaines imaginations battirent en retraite et s'évanouirent.


Le pilote ébranla le quadrimoteur, Joua adroitement des gaz pour assurer l'exacte trajectoire de sa masse, l'enleva du sol et l'installa dans le ciel avec une facilité dérisoire. L'équipage soupira. A son tour, il appartenait à l'escadre qui faisait trembler le ciel et la terre, dans le soir qui étalait à l'ouest ses bancs de pourpre et d'or. L'escadre tournoyait, elle sortait des quatre vents en noirs essaims, s'ordonnait peu à peu et devenait une bête énorme et grondante qui serrait ses ailes et s'enfonçait vers la bataille. Au sol, Morin devait entendre les ondes de l'escadre se refermer doucement sur la campagne.


Derrière eux il y avait l'exil et l'impatience. La R.A.P. n'était jamais pressée. C'était une gigantesque machine à transformer les bûcherons du Canada et les maquignons d'

Australie

en chasseurs et en bombardiers. Elle feignait de les oublier deux mois dans des huttes où ils se levaient tard et jouaient aux fléchettes ou au billard à trous.


Au début d'un nouvel hiver de guerre, Chevrier avait souffert d'en être réduit à manger, à boire et à dormir quand sa patrie agonisait. Le pain s'arrêtait dans sa gorge. Il marchait sans but avec Morin, et par les chemins craquants de gel ils parlaient des avions et des hommes qui les pilotent. Ils traversaient des villages aux gazons tondus de près. Ils achetaient dans les boutiques des choses inutiles qu'ils abandonnaient ensuite.


Ils avaient installé leurs lits côte à côte dans une baraque pleine de jeunes officiers. Le soir, pour échapper à la chambrée, on se serrait autour d'un poêle, et Morin contait des histoires de généraux perdus dans le désert ou d'intendants errant sur les quais d'un port en quête d'illusoires tonneaux de chaussures. Les rires l'interrompaient et 11 tirait de sa pipe recourbée de juteuses bouffées malodorantes. Quand les sous-lieutenants partaient en bordée, Morin attendait le retour des vadrouilleurs en pouffant sous ses couvertures. Il était le farceur des lits en bascule, du poil à gratter, des brocs en équilibre sur les portes, et ceux qu'il amusait ne retenaient de lui qu'un visage de gamin espiègle.


Chevrier avait vite dénombré les ombres qui assiégeaient Morin, et comme il avait reconnu en lui un frère, il s'était découvert pour que Morin pût reconnaître à son tour qu'il n'était pas seul. Ce fut ainsi qu'ils devinrent des amis.


Ils s'étaient retrouvés dans une école d'entraînement, où ils avaient appris à saisir les étoiles dans un sextant et à les nommer quand elles montaient une à une à l'horizon. Ils avaient eu les mêmes aventures en mer d'Irlande et sur les îles rases d'au delà du 58o de latitude nord. Puis Chevrier l'avait précédé encore ailleurs. D'Angleterre en Ecosse, il l'attendait, il l'accueillait et le quittait.


A mesure qu'il avançait dans l'exil, Morin perdait sa bonne humeur. Il était devenu acide et grinçant. Par moments, même, il gémissait. Dès qu'il débarquait dans un camp il humait l'air avec lassitude et se laissait choir sur le tas de sacs et de cantines déchargés du camion. Il pliait sous le fardeau des jours. La guerre lui apparaissait si lointaine et si insaisissable qu'il craignait de la voir aboutir à un cul-de-sac au fond duquel se dressait un mur sombre et gras. Tous les équipages qui s'étaient déjà tués lui paraissaient morts pour rien, dans l'énorme brassage indifférent de l'épreuve. Comme s'ils n'étaient morts pour rien d'utile et pour rien de visible, morts bêtement, après avoir grogné pendant des mois sans voir enfin la pâle lueur qui marquait le bout du tunnel, les étoiles de la première nuit de guerre ou l'image d'une maison et d'un jardin. Le soir, les pilotes, debout devant la cheminée du bar, buvaient avec de gros rires d'interminables tournées de bière.


Chaque école ressemblait à la précédente. On ne s'y pressait pas, on rafistolait le matériel, on se tassait un peu plus sous les nuages et sous la pluie. Le matin, Chevrier voyait Morin entrer au réfectoire avec un air traqué, s'arrêter au bout de la queue et revenir, son assiette à la main, chercher une place. Le soir, Morin se dénouait. Chevrier l'engueulait, puis il en avait des remords. Morin flottait comme un boxeur étourdi. Chevrier avait pitié de lui et l'entraînait dans sa chambre. Ils s'installaient entre le lit et la commode, et la chaleur du poêle rappelait la douceur d'une maison d'hommes en hiver. Morin tirait de sa poche sa pipe puante, et souriait. Puis il s'en allait vers le dortoir, qu'il appelait la fosse aux lions.


Après quelques jours dont la douceur avait annoncé le printemps, le ciel s'était brusquement refermé et la neige avait recouvert la campagne. Des masses d'air glacé glissaient impétueusement sur le plateau bordé de collines où l'avant-dernière base était installée. Le camp avait pris une triste figure de banquise ouverte aux vents du nord. Des grains accouraient du fond de l'horizon, culbutaient des masses de nuages aux crêtes déchiquetées par les courants. Des rouleaux d'ombres bleuâtres écrasaient la lumière sur le paysage qu'ils réduisaient à des proportions sinistres de hangars et de clôtures de barbelés.


Un adoucissement enfin était venu de l'ouest, puis le ciel ouvrit ses écluses de soleil et lâcha de hautes galopades d'hippocampes à longue crinière.


Peu à peu, la terre avait livré les molles ondulations des collines aux blés verdoyants et aux labours. Au creux des vallons, les petites fermes, serrées entre leurs murs de pierres grises et leurs haies de noyers, alignaient leurs meules de paille semblables à des ruchers aux toits pointus. L'été s'était avancé et les fumées montaient tout droit dans des nuages dorés, avec leur ventricule gonflé et leur queue aplatie de têtards.


- Ces nuages, disait Morin, on dirait qu'ils vont se mettre à battre la mesure avec les collines comme un métronome.


Les bruits mêmes avaient perdu leur vibration ordinaire pour en acquérir une autre.


Au crépuscule des nuées de quadrimoteurs sortaient de terre en bourdonnant, halaient leur charge haut dans l'espace, puis appareillaient vers le sud en lourdes escadres. L'âme de Morin se nouait, comme s'il s'était enfoncé dans les ténèbres, sans jeter un regard derrière lui ou sur ses flancs.


- Bientôt, disait-il à Chevrier, tu ne seras qu'un petit tas, un tout petit tas comme ça.


Et il rapprochait ses mains pour en marquer l'épaisseur ridicule. Chevrier haussait les épaules et pensait à l'épreuve du feu. Il allait s'y plonger l'âme et le corps, comme un fer qui doit devenir une épée, une étrave de navire ou un soc. Il lui semblait que le fer sous le feu dût changer de nature Jusqu'à devenir lui-même du feu. Pour que le marteau pût l'écraser et lui donner forme d'arme ou d'outil, le fer devait posséder encore sa nature de fer, mais aussi la vertu du feu. " La forge, méditait Chevrier, si elle est un lieu redoutable, est aussi le seul où le fer reçoive le sacre sans quoi il ne servira jamais la croisade ou le labour. "


Chevrier ne pouvait vivre sans désirer la forge. Le fer ne souffrait pas dans les flammes et sur l'enclume, et lui allait gémir sous la morsure des flammes et sous le poids du marteau, mais il s'y plaçait sans hésiter, parce qu'il ne voyait pas comment y échapper sans se perdre en même temps.


(A suivre.)


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008968.xml</field>
    <field name="pdate">1946-09-06T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
