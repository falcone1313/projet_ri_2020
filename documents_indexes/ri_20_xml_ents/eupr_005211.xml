<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Côte-Nord: la remontée du prix du fer redonne espoir</field>
    <field name="author">ANDRÉ DUBUC</field>
    <field name="id">eupr_005211.xml</field>
    <field name="description"><![CDATA[



Lac Bloom : agrandissement en vue



Depuis février 2018, Minerai de fer Québec, filiale de Champion Iron, exploite la mine du lac Bloom, à 13 km de Fermont. Fin juin, elle a déposé une étude de faisabilité qui recommande l'expansion de la mine. L'investissement de 634 millions de dollars vise la mise en service d'un second concentrateur et l'augmentation de la production annuelle de 7,4 à 15 millions de tonnes pendant 20 ans. Le minerai est transporté en train jusqu'au port de Sept-Îles. Grâce à la bonne teneur de son minerai (66 %), la mine du lac Bloom obtient une prime sur le marché par rapport au prix mondial basé sur une teneur de 62 %. « On a commencé à mobiliser des gens, des entrepreneurs, et à acheter des wagons de train, explique au téléphone Michael Marcotte, vice-président de Champion. L'étude technique [43-101] sera dévoilée à la fin du mois. On commencera à dépenser l'argent à l'été 2020. La production devrait commencer dans la première partie de 2021. »



Le financement, pas un obstacle



Le financement ne posera pas de défi insurmontable. Minerai de fer Québec dispose d'environ 200 millions en encaisse et les analystes financiers prévoient que, aux prix actuels des commodités, la minière réalisera un bénéfice avant intérêt, impôt et amortissement de près de 450 millions pour l'année en cours. L'année financière se termine le 31 mars. Les trois quarts de l'investissement requis à la réalisation de la phase 2 de Lac Bloom ont été réalisés par le propriétaire précédent, Cliffs, avant qu'il fasse faillite. Elle y avait auparavant investi 2,4 milliards US. En 2016, Champion a racheté 63,2 % des éléments d'actif de Lac Bloom pour 10,5 millions. Elle a annoncé en mai le rachat de la participation du gouvernement du Québec de 36,8 % pour 211 millions. L'agrandissement donnerait du travail à 500 personnes pendant la construction, puis à 375 personnes pendant l'exploitation. Martin St-Laurent, maire de Fermont, souhaite une annonce officielle d'ici la fin de l'année.



Pourquoi le prix du fer a-t-il augmenté ?



Le fer se vendait le vendredi 26 juillet à 120 $ US la tonne métrique, un prix en hausse de près de 70 % depuis le début de l'année. Le prix a bondi après la rupture d'une digue à roches stériles d'une mine du numéro un mondial du fer, Vale, qui a tué plus de 240 personnes au Brésil. « Ça a retiré temporairement près de 70 millions de tonnes », explique Bernard Gauthier, DG de Développement économique Port-Cartier. Des cyclones ont aussi perturbé les expéditions de fer de l'

Australie

, premier producteur mondial. Du côté de la demande, les investissements du gouvernement chinois pour contrer les effets de la guerre des tarifs avec les Américains ont stimulé la fabrication d'acier, en hausse de 10 % par rapport à 2018.



Est-ce durable ?



Du fer à 120 $ US la tonne, personne ne sait combien de temps l'embellie durera. Une chose est sûre, un changement structurel est survenu sur le marché international du fer au cours des dernières années, explique Michael Marcotte, porte-parole de Champion Iron, et la nouvelle donne tourne à l'avantage de la Côte-Nord. « La Chine a eu pour mission de bâtir une économie, au cours des 30 dernières années, en se souciant peu de l'impact environnemental. Or, les besoins de l'empire du Milieu évoluent. Le gouvernement chinois est sur une nouvelle mission : nettoyer l'air ambiant dans les villes. Il a pris pour cibles les industries polluantes comme les aciéries. Pour diminuer leurs émissions nocives, elles cherchent à s'approvisionner en fer à haute teneur, car il nécessite moins de charbon pour le brûler en vue d'en faire de l'acier. Sur la planète, le fer de haute teneur se concentre à deux endroits : au Brésil, dans les terres du géant Vale, et au Québec, dans la fosse du Labrador. » De quoi tenir les Nord-Côtiers occupés pour des années.



Effervescence ou pas ?



« C'est sûr que le prix du fer qui n'arrête plus de monter a de l'influence », laisse tomber le coordonnateur régional des Métallos, Nicolas Lapierre. Son syndicat représente la quasi-totalité des travailleurs du fer. « Je pense à la phase 2 de Lac Bloom, n'eût été le prix, ce projet-là serait encore dans les cartons. Mais, les gens ont été échaudés après 2013. On sent qu'ils restent prudents. » Martin Lévesque, directeur général de Développement économique Sept-Îles, renchérit. « Ce n'est pas le party comme en 2011 [quand le prix avait atteint les 187 $ US la tonne]. Il faudrait que l'usine de bouletage de Pointe-Noire rouvre et que la phase 2 de Lac Bloom aille de l'avant [pour s'en rapprocher]. »



Relance de l'usine de Pointe-Noire



L'usine de bouletage de Pointe-Noire a été fermée en juin 2013 par Cliffs Natural Resources, peu avant sa faillite. Elle est aujourd'hui la propriété de la Société ferroviaire et portuaire de Pointe-Noire (SFPPN) et devrait être reprise par Bedrock Industries, une société américaine qui a racheté deux aciéries canadiennes de US Steel. « Ils ont besoin de boulettes de fer. Ils nous ont joints au printemps en manifestant le désir de racheter l'usine et de négocier une entente de principe sur un futur contrat de travail », dit le coordonnateur régional des Métallos, Nicolas Lapierre. Cette entente a été signée, mais le rachat de l'usine, d'abord prévu en juin, se fait attendre.



Financement nécessaire



La SFPPN a reçu récemment 50 millions du gouvernement québécois. « La demande pour ses services est en croissance et pourrait surpasser la capacité de ses installations avant la fin de 2019 », a soutenu le gouvernement dans son dernier budget. Société en commandite propriété d'Investissement Québec, la SFPPN met en valeur ses actifs situés à Pointe-Noire, à Sept-Îles, dont environ 1200 hectares de terrains. On y trouve des voies ferrées, des quais, des gares de triage, une usine de bouletage et des bureaux.



ArcelorMittal - Port-Cartier



Dans l'immédiat, explique à

La Presse

Nicolas Dalmau, porte-parole d'ArcelorMittal Exploitation minière Canada, qui produit annuellement plus de 26 millions de tonnes de minerai de fer à la mine de Mont-Wright, près de Fermont, les efforts se concentrent sur l'analyse du projet de desserte de la Côte-Nord en gaz naturel à la suite de l'appel de propositions du gouvernement. Une décision suivra d'ici la fin de 2019 ou le début de 2020. Par ailleurs, la division étudie toujours le projet d'ajouter une troisième chaîne à l'usine de bouletage de Port-Cartier. « On parle d'un milliard comme ordre de grandeur », précise Bernard Gauthier, DG de Développement économique de Port-Cartier.



Mine Scully



L'ancienne mine de Cliffs à Wabush, au Labrador, a été rouverte fin 2018 par Tacora Resources. « Ils sont sur le bord, si ce n'est pas déjà fait, de descendre un train à Sept-Îles », fait savoir Nicolas Lapierre, dont le syndicat, mais cette fois du district des Maritimes, représente les travailleurs de la mine Scully. « C'est le troisième élément [après la phase 2 de Lac Bloom et l'usine de bouletage de Pointe-Noire] qui nous indique que les gens commencent à s'énerver avec le prix du minerai de fer. Le prix du fer a eu un impact direct sur le redémarrage de la mine Scully », avance-t-il. La capacité à terme est d'environ 6 millions de tonnes par an.



Vers un record au port



Pour les six premiers mois de l'année 2019, le tonnage transbordé au port de Sept-Îles s'élève à 12,9 millions de tonnes, près de 5 millions de tonnes de plus qu'au premier semestre de 2018. Les perspectives sont excellentes pour les deux prochaines années avec la réouverture de la mine Scully et de la fosse Moss, à Wabush, par Rio Tinto IOC. Le minerai part du Labrador pour descendre en train à Sept-Îles avant d'être mis sur un bateau. Le président-directeur général du Port, Pierre Gagnon, prévoit d'ailleurs une année record en 2020. Le record date de 2012, avec 27,9 millions de tonnes. L'an dernier, le tonnage a atteint 25,4 millions de tonnes.


Cet article est paru dans La Presse (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005211.xml</field>
    <field name="pdate">2019-07-29T00:00:00Z</field>
    <field name="pubname">La Presse (site web)</field>
    <field name="pubname_short">La Presse (site web)</field>
    <field name="pubname_long">La Presse (site web)</field>
    <field name="other_source">La Presse+</field>
    <field name="source_date">29 juillet 2019</field>
    <field name="section">Marchés</field>
  </doc>
</add>
