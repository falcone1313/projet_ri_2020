<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Les forêts d’</field>
    <field name="author">Charlotte Chabas</field>
    <field name="id">eupr_006424.xml</field>
    <field name="description"><![CDATA[


Entre les troncs calcinés qui s’enchevêtrent, sur une terre devenue noir de jais, quelques toupets vert anis surgissent soudain, droits vers le ciel. Il n’aura fallu que quelques dizaines de jours et quelques millimètres de pluie pour que Kulnura, petite bourgade à moins de 100 kilomètres au nord de Sydney, voit le retour des Xanthorrhoea, ces plantes endémiques australiennes longtemps utilisées comme colle par les aborigènes. Leur photographie, immortalisée par un riverain, a apporté une touche d’espoir dans un pays qui subit depuis septembre les pires incendies de son histoire moderne avec plus de huit millions d’hectares brûlés en quatre mois.


Le pouvoir de régénération des forêts australiennes est pourtant connu de longue date, tant le feu fait partie des cycles saisonniers de la gigantesque île australe. La majeure partie de son territoire, notamment sa côte Est, est couverte par les eucalyptus, ces arbres qui se sont adaptés pour survivre, et même prospérer, en cas de feux. Tout est fait pour : les feuilles qui, en tombant, forment un épais tapis inflammable, les bandes sous son écorce qui facilitent le chemin du feu jusqu’à la couronne de l’arbre, l’huile combustible contenue dans son tronc, et la canopée clairsemée qui permet de laisser passer le vent pour transmettre le feu d’un arbre à l’autre et projeter des braises le plus loin possible. « L’eucalyptus provoque même de la vapeur qui fait de lui un pyrophyte actif, qui favorise les départs de feu », souligne Rod Fensham, biologiste à l’université du Queensland.



« Plus il y a de feux, plus il y aura d’eucalyptus »



Pourquoi un tel attrait pour les flammes qui le carbonisent ? Les eucalyptus disposent à leur racine d’organes souterrains qui assurent la survie du végétal. Surtout, ses graines prospèrent sur les sols riches en cendres, contrairement aux autres essences. Le feu permet ainsi au célèbre arbre à reflets bleus d’assurer sa suprématie sur le règne végétal.


« Plus il y a de feux, plus il y aura d’eucalyptus », résume David Phalen, professeur du département vétérinaire de l’université de Sydney et spécialiste de la biodiversité australienne. Une équation qui avait valu au Portugal, après les terribles incendies de 2017, de proposer une loi pour « interdire jusqu’en 2030 toute nouvelle plantation d’eucalyptus, qui occupent aujourd’hui plus d’un quart des surfaces boisées du pays, et sont particulièrement invasifs et dotés d’un feuillage sec très inflammable ».


Car, outre incarner le cauchemar des pompiers, l’eucalyptus menace la biodiversité des forêts. « A force d’incendies, surtout s’ils sont de plus en plus fréquents, on risque de se retrouver avec seulement quelques variétés suffisamment résistantes pour avoir le temps de repousser », confirme Rodney Keenan, spécialiste des forêts à l’université de Melbourne.


Ces inquiétudes pour le patrimoine végétal de l’

Australie

sont d’autant plus importantes que les incendies qui font rage cette année dans le pays sont d’une violence inédite. « Plus l’environnement est sévèrement brûlé, plus cela prendra du temps à la végétation de se régénérer, confirme Rod Fensham. En général, les feux de brousse laissent des parcelles épargnées à partir desquelles la repousse se fait. Mais cette fois, on voit des zones complètement détruites sur des centaines de kilomètres carrés. Ça va forcément ralentir le processus. »



37 extinctions en trente ans



Le risque ? Que des espèces invasives plus rapides à la repousse fleurissent en lieu et place des boronias, eremophilas soyeux, astralagus ou des très rares orchidées solaires à pointes sombres. Selon les biologistes, l’

Australie

abrite quelque 24 000 plantes endémiques et 250 000 espèces de champignons.


Déjà, certaines variétés importées par l’homme ont mis à mal l’équilibre ancestral des forêts australiennes. En 2011, une équipe de chercheurs du Centre de protection des forêts tropicales du nord du Queensland avait ainsi travaillé sur la lantana camara, cette plante introduite par les colons au XIXe siècle pour ses si jolies grappes fleuris allant du jaune citron au grenat le plus profond. Sauf que cette vivace originaire d’Amérique du Sud, très craintive au gel, a trouvé en

Australie

un terrain de jeu inestimable. Présente dans plus des trois quarts du territoire australien, la lanta camara « contribue à augmenter la fréquence et l’intensité des feux dans les forêts australiennes », soulignait l’étude, pointant notamment sa formation en buissons boisés très secs qui favorise le feu, et sa grande vivacité à repousser sur les sols carbonisés. Une célérité qui vaut au végétal d’être classé parmi les cent espèces les plus nuisibles par l’Union internationale pour la conservation de la nature. Selon le gouvernement australien, 37 variétés endémiques ont disparu en trente ans.



Les forêts humides menacées



« Mais le pire est surtout à craindre du côté des forêts tropicales », prévient Rod Fensham, de l’université du Queensland. Début janvier, son séjour dans le parc national de Forty Mile Scrub a confirmé ses inquiétudes. La partie humide de la forêt, d’ordinaire résistante aux pires incendies, a entièrement brûlé. « Le feu a atteint une flore qu’on a longtemps cru inatteignable par les flammes, avec son feuillage très dense qui forme une canopée épaisse, préserve les sols de l’évaporation et empêche le vent de s’engouffrer », reprend le biologiste. Quel âge avaient ces arbres ? « Ils étaient là depuis toujours », se désole le spécialiste, qui parle de « patrimoine irremplaçable ».


La sécheresse qui touche cette année l’île a été d’une telle intensité qu’elle a atteint même les plus anciennes forêts humides datant du Gondwana, ce supercontinent qui s’est fracturé voilà 30 millions d’années pour créer l’

Australie

. Si l’étendue des dégâts n’est pas encore connue — plusieurs centaines de feux sont encore actifs dans la Nouvelle-Galles du Sud malgré des pluies ces derniers jours — un travail de recensement est déjà en cours par satellite. « Ce qui est sûr, c’est que des digues de forêts humides ont cédé dans de nombreuses régions, et tout l’écosystème qui l’accompagne », prévient David Phalen, de l’université de Sydney.


Dans la presse australienne, le professeur David Lindenmayer, qui travaille à Canberra, se disait ainsi particulièrement préoccupé pour la région montagneuse du nord-est de l’Etat de Victoria. Là, « l’intervalle de retour du feu devrait être d’environ cent ans ». Pourtant, la région a été touchée par d’intenses feux en 1939, 1983, et 2009. « Le nombre de feux et leur fréquence nous montrent que le régime des incendies est en train de changer », affirme le spécialiste de la biodiversité, qui y voit « les conséquences directes du réchauffement climatique ».


Comment assurer à l’avenir la protection de cette flore unique au monde ? Pour les biologistes, des opérations d’ensemençage, menées à grande échelle, pourraient permettre de réduire le risque d’extinctions en série. Surtout, il faudra mieux entretenir ces forêts, et peut-être « créer des zones de protection autour des forêts humides, préservées de toute culture ou habitations résidentielles, où l’on mènera des feux tactiques », souligne Rod Fensham. Une mesure impopulaire parmi la population, notamment parce qu’elle crée d’importantes fumées étouffantes. « Si on veut protéger l’environnement, il va falloir que l’homme accepte de modifier ses habitudes, et cesser de s’implanter n’importe où comme bon lui semble. »


Cet article est paru dans Le Monde (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006424.xml</field>
    <field name="pdate">2020-01-17T00:00:00Z</field>
    <field name="pubname">Le Monde (site web)</field>
    <field name="pubname_short">Le Monde (site web)</field>
    <field name="pubname_long">Le Monde (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">climat</field>
  </doc>
</add>
