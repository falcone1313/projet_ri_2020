<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">La révolution syrienne revit en fiction</field>
    <field name="author">Benjamin Barthe</field>
    <field name="id">eupr_004917.xml</field>
    <field name="description"><![CDATA[


Amman envoyé spécial - Le chant des grillons, berceuse des nuits méditerranéennes, enveloppe un quartier de la banlieue de Damas. Accoudés au rebord d'une fenêtre, Sami et Karim, son neveu, observent le scintillement d'un avion dans le ciel. « Tu penses que c'est un A320 ou un A330 ? », demande le gamin à son oncle récemment revenu d'un long exil en

Australie

. Soudain, des cris de femme parviennent du perron de la maison d'en face : « Mon petit-fils, c'est mon petit-fils ! »


Dans la rue, un jeune homme se fait tabasser par des gros bras du régime Assad, qui l'embarquent dans une camionnette et démarrent aussitôt, laissant la grand-mère en sanglots sur le trottoir. Karma, la mère de Karim, accourue à la fenêtre, le prend dans ses bras, tandis que Sami tire le rideau, le regard interdit. « On coupe, on coupe, et on la refait tout de suite !,ordonne une voix dans la pièce d'à côté. La police nous a laissé jusqu'à 23 heures pour les cris. Il nous reste six minutes, faisons vite. »


Cette scène, qui débute dans la douceur et bascule dans la violence d'un rapt politique, est fictive. Mais vraisemblable. Elle a été tournée à la mi-juillet dans un quartier d'Amman, Jabal Weibdeh, pour les besoins d'un film intitulé The Translator (« le traducteur »), attendu au printemps 2020 sur les écrans français. Ce long-métrage est le premier d'un couple de cinéastes franco-syriens, Anas Khalaf et Rana Kazkaz, remarqué en 2016 pour un « court » coup de poing, Mare Nostrum, consacré au drame des migrants noyés en Méditerranée et qui a été multiprimé dans les festivals.



« Rien d'autre que la liberté »



The Translator raconte l'histoire de Sami, un trentenaire syrien, traducteur de la télévision australienne, habitant Sydney, que son passé rattrape lorsqu'en mars 2011, sur une vidéo en provenan ce de Damas, il reconnaît son frère Zeïd parmi un groupe de manifestants raflés par les services de sécurité syriens. Sami retourne alors dans son pays en pleine ébullition révolutionnaire et part à la recherche de son frère. Une quête qui permettra à cet homme, habitué à s'effacer derrière les mots des autres, de s'affirmer et de trouver sa voix.


« Notre film sera le premier long-métrage de fiction à traiter de front la révolution syrienne, explique Anas Khalaf, qui joue un petit rôle l'inquiétant responsable de la sécurité du laboratoire de Bakou, où est envoyée Marina Loiseau, la taupe de la DGSE dans la troisième saison du Bureau des légendes, la série de Canal+. Notre modèle absolu, c'est Missing, de Costa-Gavras, le thriller politique par excellence, qui dénonce l'implication des Etats-Unis dans le coup d'Etat de Pinochet, au Chili, en 1973. »


« Notre ambition est de rappeler que les Syriens qui sont descendus dans la rue en 2011 ne réclamaient rien d'autre que la liberté et la dignité, ajoute Nicolas Leprêtre, codirecteur de la société Georges Films, une petite maison de production créée en 2015, qui a accompagné Mare Nostrum et travaille aujourd'hui sur The Translator.


Pendant de longues années, la guerre en Syrie et ses centaines de milliers de morts ont semblé tenir à distance les cinéastes. La fiction semblait un luxe impensable, presque une faute de goût, devant un tel tombereau de souffrances. La mise en images du conflit était le monopole des militants, et plus particulièrement des révolutionnaires, avides de documenter les horreurs perpétrées par le régime Assad et de pallier la quasi-absence de la presse étrangère, souvent interdite de séjour en Syrie.


Des documentaires ont commencé à voir le jour à partir de 2013-2014, souvent réalisés par les plus doués de ces journalistes-citoyens. Mais ce n'est que ces deux dernières années que le conflit syrien, sujet incandescent, s'est introduit dans des films de fiction. Avec Une famille syrienne (2017), du Belge Philippe Van Leeuw, un huis clos familial sous les bombes, Mon tissu préféré (2018), de la Syrienne Gaya Jiji, récit de l'éveil des sens d'une adolescente sur fond de manifestations, et The Day I Lost My Shadow (2018, non sorti en France), de sa compatriote Soudade Kaadan, l'histoire d'une mère en quête d'une bouteille de gaz dans un Damas en guerre. Trois longs-métrages où le conflit sert d'arrière-plan davantage que de matière première.


« Le documentaire, parce qu'il demande de saisir le moment, est traditionnellement la première chose qui arrive, constate Nicolas Leprêtre. Avec la fiction, il y a toujours un délai supplémentaire, pour trouver l'histoire et les financements, mener le tournage, etc . » « Le scénario est nourri de choses vécues ou d'anecdotes que l'on nous a racontées, ajoute Anas Khalaf. Il nous a fallu du temps pour digérer tout ça. »



« J'avais trop peur »



Les premières lignes du script ont été rédigées au printemps 2011, à l'époque où les cinéastes et leurs deux enfants vivaient à Damas et assistaient en direct aux prémices du soulèvement. Un moment d'euphorie qui a tourné court. Rana Kazkaz est partie aux Etats-Unis à l'été, « après être tombée un jour sur un tank en allant chercher [s]es enfants à l'école . Anas Khalaf l'a rejointe un an plus tard, convaincu que Bachar Al-Assad ne tomberait pas. L'élaboration du scénario s'est poursuivie cahin-caha pendant plusieurs années, au gré de pérégrinations entre les Etats-Unis, la Jordanie, et le Qatar où vit désormais le couple.


« Les premières versions ne mentionnaient même pas que l'histoire se déroule en Syrie, confie Rana Kazkaz, qui est coscénariste avec la Française Magali Negroni. J'avais trop peur. J'avais le sentiment que, où que je sois, le régime pourrait me trouver. Me libérer de cette terreur a pris du temps », ajoute la quadragénaire, dont quinze membres de la famille ont péri dans le massacre d'Hama, en 1982. Une tuerie, qui a fait entre 10 000 et 40 000 morts selon les estimations, ordonnée par Hafez Al-Assad, le père de l'actuel chef de l'Etat, en représailles à une révolte islamiste.


Le script a obtenu en 2017 le prix Arte de la Cinéfondation, aide aux projets prometteurs. Pour le rôle de Sami, les réalisateurs ont choisi le Palestinien Ziad Bakri, à la beauté sombre et au jeu intériorisé, qui jouait dans Mare Nostrum etincarnait dans Le Bureau des légendes l'agent des renseignements syriens qui surveille Malotru (Mathieu Kassovitz). « Jouer dans ce film, c'est ma façon à moi de soutenir les Syriens qui font face à l'agression du régime et à un monde qui dort », explique-t-il.


Le second rôle principal, celui de Karma, avait été promis à la Syrienne Fadwa Souleimane, l'égérie des anti-Assad, célèbre pour ses harangues sur les barricades d'Homs, au début du soulèvement. Elle est morte d'un cancer en 2017, alors qu'elle vivait en exil à Paris. Le personnage est finalement incarné par la Libanaise Youmna Marwan.



L'espoir évanoui



Plusieurs Syriens figurent toutefois dans la distribution, comme Sawsan Arsheed, vedette des « moussalssalat » (feuilletons) d'avant la révolution, qui a trouvé elle aussi refuge en France. L'un des rebondissements du scénario fait d'ailleurs écho aux conditions dramatiques dans lesquelles elle a dû fuir la Syrie : « J'avais déclaré sur Facebook que j'étais contre les tueries. Les sbires du régime ont alors sommé mon mari de présenter des excuses à la télévision, en menaçant de s'en prendre à nos enfants s'il ne s'exécutait pas. On est partis le jour même, avant qu'ils aient le temps de mettre nos noms à la frontière, avec un sac de vêtements, sans dire au revoir à personne. Ça fait du bien qu'un film aborde enfin ce moment de notre histoire. »


Pour Anas Khalaf et Rana Kazkaz, la joie d'arriver au bout d'une aventure de huit ans se teinte d'une forme d'amertume. Faire entrer la révolution au cinéma, passer de la vidéo de témoignage au film de fiction, c'est prendre acte de l'évanouissement irrémédiable de ces mois d'espoir. « Oui, la révolution a tristement échoué, confesse Anas Khalaf. Mais, grâce au pouvoir de la fiction, nous espérons faire revivre cette époque dans les esprits. »


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004917.xml</field>
    <field name="pdate">2019-07-24T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Le Monde (site web)</field>
    <field name="source_date">24 juillet 2019</field>
    <field name="section">Culture</field>
  </doc>
</add>
