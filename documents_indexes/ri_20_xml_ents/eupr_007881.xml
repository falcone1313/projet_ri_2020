<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Margaux ouvre-moi ta porte !</field>
    <field name="author">Spaak, Isabelle</field>
    <field name="id">eupr_007881.xml</field>
    <field name="description"><![CDATA[


L’œnotourisme?? Ah mais, comme c’est drôle que vous me posiez la question », s’étonne Corinne Mentzelopoulos. « Hier justement, je feuilletais un magazine de vin en ligne avec un article sur la question. J’ai refermé la page en me disant que l’avenir, c’est ça?! » Comment ça, l’avenir c’est ça?? Et pourquoi?? s’étonne-t-on en interrogeant plus avant la « dame de Margaux » surnom de la très respectée directrice de Château Margaux depuis bientôt quatre décennies. Elégante jusque dans la décontraction de sa conversation, sa modestie (« Margaux me dépasse toujours entièrement »), ses éclats de rire, sa spontanéité au téléphone depuis la Grèce où elle séjourne chaque été. La Grèce, pays natal de son père, André Mentzelopoulos, né à Patras en 1915. Un visionnaire qui avait fait fortune en acquérant en 1958 les magasins Félix Potin pour en faire une société florissante avant de tomber amoureux de Château Margaux, acheté sur un coup de cœur en 1977 pour la beauté de ses colonnes lui rappelant celles du Parthénon.


A l’époque, le vaisseau amiral de l’appellation la plus méridionale du Médoc est un beau navire en perdition. Le Médoc souffre dans les années 1970. Aléas climatiques, millésimes difficiles. Margaux n’est pas en reste. En moins de trois ans, André Mentzelopoulos dessine les grandes lignes de la renaissance du cru mythique avant de décéder brusquement, à l’orée des vendanges de 1980. Agée de 27 ans, sa fille unique, Corinne reprend le flambeau « sans rien y connaître », dit-elle. Dans les pas de son père, elle hisse le château à des sommets inégalés. Les vins sont encensés par le critique américain Robert Parker, la propriété convoitée par Bill Gates, patron de Microsoft, Château Margaux vénéré jusqu’en Chine depuis que le futur président Hu Jintao a décidé à l’improviste une visite en 2001. Les prix s’envolent. Mais, toujours, Corinne Mentzelopoulos a su conserver le respect et l’admiration de ses pairs pour sa gestion irréprochable du domaine.


« Château Margaux donne sa force et son nom à l’appellation comme un porte-avions dont nous serions les escorteurs », reconnaît d’ailleurs Emmanuel Cruse, troisième génération à la tête du vénérable Château d’Issan à Cantenac dont le vin est célébré depuis qu’Aliénor d’Aquitaine en servit en 1152 lors de son mariage avec Henri Plantagenêt, futur roi d’Angleterre. Information déclenchant une salve enthousiaste de « Wow?! » et de « How nice?! » de la part du jeune couple de la city qui s’est inscrit ce matin pour un tour guidé de la propriété familiale ceinte de douves. Tandis que de non moins jeunes et branchés trentenaires Asiatiques se mitraillent de selfies devant le chai au fronton paré des lettres ISSAN.


Situé à moins de un kilomètre de Château Margaux, Issan en partage le superbe terroir riche en galets. Cette légendaire terrasse T4 : 6 kilomètres de long, 2 kilomètres de large. La Gironde en contrebas. Une terre de croupes graveleuses hautes de 8 à 21 mètres façonnées par l’érosion. Nulle part ailleurs dans le Médoc on ne trouve un sol aussi pierreux. Un terroir si pauvre, formé de dépôts d’alluvions et de graves garonnaises charriés depuis les Pyrénées au quaternaire, qu’il ne convient qu’à la vigne, forcée d’élancer courageusement ses racines jusqu’à 5 mètres de profondeur pour y chercher de l’eau qu’elle trouve toujours, même en période sèche. Les plus beaux crus classés de l’appellation – qui en compte 21 – se partagent cette terrasse magnifique.


Margaux la sublime. L’appellation qui « fait pétiller les yeux dans le monde entier », poursuit Emmanuel Cruse. « Quand je voyage, on me demande toujours où se trouve mon domaine. Je dis Margaux, tout le monde s’esbaudit. » Un paradoxe pour l’appellation. Immortalisée par Ernest Hemingway qui aimait tant le vin de Château Margaux qu’il en prénomma sa fille. Margaux se confond souvent avec son – seul – premier cru classé en 1855 auquel l’AOC doit son nom. On sait moins qu’elle regroupe quatre communes – Arsac, Labarde, Margaux-Cantenac et Soussans – sur 1?500 hectares de vignobles dont 84 % certifiés ou en cours de certification haute valeur environnementale (HVE) pour 65 viticulteurs déclarés avec une surface moyenne de 23,26 hectares par propriété et une production totale de 56?000 hectolitres par an (2018). Soit 1 % de la totalité de la production du Bordelais, et 31 % des quatre appellations réunies (margaux, pauillac, saint-estèphe, saint-julien).


Ce que l’on ne soupçonne pas non plus, ce sont les 150?000 personnes qui visitent l’appellation chaque année. Un tourisme viticole en pleine expansion. A Margaux comme dans le monde entier. « Le classement que j’ai lu, explique Corinne Mentzelopoulos, répertorie les 50 meilleurs châteaux dans le monde. Non pas sur la qualité des vins, mais sur l’ensemble de l’accueil. Nous sommes cinq français. Château Margaux est classé à égalité avec Smith Haut Lafitte. Pour eux c’est normal, ils ont été parmi les premiers à faire des merveilles en la matière. Mais nous?? A L’exception des visites personnalisées et du temps que nous accordons à chacune d’elles, notre hall d’accueil, nous n’avons ni installation dédiée ni infrastructure hôtelière. Je sais pourtant qu’il viendra un moment où il le faudra. La réflexion mérite d’être amorcée. Peut-être pas par moi, mais par la prochaine génération. Les gens veulent vivre une expérience.»


Le nouveau président du syndicat viticole de Margaux, Edouard Miailhe, en est tellement convaincu qu’il a pris les devants. Depuis son élection en septembre 2018, il a fait de l’œnotourisme l’une de ses priorités. Déjà, il se réjouit. « Nous sommes désormais 14 propriétés sur les 65 de l’appellation à proposer une offre en ce sens, parmi lesquelles 9 sont des crus classés. »


Septième génération à la tête de Château Siran, autrefois propriété du comte et de la comtesse de Toulouse-Lautrec, arrière-grands-parents du peintre, Edouard Miailhe a été élevé à bonne école. En précurseurs, ses parents ont bricolé un circuit entre le cuvier, les chais, un petit musée, un promontoire offrant une vue à 360 degrés sur les vignobles et la découverte inattendue d’un abri antiatomique transformé en vinothèque, clou de la promenade. Le tout professionnalisé désormais par leur fils à des tarifs compris entre 10 et 30 euros.


D’autres domaines n’ont pas attendu l’encouragement de leur syndicat pour s’inscrire dans cette optique. Ainsi, Château Kirwan, troisième grand cru classé. Au XIXe siècle, il appartenait au maire de Bordeaux, Camille Godard, qui le dota d’un parc majestueux avec vivier, essences rares et tonnelle de roses impressionniste. La propriété offre également un catalogue impressionnant d’ateliers en tout genre destinés au néophyte souhaitant découvrir le vin et son élaboration.


Dès 1995, Nathalie Schÿler, l’une des héritières de l’entreprise familiale de négoce propriétaire de Kirwan depuis 1925, s’y est attelée. Aujourd’hui, les 15?000 visiteurs annuels du château ont l’embarras du choix. Apéro gourmand, dégustation d’une bouteille de leur année de naissance dans le chai, circuit découverte classique, en hélicoptère ou 2.0. Ceux qui le souhaitent peuvent aussi choisir de vivre la vie d’un œnologue en participant à l’assemblage d’un cru ou expérimenter celle d’un vigneron avec taille en hiver, effeuillage en été, vendanges à l’automne. D’autres choisiront de participer à un atelier de cuisine, à la création d’un parfum, à un pique-nique dans le parc. Il y en a pour tous les goûts. Et pour toutes les bourses. De 15 à 250 euros, voire davantage s’il s’agit d’une demande personnalisée.


« Il suffit de nous demander », assure la directrice du développement, Nassima Benrabia, ravie de pouvoir concrétiser les rêves de chacun et de laisser libre cours aux siens. Ainsi cette balade en side-car imaginée autour d’un repas en trois temps (entrée dans un château, plat principal dans un autre et dessert dans un troisième).


A Château Giscours, troisième grand cru classé de 300 hectares (90 hectares de vignobles) situé sur le magnifique plateau de Labarde et à Château du Tertre, grand cru classé de 52 hectares (l’une des plus anciennes seigneuries du Médoc), tous deux acquis et restructurés avec une grande exigence au milieu des années 1990 par l’homme d’affaires, esthète et collectionneur néerlandais Eric Albada Jelgersma, disparu en juin 2018, la démarche de s’ouvrir aux visiteurs va de soi.


« Depuis que le négoce bordelais gère la vente des crus du Médoc, les propriétés ont perdu le contact avec leurs clients. Aujourd’hui, le consommateur veut savoir si ce qu’il boit est écologiquement correct. C’est normal. Giscours est un village qui vit au rythme de la nature. Nous lui faisons découvrir le produit, notre histoire, notre métier. Nous avons axé nos propositions sur l’art de vivre et l’accueil. Le qualitatif plutôt que le quantitatif. Notre personnel est formé à cela », insiste Alexander van Beek, désormais à la tête des deux domaines pour la famille Jelgersma. Pour un chiffre d’affaires de 1,3 million d’euros en 2018 sur les deux propriétés, une équipe de 15 personnes (guide, femme de chambre, chef de cuisine, chargé événementiel…) est spécialement dédiée au réceptif.


Mais du vigneron au maître de chai, chacun, à Giscours, est invité à prendre le temps d’expliquer son métier au visiteur. Grâce au décorateur belge Axel Vervoordt, le château du Tertre est devenu une maison d’hôtes au charme fou, l’ancien terrain de polo du Château Giscours a été réhabilité pour accueillir le Bordeaux Giscours Cricket Club, une vingtaine de mariages sont organisés dans la ferme Suzanne, qui fut celle du château, et Marc Verpaalen, dynamique et très attentionné directeur réceptif, s’est attelé à l’aménagement du parc dessiné par le paysagiste Eugène Bühler. Avec, à terme, l’ambition de transformer la bâtisse XIXe, jadis destinée à accueillir la princesse Eugénie si par hasard elle passait par là, en hôtel de grand luxe. « On en manque cruellement dans la région », constate-t-il au retour d’un rassemblement international de propriétés viticoles, en

Australie

, sur le thème de l’œnotourisme où il représentait l’appellation Margaux.


Tout ne va pourtant pas pour le mieux dans le meilleur des mondes. S’il paraît désormais incontournable pour la promotion des propriétés viticoles, le concept de l’œnotourisme fait encore souvent grincer des dents. Fleurons d’architecture ou demeures familiales depuis plusieurs générations, les châteaux bordelais sont, avant tout, des lieux de travail. Leur vocation est d’élaborer quelques-uns des crus les plus prestigieux du monde. Non de se transformer en parc de loisirs.


« Mais tu te crois où?? A Napa?? » ont persiflé les voisins de Laurent Fortin, directeur de Château Dauzac depuis 2012, lorsqu’il a fait installer des panneaux jaune bouton d’or aux couleurs de la propriété pour indiquer aux automobilistes la nature des cépages qu’ils aperçoivent au bord de la route : cabernet sauvignon (68 % du vignoble) ou merlot (32 %).


Nommé par la Maif (le groupe d’assurances propriétaire du château) pour redonner son lustre au cinquième grand cru classé, Laurent Fortin a vécu quatorze ans aux Etats-Unis où il fut le premier à commercialiser du vin au verre dans les restaurants new-yorkais. Après quatre ans en Chine, et la direction d’une union de Caves Coopératives, sa nomination à Dauzac représentait un défi. Les 49 hectares de vignes d’un seul tenant au cœur d’un écosystème préservé de 120 hectares de prés et de forêts situé entre Macau et Labarde avaient besoin d’un nouveau souffle.


Le premier volet du plan stratégique qui lui a été confié visait « l’excellence du vin et l’instauration d’une viticulture plus pointue, respectueuse de l’environnement et des humains ». Dès son arrivée, les CMR (produits chimiques cancérogènes, mutagènes, reprotoxiques) ont été arrêtés, un nouveau chai installé en 2013, la sélection parcellaire mise en place, la biodiversité valorisée. Mais Dauzac devait aussi reprendre place sur le marché de Bordeaux et s’ouvrir sur la planète. « Nous sommes les seuls à avoir lancé une application avec déjà plus de 27?000 téléchargements ».


Le domaine se veut accueillant et accessible. Des bancs et des tables de pique-nique ont été installés au bord du lac pour qui veut, les grilles du château sont ouvertes. Une vaste boutique au décor de bois clair propose un éventail de porte-clés, parapluies, tire-bouchons, tapis de souris et autres produits dérivés siglés Dauzac. Outre les quelque 7?000 bouteilles vendues en direct et le miel bio de la propriété, ce sont « autant de souvenirs qui contribuent au rayonnement de la marque », justifie Laurent Fortin, qui compte beaucoup sur ces « achats d’impulsion » pour conquérir de nouveaux marchés. Car le visiteur rentre dans son pays avec un objet qui lui rappelle sa visite et les bons moments passés. « Le but?? Qu’il devienne notre ambassadeur », insiste le président du syndicat.


Pour certains, le pas à franchir ne va pourtant pas de soi. Par quel biais s’engager?? A Brane-Cantenac, la directrice commerciale Marie-Hélène Dussech concède réfléchir. Certes, il existe déjà deux formules de visites-dégustations au domaine, l’une avec la découverte du métier de tonnelier. Mais quelle direction prendre pour aller plus loin??


Deuxième grand cru classé de Margaux, Brane-Cantenac est un des fleurons de l’appellation avec ses 75 hectares de vignes sur deux des plus belles croupes argilo-sableuses. Mais Brane-Cantenac est aussi « une maison de famille où nous avons tous grandi, raconte Henri Lurton, troisième d’une fratrie de dix enfants. Mon père, Lucien, vit encore sur la propriété et, jusqu’à récemment, nos enfants jouaient tous les dimanches au foot dans le parc. Je vois mal des gens s’y promener. » Pour le moment, Henri Lurton préfère se concentrer sur son métier et les spécificités d’une AOC réputée pour ses vins dits «féminins » de par leur souplesse et leur élégance. Mais aussi pour « ses crus plus puissants, plus taniques avec une signature aromatique très marquée ». Une diversité qui contribue à brouiller les pistes. « Margaux n’est pas reconnue à sa juste valeur, estime Henri Lurton. Elle manque de stars, à l’exception de Margaux et Palmer. »


Château Palmer. L’exquise et secrète. Un havre de 66 hectares planté de cabernet, merlot et petit verdot. Des rangées enherbées, de longs bras de vignes laissées libres de s’épanouir en harmonie avec les astres, les fleurs, les animaux, la terre. Palmer est un troisième grand cru converti à partir de 2008 en biodynamie. Ils sont trois dans ce cas sur l’AOC. Plus qu’une orientation viticole, une philosophie, une partition musicale ne souffrant aucune interférence. Passionné de jazz, Thomas Duroux y veille. Comme il veille sur la « personnalité » de chacune de ses parcelles. « Faire un grand vin, c’est mettre un lieu dans un verre, façonner un style », dit-il.


Et question style, Rauzan-Ségla, situé pile en face sur la magique terrasse 4, n’est pas en reste. Aux manettes de ce deuxième grand cru appartenant à Alain et Gérard Wertheimer (Chanel) depuis 1994, Nicolas Audebert. Un pied à Margaux au bord de la Gironde, l’autre à Saint-Emilion près de la Dordogne, où les Wertheimer possèdent également les châteaux Canon et Berliquet. Le bec d’Ambès entre les deux.


Nicolas Audebert a travaillé pour la maison Krug avant de rejoindre Cheval des Andes, mariage argentin entre Cheval Blanc (LVMH) et le domaine Terrazas de los Andes près de Mendoza. Les grands espaces, il connaît. Comme il connaît sur le bout des doigts les plus infimes typicités des 66 hectares de Rauzan-Ségla. Une « mosaïque incroyable de 21 types de sols différents, un millefeuille, un puzzle à assembler, une passion » qu’il sillonne au volant de sa Jeep vintage.


Depuis les graves fines et sableuses des terrasses 3 et 5 en passant par les graves profondes et miraculeuses de la 4, nous descendons en pente douce vers les pentes argilo-calcaires qui mènent vers le palus, zone inondable en bordure du fleuve, lien aquatique entre les deux propriétés Wertheimer, chacune sur une rive. Nicolas Audebert y a fait aménager un carrelet, l’une de ces cabanes de pêche en surplomb de l’eau. Un lieu hors du temps et de l’agitation. Un refuge secret où la beauté sauvage du paysage s’offre aux rares privilégiés – dont nous sommes – invités à y pique-niquer sur quelques bottes de pailles parées de coussins brodés. Pour arriver dans ce paradis suspendu, l’équipée fut entrecoupée de plusieurs arrêts permettant à « l’Argentin » de toucher « sa » terre, rouler amoureusement un galet poli dans sa paume, se saisir à pleine main d’une motte de glaise si grasse, si compacte qu’on dirait un bloc de pierre. Des gestes de couturier avec ses étoffes. Encensés pour leur fraîcheur et leur noblesse, les vins de Rauzan-Ségla sont à l’image de Chanel. L’expression d’un terroir, de la minutie des hommes, du temps qui s’écoule à l’abri des regards. « Ceux qui connaissent la maison savent que nous travaillons dans la durée. Ils savent aussi que notre philosophie est celle du “oui et non”. Oui, nous sommes une grande marque qui fait parler d’elle. Non, nous n’en avons pas tellement envie. » Ouvrir grand ses portes?? Pas vraiment le genre de la maison.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007881.xml</field>
    <field name="pdate">2019-10-23T00:00:00Z</field>
    <field name="pubname">Le Figaro (site web)</field>
    <field name="pubname_short">Le Figaro (site web)</field>
    <field name="pubname_long">Le Figaro (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Vin</field>
  </doc>
</add>
