<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">[Succès. Croissance du PIB, emploi, confiance...]</field>
    <field name="author">Par              &#13;
  &#13;
     Frédéric Paya&#13;
     Marie de Greef-Madelin</field>
    <field name="id">eupr_008153.xml</field>
    <field name="description"><![CDATA[


Succès. Croissance du PIB, emploi, confiance des ménages, bénéfices des sociétés... L'activité américaine n'a jamais été aussi dynamique qu'aujourd'hui. Donald Trump gère son pays comme une entreprise : pas d'ami dans le business !


Le 1er juin, l'administration américaine a frappé l'Union européenne en augmentant les tarifs douaniers à 25 % sur les importations d'acier et à 10 % sur celles d'aluminium. Cela fait suite à une décision promulguée par Donald Trump le 8 mars pour lutter contre la sous-utilisation des unités de production outre-Atlantique -- le taux d'utilisation est tombé à 73 % pour l'acier (l'importation des produits en acier représentait 29 milliards de dollars en 2017) et à 39 % pour l'aluminium (pour 19 milliards de dollars de produits importés).


Le président avait accordé des exemptions temporaires à l'Argentine, l'

Australie

, le Brésil, le Canada, le Mexique et l'Union européenne, le temps pour eux de revoir leurs plans d'exportation. L'Europe a eu beau plaider pour que son exemption soit permanente, mettant en avant qu'elle était l'alliée des États-Unis, l'argument n'a pas été suffisant : l'Union européenne, le Canada et le Mexique ont été rattrapés par la hausse des tarifs douaniers.


Lors du Forum annuel de l'OCDE, qui s'est tenu les 29 et 30 mai à Paris, Emmanuel Macron a dénoncé la « loi du plus fort ». Plus que celle-ci, Donald Trump ne fait rien d'autre que de mettre en pratique son programme électoral qui se résume en une formule : « America first ». Un slogan repris à la tribune du Forum par Wilbur Ross, secrétaire au Commerce américain : « La première obligation d'un pays est de protéger ses citoyens et ses moyens de subsistance. On peut considérer que c'est du populisme, mais cela nous tient à coeur. »


À la tête des États-Unis depuis bientôt dix-huit mois, Donald Trump fait ce qu'il a toujours su faire : du business avec ses propres règles, au risque de bousculer l'ordre établi. Sans (aucun) filtre, il entreprend ce qu'il dit : il est sur tous les fronts, et les résultats montrent que sa politique économique est efficace.


Dans les salles de marchés, on n'en revient d'ailleurs toujours pas : le Dow Jones s'est apprécié de 17 % depuis un an et de 31 % depuis l'élection de Donald Trump, atteignant des niveaux jamais acquis. Pourtant, avant son élection, l'agence de notation Moody's avait passé au crible son programme et était allée jusqu'à évoquer un « choc » négatif massif sur l'économie, qui se traduirait par 3 millions et demi de suppressions d'emploi. Depuis l'investiture de Donald Trump, le taux de chômage aux États-Unis est tombé à 3,8 %, au plus bas depuis dix-sept ans !


La croissance pourrait atteindre 2,8 % en 2018


Il est vrai que la réforme fiscale adoptée le 20 décembre 2017 donne des résultats. Elle vise à diminuer les prélèvements des ménages et des entreprises sur dix ans de 1 450 milliards de dollars, soit 7 % du PIB américain ! Elle passe par une baisse de l'impôt sur les sociétés de 35 à 21 % et l'instauration d'un taux préférentiel pour inciter les entreprises à rapatrier une partie de leurs bénéfices réalisés à l'étranger. Les baisses d'impôts entraînent mécaniquement une hausse des dépenses de consommation des ménages et, de fait, une meilleure attractivité et une augmentation des investissements des sociétés.


Cette spirale vertueuse conduit les analystes financiers à réviser leurs prévisions de bénéfices des entreprises. Parmi les grands gagnants, les multinationales, bien sûr, qui ont logé des milliards de dollars à l'étranger, à commencer par les Gafa et autres sociétés technologiques ainsi que les géants de la pharmacie. Au total, selon le fournisseur de données financières FactSet, les analystes ont augmenté de 2,2 % pour 2018 leurs prévisions de bénéfice médian pour les sociétés de l'indice S&P 500, à 150,10 dollars par action. Jamais, depuis 1996, une hausse d'une telle ampleur n'interviendrait. À l'échelle du pays, les économistes prévoient un gain de PIB lié à la réforme fiscale de 0,3 à 0,5 %, si bien que la hausse du PIB, après huit années consécutives de croissance, pourrait atteindre 2,8 % à la fin de l'année.


Revers de la médaille, ce surcroît d'activité, qui laisse prévoir un retour de l'inflation, inquiète : sur les marchés financiers, les taux d'intérêt ont sensiblement augmenté au cours des derniers mois. La Réserve fédérale américaine, dirigée depuis le mois de février par le républicain modéré Jerome Powell, a augmenté d'un quart de point ses taux directeurs en mars, reconnaissant que « les perspectives économiques se sont renforcées ces derniers mois ». Trois nouvelles hausses seraient à attendre cette année, avec, pour conséquence, d'inévitables tensions sur les marchés obligataires mondiaux.


Si l'économie intérieure est florissante, Donald Trump n'est pas moins actif hors des frontières : en moins de deux ans, il a renégocié directement les traités signés par l'administration Obama qui lui semblaient défavorables et a quitté les institutions où les États-Unis lui apparaissaient perdants. Avec une même tactique : dénoncer pour forcer à renégocier et en ressortir gagnant. Sans doute était-ce la technique qu'il enseignait dans un de ses livres, The Art of the Deal (traduit en français par Trump par Trump).


Première décision, sitôt arrivé à la Maison-Blanche, il a engagé le retrait des États-Unis du TPP (partenariat transpacifique), néfaste selon lui pour les travailleurs américains. En janvier dernier, à Davos, Donald Trump a toutefois indiqué que les États-Unis pourraient revenir si les pays aboutissaient à un meilleur accord.


Même technique en janvier 2017 quand le président américain s'en est pris à l'Alena (accord de libre-échange nord-américain). Le « pire accord » jamais signé, selon lui. Il rend ce traité, qui annule les barrières douanières entre les États-Unis, le Mexique et le Canada, responsable du déficit commercial américain et craint particulièrement la destruction d'emplois manufacturiers. Depuis, les discussions ont repris : « Je vais vous dire qu'à la fin nous gagnerons, nous gagnerons et gagnerons gros », a assuré Donald Trump.


La même année, le 1er juin, il s'est retiré de l'accord de Paris sur le climat. Pour lui, et c'était d'ailleurs une de ses promesses électorales, le texte signé par 195 pays en décembre 2015 devait avoir des conséquences négatives sur l'économie (les États-Unis sont le deuxième émetteur de gaz à effet de serre derrière la Chine). Le pays pourrait cependant atteindre les objectifs.


Dernière décision, le 8 mai, lorsque Donald Trump a annoncé le retrait des États-Unis de l'accord « désastreux », selon ses termes, sur le nucléaire iranien. Autre promesse électorale qui se traduira par de nouvelles sanctions qui frapperont les nouveaux partenaires économiques dont la France, la Grande-Bretagne et l'Allemagne, qui espéraient se faire une place à Téhéran, laissant les 28 face à leur impuissance.


Accédez à tous les contenus payants de Valeurs actuelles à partir de 1€ par mois. Lire
Commentaires Ajouter un commentaire
Commentaire
À propos des formats de texte Texte brut Aucune balise HTML autorisée. Les lignes et les paragraphes vont à la ligne automatiquement. Les adresses de pages web et les adresses courriel se transforment en liens automatiquement.

Lire la charte d'utilisation des commentaires


Cet article est paru dans Valeurs Actuelles (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008153.xml</field>
    <field name="pdate">2019-10-28T00:00:00Z</field>
    <field name="pubname">Valeurs Actuelles (site web)</field>
    <field name="pubname_short">Valeurs Actuelles (site web)</field>
    <field name="pubname_long">Valeurs Actuelles (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
