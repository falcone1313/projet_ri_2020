<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Idées - Pour le vote des expatriés</field>
    <field name="author">Pierre J. Dalphond - Ex-juge de la Cour d'appel, sénateur indépendant</field>
    <field name="id">eupr_005479.xml</field>
    <field name="description"><![CDATA[


En décembre dernier, le projet de loi C-76 modifiant la Loi sur les élections au Canada a été adopté. Donnant plein effet à l'article 3 de la Charte canadienne des droits et libertés, il abroge une disposition introduite en 1993 qui privait du droit de vote la plupart des Canadiens résidant à l'étranger depuis cinq ans et plus.


Cette restriction reposait sur le postulat suivant : si vous ne résidez plus au pays depuis cinq ans, vous n'êtes plus concerné par ce qui s'y passe et vous n'avez plus d'intérêt dans les politiques canadiennes.


Le Canada adoptait ainsi une position contraire à la plupart des pays démocratiques, notamment les États-Unis, la France, l'Espagne, l'Italie, la Suisse et la Belgique.


En réalité, seules quelques démocraties restreignent le droit de vote des non-résidents, mais aucune autant que le Canada. Ainsi, en Allemagne, l'absence doit durer 25 ans, au Royaume-Uni, 15 ans, et en

Australie

, 6 ans. Une durée plus courte, 3 ans, n'existe qu'en Nouvelle-Zélande, cependant remise à zéro par une visite, même d'une journée.


La restriction de 1993 était aussi discriminatoire, puisqu'elle ne s'appliquait pas aux militaires, fonctionnaires (fédéraux et provinciaux) et employés d'organisations internationales. Ainsi, une Canadienne travaillant à l'étranger pour Bombardier depuis 6 ans ne se qualifiait pas pour voter, contrairement à un Canadien fonctionnaire de l'ONU à Genève depuis 15 ans.


D'ailleurs, le 11 janvier 2019, la Cour suprême du Canada déclare inconstitutionnelle cette restriction puisque rien ne la justifiait.


Campagne de peur


Malgré ce jugement, certains apparatchiks se livrent à une campagne de peur, parlant du danger que le vote d'expatriés sans intérêt dans l'avenir du pays devienne critique dans le choix des gouvernements et, même, qu'il serve d'un moyen d'influence étrangère sur nos élections.


Avec égards, ils font fi des faits.


Tous les expatriés utilisent un passeport canadien et peuvent bénéficier des services consulaires canadiens.


De plus, la plupart d'entre eux ont de la famille au pays et sont soucieux de l'avenir de celle-ci.


Nombreux sont ceux qui maintiennent des liens économiques avec le Canada et y paient des impôts. Ainsi, 186 200 sont bénéficiaires du RPC et 139 400 de la Sécurité de la vieillesse. Des milliers d'autres tirent des revenus d'investissements au pays.


Plusieurs bénéficient d'avantages dans le pays hôte découlant d'ententes avec le Canada, comme la possibilité d'un permis de travail ou l'évitement de la double taxation.


De même, quel motif rationnel peut-il justifier d'exclure de notre vie démocratique les Canadiens vivant à l'étranger pour la promotion de sociétés d'ici, faisant des études supérieures ou occupant des emplois qui n'existent pas au Canada et qui font le projet de revenir au pays sans en connaître la date exacte ?


Ces critiques omettent aussi de souligner qu'un Canadien vivant à l'étranger ne peut voter que s'il fait le nécessaire pour être inscrit au Registre national des électeurs et démontre avoir résidé, avant son départ, dans la circonscription électorale où il souhaite voter. De plus, pour exercer son vote, il doit remplir le bulletin spécial reçu d'Élections Canada et le retourner dans le délai prescrit.


Ces démarches sont indicatives d'un intérêt réel pour notre vie démocratique.


Cela démontré, qu'en est-il du danger qu'ils influent sérieusement sur les résultats électoraux ?


Il n'existe pas de chiffres précis sur le nombre de Canadiens vivant l'étranger. Les évaluations varient entre moins d'un million à près de trois millions.


Peu importe, puisque les faits démontrent que ce péril est inexistant.


D'abord, peu d'expatriés se prévalent de leur droit. Lors de la dernière élection générale en 2015, 14 000 Canadiens résidant à l'étranger, incluant militaires et fonctionnaires, se sont inscrits. Uniquement 11 000 ont rempli le bulletin spécial, puis l'ont retourné à temps.


Ensuite, ces 11 000 votes ont été dépouillés dans l'une des 338 circonscriptions du pays, où ils n'ont pas eu d'impact significatif. Seules 24 circonscriptions ont reçu plus de 100 votes internationaux. Le plus grand nombre a été enregistré dans Ottawa-Vanier : 496. Or, le candidat élu a recueilli 24 280 votes de plus que le deuxième.


Avec la fin de la restriction et la publicité qui l'entoure, il faut espérer que plus d'expatriés se prévaudront de leur droit de vote en 2019. Le directeur général des élections estime que 30 000 d'entre eux le feront. S'il y a entre 1 million et 3 millions d'expatriés, cela représentera de 3 à 10 % d'entre eux.


Avec le temps, ce nombre devrait augmenter, mais l'expérience chez nos voisins du sud révèle qu'environ 10 % des expatriés admissibles font le nécessaire, même aux présidentielles. Advenant que ce soit aussi le cas au Canada, le Registre national contiendra entre 100 000 et 300 000 expatriés parmi ses 26 millions d'inscrits, soit entre 0,4 et 1 % des électeurs.


Au vu de ces faits, prétendre que le choix des prochains gouvernements dépendra des expatriés tient de l'élucubration.


Quant à la menace d'une influence extérieure dans nos élections exercée par la manipulation des expatriés vivant dans des pays totalitaires, aucune preuve ne la soutient, même aux États-Unis. En réalité, la puissance étrangère qui souhaite influer visera les 99 % ou plus d'électeurs au pays, comme les trolls informatiques russes aux États-Unis l'ont démontré.


De toute façon, le risque que des expatriés puissent être manipulés dans un pays ne justifierait pas de pénaliser ceux habitant ailleurs dans le monde. Tout au plus, cela requerrait des mesures ciblées.


Le droit de vote est fondamental en démocratie. Cessons d'invoquer des prétextes pour le nier aux Canadiens résidant à l'étranger.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005479.xml</field>
    <field name="pdate">2019-01-24T00:00:00Z</field>
    <field name="pubname">Le Devoir</field>
    <field name="pubname_short">Le Devoir</field>
    <field name="pubname_long">Le Devoir</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Idées</field>
  </doc>
</add>
