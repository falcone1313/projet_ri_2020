<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">L'espace intérieur</field>
    <field name="author">Par Michel Verlinden à Londres</field>
    <field name="id">eupr_004275.xml</field>
    <field name="description"><![CDATA[


A Londres, Antony Gormley s'empare de la Royal Academy le temps d'une rétrospective qui sonde le vide en et hors de nous. L'art de la sculpture y flirte avec les sommets.


C'est une minuscule pièce qui ouvre cette rétrospective revenant sur quarante années de carrière internationale d'Antony Gormley (1950, Londres) et qui cueille le visiteur dès l'extérieur. On est donc très loin d'un certain gigantisme ayant contribué à établir la réputation de l'artiste. Ainsi de l'imposant Angel of the North, à Gateshead, oeuvre étrangement cruciforme dotée de proportions déconcertantes (une hauteur de 20 mètres pour 54 mètres d'envergure). Depuis 1988, cet « Ange du Nord » marque l'inconscient collectif anglais, tout à la fois point de repère géographique pour le Nord-Est du pays et profession de foi politique anti-Thatcher ( « Gabriel », comme on le surnomme là-bas avec affection, est entièrement réalisé en acier, un matériau dont la connotation industrielle hérissait la Dame de fer en raison de sa pesanteur sociale).


Comme oublié à même le pavé de la cour intérieure de la Burlington House, Iron Baby (1999) se découvre à la façon d'un petit tas sombre en fer noir. Distrait et pressé, on buterait facilement dessus... c'est bien là tout le message de Gormley qui nous invite, par le biais de cette oeuvre liminaire, à ralentir le pas dans un monde frénétique où il est coutume de s'écraser symboliquement les uns et les autres. Heureusement, il ne manque pas d'âmes sensibles qui veillent au grain empathique. Courbées vers le sol, elles s'inquiètent pour le bébé sombre dont les formes suggèrent la quintessence de la fragilité. Il est réjouissant de constater que l'humanité restaure ici son image : l'observation des visages exprimant toutes les nuances de la compassion des visiteurs laisse sans voix. Sans oublier ce moment crucial, empli de désarroi, où les mêmes visiteurs reprennent leur chemin chagrinés, dépités de ne pouvoir emmener le petit d'homme, pourtant inerte, avec eux.


Puissant est cet incipit sculptural, lui qui donne à voir, peu importe qu'il pleuve ou qu'il vente, ce bouleversant nourrisson d'à peine six jours. Le tout n'est pas sans convoquer une dimension personnelle puisque celle dont la frêle silhouette a servi de modèle n'est autre que la propre fille du plasticien. Posé sur la pierre froide, le petit corps évoque à la fois l'humilité la plus totale, celle d'une position de prière, et l'abandon aux lois de l'existence. Cela nous rappelle avec force qu'une fois né, on n'a plus qu'à vivre et mourir. Détail touchant : face à cet incertain futur, la minuscule main cherche la bouche, primitive tentative d'apporter une réponse au manque constitutif caractérisant la condition humaine.



Pouvoir d'organisation



Ce qui interpelle avant de pousser la porte de la Royal Academy, c'est de savoir à quelles échelles le visiteur va être confronté : intimes et émotionnelles, comme le foetus laissé derrière soi, ou monumentales et invasives à la manière des imposantes installations déployées depuis l'embouchure de la Mersey (du côté de Liverpool), jusqu'au lac Ballard en

Australie

. Fin stratège et jamais en manque de subtilité, le sculpteur, qui a collaboré à maintes reprises avec le chorégraphe belge Sidi Larbi Cherkaoui, s'est amusé à multiplier les allers-retours entre les deux registres. L'oeil passe donc sans transition de la délicatesse de Fruits of the Earth (1978-1979), enfermant entre autres des pommes dans du plomb, et de Body and Light (1990-1996), des dessins réalisés à partir de carbone et caséine, à Matrix III (2019), une sorte d'immense nuage métallique composé d'un maillage inextricable de cages d'acier suspendues à hauteur d'homme, chacun de ces rectangles grillagés occupant une superficie équivalent à la taille d'un standard de l'intimité, en l'occurrence la chambre à coucher.


Le fil rouge qui traverse ces régimes d'exposition ? Il réside dans la capacité de Gormley à nous faire ressentir l'espace au-delà de l'expérience banalisée que nous vivons chaque jour. Pour ce faire, celui qui a reçu le prestigieux Turner Prize en 1994 possède une méthode bien à lui, qui consiste à sortir la dimension spatiale objective de ses gonds à travers l'état méditatif. Celui-ci lui vient directement de son enfance. Gamin, obligé de faire une sieste après le déjeuner dans une chambre exiguë, Gormley s'est rendu compte qu'en fermant les yeux, il pouvait contacter un espace infini à l'intérieur de lui. Après avoir appréhendé cette structure mentale, il n'a eu de cesse d'en exercer l'incroyable puissance d'organisation. « Créer de l'espace en nous et en dehors de nous », tel est le credo qui l'anime. Il s'exprime tout particulièrement à travers une oeuvre comme Clearing VII (2019), soit une pièce envahie par huit kilomètres de tube d'aluminium flexible agencé en arc de cercle du sol au plafond et d'un mur à l'autre. Physique et même oppressante, l'installation oblige le visiteur à enchaîner les contorsions pour atteindre l'autre côté de la salle... mais observée à distance, elle ressemble à une sorte de graffiti déployé en trois dimensions.


Ce pouvoir organisationnel hérité de ses jeunes années, Gormley l'a utilisé jusqu'aux limites du genre, exerçant une véritable pression sur le bâtiment qui accueille sa rétrospective. C'est flagrant avec l'ambivalent Host (2019), prouesse technique et, surtout, vaste étendue remplie d'eau de mer et d'argile qui réfléchit les variations du ciel par le biais d'une verrière. Une porte se profile également de l'autre côté du point de vue que l'on peut en prendre, il n'est pas interdit d'y voir une inatteignable issue de secours. On l'aura compris, ce spectacle sublime invitant à la méditation évoque tant les origines du vivant que l'apocalyptique montée des eaux que l'on n'en finit pas de nous promettre. Mentionnons enfin Cave (2019), sorte d'intestin géant métallique à travers lequel tout un chacun est invité à passer. L'oeil y fait l'expérience de l'obscurité et également de la précieuse lumière avarement concédée par une trouée. La main ? Elle tremble sous la froideur métallique. L'esprit, enfin, hésite entre odyssée pariétale et métaphore d'un salutaire voyage à l'intérieur de soi, celui auquel tout le parcours invite.


Antony Gormley
: à la Royal Academy of Arts, à Londres, jusqu'au 3 décembre prochain.

www.royalacademy.org.uk



]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004275.xml</field>
    <field name="pdate">2019-10-24T00:00:00Z</field>
    <field name="pubname">Le Vif/L'Express Le VifExpress</field>
    <field name="pubname_short">Le Vif/L'Express</field>
    <field name="pubname_long">Le Vif/L'Express Le VifExpress</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">CULTURE;EXPOS</field>
  </doc>
</add>
