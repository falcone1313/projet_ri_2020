<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Et l'Athénien s'éteignit</field>
    <field name="author">LAURENT FAVRE</field>
    <field name="id">eupr_005570.xml</field>
    <field name="description"><![CDATA[


6322


, MELBOURNE @LaurentFavre


Pour s'être approché trop près du Soleil, Icare se brûla les ailes et tomba dans la mer Egée. Stefanos Tsitsipas, jeune demi-dieu grec qui faillit mourir noyé en mer Egée en 2015, s'est sans doute trop approché du Soleil (la Vérité, dans la mythologie grecque) en demi-finale de l'Open d'

Australie

. La sensation du tournoi, qui s'était senti pousser des ailes après sa victoire en huitième de finale sur Roger Federer, a subi jeudi une très cuisante défaite face à Rafael Nadal: 6-2 6-4 6-0, et une heure quarante seulement de jeu.


C'est peu dire que Tsitsipas est tombé de haut. La chute, ajoutée au vertige, le laissa estourbi. Il s'en relèvera, mais en conférence de presse il n'avait visiblement pas encore tout compris. « Honnêtement, je n'ai aucune idée de ce que je peux tirer de ce match, avoua-t-il, perplexe. Ce n'est pas que j'étais proche d'arriver à quelque chose: je n'ai fait que six jeux en tout. Je ne sais pas, je me sens très bizarre. Je me sens heureux avec mon tournoi, mais en même temps je suis déçu. J'ai l'impression que je pouvais faire un peu mieux aujourd'hui. Je ne sais pas. C'est ce que je ressentais. Mais c'est un sentiment très, très bizarre. J'ai presque eu l'impression que je ne pouvais pas mieux jouer. Je ne sais pas, je ne sais pas... »



Le Minotaure de Manacor



Abandonnons Stefanos Tsitsipas dans le labyrinthe de ses pensées pour parler de Rafael Nadal, le Minotaure de Manacor. Il a été monstrueux, maîtrisant totalement la rencontre, dominant son adversaire dans tous les compartiments du jeu. Très performant au service, il a remporté 85% de points sur sa première balle et placé 5 aces. Il s'est procuré 11 balles de break et ne les a gâchées (à une exception près à 2-2 et 0-40 dans la deuxième manche) que lorsqu'il en avait plusieurs consécutives. Avant de concéder une balle de break sur l'ultime jeu du match à 5-0 30-40, il n'avait jamais perdu plus de deux points sur ses jeux de service. Ce n'est pas tout; Rafael Nadal a encore gagné plus de points que son adversaire dans les filières courtes, les filières moyennes, les filières longues. Il a réussi plus de coups gagnants (28 contre 17), commis moins de fautes directes (14 contre 22), est monté plus souvent au filet (22 contre 19) pour un bien meilleur résultat (18 points gagnés contre 10).


Devant ceux qui s'étonnent de le voir pratiquer un jeu aussi agressif, l'Espagnol a presque encore les naseaux qui fument. « Parce que j'ai eu beaucoup de succès sur terre battue, certaines personnes pensent que je ne suis qu'un joueur de fond de court qui renvoie la balle, répond-il. Bien sûr, je ne fais pas service-volée - parce que je ne vais pas jouer le jeu de Federer si je n'ai pas le service de Federer - et je ne frappe pas des coups gagnants sur chaque balle, mais je joue tous les coups avec une intention. Il n'y a pas de meilleure manière d'être agressif que de taper chaque balle dans le but de causer des dommages à l'adversaire. Cela a été mon but durant toute ma carrière. Actuellement, je peux créer ce préjudice un peu plus tôt qu'avant parce que je sers très bien, cela rend ma première balle [après le service, c'est-à-dire le troisième coup du point] plus facile. Et j'ai la détermination pour le faire. C'est tout. »


Sur le court bleu Méditerranée de la Rod Laver Arena, Rafael Nadal a allumé une sorte de feu grégeois, cet antique incendie qui se propage davantage si l'on tente de l'éteindre. Stefanos Tsitsipas a tout tenté: servir fort, prendre la balle tôt, monter au filet. Le matin du match, son père, Apostolos, et son conseiller, Patrick Mouratoglou, visionnaient des images de la victoire l'an dernier de Marin Cilic sur Nadal pour trouver des solutions. « Il y en a toujours, mais il faut parvenir à les appliquer », résumait Patrick Mouratoglou. Tout ce qu'il a tenté lui est revenu encore plus fort. « Rafa est juste très agressif à la base et il ne vous donne pas de rythme, détaillait encore Stefanos Tsitsipas après le match. Son style de jeu est différent de celui des autres joueurs. Il a ceci, je ne sais pas, un talent qu'aucun autre joueur n'a. Je n'ai jamais vu un joueur avoir ça. Il te fait mal jouer. Je ne sais pas... J'appellerais ça un talent. »



Au terminus des ambitieux



Rafael Nadal se hisse en finale de l'Open d'

Australie

deux ans après un match inoubliable face à Roger Federer. Il n'a pas perdu un set en chemin et a surclassé trois des grands espoirs du tennis: l'Australien Alex De Minaur au troisième tour, l'Américain Frances Tiafoe en quart de finale et Stefanos Tsitsipas dans ce qui s'apparentait à un choc des générations. Au-delà de la différence d'âge (20 ans pour Tsitsipas, 32 pour Nadal), il y a une véritable opposition de styles. Les nouveaux joueurs sont très grands (1,95 m de moyenne pour les Zverev, Khachanov, Medvedev, Tsitsipas, 1,85 m pour Nadal et Federer) et très offensifs. Avec eux, le tennis redevient un jeu d'attaque. « Parce que les contreurs, Nadal, Djokovic, Murray ont dominé et qu'il a fallu trouver des solutions face à eux », explique Patrick Mouratoglou.


Mais une autre tendance, plus forte, prédomine encore: la condition physique qu'un grand joueur met des années à acquérir. A 20 ans, Stefanos Tsitsipas n'est pas encore au point. Sa victoire sur Roger Federer a été comparée à celle de Federer sur Pete Sampras en 2001 à Wimbledon. Même âge au moment du crime de lèse-majesté, même stade de la compétition (huitième de finale), même désillusion peu après. On rapporte à Tsitsipas que Federer n'a commencé à tout gagner que deux ans après cet exploit. Cela ne le console qu'à peine: « Je ne sais pas. J'essaie de comprendre. Je n'ai pas envie de perdre dix fois contre Rafa... »


Singularité de l'Open d'

Australie

, Rafael Nadal ne connaîtra que vendredi le nom de son adversaire en finale, par rapport à qui il aura un jour de récupération supplémentaire.


Ce sera soit le Français Lucas Pouille, métamorphosé depuis qu'il est conseillé par Amélie Mauresmo, soit - plus probablement - le Serbe Novak Djokovic, pas impérial jusque-là mais quand même numéro un mondial (match vendredi soir, à partir de 9h30 en Suisse).


« Le style de jeu de Rafael Nadal est différent de celui des autres joueurs. Il a un talent qu'aucun autre joueur n'a »


STEFANOS TSITSIPAS


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005570.xml</field>
    <field name="pdate">2019-01-25T00:00:00Z</field>
    <field name="pubname">Le Temps</field>
    <field name="pubname_short">Le Temps</field>
    <field name="pubname_long">Le Temps</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Sport</field>
  </doc>
</add>
