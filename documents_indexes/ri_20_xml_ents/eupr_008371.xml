<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">La Presse en Bolivie: au pays de l'or blanc</field>
    <field name="author">TEXTE : PHILIPPE MERCURE, PHOTOS : FRANÇOIS ROY</field>
    <field name="id">eupr_008371.xml</field>
    <field name="description"><![CDATA[



La ruée vers le lithium



Les bras couverts de tatouages tendus vers le volant, des mèches noires s'échappant de sa tuque, Diego conduit le 4 x 4 à vive allure. Ici, il n'y a ni routes, ni panneaux, ni feux de circulation. Nous sommes à 3650 mètres d'altitude, sur un plateau encaissé au milieu des Andes, en train de rouler au fond d'un lac asséché depuis des milliers d'années.


Autour, le paysage est blanc, immaculé, hallucinant. Le salar d'Uyuni, en Bolivie, est le plus grand désert de sel du monde. Le regard ne rencontre rien avant les montagnes qui se dessinent, bleutées, à l'horizon. Jusqu'à ce qu'une forme surgisse au loin dans l'immensité.


Diego bifurque vers l'intrigante silhouette et finit par immobiliser le véhicule devant une pancarte plantée directement dans la croûte de sel qui fait ici office de sol. « NO PASAR - Area Restringida », peut-on y lire. Défense de passer, zone d'accès restreint.


Dans le siège du passager, Maribel Mercado laisse échapper un juron. Cette guide sillonne le salar d'Uyuni depuis 19 ans pour le faire visiter aux voyageurs du monde entier. Ce coin du désert, loin des infrastructures touristiques, avait échappé à son attention récemment.



« C'est horrible ! La magie, ici, c'est justement qu'il n'y a pas de signe comme ça. »



– Maribel Mercado, guide dans le salar d'Uyuni


La pancarte n'est pourtant qu'un prélude. Au-delà, on voit de petites buttes de terre brune qui jurent avec la blancheur des lieux. Sur ce qui semble être une route surélevée, une pelle mécanique jaune s'active. Des lignes électriques érigées directement dans le désert convergent vers des bâtiments blanc et bleu qui, d'ici, paraissent tout petits.


« Blanc et bleu, bien sûr », laisse tomber Maribel. Coïncidence ou non, ce sont les couleurs du parti Movimiento al Socialismo, ou Mouvement pour le socialisme, du président bolivien Evo Morales. Au moment de notre visite, il sollicitait un quatrième mandat. On saura aujourd'hui, jour des élections nationales, s'il a réussi à se maintenir au pouvoir.



Un métal en demande



De près, les bâtiments bleu et blanc s'avèrent gigantesques. On y accède après avoir franchi une guérite gardée par des soldats en tenue de camouflage de l'armée nationale. Ces installations sont au coeur du plan de la Bolivie, le pays le plus pauvre d'Amérique du Sud, pour propulser son développement. Un plan suivi de près par des observateurs de partout sur la planète.


Ce qu'on cherche ici à exploiter est le lithium. Ce métal, le plus léger du tableau périodique, entre dans la fabrication des batteries lithium-ion qui font fonctionner nos téléphones intelligents, tablettes électroniques et ordinateurs portables. Voilà des années qu'on se l'arrache sur les marchés. Mais l'arrivée des voitures électriques a transformé la ferveur en folie. La pile d'un iPhone contient un peu plus de 2 grammes de carbonate de lithium ; la batterie d'une voiture Tesla en compte 63 kilogrammes. La transition énergétique qui démarre a déjà rendu le monde affamé de lithium. Et si tout va comme le souhaitent les écologistes, ce n'est qu'un début. Le groupe d'analyse de marché CRU Group prévoit que la demande de lithium doublera d'ici cinq ans. Or, la Bolivie est assise sur les plus grosses réserves mondiales. Certains observateurs ont trouvé une formule qui frappe l'imaginaire. La Bolivie, disent-ils, pourrait devenir « l'Arabie saoudite de l'ère de la voiture électrique ».



Les Caraïbes en plein désert



Le long procédé d'extraction du lithium commence par une vision surprenante : de l'eau en plein désert. Une eau turquoise, digne des Caraïbes, qui clapote dans d'immenses bassins.


« Le procédé nécessite huit bassins. Nous avons 20 lignes en parallèle. Ça fait 160 bassins », explique fièrement Jhonny Alejo Choque, lunettes de soleil au visage, en pointant le damier de réservoirs qui s'étend à perte de vue. L'homme est administrateur pour Yacimientos de litio bolivianos, la société d'État bolivienne qui gère l'exploitation du lithium.


Cette eau salée, appelée saumure, a été prélevée sous la croûte de sel du désert. Sur une superficie de 500 km2, 89 trous pouvant aller jusqu'à 50 mètres de profondeur ont été creusés. L'eau souterraine a été pompée jusqu'aux bassins. Le précieux lithium s'y trouve dissous, avec plusieurs autres composés à base de magnésium, de potassium et de sodium.


L'eau passe plusieurs mois ici, où elle s'évapore pendant que les composés qu'elle contient sont précipités les uns après les autres au fond des bassins. Quand ils ont atteint une concentration suffisante, on les récolte pour les traiter.


Jhonny Alejo Choque franchit les portes d'une petite usine, un peu à l'écart du désert, qui grouille de travailleurs en combinaison rouge. Des machines produisant des bruits assourdissants pressent les sels pour en extraire le liquide, chauffent les préparations, les filtrent, les font circuler entre les différents étages de l'édifice. Au bout du processus, un tapis roulant achemine une pâte blanche qui est brisée par un peigne en une poudre farineuse.


« Li2CO3. Du carbonate de lithium pur à 99,5 %. C'est ça, l'or blanc », dit Ariel Cruz, 25 ans, qui dirige les opérations de l'usine. Le jeune homme qui semble à peine sorti de l'adolescence plonge la main dans cette poudre encore chaude et, à la surprise générale, la porte à la bouche.


« Ça goûte le bicarbonate de soude », lance-t-il.


Armés de pelles, des travailleurs récoltent ce carbonate de lithium et en remplissent des sacs de 20 kg, qu'ils pèsent et transvident dans de plus gros de 500 kg. Marqués de l'inscription « carbonato de litio », une douzaine d'entre eux s'empilent dans un coin. Chacun vaut environ 6000 $ US.



Non aux multinationales



Le carbonate de lithium qui sort de cette usine-pilote est l'aboutissement d'un processus long et tortueux. Au milieu des années 2000, attirées par les immenses réserves de lithium de la Bolivie, plusieurs multinationales ont approché le gouvernement bolivien dans l'espoir d'en tirer profit. Mais le président socialiste Evo Morales leur a fermé la porte au nez. Ces ressources appartiennent à la Bolivie, a-t-il clamé haut et fort, et seront exploitées par la Bolivie.


« Pour plusieurs raisons politiques, le gouvernement a ignoré bien des gens qui avaient une expertise dans l'exploitation du lithium. Ils ont décidé d'aller de l'avant seuls et ils ont eu beaucoup de problèmes parce qu'ils ne connaissaient rien là-dedans », commente Juan Carlos Zuleta, un économiste bolivien rencontré dans un café du chic quartier San Miguel, à La Paz.


Mais la Bolivie est allée plus loin. Elle a décidé qu'elle ne se contenterait pas d'extraire le lithium du désert d'Uyuni. Elle fabriquerait aussi les batteries des voitures électriques qui l'utilisent.



« Ils partent d'une position extrêmement politique, très radicale, qui dit : on va tout développer nous-mêmes. »



– Joel Flores-Carpio, un Franco-Bolivien qui dirige Strategis, une entreprise qui aide les groupes étrangers à faire des affaires en Bolivie


L'usine-pilote d'où sort du carbonate de lithium a été inaugurée en 2008. Pendant des années, on a peiné à en tirer quoi que ce soit. En 2016,

La Presse

a approché les autorités boliviennes dans l'espoir de la visiter. L'autorisation a été refusée. Raison invoquée : les reportages internationaux « donnent une image négative » de l'industrie bolivienne du lithium.


Cette année, les portes se sont ouvertes. « Ce n'est pas un hasard. Maintenant, ils ont des choses à montrer », estime Joel Flores-Carpio. Face aux problèmes qui se multipliaient, le gouvernement bolivien s'est résolu à demander de l'aide. Il a formé une coentreprise avec une société allemande, ACI Systems. Une entreprise chinoise a été engagée pour construire une véritable usine industrielle, beaucoup plus grande que l'usine-pilote, dont les plans ont été conçus par la société allemande KU Tech. Ces installations ultramodernes sont celles qu'on aperçoit du désert. Pour l'instant, elles ne produisent que du chlorure de potassium, un sous-produit de l'extraction du lithium qui est exporté comme engrais vers le Brésil. Un complexe de la même taille, pour le lithium lui-même, est en construction.


Cette année, la firme américaine SRK a aussi confirmé que les ressources de lithium de la Bolivie s'élèvent à 21 millions de tonnes - le double de certaines estimations passées. La société française ECM Greentech, soutenue par le Commissariat à l'énergie atomique et aux énergies alternatives de France, a aidé à construire une petite usine-pilote de batteries à La Palca, à 400 kilomètres au nord-ouest du désert d'Uyuni. Selon Manuel Oliviera Andrade, un chercheur en développement durable de l'Université Mayor de San Andrés, à La Paz, qui a consacré son mémoire de maîtrise au projet de lithium, les investissements frisent le milliard US et se poursuivent.


Malgré tout, M. Andrade doute que la Bolivie soit finalement sur la bonne voie.


« Il y a des discours, des promesses, des partenariats, beaucoup d'infrastructures et d'argent dépensé. Mais au bout du compte, il faut regarder les résultats. Or, la Bolivie n'a produit que 200 tonnes de lithium en 6 ans. C'est très, très peu », dit-il à

La Presse

. Le Chili et l'Argentine, en comparaison, ont produit respectivement 16 000 et 6200 tonnes de lithium l'an dernier. Selon M. Andrade, le procédé de l'usine-pilote est si peu efficace que ce serait une erreur de le répliquer à grande échelle dans l'usine industrielle pourtant en construction.



Des experts sceptiques



Le projet de la Bolivie de construire les batteries de voitures électriques soulève aussi le scepticisme. Les trois experts interviewés par

La Presse

estiment que le partenaire allemand choisi, ACI Systems, n'a ni l'expérience ni l'accès au marché mondial pour fabriquer et vendre des batteries électriques.


« Le lithium va améliorer l'économie de la Bolivie. Nous sommes fiers de travailler ici parce que cette industrie supportera les plus jeunes générations. Toute la Bolivie va en profiter - j'espère en fait que cela aidera le monde entier », dit l'administrateur Jhonny Alejo Choque.


« Ce rêve de faire de la Bolivie l'Arabie saoudite du lithium, ce sont les journalistes et les politiciens qui l'ont inventé, dit quant à lui Manuel Oliviera Andrade. Nous avons les ressources, c'est vrai. Mais pour l'instant, tout le reste n'est justement que ça : un rêve. »



Les enjeux en un coup d'oeil



Comment procède-t-on à l'extraction du lithium ? Les grandes lignes du procédé.



Puits :

L'eau souterraine contenant le lithium est pompée à la surface.



Bassins :

L'eau est amenée dans des bassins, où elle s'évapore, laissant les composés au fond.


Le procédé peut prendre jusqu'à 18 mois.



Usine de production :

Le sel (chlorure de sodium), le chlorure de magnésium et le carbonate de lithium sont séparés. Ce dernier doit atteindre une pureté de 99,5 % pour être utilisé dans les batteries.


Ce qu'on appelle le triangle du lithium se trouve à l'intersection du Chili, de l'Argentine et de la Bolivie. On y trouve plusieurs dépôts de sel appelés

salars

(désert de l'Atacama au Chili, déserts d'Arizaro et d'Olaroz-Cauchari en Argentine, désert d'Uyuni et de Coipasa en Bolivie). Ensemble, les trois pays comptent environ 60 % des ressources mondiales.



Plus grands producteurs mondiaux (en tonnes, 2018)




Australie

: 51 000


Chili : 16 000


Chine : 8000


Argentine : 6200



Plus grandes ressources de la planète (en millions de tonnes)



Bolivie : 21


Argentine : 14,8


Chili : 8,5



Australie

: 7,7


États-Unis : 6,8


Chine : 4,5


Canada : 2



Usages



Les batteries des voitures électriques sont en train de propulser la demande pour le lithium.


Batteries rechargeables : 56 %


Verre et céramique : 23 %


Lubrifiants : 6 %


Production de polymères : 5 %


Autres : 11 %


Sources : Graphic News, U.S. Geological Survey



Sacrifier un désert pour sauver le monde ?



Tomas Colque montre les sillons bien droits qui traversent son lopin de terre. Il les a creusés la veille avec un tracteur loué. Le même tracteur est maintenant à l'oeuvre dans le lot du voisin, soulevant un nuage de poussière. Un renard des Andes, dérangé par le remue-ménage, détale pour trouver un peu de tranquillité.


Nous nous trouvons près du village de Colcha K, sur une bande de terre coincée entre le désert de sel d'Uyuni et des montagnes plantées de cactus. Tomas Colque appartient au groupe ethnique des Quechuas, un peuple amérindien qui descend des Incas. Comme plusieurs personnes dans le coin, il cultive du quinoa.


« L'eau, ici, est un grand enjeu. Elle est précieuse », dit-il en montrant sa terre sablonneuse qui semble en effet particulièrement sèche.


Colcha K est le village situé le plus près des installations de lithium. Ici, les changements qui transforment ce coin du désert suscitent bien des inquiétudes. « Ils prennent l'eau du Rio Grande », dit Tomas Colque en parlant de la rivière qui coule près du village.



« Il reste de moins en moins d'eau. Pour l'instant, ce n'est pas dramatique. Mais dans quelques années, il y aura des conséquences. »



– Tomas Colque


Celui qui a été nommé à la tête d'un groupe de citoyens regroupant 46 communautés de la région affirme que des flamants roses vivaient auparavant à l'endroit où se trouve l'usine de lithium. « Quand ils volaient par ici, on savait qu'il allait pleuvoir, affirme-t-il. Maintenant, ils ont disparu. »



Des impacts inconnus



Tomas Colque n'est pas le seul à s'inquiéter. Manuel Oliviera Andrade, chercheur en développement durable à l'Université Mayor de San Andrés, à La Paz, a obtenu un prix de l'UNESCO pour son mémoire de maîtrise sur le projet de lithium. Il observe son développement avec appréhension.


« En ce moment, nous n'avons aucune étude scientifique sur les impacts environnementaux de l'exploitation du lithium en Bolivie. Nous avons des références sur les projets similaires qui se trouvent au Chili et en Argentine, mais encore là, les connaissances sont très incomplètes », dit-il. Selon lui, la loi bolivienne a obligé le gouvernement à faire des études d'impact environnemental, mais celles-ci ont été gardées secrètes.


Le chercheur s'inquiète des « nombreux produits chimiques » qui entrent dans le procédé d'extraction du lithium, notamment la chaux utilisée pour retirer le magnésium de la saumure (le gisement bolivien contient cinq fois plus de magnésium que ceux du Chili et de l'Argentine).



« Nous sommes préoccupés par le fait que le projet pourrait générer beaucoup de résidus de chaux. »



– Manuel Oliviera Andrade


Mais le plus grand enjeu est sans doute l'eau. De l'eau douce, puisée dans les deux rivières de la région, est utilisée directement dans le procédé et pour laver les équipements. Et il y a toute cette eau souterraine qui est pompée de sous la surface du désert. Manuel Oliviera Andrade estime que la production de 15 000 tonnes de carbonate de lithium, le chiffre annuel visé par le gouvernement, nécessiterait 28 millions de mètres cubes d'eau - près de 7500 piscines olympiques.


Jhonny Alejo Choque, de l'entreprise d'État Yacimientos de litio bolivianos, n'en fait pas grand cas. « C'est comme si on aspirait avec une paille - rien de plus », dit-il. Mais M. Andrade n'en est pas si sûr.


« L'impact de ça sur l'environnement désertique et semi-désertique de la région est absolument inconnu », dit-il, affirmant que les activités se déroulent dans un « écosystème fragile ».


Le chercheur rappelle que les communautés de la région vivent en grande partie de la culture de quinoa, de l'élevage de lamas et du tourisme, des activités qui pourraient être menacées par l'industrie du lithium. L'ironie de sacrifier un écosystème pour fabriquer des batteries servant aux voitures électriques censées sauver l'environnement ne lui échappe pas.


« Ce sont les deux côtés d'une même médaille », dit-il.


Cet article est paru dans La Presse (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008371.xml</field>
    <field name="pdate">2019-10-20T00:00:00Z</field>
    <field name="pubname">La Presse (site web)</field>
    <field name="pubname_short">La Presse (site web)</field>
    <field name="pubname_long">La Presse (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Amérique latine</field>
  </doc>
</add>
