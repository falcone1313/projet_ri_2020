<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Le nombre de calories nécessaires à la population mondiale devrait augmenter de 80% d'ici la fin du siècle. Saurons-nous relever le défi ?</field>
    <field name="author">Béatrice de Reynal</field>
    <field name="id">eupr_007544.xml</field>
    <field name="description"><![CDATA[



Atlantico.fr : Selon une nouvelle étude de l'Université de Göttingen en Allemagne, les besoins caloriques de la population mondiale devraient s'accroître de 80% d'ici la fin du siècle. Cette hausse serait liée à la démographie, à l'augmentation de la population mais aussi à une croissance de l'IMC (l'indice de masse corporelle). D'après des informations de la BBC, nos besoins en nourriture risquent de s'accroître significativement d'ici la fin du siècle. Selon les chercheurs, « l'augmentation des besoins caloriques sera de 253 kcal par personne entre 2010 et 2100 » . Les pays subsahariens seraient potentiellement parmi les premiers pays affectés par ce phénomène. D'après l'ONU, la population mondiale devrait passer de près de sept milliards en 2010 à près de onze milliards en 2100. Or, les auteurs avertissent que le fait de ne pas répondre au besoin de plus de calories pourrait entraîner une plus grande inégalité mondiale.




Ces besoins pourraient-ils augmenter dans les années à venir, accroître les inégalités et la pauvreté et provoquer une crise alimentaire de grande ampleur comme le suggère cette étude ?




Béatrice de Reynal :

Les études existantes montrent comment la croissance démographique et l'augmentation des revenus entraîneront une augmentation massive de la future demande mondiale de nourriture.


Elle est logique et non surprenante, plus que nous aurons plus de bouches à nourrir, des vies allongées et un niveau de pauvreté qui va baisser - ce qui est bien - avec donc un apport énergétique pour tous plus équilibré.


Ces chercheurs allemands dont l'étude vient d'pitre publiée dans PlosOne (

The effect of bigger human bodies on the future global calorie requirementsLutz Depenbusch Stephan Klasen, December 4, 2019

) évaluent à 80 % à l'augmentation des besoins caloriques d'ici la fin du siècle... soit sur 80 années ! Pourquoi ? Parce que la démographie est explosive dans certains pays émergents africains, et que la faim régressent dans le monde, même si elle le fait si lentement, on a aujourd'hui plus de sujets en surpoids ou obèses que de sujets sous-nutris (800 millions d'individus). Ce qui est une bonne chose, mais qui, bien sûr, nécessite une bonne organisation.


La croissance de l'IMC notamment dans les groupes dénutris est bien sûr, une bonne chose. Mais ce que ces chercheurs craignent pardessus tout est une augmentation du surpoids, de l'obésité et du diabète, maladie déclarée comme ennemi public n°1 dans la monde pour l'OMS qui voit émerger une véritable épidémie de diabète « sucré » .



Catherine Grangeard :

Oui, il est certain que les besoins augmentent quand la population augmente ! Si l'on passe de 7 à 11 milliards de personnes sur terre, il va de soi que ces 4 milliards d'individus supplémentaires vont entraîner des besoins en conséquence... Et ce sera important ! C'est un scoop de La Palice ? Sérieusement, sur ce point cette étude n'apporte pas grand-chose.


Qui plus est, la richesse économique se développant, la consommation grimpe. Ça s'est, toujours et partout, passé ainsi. En France, la consommation alimentaire a progressé depuis le début du siècle précédent.


Nous constatons que certaines régions autrefois épargnées par les problèmes liés à l'obésité sont alors touchées. Le développement économique amène donc une consommation accrue.


Sur ce point, les conclusions de cette étude sont indiscutables. Après, je ne sais pas si le calcul de 253 calories est juste ou pas, mais c'est un détail.



Jean-Louis Lambert :

La croissance de la population et des revenus va faire augmenter la consommation globale. Mais les modes de vie plus sédentaires entraînent des besoins caloriques moindres. Une consommation excessive est source de surpoids. Une augmentation de l'IMC peut pousser à une augmentation de la consommation. Mais alors sur le plan sanitaire, le besoin est plutôt de baisser les rapports caloriques. Prédire une augmentation des besoins caloriques de près de 10% me semble donc faux.


Par contre l'augmentation de la consommation va être confrontée à l'évolution des disponibilités qui ne suivra pas forcément le même rythme. Ce sont les populations pauvres qui seront alors touchées. Elles devront réduire leur consommation et cela pourrait être bénéfique pour les obèses.


Globalement donc, je suis en désaccord avec le scénario catastrophe.



Comment en sommes-nous arrivés-là ? Quels sont les éléments qui peuvent expliquer cet accroissement des besoins caloriques ? Et quels sont les mécanismes qui ont engendré ce cercle vicieux ?




Béatrice de Reynal :

Ces chercheurs ont essayé d'estimer l'effet potentiel de l'augmentation du poids humain, causée par l'augmentation de l'IMC et de la taille, sur les futurs besoins en calories au lieu d'avoir une approche économique de marché ou strictement calorique.


Ils ont développé quatre scénarios différents pour montrer l'effet de l'augmentation de la taille humaine et de l'IMC. Dans un monde où le poids par groupe d'âge-sexe resterait stable, ils ont posé des prévisions une augmentation des besoins en calories de 61,05% entre 2010 et 2100.


L'augmentation de l'IMC et de la taille pourrait ajouter 18,73 points de pourcentage à cela. Cette augmentation supplémentaire représente plus que les besoins caloriques combinés de l'Inde et du Nigéria en 2010. Ces augmentations toucheraient particulièrement les pays d'Afrique subsaharienne, qui seront déjà confrontés à une augmentation massive des besoins caloriques en raison de la forte croissance démographique. Les fortes différences régionales appellent à des politiques qui améliorent l'accès à la nourriture dans les régions actuellement économiquement faibles. De telles politiques devraient éloigner la consommation des aliments riches en énergie qui favorisent le surpoids et l'obésité, afin d'éviter le fardeau direct associé à ces conditions et de réduire l'augmentation des calories requises. Un apport insuffisant de calories ne résoudrait pas le problème mais entraînerait la malnutrition des populations ayant un accès limité à la nourriture. La malnutrition ne diminuant pas mais favorisant l'augmentation des niveaux d'IMC, cela pourrait même aggraver la situation.


Les courbes ci-dessous montrent que les augmentations dues au changement démographique déclenchent des taux de croissance des besoins énergétiques particulièrement élevés dans la première moitié du 21e siècle. Alors que les augmentations dues à la stabilisation de la démographie après ce point (comme la croissance de la population devrait ralentir), les effets dus à l'augmentation de la taille et de l'IMC deviennent plus importants. Par conséquent, ignorer les augmentations possibles de l'IMC et de la hauteur conduit à une sous-estimation de l'évolution des besoins énergétiques, en particulier dans la seconde moitié du 21e siècle.


Sur la base de leur modèle, les besoins quotidiens moyens étaient de 2285 kcal par personne en 2010. Avec l'augmentation de l'IMC et de la taille, cela augmente à 2425 en 2050 et à 2538 kcal en 2100. Par rapport au scénario 2, cela signifie que leur hypothèse de 2500 kcal par habitant est raisonnablement élevée pour les évolutions jusqu'en 2050. Cependant, elle serait dépassée d'ici 2080 dans le scénario quatre.



Catherine Grangeard :

L'étude montre aussi très justement que plus un individu est grand et fort, plus il a besoin de calories. Puisque l'humanité est mieux nourrie, elle grandit et pèse plus lourd. C'est une suite logique... Enfin, c'était le cas avant le règne de la malbouffe. De nombreuses études internationales ont montré que les tailles ont progressé depuis le début du siècle précédent. L'espérance de vie suivait le mouvement. La qualité de la nourriture désormais fait grossir, je précise, plus exactement son manque de qualité... Les calories grasses et vides font prendre du poids sans pour autant avoir une qualité nutritionnelle intéressante. L'espérance de vie commence à chuter aux USA, comme vous le savez.


La mondialisation de l'alimentation a déstabilisé les habitudes, adaptées depuis des générations aux habitants et à leurs ressources locales. Ainsi, cette uniformisation a eu des effets délétères. Des populations entières, en adoptant une alimentation importée avec les grandes chaînes agroalimentaires ont remplacé les modes de vie ancestraux par une nouvelle façon de consommer et les soucis sont immédiatement engendrés...


La croissance démographique du siècle dernier a entraîné évidemment des besoins supplémentaires.


Le cercle vertueux -puisque la santé s'est améliorée- devient-il obligatoirement vicieux ?


Toute médaille aurait son côté recto et son autre côté verso. Est-ce si surprenant ?



Quelles seraient les régions les plus exposées ? L'Europe, la Chine ou les Etats-Unis sont-ils menacés ?




Béatrice de Reynal :


Changements au niveau des pays



Le top 5 des pays qui vont notablement augmenter leurs consommations de calories se retrouvent le Nigéria (+577 564 Tera calories), la DR Congo, la Tanzanie et le Pakistan. En parallèle, le top 5 des pays qui vont voir baisser leurs consommations de calories nous avons la Chine (- 218 827 tera calories), le Japon, la Russie, la Thaïlande et l'Ukraine.



L'effet de l'accroissement démographique sur les besoins caloriques des pays



On voit que les besoins sont assez stables dans la plupart des pays développés : Toute l'Amérique, l'

Australie

, l'Europe. Ils sont en décroissance pour la Chine, etc.



Catherine Grangeard :

Quels sont les pays qui ont le plus fort de croissance démographiques ? Ce seront ceux-là qui auront le plus de problèmes. Les autres auront une évolution maîtrisable. C'est parce qu'un saut important en peu de temps se produit que les dérèglements suivent.


Au sein de chaque pays, plus les différences de niveau de vie sont importantes et plus la situation sera explosive. Il est d'autant plus insupportable d'avoir faim quand votre voisin se gave !



Sommes-nous capables de faire face à ce défi et pour lutter contre la pauvreté et la malnutrition ? Qu'est-ce qui pourrait avoir le plus d'impact sur la sécurité alimentaire à travers la planète ? Quelles seraient les solutions, les mesures ou les bonnes pratiques à appliquer sur le plan de la politique alimentaire mondiale ou de l'agriculture afin d'éviter un tel scénario catastrophe ?




Béatrice de Reynal :

Dans la première moitié du siècle, l'explosion démographique impliquera des besoins en énergie accrus. Ensuite, c'est l'augmentation de l'indice de masse corporelle qui va être la cause de l'augmentation des besoins, lorsque le changement démographique commencera à se stabiliser. Or, pour ne pas engendrer de risques de surpoids et d'obésité comme l'ont connu les pays riches dès la moitié du XXe siècle, il faudra donner accès à une nourriture équilibrée à tous et éviter les aliments les plus denses.


Bien sûr, une préoccupation plus grande concerne les besoins futurs de production et de distribution alimentaires nécessaires pour assurer la sécurité alimentaire pour tous. Les besoins énergétiques affectent la demande alimentaire, ainsi que la part considérable des denrées alimentaires perdue ou gaspillée. Faut il prévoir de produire la part qui sera gaspillée ? L'ampleur de cette question dépendra fortement de l'organisation de la chaîne d'approvisionnement et des modes de consommation.


Deuxièmement, la consommation de viande et de produits laitiers agit comme un multiplicateur sur la demande de produits agricoles, car la production de ces produits a besoin d'énergie supplémentaire sous forme d'aliments pour animaux. Mais de nombreux pays ont déjà atteint des niveaux de consommation de viande assez élevés, et commencent à réduire cette consommation alors que les produits émergents vont demander de très grosses quantités de produits animaux.


Troisièmement, la sécurité alimentaire pour tous va impliquer des quantités de nourriture importantes pour garantir une faim zéro.


Si l'objectif est d'assurer la capacité de chaque personne à prendre des décisions libres sur son alimentation, ces estimations pourraient être un point de référence utile.


N'oublions pas que la faim est le plus souvent provoquée par les hommes : les conflits et le refus d'accès aux populations à des zones où ils pourraient vivrait survivre, mais aussi la spéculation sur les denrées de première nécessité.


Une meilleure protection des réseaux de distribution de nourriture est nécessaire.


Sur la question d'avoir une planète saturée d'occupants, la réponse est dans les travaux des économistes de l'INRA qui ont démontré que la planète peut et sait nourrir tous les hommes, à 3 conditions : que la surface utile agricole soit cultivée de denrées alimentaires destinées essentiellement et directement aux hommes, un peu pour les animaux qui nourrissent les hommes, et peu aux biocarburants. Ensuite, que le gâchis alimentaire soit mieux maîtrisé, notamment avec une meilleure gestion des produits des récoltes et une meilleure gestion des denrées stockées. Enfin, en laissant libre circulation des nourritures aux populations défavorisées.


Pour une justice « zéro faim » et « sans risque » , il est évident qu'une légère surproduction sera toujours nécessaire. mais aujourd'hui, l'injustice alimentaire mondiale n'est plus acceptable.



Catherine Grangeard :

Creusons l'étude présentée : 60% de l'augmentation des calories serait le résultat de l'évolution démographique dans le monde. Aussi, si rien ne vient freiner une telle augmentation de population, les problèmes seront probables. Mais, est-il imaginable que d'ici la fin du siècle, les crises écologiques ne viennent pas modifier ces prévisions sur le papier ? Sur ce point l'étude est silencieuse. Elle fait comme si de rien était.... On ne peut ignorer que le réchauffement climatique entraînera une redistribution des cartes. Ce qui cyniquement veut dire que cette augmentation de 4 milliards d'individus sur la planète pourrait ne jamais se produire...


8% de l'augmentation des calories par rapport aux niveaux de 2010 proviendrait d'une augmentation prévue de la taille et du poids de la population mondiale. Cette proportion est peu conséquente. Elle pourra donc se gérer bien plus facilement. « En ce qui concerne la nourriture qui équivaudrait à une augmentation de 253 calories dans le régime alimentaire quotidien d'une personne, le Dr Depenbusch a déclaré que deux grandes bananes supplémentaires ou une portion de frites seraient au menu. »


Ces experts ont averti qu'une demande croissante de nourriture entraînerait une augmentation des prix des denrées alimentaires. Alors que les pays riches pourraient absorber ces augmentations, les pays pauvres non. Ce qui entraînerait davantage de malnutrition. Un échec de la politique alimentaire mondiale pour répondre à cette augmentation de la demande de plus d'énergie pourrait exacerber les inégalités alimentaires et économiques.


J'ajoute enfin que s'il y a malnutrition, alors la démographie ne va plus croître autant.


Et si à l'inverse la population s'enrichit, elle a moins d'enfants que quand elle est pauvre.


Ces paramètres sont prouvés.


Les catastrophes écologiques sont accompagnées de migrations, de guerres, donc de morts. Ce qui réduit une progression linéaire de la population. Les inégalités menant aux famines amènent aussi des crises politiques sanglantes.


Nous pouvons donc conclure qu'il n'est pas certain que 11 milliards de Terriens soit une prévision raisonnable.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007544.xml</field>
    <field name="pdate">2019-12-26T00:00:00Z</field>
    <field name="pubname">Atlantico (site web)</field>
    <field name="pubname_short">Atlantico (site web)</field>
    <field name="pubname_long">Atlantico (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
