<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Comment défendre la planète contre un astre tueur ?</field>
    <field name="author">Sylvie Rouat</field>
    <field name="id">eupr_006759.xml</field>
    <field name="description"><![CDATA[


Le 15 avril 2018, un astéroïde de la taille d’un terrain de football a frôlé la Terre à seulement 192.000 kilomètres, soit la moitié de la distance Terre-Lune. Problème : l'astéroïde (2018) GE3, en provenance de la ceinture d’astéroïdes, n’a été repéré qu’in extremis, moins de 24h avant son passage par un observatoire de la Nasa dans l'Arizona. Si le pire a été évité, cet événement démontre que nous sommes susceptibles d’être frappés à tout moment par un astéroïde surprise. Cela a été le cas le 15 février 2013, lorsqu’un météore d’environ 17 à 20 mètres a traversé, à la vitesse de 66.000 km/h, le ciel au-dessus de la ville de Tcheliabinsk, en Russie. Son explosion à quelque 30 km d’altitude a engendré une onde de choc "d’une puissance équivalent à 30 bombes d’Hiroshima", souligne Rolf Densing, directeur des opérations à l’ESOC, le centre de contrôle de l’ESA à Darmstadt (Allemagne). Il s’exprimait à l’occasion de l’ouverture dans ce même centre de la conférence sur la détection des NEO (Near-Earth Objects, soit les comètes et les astéroïdes qui s’approchent de la Terre) et des débris spatiaux. Cela a provoqué d’innombrables dégâts matériels et blessé près d’un millier de personnes. En 1908, le bolide de 40 mètres qui explosa à plus de 5 km d’altitude au-dessus de Toungouska, en Sibérie, a déployé autant d’énergie que 500 bombes d’Hiroshima !


Certes, les statistiques indiquent qu’un tel événement ne se reproduit que tous les 300 ans, contre 5 ans pour les objets de 10 mètres. Mais la réalité n’obéit pas toujours aux règles de la probabilité. D’autant qu’à ce jour "on estime qu’on n’a détecté que 1% des astéroïdes d’une taille allant de 15 à 500 mètres", souligne Nicolas Bobrinsky, chef du programme "Space Situationnal Awareness" (SSA ou surveillance spatiale) à l’ESA. Soit 99% de chances qu’un superbolide déboule demain sans prévenir. Aussi, des chercheurs de l’Agence spatiale européenne (ESA) entendent-ils mettre ce danger planétaire sur le devant de la scène, lors de la conférence ministérielle "Space19+", qui se tiendra en novembre 2019 à Séville (Espagne). Les ministres des pays membres de l’agence se réuniront pour lancer les futures missions et voies de développement de l’Europe spatiale.


Orbites des 1400 astéroïdes potentiellement dangereux pour la Terre ©Nasa.


Déjà, un accord a été signé entre l’ESA et l’Agence spatiale italienne (ASI) pour la construction d’un nouveau télescope automatique "œil de mouche" (Fly-Eye) à 1800 mètres d’altitude sur le mont Mufara, en Sicile. "Pour détecter suffisamment tôt les astéroïdes à l’approche, il faut un grand champ de vision, de l’ordre de 6,7° sur 6,7°", indique Rüdiger Jehn chef du bureau de défense planétaire de l’ESA. Doté d’un miroir d’un mètre de diamètre, ce nouveau télescope permettra de surveiller la voûte céleste de l’hémisphère nord en 48h dans 16 canaux optiques. "Un accord a été passé avec l’ESO [European South Observatory ou observatoire austral européen] pour en construire une deuxième, au Chili, afin d’assurer la couverture du ciel depuis l’hémisphère sud, signale Nicolas Bobrinsky. Un troisième pourrait être construit, à plus long terme et si nous avons les fonds nécessaires, en

Australie

".



Comment modifier la trajectoire d'un astéroïde



Si ce dispositif permettait de détecter un astre tueur, que pourrions-nous faire, hormis évacuer la région menacée en catastrophe ? Pour tenter de répondre à cette question, la Nasa a mis sur pied la mission Dart (Double Asteroid Redirection Test), qui doit être lancée en 2021. Il s’agit de faire la démonstration qu’un impact cinétique est susceptible de modifier la trajectoire d'un astéroïde. En l’occurrence, l’objectif est d’atteindre en octobre 2022 le système binaire Didymos, comparé d’un corps principal de 780 mètres de diamètre autour duquel orbite sa lune de 160 mètres (la taille de la Grande pyramide de Gizeh), surnommée Didymoon, une taille plus proche de celle des objets qui constituent une menace planétaire. Cet impact cinétique sera provoqué par la sonde elle-même qui entrera en collision avec Didymoon à la vitesse de 6km par seconde, ce qui devrait diminuer sa vitesse orbitale autour du corps principal de moins de 1%, suffisamment cependant pour être mesuré par les grands télescopes terrestres. A cette date, en effet, le couple d’astéroïdes sera au plus proche de nous, à seulement deux fois la distance Terre-Soleil (11 millions de km).


Dart, la mission de déviation d'un astéroïde. ©Nasa


Initialement, la mission européenne AIM (Asteroid Impact Mission) devait l’accompagner, afin de filmer et enregistrer les paramètres de l’impact en direct. Las, lors du conseil des ministres de l’ESA à Lucerne, en 2016, AIM a été recalée. Mais une enveloppe budgétaire a été maintenue pour continuer le développement d’une mission. C’est ainsi qu’est née Hera (du nom de la déesse grecque du mariage), une mission bien moins chère qu’AIM : 290 millions d’euros versus 450 millions pour cette dernière. Prévue pour être lancée en 2024, elle a pour objectif l’étude, en 2026, du cratère créé par l’impact de Dart. "En 2017 a été menée une grande campagne d’observation du système Didymos, indique Ian Carnelli, en charge du programme de préparation des futures missions spatiales à l’ESA. Cela a permis de bien caractériser ces corps avant l’impact. À ce jour, la Didymoon n’oscille pas. Si nous mesurons une oscillation, cela ne pourra être dû qu’à l’impact avec Dart."



Hera : une mission pour analyser le cratère d'impact créé par Dart



L’analyse du cratère – forme, température, structure, etc. – permettra d’évaluer la masse de matériaux soulevés par l’impact, sachant que 90% d’entre eux s’échappent dans l’espace. "Ces éjectas agissent comme une deuxième poussée. Il faut en tenir compte pour calculer la quantité d’énergie que vous devez développer pour obtenir la déviation voulue", souligne Ian Carnelli. Tous ces paramètres permettront ensuite de modéliser l’événement et d’évaluer son efficacité. C’est une première étape nécessaire pour forger des scénarios précis, sachant que cette expérience n’est pas reproductible en laboratoire. La forme de l’astéroïde, sa composition, sa porosité, sa vitesse doivent aussi être pris en compte pour définir un jour, si besoin est, une mission de défense planétaire. "Mais le paramètre clé, c’est le temps, affirme Rüdiger Jehn. Nous devons déterminer à quelle distance de la collision il nous faut infléchir la trajectoire de l’objet menaçant."


La mission Hera et ses deux cubesats ©Esa.


Si cette mission est sélectionnée en novembre 2019, ce sera la première mission scientifique vers un couple d’astéroïde, des systèmes très mal connus qui représentent environ 15% des astéroïdes recensés. Elle servira aussi de démonstrateur pour des techniques nouvelles, comme la navigation autonome autour d’un astéroïde. Une caméra enregistrant dans la partie visible et proche infrarouge du spectre, un radar laser pour mesurer l’altimétrie de surface, un imageur hyperspectral et un radar fourniront des informations sur la surface, la composition et la structure interne du plus petit astéroïde jamais visité.



Des cubesats lointains



La mission sera accompagnée de deux cubesats (nanosatellites composés de cubes de 10 cm de côté), premiers à atteindre cette lointaine distance de la Terre. L’intérêt de ces tout petits satellites à bas coût, c’est qu’on peut les faire s’aventurer dans des régions critiques d’observation, sans mettre en péril la mission principale. Ces deux satellites ont été baptisés Apex et Juventas. Le premier, Apex, est développé par un consortium suédois, finlandais, tchèque et allemand. Il analysera la lumière solaire réfléchie par le système Didymos afin notamment de comprendre les interactions entre les deux corps. Guidé par une caméra de navigation et un lidar (radar laser), Apex se posera en fin de mission sur Didymoon, pour effectuer des observations rapprochées de l'astéroïde.


Le second cubesat porte le nom de Juventas et est développé par deux sociétés danoise et roumaine. Il doit mesurer le champ de gravité ainsi que la structure interne de Didymoon. Comme Apex, il est censé se poser en fin de mission sur la petite lune astéroïdale, afin de prolonger l’enquête in situ. Hera fera de même en achevant sa mission à la surface du corps principal de Didymos, une opération suicide qui devrait permettre de recueillir les ultimes données de cet astéroïde. La mission Hera doit encore être soutenue par les ministres des pays membres de l’ESA, sachant que le pays le plus farouchement opposé à ce projet de sauvegarde de la planète était, en 2016, la France !


Cet article est paru dans Sciences et Avenir (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006759.xml</field>
    <field name="pdate">2019-01-23T00:00:00Z</field>
    <field name="pubname">Sciences et Avenir (site web)</field>
    <field name="pubname_short">Sciences et Avenir (site web)</field>
    <field name="pubname_long">Sciences et Avenir (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
