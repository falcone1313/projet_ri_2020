<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Xi Jinping, au nom du père et du Parti</field>
    <field name="author">François Bougon, Frédéric Lemaître</field>
    <field name="id">eupr_005194.xml</field>
    <field name="description"><![CDATA[


A 250 kilomètres à l'est de Pékin, l'atmosphère venteuse de

la station balnéaire de Beidaihe

est appréciée par les dirigeants communistes quand une chaleur éprouvante écrase la capitale. Après la période des luttes révolutionnaires au fin fond de la Chine du Loess - cette « terre jaune » où les habitants vivent dans des grottes aménagées -, les villas somptueuses édifiées par de riches marchands ou des diplomates, à partir de la fin du XIXe siècle, dans ce port de pêche du golfe de Bohai font figure de paradis, dans les années 1950, pour les nouveaux maîtres de la Chine et leurs familles.


Parmi ces privilégiés figurent Xi Zhongxun - l'un des premiers à avoir embrassé la cause de la révolution, près de quarante ans auparavant, à l'âge de 13 ans - et sa femme Qi Xin, employée à l'Institut d'études du marxisme-léninisme. Le couple a quatre enfants, deux filles et deux garçons. Le plus jeune des garçons se prénomme Yuanping, il est né en 1956. Son frère aîné s'appelle Jinping, il a vu le jour trois ans plus tôt, et connaîtra un destin national : un demi-siècle plus tard, il accédera aux plus hautes fonctions politiques du pays. Tous sont inscrits dans les meilleures écoles, réservées à l'élite rouge. Xi Jinping fréquente ainsi l'Ecole du 1er-Août, baptisée ainsi en référence à la date de fondation de l'Armée populaire de libération, où l'un de ses camarades de classe est Liu Yuan, le fils du président Liu Shaoqi.


Mais à Beidaihe, où l'océan change de couleur au gré des passages de bancs de poissons, c'est le premier des hiérarques chinois, Mao Zedong, qui règne en maître, et rares sont ceux qui peuvent l'approcher. Qu'il pleuve ou qu'il vente, l'homme qui a proclamé la fondation de la République populaire, le 1er octobre 1949, du balcon de la


porte de la Paix-Céleste (Tiananmen, en chinois), donnant sur la place du même nom, à Pékin, adore nager dans les eaux du golfe. Rien ne semble l'effrayer, ni les requins - un filet a été aménagé pour les tenir au loin - ni la houle provoquée par les orages d'été. Ses gardes du corps ont ancré au large une petite plate-forme où il peut s'allonger et se reposer si nécessaire.


Un jour où son médecin l'accompagne, le Grand Timonier lui lance, en désignant la mer agitée :

« Ne trouvez-vous pas amusant de lutter contre le vent et les vagues ? »


« C'est la première fois que je vis une telle expérience »

, répond le docteur Li Zhisui, qui cache bien son inquiétude et racontera cette anecdote, des années plus tard, dans ses Mémoires publiés à l'étranger (

La Vie privée du président Mao

, Plon, 1994).

« C'est comme chevaucher les grands vents et fendre les vagues puissantes sur une distance de 1 000 kilomètres »

, s'enthousiasme le Grand Timonier, en pleine lutte contre le courant.


Mao en eaux troubles


Cette fascination pour les éléments déchaînés ressort également dans un de ses poèmes, inspiré d'oeuvres anciennes et rédigé à l'été 1954. Il y est question de pluies torrentielles, de vagues montant jusqu'au ciel, de bateaux qu'on ne peut plus voir au loin. Mao y évoque aussi l'empereur du Wei, le terrible Cao Cao, entré dans l'histoire sous le surnom de Messager du chaos, et personnage de l'un des classiques de la littérature chinoise,

Les Trois Royaumes

... Comment ne pas imaginer que Mao lui-même se voie en nouveau Cao Cao, à la fois homme de lettres et souverain impitoyable ?


Celui qui a mené les communistes chinois à la victoire est de plus en plus considéré comme un dieu vivant. Un tel culte de la personnalité prend toute son ampleur en 1966, au déclenchement de la Révolution culturelle. Le Grand Timonier s'appuie alors sur une jeunesse fanatisée, les gardes rouges, pour se débarrasser de ses opposants au sein du Parti communiste chinois (PCC).


En 1962 pourtant, ces derniers n'ont pas encore dit leur dernier mot. Cette année-là, Mao est même affaibli par l'échec du Grand Bond en avant, lancé quatre ans auparavant. Grâce à ce mouvement d'industrialisation à grande échelle, il pensait avoir trouvé le moyen de dépasser un jour la Grande-Bretagne et les Etats-Unis, ainsi que cet allié russe dont il se méfie de plus en plus depuis la mort de Staline, en 1953. Il a été choqué par le rapport secret lu par le numéro un soviétique, Nikita Khrouchtchev, dans la nuit du 24 au 25 février 1956, à Moscou, au XXe Congrès du Parti communiste de l'Union soviétique, marquant le début de la déstalinisation. Pour Mao, l'URSS et la Yougoslavie sont sur la voie de la restauration du capitalisme. Et ses adversaires au sein du PCC sont autant de Khrouchtchev en puissance.


« Près de 20 000 personnes vont être victimes de cette affaire de roman et de la "clique antiParti de Xi Zhongxun" »


Zhang Zhigong, ancien secrétaire du père de Xi Jinping


Avec le Grand Bond en avant, il était persuadé de propulser la Chine au plus haut, d'effacer le siècle d'humiliations coloniales, coupable d'avoir plongé son pays dans le chaos et la misère. Mais c'est un échec. Dans les campagnes, où les exploitations familiales ont été regroupées dans des communes populaires - une appellation inspirée de la Commune de Paris de 1871 - et les récoltes réquisitionnées pour financer le développement industriel, il a provoqué une famine. Trente-six millions de Chinois sont morts entre 1959 et 1961, estime aujourd'hui le journaliste Yang Jisheng

dans son oeuvre monumentale, Stèles



(Le Seuil, 2012). Des cas de cannibalisme ont même été recensés.


De plus en plus critiqué, Mao le nageur intrépide évolue désormais en eaux troubles. Certains hiérarques du Parti réclament de laisser un peu plus d'oxygène aux paysans, d'abandonner ces cantines collectives imposées dans les communes populaires. Bref, de redonner davantage de marges de manoeuvre aux agriculteurs.


Le sujet est évoqué début 1962 dans une grande réunion de cadres locaux à Pékin, surnommée la Conférence des 7 000. Ce rassemblement de milliers de responsables du Parti communiste, venus de toutes les provinces, dure bien plus longtemps que prévu,


car tous avaient leur mot à dire en ces temps de confusion.

« Qu'ils s'épanchent pendant la journée, qu'ils aillent au théâtre le soir, nous ferons d'une pierre deux coups, tout le monde sera content »

, prévient Mao, jamais avare de bons mots. Mais le déferlement de critiques le surprend : ce ne sont que complaintes et griefs envers son Grand Bond en avant.


« On te laisse les clés de la maison »


Pour le Grand Timonier, qui attribue l'échec de son plan de développement aux catastrophes naturelles subies par le pays, c'est surtout le signe que le combat n'est jamais achevé. Même au sein du pouvoir, la lutte des classes se poursuit, et il y a urgence à débusquer ceux qui comptent reprendre le chemin du capitalisme. La confrontation est inévitable. Xi Zhongxun sera, bien malgré lui, l'une des premières victimes de la contre-offensive de Mao. Pourtant, rien ne laissait penser au père de Xi Jinping qu'il serait sacrifié, ni qu'il passerait du statut de haut dirigeant choyé à celui, infamant, de

« traître »

et de meneur d'une

« faction antiParti ».

N'a-t-il pas lié son destin à celui de Mao depuis que ce dernier lui a sauvé la vie alors qu'il était tout jeune ?


La scène remonte à 1935, pendant la conquête du pouvoir par les communistes. Xi Zhongxun et les principaux dirigeants du


soviet du nord du Shaanxi sont ligotés et jetés dans une prison de fortune. Dans les instances locales du Parti, l'époque est alors aux règlements de comptes entre factions. Des éléments « gauchistes » ont mené un putsch et accusé les anciens leaders de

« déviationnisme de droite ».



C'est à ce moment-là que Mao, entouré des survivants de l'Armée rouge, débarque dans le soviet, à l'issue de la Longue Marche. Il s'enquiert de ceux dont la réputation de combattants révolutionnaires a dépassé les frontières de la province. Quelle n'est pas sa surprise de les voir emprisonnés, et même sur le point d'être exécutés ! Il ordonne une enquête, qui les lave de tout soupçon. Leurs accusateurs deviennent accusés. Lorsque Mao rencontre le combattant Xi Zhongxun, tout juste sorti de prison, il s'exclame, tant sa surprise est grande :

« Qu'il est jeune ! »



Les années ont passé, et Mao le sauveur se mue en bourreau. En cet été 1962, les années de lutte sont de vieux souvenirs pour Xi Zhongxun, ce vétéran de la révolution transformé en gestionnaire de la Chine nouvelle. Au poste de vice-premier ministre, il est absorbé par sa tâche au sein du PCC et du gouvernement. Si tout le monde est parti se détendre dans la station balnéaire de Beidaihe, lui est resté à Pékin pour présider une réunion consacrée à l'industrie dans les petites villes. Avant de partir, le premier ministre, Zhou Enlai, lui a lancé, sur le ton de la plaisanterie :

« Camarade Zhongxun, on te laisse les clés de la maison. »



Une vengeance à l'oeuvre


Pourtant, c'est à Beidaihe que va se jouer son destin. Les dirigeants s'y réunissent afin de préparer la réunion plénière de la dixième session du Comité central, programmée un mois après. La teneur des discussions donne une idée des événements à venir. Alors qu'il était prévu de débattre de la production agricole, c'est la lutte des classes, chère à Mao, qui domine les échanges.


Mao lui-même s'inquiète des

« trois vents mauvais »

qui soufflent, selon lui, depuis la Conférence des 7 000 : la critique généralisée de la politique du Parti -

« voir tout en noir »,

selon ses mots -, la volonté de plusieurs dirigeants, tels Liu Shaoqi et Deng Xiaoping, de revenir sur la création des communes populaires, la remise en cause de certaines

« condamnations politiques »,

autrement dit les limogeages précédents, une allusion au destin de Gao Gang, un dirigeant victime d'une purge, en 1954. C'est d'ailleurs ce dernier point qui va provoquer la chute de Xi Zhongxun.




Le prétexte est un ouvrage historique intitulé

Liu Zhidan,

du nom d'un héros du soviet dans lequel ce même Xi Zhongxun a combattu. Ce martyr révolutionnaire est mort au combat en 1936, et le livre en question est l'oeuvre de sa belle-soeur, Li Jiantong. Publié par les Editions des ouvriers, il devait s'agir, à l'origine, d'un opus sérieux destiné à l'éducation des prolétaires. Mais les éditeurs ont décidé de le transformer en roman. Pour l'écrire, Li Jiantong s'est rendue sur place et a interrogé de nombreux témoins.


Quand elle soumet son texte à Xi Zhongxun, ce dernier n'est guère convaincu par l'opportunité d'une telle publication. Il se dit qu'il sera sans doute question dans ce livre de l'un de leurs camarades de l'époque, Gao Gang, et que cette simple évocation pourrait déplaire en haut lieu. On lui reprochait alors d'avoir voulu se débarrasser de Deng Xiaoping et de Zhou Enlai et de s'être convaincu tout seul que Mao approuvait cette manoeuvre. L'affaire est trop sensible pour qu'on laisse un livre évoquer sa mémoire. Mais l'auteure n'écoute pas Xi Zhongxun. Tout en prenant en compte certaines de ses remarques, elle poursuit son enquête.


Kang Sheng, l'homme chargé des basses oeuvres de Mao, entend bien profiter de l'occasion pour se venger de Xi Zhongxun. Il ne faut pas se fier à l'allure de lettré que lui donnent sa petite moustache et ses lunettes rondes : Kang Sheng, passé par Moscou dans les années 1930 pour se former aux méthodes de répression stalinienne, est impitoyable et cruel.


Au moment de la réforme agraire, Xi Zhongxun s'était opposé à lui en raison des méthodes draconiennes qu'il avait appliquées dans sa province natale du Shandong, lançant une campagne d'élimination des propriétaires terriens. A la lecture de

Liu Zhidan

, Kang Sheng se dit qu'il tient là sa revanche. Il persuade Mao que l'ouvrage est une tentative pour réhabiliter Gao Gang, l'exclu de 1954, et de s'en prendre à lui, le Grand Timonier...


L'humiliation publique


Dès lors, le sort de Xi Zhongxun est scellé. Une commission d'enquête est mise en place sous l'égide de Kang Sheng. Le coup est rude. Comment est-ce possible ? Mao peut-il l'abandonner ainsi ? Ne l'avait-il pas convoqué, à l'été 1957, à Zhongnanhai, à l'ouest de la Cité interdite, là où vivent et travaillent les plus hauts dirigeants communistes ? Mao se trouvait alors dans la piscine où il aimait se baigner, Xi Zhongxun à genoux pour discuter avec lui. A son retour, celui-ci en avait parlé à sa famille et à son entourage, il avait dit que le président Mao lui avait prodigué des encouragements et l'avait félicité pour son travail...


Cinq ans après cette entrevue au bord de l'eau, Xi Zhongxun, d'habitude si enjoué, si ouvert aux autres, s'est réfugié dans le silence. Ses enfants s'inquiètent pour lui. En rentrant de l'école, ils le trouvent seul, assis dans une pièce sans lumière.

« N'es-tu pas allé à Zhongnanhai ? »

, lui demandent-ils, évoquant cette rencontre comme un talisman. Leur père ne répond plus. Les Grecs antiques avaient inventé l'ostracisme, le bannissement d'un de leurs citoyens. C'est ce qui attend Xi Zhongxun, dont la mise à l'écart politique se double d'un éloignement géographique du coeur du pouvoir vers une province lointaine, le Henan. A Luoyang, le vice-premier ministre n'est plus que vice-directeur d'une usine de machines-outils pour l'extraction minière. La chute est sévère.


Des dizaines de milliers de cadres des cinq provinces du nord-ouest, là où Xi Zhongxun s'est battu aux premiers temps de la révolution, vont, eux aussi, être victimes de l'enquête ouverte par Kang Sheng. Pis : celle-ci est relancée au début de la Révolution culturelle, en 1966. Xi Zhongxun est alors envoyé manu militari à Xi'an, capitale du Shaanxi, et remis aux gardes rouges, qui le feront parader dans la ville avant de l'humilier lors d'un meeting public. Des photos sont prises pour être envoyées à Pékin. On le voit, tête courbée, le corps cassé en deux, le dos droit, les bras écartés, le tout sous les insultes de la foule.


Certains de ses proches ou de ses connaissances sont également inquiétés. La chasse est ouverte. Et personne ne saurait y échapper, pas même le patron du Grand Restaurant de Xi'an, à Pékin, où Xi Zhongxun avait ses habitudes pour retrouver les goûts de sa province natale. Mao en personne était venu savourer avec lui le

paomo

, une soupe de mouton dans laquelle on émiette de petits pains cuits à la vapeur. En décembre 1962, après la chute de Xi, le propriétaire de l'établissement est arrêté, accusé d'avoir abrité


un

« organe d'espionnage de Xi Zhongxun »,

et envoyé en camp de travail.


Plus rien n'arrête Kang Sheng. Il voit des complots partout, avec des conséquences effroyables.

« Près de 20 000 personnes vont être victimes de cette affaire de roman et de la "clique antiParti de Xi Zhongxun"

,


écrit son ancien secrétaire, Zhang Zhigong, dans un livre de souvenirs publié en 2013 (non traduit en français).

Près de 200 ont été tuées, ont sombré dans la folie ou sont devenues gravement handicapées. En fait, nous sommes peut-être loin de la réalité des chiffres pour les victimes. »



La réhabilitation à Canton


Xi Zhongxun ne devra son salut qu'au soutien indéfectible de Zhou Enlai, le premier ministre, qui dépêche un avion à Xi'an pour le ramener à Pékin. Là, il demande aux militaires de le protéger avant de le renvoyer dans la province du Henan. En 1972, c'est encore Zhou qui autorise son épouse, Qi Xin, accompagnée de ses enfants, dont Xi Jinping, à rendre visite à son mari, après une séparation de sept ans.


Il faudra toutefois attendre 1979 pour que Xi Zhongxun soit totalement réhabilité. Alors même que son cas n'a pas été réglé, il est nommé dans le sud de la Chine, à Canton, pour s'occuper de la « porte sud de la Chine ». Dans cette région située en face de Hongkong et de Macao, l'ancien proscrit lance la politique de réforme. Il se sait soutenu, à Pékin, par le nouvel homme fort du pays, Deng Xiaoping.


« Xi Jinping n'a pas été le fils de son père. Il a été élevé par le régime maoïste, par Mao »


Michel Bonnin, sinologue


A peine arrivé, il doit affronter un problème urgent : une vague d'émigration illégale vers Hongkong et Macao. Il se rend dans des villages désertés par les jeunes. Au loin, il voit les lumières de Hongkong, si attirantes pour une jeunesse désemparée. Sa conviction est faite : ce n'est qu'en développant la région que les autorités pourront dissuader la population de se jeter à l'eau pour rejoindre la colonie britannique. Réformateur et pragmatique, il pose les bases de la future zone économique spéciale de Shenzhen.


Il s'occupe tout particulièrement de panser les cicatrices de la Révolution culturelle. Lui qui a souffert dans sa chair des règlements de comptes politiques consacre désormais du temps aux dossiers de réhabilitation des personnes injustement mises en cause. Après les avoir fait sortir de prison, il passe ainsi des heures et des heures avec les trois jeunes gens qui avaient fait sensation en affichant, dans une grande rue de Canton, un dazibao (une affiche) sous le nom collectif de Li Yizhe. Ils attaquaient l'absence de démocratie et de respect du droit.


De lui, on retient aussi l'image d'un homme bienveillant envers les minorités, en particulier les Tibétains, dont la Chine a annexé le territoire en 1951.

« Xi Zhongxun a eu des contacts étroits avec le dalaï-lama dans les années 1950, en particulier en 1954 lorsque le dalaï-lama s'est rendu à Pékin »

, précise Pierre-Antoine Donnet, journaliste et auteur de

Tibet mort ou vif

(Gallimard, 1990). Le chef des Tibétains, alors âgé d'une vingtaine d'années, lui a d'ailleurs offert une montre. Dans les années 1980, Xi Zhongxun entretient également de bonnes relations avec le panchen-lama, deuxième plus haute figure du bouddhisme tibétain.


Le fils du régime


Malgré les années de disgrâce, Xi Zhongxun est resté fidèle à Mao.

« Xi Zhongxun a donné sa vie entière à Mao - lui et les autres n'étaient rien sans Mao, à l'époque de la République de Chine. Ils lui sont immensément redevables de leur avoir donné le régime - donc même quand ils étaient punis, ils n'en gardaient pas moins la foi,

juge Feng Chongyi, chercheur spécialiste du pouvoir chinois établi en

Australie

.

Xi Zhongxun n'a jamais eu les mêmes idées que Hu Yaobang ni Zhao Ziyang

[deux dirigeants réformistes]

. Ses années à la tête du Guangdong



[province du sud]

, les réformes qu'il y mène, c'est pour améliorer le niveau de vie, car il voyait bien que c'est ce dont avaient besoin les gens, c'est une réponse populiste. »



Depuis l'arrivée de son fils Xi Jinping au pouvoir, fin 2012, la mémoire de Xi Zhongxun est d'autant plus cultivée. Bien que le lieu de sa naissance, Fuping, ville moyenne de la province du Shaanxi, n'ait pas grand charme - rien à voir avec Xi'an et sa fameuse armée de terre cuite, un peu plus au sud, ni même avec Yan'an au nord, terre mythique de l'épopée maoïste -, des dizaines de cars de touristes y affluent chaque jour. Tous se dirigent vers le plus bel endroit de la ville : un parc de plusieurs hectares aux arbres parfaitement entretenus et aux allées revêtues de brique pilée rouge. Un luxe incongru au regard de la modestie de l'environnement.


Ce parc abrite depuis 2005 un mémorial dédié à Xi Zhongxun. Depuis que son fils est aux plus hautes fonctions, le lieu a été décrété

« base nationale de démonstration de culture patriotique ».

D'où ces cars de retraités, de membres du Parti ou de groupes scolaires. L'homme, même mort (en 2002), ne se laisse pas approcher facilement. Pour avoir le droit de faire, sans marquer d'arrêt, le tour de sa sculpture de granit blanc, encore faut-il avoir au préalable passé un portique de sécurité, montré ses papiers d'identité, laissé le moindre sac au vestiaire, accepté d'être filmé par les caméras de sécurité et ne pas avoir fait trop mauvaise impression à la dizaine d'hommes en noir dotés d'une oreillette qui l'entourent en permanence.


La magnificence de ce mausolée, tout comme la présence, de l'autre côté de la route, d'un musée en son honneur, ne s'explique que par la carrière du fils. Si le musée expose son ascension auprès de Mao, puis sa proximité avec Deng Xiaoping à partir de 1978, la période de la Révolution culturelle (1966-1976) n'a droit qu'à la portion congrue. Sur les deux étages du musée, ses seize années de purgatoire se résument à trois photos et à un petit texte expliquant combien son séjour auprès des travailleurs était

« nécessaire »

et enrichissant.


L'absence totale de critique à l'égard de la Révolution culturelle n'est pas l'aspect le moins saisissant de la visite. A Fuping, la réécriture de l'histoire est bien en marche. D'ailleurs, la boutique de souvenirs est presque uniquement consacrée à son fils et à Mao.

« Xi Jinping n'a pas été le fils de son père

, juge le sinologue Michel Bonnin.

Il a été élevé par le régime maoïste, par Mao. »

Pour le comprendre, il faut se pencher sur ce qu'a été la vie du jeune Xi, à la campagne, durant la Révolution culturelle.


Cet article est paru dans Le Monde (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005194.xml</field>
    <field name="pdate">2019-07-29T00:00:00Z</field>
    <field name="pubname">Le Monde (site web)</field>
    <field name="pubname_short">Le Monde (site web)</field>
    <field name="pubname_long">Le Monde (site web)</field>
    <field name="other_source">La Matinale du Monde</field>
    <field name="source_date">30 juillet 2019</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
