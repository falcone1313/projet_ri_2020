<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Davos face à la planète qui brûle</field>
    <field name="author">STÉPHANE BUSSARD</field>
    <field name="id">eupr_007003.xml</field>
    <field name="description"><![CDATA[


6322


, DAVOS @StephaneBussard


La planète brûle. Au coeur de l'hiver davosien, le changement climatique est un thème incontournable. Parmi l'élite économique mondiale, on parle d'investissements, de solutions technologiques. Mais les scientifiques et les organisations non gouvernementales de défense de l'environnement ne se contentent plus d'un débat qu'ils jugent certes nécessaire, mais largement insuffisant face à la réalité climatique.


La présence de Greta Thunberg dans la station grisonne, venue mardi en train depuis la Suède pour exprimer l'angoisse climatique d'une jeunesse qui voit les gouvernements englués dans des querelles absurdes les empêchant d'agir avec courage, est révélatrice du changement de paradigme qui est sur le point de s'opérer: sans une mobilisation de la société dans son ensemble, la cause climatique n'avancera pas assez vite. Le secrétaire général de l'ONU, Antonio Guterres, l'a rappelé jeudi à Davos: « Nous sommes en train de perdre la bataille. » Al Gore, ex-vice-président américain et protagoniste du film Une vérité qui dérange, lui aussi présent à Davos, dit la même chose, mais avec des comparaisons plus robustes: « Nous sommes dans un état d'urgence total. La pollution provoquée par l'homme et causant le réchauffement climatique est l'équivalent de l'énergie dégagée par 500 000 bombes de type Hiroshima explosant chaque jour. »


Malgré l'Accord de Paris sur le climat de 2015, qui entrera en vigueur en 2020, la planète fait fausse route. Les concentrations de CO2 dans l'atmosphère n'ont jamais été aussi élevées depuis au moins 3,5 millions d'années. A Davos, Jonas Rockström, directeur du Potsdam Institute for Climate Impact Research l'admet lui-même: la rapidité du phénomène de réchauffement est plus importante que ne l'ont imaginé les scientifiques. L'urgence d'agir devient plus urgente encore.



Le canari dans la mine



« Nous avons douze ans pour réduire de 50% les émissions de CO2. Cela veut dire que les deux prochaines années seront déjà vitales pour commencer à infléchir la courbe, relèvet-il. L'Arctique est le canari dans la mine. Il se réchauffe deux fois plus vite que la planète. Si aucun changement radical n'intervient, il n'y aura plus de glace dans quelques décennies. Or il fait partie, avec notamment l'Antarctique ou la chaîne de l'Himalaya, du système de refroidissement de la planète. Nous enregistrons aujourd'hui les plus hautes températures depuis la dernière ère glaciaire. En Afrique, un record a été récemment été établi: 52,1°C. L'

Australie

n'en est pas loin non plus. » Les cinq années les plus chaudes ont été enregistrées à partir de 2010 et 2018 a été la quatrième année la plus chaude.


Interrogé par le prince William à Davos, le légendaire naturaliste et commentateur de documentaires sur la nature David Attenborough met en garde l'humanité contre sa capacité de détruire la planète. « Le problème, avance-t-il, c'est qu'une majorité de l'humanité est déconnectée des richesses de la nature. » Jonas Rockström met la présente situation en perspective. Nous avons quitté l'ère de l'holocène (époque géologique de plus de 10 000 ans) pour entrer dans celle de l'anthropocène (où l'impact de l'être humain sur la biosphère est très marqué). » S'il y a urgence, à en croire le dernier rapport du Groupe d'experts intergouvernemental sur l'évolution du climat, nous avons une fenêtre de douze ans pour infléchir les courbes, à savoir une réduction de 50% des émissions de dioxyde de carbone d'ici à 2030 et de 100% d'ici à 2050. On est pour l'heure loin, très loin du compte.


Sachant qu'une hausse ne serait-ce que de 1°C perturbe déjà les jetstreams, ces courants soufflant d'ouest en est en haute altitude, l'inaction serait dévastatrice; 2°C de hausse en moyenne « équivaudraient à une hausse de 5°C en Arctique. Ce serait un point de non-retour pour la banquise », précise Jonas Rockström. Les jet-streams sont déjà chamboulés. Le fameux vortex polaire ne se limite plus à la zone arctique. Il descend parfois beaucoup plus au sud, comme ce fut le cas en 2013 au nord des Etats-Unis, qui connurent un froid quasi polaire.


La peur, aujourd'hui, c'est de voir les quelque 300 éléments du système climatique (la forêt amazonienne, les zones humides, les récifs coralliens, les glaciers, les jet-streams, les courants marins, etc.) de la planète basculer et s'emballer. C'est la notion de « hothouse Earth », de serre chaude, un phénomène par lequel la Terre deviendrait son propre ennemi par des dérèglements du climat tels qu'ils s'amplifieraient mutuellement. La déforestation intervient bien sûr dans l'équation. Al Gore met en garde: 20% de la forêt amazonienne a déjà été détruite et l'équivalent d'un terrain de football disparaît chaque seconde.


Si, contrairement à l'objectif de l'Accord de Paris, la hausse de la température du globe devait être de 3, voire de 4°C, la hausse du niveau des mers pourrait être de 15 à 20 mètres. La Floride serait en bonne partie sous l'eau et le Bangladesh, avec les quelque 100 millions de personnes qui habitent la région, disparaîtrait de la carte avec des répercussions géopolitiques considérables. Le tableau est sombre, mais il serait criminel, selon Al Gore, de dire qu'il est trop tard pour agir. S'il importe de réduire le nombre de centrales à charbon et l'exploitation des gaz de schiste qui émet beaucoup de méthane, il est aussi incontournable d'agir sur l'agriculture.


La déforestation de l'Amazonie réduit non seulement sa capacité de puits de carbone, mais favorise aussi des activités qui produisent beaucoup de CO2, dont l'élevage de bétail. Les scientifiques travaillent désormais activement sur des régimes alimentaires durables qui encouragent la consommation de poisson, de haricots, de fruits au détriment de la viande. Sans cela, explique la biologiste marine de la National Geographic Society Heather Koldewey, « nous courons au désastre absolu » en termes de sécurité alimentaire et d'environnement.


Sur le front des énergies renouvelables, constate Jonas Rockström, « les capacités en matière d'énergie solaire doublent tous les quatre ans. Si on continue ainsi, 50% de l'électricité dans le monde pourra être produite avec le solaire et l'éolien en 2030 déjà! » Le stockage de l'énergie progresse aussi par le biais de l'hydrogène et les infrastructures hydroélectriques ainsi que par la très forte innovation dans le secteur des batteries. Directrice exécutive de Greenpeace International, Jennifer Morgan le confie au Temps: « Il est temps pour chacun de faire son introspection pour puiser les ressources pour agir. Car ce n'est pas à Davos que le changement de paradigme va intervenir. »


52,1°C Record de température récemment enregistré en Afrique.


MAIS ENCOREPression populaire Via Twitter, l'agence Reuters sonde les internautes depuis le début de la semaine. Ils étaient plus de 400 000 à avoir répondu, jeudi. Pour 62% d'entre eux, la question climatique doit être le sujet numéro un du WEF 2019. Loin derrière suivent le commerce (20%), l'accès au logement (12%) et les inégalités de genre (6%). S. P.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007003.xml</field>
    <field name="pdate">2019-01-25T00:00:00Z</field>
    <field name="pubname">Le Temps</field>
    <field name="pubname_short">Le Temps</field>
    <field name="pubname_long">Le Temps</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Temps fort</field>
  </doc>
</add>
