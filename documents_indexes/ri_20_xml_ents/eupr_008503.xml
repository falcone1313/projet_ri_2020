<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Toyota mise à fond sur l’hydrogène, malgré les énormes écueils</field>
    <field name="author">Alain-Gabriel Verdevoye</field>
    <field name="id">eupr_008503.xml</field>
    <field name="description"><![CDATA[


Le véhicule électrique, ce n’est pas si pressé. Après plus de treize millions de véhicules hybrides essence-électriques (non rechargeables) depuis 1997 (dont 2,1 millions en Europe depuis 2000), Toyota ne devrait pas commercialiser de modèle 100% électrique avant… l’hiver 2020-2021. Et encore sa mini-voiture zéro émission ultra-compacte, donnée pour une autonomie de cent kilomètres seulement, n’est même pas prévue à l’heure actuelle en Europe. La firme nippone se montre sceptique sur la rentabilité des électriques et leur pertinence par rapport aux besoins des consommateurs. Toyota préfère toujours miser sur l’hybride dont il fut le pionnier, mais aussi… sur la voiture à hydrogène, présentée comme l’étape du futur. Avec une montée en cadence prévue à partir de 2025. C'est, selon Toyota, la solution d'après l'électrique.


Nouvelle Mirai II élégante


Présentée au salon de Tokyo, qui ouvre ce mercredi 23 octobre ses portes aux professionnels, la nouvelle limousine Mirai II à pile à combustible adopte pour l’occasion un style élégant, qui la fait ressembler à une luxueuse Toyota Crown, la grande berline traditionnelle haut de gamme dans l'archipel. Plus dynamique, raffinée, rabaissée, allongée jusqu’à près de 5 mètres (4,97 mètres exactement, 1,88 de large, 1,47 de haut), elle a abandonné le style torturé pseudo-avant-gardiste de l’ancienne. Preuve que, désormais, Toyota veut la vendre comme une vraie voiture et en faire un modèle séduisant ainsi que raffiné. Le constructeur insiste sur le fait qu’il s’agit pour l’instant d’un concept, mais la voiture semble fin prête pour la série. Elle sera d’ailleurs commercialisée "fin 2020", indique Yoshikazu Tanaka, ingénieur en chef du modèle. La précédente Mirai datant de fin 2014, qui fut la première voiture à hydrogène lancée en production dans le monde (avec la Honda Clarity et la Hyundai coréenne iX35) "était vendue à raison de 700 unités par an au début, puis nous sommes montés à 3.000 annuellement, soit 10.000 vendues depuis fin 2014", s’enorgueillit Yoshikazu Tanaka. 92 exemplaires ont même trouvé preneur en France. Clients: Air liquide, Engie, l'équipementier Plastic Omnium, le CEA (Commissariat à l'énergie atomique), la STEP pour ses taxis Hype…


10.000 ventes en cinq ans, c’est à la fois dérisoire pour un constructeur qui écoule plus de dix millions de véhicules par an. Et beaucoup… pour un modèle aussi révolutionnaire et exclusif, vendu 80.000 euros environ (pour la première génération de Mirai). Chez Toyota, on se montre enthousiaste: "on recharge en trois minutes, la voiture ne dégage pas de CO2", insiste l’ingénieur en chef. Autre avantage et non des moindres: l’autonomie est la même que celle d’une bonne voiture thermique, palliant là un des défauts majeurs de l’électrique. Et Toyota affirme que la dernière Mirai II accroît encore son rayon d'action de 30% par rapport à la première mouture.


De nombreux problèmes à résoudre


Pourtant, tout reste encore à prouver. "La pile à combustible dure 150.000 kilomètres, pas plus. Le vrai coût de la voiture est quatre fois plus élevé que celui d’une voiture thermique. Et l’hydrogène est toujours produit de façon sale", explique un expert indépendant. Sa production se fait aujourd’hui encore très majoritairement à partir de ressources fossiles! La fabrication d'un kilo d'hydrogène génère dix kilos de CO2, selon le magazine spécialisé

L'Argus

. Pas terrible! Pour que l’hydrogène soit propre, il faut donc qu’il soit issu de sources renouvelables. Ca viendra… Mais à quel coût? Car l’hydrogène est déjà cher. Il se transporte en outre difficilement dans de bonnes conditions de sécurité. Et une station nécessite un million d’euros d’investissement, dix fois plus que pour une station d’essence. L’hydrogène "vert" sera de toutes manières toujours beaucoup plus cher pour l’automobiliste que l’électricité stockée dans une batterie, assure L’Argus. En France, l’hydrogène, ce n’est clairement pas, du coup, une priorité, comme on nous le confiait dernièrement à Bercy. Pas pour les voitures particulières en tous cas.


Il n’empêche. Toyota y voit un avenir au Japon -où les pouvoirs publics sont prêts à accompagner le développement-, en Amérique du nord. Des tests démarrent au Canada, en

Australie

, en Chine. Toyota prévoit d’ailleurs de fabriquer, dès 2020-2021, 30.000 voitures à pile à combustible par an avec la Mirai II et un autre modèle sous son label de luxe Lexus. Toyota n’est pas seul. Le coréen Hyundai y croit aussi avec son SUV Nexo, commercialisé il y a un an en Europe, à plus de 70.000 euros.


Débouchés sur les poids-lourds, bus...


Mais, le débouché de l’hybride, ce sera surtout les poids-lourds des flottes, alors que l’électricité ne se prête guère aux véhicules devant transporter une forte charge sur longues distances. Toyota l’annonce pour les camions, les bus, mais aussi les chariots élévateurs. Comme Hyundai, qui a déjà reçu une commande pour mille camions en Suisse. Pour sa part, Renault (actionnaire de Challenges) annonce ce mardi 22 octobre le lancement d’une fourgonnette Kangoo Z.E. Hydrogen fin 2019 et d’un fourgon Master Z.E. Hydrogen en 2020. En partenariat avec Symbio, filiale de Michelin et au cœur d'une co-entreprise en cours de création avec Faurecia. Mais il s’agit encore d’une petite pile à combustible. Et les deux véhicules seront surtout destinés à des expérimentations en grandeur nature.


Toyota teste actuellement au Japon un bus à hydrogène, le Sora, capable d’emmener 79 personnes avec un réservoir quatre fois plus gros que celui d’une voiture à pile à combustible. Une centaine d’exemplaires devrait même circuler pour les Jeux olympiques de Tokyo en 2020. Ce Sora a une autonomie de 200 kilomètres vu les conditions de roulage dans la capitale japonaise et se recharge en quinze minutes maximum, affirme Toyota, qui espère carrément remplacer les 35.000 bus qui circulent au Japon vers… 2030-2050. Une politique des petits pas.


Toyota reconnaît volontiers que le bus Sora coûtera… quatre à cinq fois plus qu'un bus diesel. Le différentiel sera comblé par le gouvernement japonais, précise-t-on laconiquement chez le constructeur, qui affirme avoir besoin d'aides d’Etat. Il espère certes s'en passer un jour, mais se garde d'en préciser l'horizon.


Pour promouvoir ses véhicules à hydrogène, Toyota ne compte pas que sur les Jeux olympiques de Tokyo l’an prochain, mais aussi sur ceux de Paris en 2024, puisqu'il fournira les véhicules officiels. Ceux-ci fonctionneront avec des motorisations hybrides, électriques… et à hydrogène. La firme espère bien négocier avec la mairie de Paris pour faire fonctionner les fameux bus à pile à combustible.


Cet article est paru dans Challenges (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008503.xml</field>
    <field name="pdate">2019-10-22T00:00:00Z</field>
    <field name="pubname">Challenges (site web)</field>
    <field name="pubname_short">Challenges (site web)</field>
    <field name="pubname_long">Challenges (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
