<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Les exilés ouïghours du monde entier craignent le bras long de l'Etat policier chinois</field>
    <field name="author">AFP</field>
    <field name="id">eupr_008556.xml</field>
    <field name="description"><![CDATA[


Les musulmans ouïghours qui ont échappé à la répression dans la région chinoise du Xinjiang vivent toujours dans la peur : être à l'étranger, avoir un passeport occidental, ne suffit pas à les protéger d'une campagne mondiale d'intimidation sans merci.


Textos et messages audio inquiétants, menaces explicites contre les proches vivant toujours au Xinjiang, le puissant appareil sécuritaire de l'Etat chinois a le bras long. Pour tenter de réduire les opposants au silence et recruter des informateurs, il touche des démocraties aussi lointaines que les Etats-Unis et la Nouvelle-Zélande.


La Chine est soupçonnée par des associations de défense des droits de l'homme d'avoir interné jusqu'à un million de personnes, des Ouïghours et d'autres membres de minorités surtout musulmanes, dans des camps de rééducation.


Ceux qui ont réussi à fuir et s'installer à l'étranger expliquent qu'ils ne sont pas pour autant en sécurité, se plaignant d'être harcelés à distance, ainsi que leur famille, jusqu'à les pousser au désespoir.


Guly Mahsut, 37 ans, qui a fui au Canada, dit avoir eu des envies de suicide et avoir été hospitalisée après avoir reçu des messages en rafales de la police du Xinjiang menaçant sa famille restée là-bas.


"Vous auriez dû être plus coopérative. Ne devenez pas la cause du malheur de vos proches et de votre famille à Toksun. Vous devriez être plus attentive à votre famille", peut-on lire sur un message d'un dénommé "Kaysar".


Elle pense avoir été ciblée car elle a dénoncé les autorités de Pékin sur internet et a aidé des Ouïghours à rechercher de l'aide à l'étranger.


Elle a reçu des messages des siens, dont sa soeur cadette, la suppliant d'arrêter ses activités politiques et de "coopérer" avec les autorités.


Une enquête de l'AFP avait récemment révélé que des Ouïghours établis en France étaient aussi soumis aux pressions des autorités chinoises.


- Numéros usurpés -


Cette fois, l'agence a interviewé une dizaine d'exilés sur quatre continents et a eu accès à de multiples messages imputés aux services de sécurité chinois.


Tout tend à dresser le tableau d'une campagne systémique de Pékin en vue d'infiltrer la diaspora dans le monde entier, de recruter des informateurs, de susciter la méfiance et d'étouffer les critiques du régime.


Shir Muhammad Hasan, 32 ans, a réussi à gagner l'

Australie

en 2017. Avec son statut de réfugié, il se croyait en sécurité. A peine un an après, les messages sinistres commencèrent à affluer.


"Je suppose que ta famille t'a déjà dit que je te cherchais ?", dit le premier message. D'autres textos suivent, lui demandant d'envoyer des détails sur sa vie avant d'exiger, sur un ton comminatoire, un rendez-vous "pour faire connaissance". "Je t'ai demandé de m'envoyer un bref résumé sur toi-même mais tu ne l'as pas fait", écrit l'auteur dans un dialecte ouïghour local, ponctué du mandarin parlé en Chine. "Il faut qu'on s'assoit et qu'on parle".


Ce bombardement a duré six mois puis s'est arrêté tout net. Le jeune homme est bouleversé, il ne sait pas si et quand ses tourmenteurs vont recommencer.


L'AFP n'a aucun moyen de vérifier l'identité des auteurs des messages. Ils ont été envoyés via des comptes WhatsApp cryptés liés à des numéros de téléphones portables hongkongais inactifs ou parfois à des numéros usurpés.


Interrogée sur l'enquête, la porte-parole du ministère chinois des Affaires étrangères, Hua Chunying, a déclaré que les témoignages des membres de la diaspora ouïghoure étaient "sans fondement" et le fait de "critiques professionnels" tentant de salir et de calomnier la Chine.


Les exilés font néanmoins des récits étonnamment proches d'un même modus operandi. Dans un premier temps, la famille au Xinjiang est contactée, puis ce sont les proches qui posent des questions et enfin, ils sont directement contactés par les services chinois sur messagerie sécurisée.


Un Ouïghour qui vit avec sa femme aux Etats-Unis déclare que ses proches au Xinjiang se sont vu demander "des informations sur mon école, mon statut, sur comment j'avais pu aller à l'étranger".


"Quand je leur ai demandé +Pourquoi vous avez besoin de ces informations ?+, ils ont répondu qu'ils devaient remplir un formulaire", explique-t-il.


- Le salaire des espions -


Les services de sécurité ont demandé à d'autres familles au Xinjiang les numéros de téléphone des exilés, donnant le coup d'envoi des campagnes de harcèlement.


Ces pratiques ont des effets dévastateurs sur de nombreux exilés paralysés par la peur de ce qui pourrait arriver à leurs proches restés en Chine s'ils ne se soumettaient pas.


Arslan Hidayat vit à Istanbul. Il n'a pas été directement ciblé mais des trolls nationalistes ont envahi son blog Facebook.


Il est né en

Australie

et veut parler au grand jour mais ses proches plus âgés, y compris sa belle-mère dont le mari est en détention, pensent que le fait de se taire limitera la colère des autorités.


Les efforts chinois en vue de créer des réseaux d'informateurs sont également source de frictions et de méfiance au sein des communautés ouïghoures à l'étranger.


Pendant le plus clair de la décennie passée, les étudiants ouïghours bénéficiaires de bourses à l'étranger se voyaient demander quantité d'informations sensibles. Certains croient qu'on leur demandait en fait d'être des espions.


"Au moment des demandes de bourses, les postulants doivent donner des informations détaillées sur leur famille en Chine, mais aussi leurs études, leur vie, leurs activités dans le pays d'accueil", raconte un doctorant qui vit aujourd'hui en

Australie

.


"Une condition pour obtenir la bourse c'est de maintenir un contact étroit avec l'ambassade de Chine et avec un contact dans le Bureau d'éducation du Xinjiang".


L'étudiant ajoute : "cela peut servir à recueillir des informations sur les demandeurs et leurs proches à l'étranger, ou pire, c'est le salaire des espions déguisé en bourse d'études".


- Larmes -


D'après James Leibold, un spécialiste des relations ethniques en Chine et professeur à l'Université La Trobe de Melbourne, l'intimidation est méthodique.


"La portée de l'Etat-parti chinois est aujourd'hui bien plus grande. Il viole d'une certaine manière la souveraineté de différents pays à travers le globe en s'ingérant dans la vie de citoyens de ces pays".


Pékin veut que les exilés "se taisent sur ces questions, qu'ils s'abstiennent de faire du lobbying auprès de la classe politique locale, de parler aux médias, de causer des problèmes aux ambassades et aux consulats de Chine", poursuit M. Leibold.


Les médias ont évoqué des campagnes similaires contre les Tibétains, les dissidents, les militants taïwanais, les membres de la secte interdite Falun Gong et les étudiants chinois à l'étranger.


Certains Ouïghours, même ceux dotés d'une nationalité étrangère ou d'un titre de séjour permanent, pensent qu'il n'y aucun moyen d'échapper à l'Etat policier chinois.


Ces cinq dernières années, la Thaïlande et l'Egypte ont interpellé des Ouïghours et les ont renvoyés en Chine. Mais le tableau n'est pas rose même dans des démocraties ouvertes comme la Nouvelle-Zélande et la Finlande.


Shawudun Abdughupur, 43 ans, a fui à Auckland après avoir assisté aux émeutes interethniques meurtrières de juillet 2009 au Xinjiang.


Il a beau avoir la nationalité néo-zélandaise, il n'aime pas parler publiquement, par peur pour lui-même mais aussi pour sa mère âgée de 78 ans. Il pense qu'elle est dans un camp.


"Je ne peux pas dire grand-chose", explique-t-il à l'AFP dans sa première interview filmée, luttant contre les larmes. "Je ne sais pas si le gouvernement néo-zélandais peut me protéger. Comment pourrait-il me protéger ?".


- "Serrer les rangs" -


Après avoir refusé de livrer des détails sur ses rencontres avec d'autres Ouïghours, il reçut ce message glaçant : "On peut te retrouver. On est en Nouvelle-Zélande".


Quand il s'en est ouvert à la police, celle-ci a traité l'incident comme n'importe quel message importun. Elle l'a renvoyé sur Netsafe, une association qui s'occupe de sécurité en ligne, laquelle l'a réorienté... vers la police.


La police néo-zélandaise, à l'image d'autres polices nationales, s'est refusée à tout commentaire pour des raisons de protection de la vie privée.


Halmurat Uyghur, 35 ans, qui vit au nord d'Helsinki, raconte avoir plusieurs fois signalé des messages menaçants à la police finlandaise. Peine perdue.


"Je ne me sens pas en sécurité. Qui sait ce qui va se passer ?", dit-il.


Un ancien responsable de la sécurité nationale américaine confirme que la question du comportement de la Chine à l'égard des "fugitifs" avait été soulevée avec Pékin "via les services de sécurité" ainsi qu'au "plus haut niveau".


Les autorités américaines actuelles ont pris acte des informations sur le harcèlement de Ouïghours aux Etats-Unis mais se sont refusées à les commenter publiquement.


Pour Ben Rhodes, conseiller à la sécurité sous la présidence de Barack Obama, les démocraties pourraient limiter les agissements chinois mais ont besoin pour cela de "resserrer les rangs".


Pékin "est habituellement intransigeant sur ce qu'il considère comme ses affaires intérieures", dit-il. "Pour faire vraiment face, il faudrait que les Etats-Unis réunissent d'autres pays afin de résister collectivement à la Chine".


-- Cette enquête a été réalisée avec des contributions supplémentaires de journalistes de l'AFP à Auckland, Bangkok, Berlin, Pékin, Hong Kong, Istanbul, Melbourne, Washington, Wellington et au Caire --


Cet article est paru dans Challenges (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008556.xml</field>
    <field name="pdate">2019-07-23T00:00:00Z</field>
    <field name="pubname">Challenges (site web)</field>
    <field name="pubname_short">Challenges (site web)</field>
    <field name="pubname_long">Challenges (site web)</field>
    <field name="other_source">AFP - Journal Internet AFP (français)</field>
    <field name="source_date">23 juillet 2019</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
