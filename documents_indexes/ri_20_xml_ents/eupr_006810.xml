<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Télécoms : la guerre froide technologique est déclarée</field>
    <field name="author">Michel Nakhla</field>
    <field name="id">eupr_006810.xml</field>
    <field name="description"><![CDATA[


Les smartphones et les équipements des réseaux Internet du groupe chinois Huawei (lien :

https://www.huawei.com/fr/

)contiendraient, selon les autorités américaines (lien :

https://www.lesechos.fr/idees-debats/editos-analyses/0600215098706-tous-sur-ecoute-2224535.php

), des fonctionnalités ou des logiciels qui pourraient servir à l'espionnage. À ces soupçons supposés se rajoute l'obligation pour les entreprises chinoises de collaborer, en matière d'information, avec les autorités politiques si ces dernières l'exigent. Faut-il en déduire que Huawei serait lié au pouvoir chinois ?


Pékin se défend en invoquant une concurrence « déloyale et méprisable » (lien :

http://www.globaltimes.cn/content/1131055.shtml

) qui vise simplement à étouffer la concurrence et à empêcher l'expansion de Huawei dans le domaine de la technologie. L'arrestation (lien :

http://www.lefigaro.fr/flash-eco/2018/12/07/97002-20181207FILWWW00382-huawei-la-directrice-financiere-accusee-de-fraude-par-les-etats-unis.php

) très médiatisée, début décembre 2018 au Canada à la demande des États-Unis, de la directrice financière du géant des télécoms chinois Huawei, par ailleurs fille du patron et fondateur du groupe, a relancé les rivalités. Des soupçons de violation des sanctions américaines contre l'Iran pèseraient sur elle.


Ces signaux soulignent que la guerre froide technologique entre les États-Unis et la Chine est bel et bien déclarée, sur fond d'arrivée de la nouvelle génération des réseaux mobiles, la 5G. Dans ce contexte, ne s'agit-il pas simplement d'une extension de la concurrence, dans le domaine des infrastructures, aux technologies de réseau ?



Des équipementiers de plus en plus conquérants



La 5G est appelée à devenir la colonne vertébrale de la transition numérique des économies mondiales : les objets connectés, les voitures autonomes, ou encore les applications reposant sur l'intelligence artificielle ou la réalité virtuelle et augmentée. Elle promet non seulement d'améliorer le débit et la qualité des connexions sans fil, mais aussi d'être une clé pour l'industrie du futur « industrie 4.0 » (lien :

https://www.visiativ-industry.fr/industrie-4-0/

), l'Internet des objets (lien :

https://www.futura-sciences.com/tech/definitions/Internet-Internet-objets-15158/

) ou encore dans le domaine de la santé connectée. Estimés à près de 30 milliards d'euros (lien :

https://www.magazine-decideurs.com/news/arrestation-d-une-dirigeante-de-huawei-la-guerre-de-la-5g-est-relancee

) d'ici à 2026, les investissements dans les infrastructures 5G aiguisent l'appétit des entreprises mondiales comme elles attisent les tensions internationales.


En toile de fond, la rivalité entre la Chine et les États-Unis pour la domination du réseau Internet est bien réelle. Pékin ambitionne de devenir N.1 mondial de l'Internet industriel via son plan « Made in China 2025 » (lien :

https://www.chine-magazine.com/la-nouvelle-economie-made-in-china-2025/

). Ce plan soutient les grandes entreprises d'État dans le but de concurrencer les géants mondiaux, en majeure partie américains. Il sera d'ailleurs suivi d'un autre plan, plus ambitieux : « Internet + ».


Huawei a très bien compris les intérêts qu'il pouvait retirer de ce contexte. Le groupe a ainsi fortement investi pour devenir un leader mondial dans les futures technologies de réseaux et pour façonner le standard futur de la 5G. Avec un chiffre d'affaires d'environ 81 milliards d'euros en 2017 (lien :

https://www.capital.fr/entreprises-marches/huawei-un-geant-chinois-des-telecoms-frappe-de-soupcons-1318792

), Huawei consacre en effet des investissements colossaux à la recherche et développement : 11,6 milliards d'euros en 2017, soit près de 15 % du chiffre d'affaires du groupe. Dans cette perspective, le groupe chinois a déjà mis en oeuvre des centaines de réseaux dans le monde, ce qui signifie qu'une fraction importante de la population mondiale dépend de ses technologies (lien :

https://www.letemps.ch/economie/banni-plusieurs-pays-huawei-veut-setendre-suisse

). British Telecom, Vodafone, Orange, Deutsche Telekom ou encore Telefonica se fournissent d'ailleurs déjà chez Huawei.



Huawei, un équipementier devenu incontournable



La différenciation entre les opérateurs passe aussi par l'intégration de nouvelles technologies provenant d'entreprises très spécialisées. L'objectif des opérateurs n'est plus là de répondre simplement à la demande, mais de modéliser l'expérience client et d'utiliser les données collectées pour innover en matière de service. L'apprentissage à partir de l'information sur les consommateurs par l'intelligence artificielle tient une place centrale dans un processus de différenciation par la création de nouvelles offres. Ces choix sont coûteux et impliquent des compétences très diverses.


En déléguant un ensemble d'activités d'investissement à des équipementiers comme Huawei et d'autres, les opérateurs bénéficient d'une réduction des coûts due à une économie d'échelle. Par exemple, le groupe équipe de plus en plus d'opérateurs dans le monde en fournissant les mêmes systèmes électroniques. L'avantage que Huawei apporte le place naturellement au coeur de la stratégie d'innovation des opérateurs. Il leur permet de maintenir leurs prix sur un marché fortement concurrentiel.


La position du groupe chinois lui donne ainsi un accès extraordinaire aux informations sur les consommateurs et sur les activités des réseaux à l'échelle mondiale.



Huawei dans le collimateur des États-Unis



Huawei, fondé par Ren Zhengfei (lien :

https://www.numerama.com/business/340705-ren-zhengfei-enfant-solitaire-des-montagnes-pluvieuses-fondateur-discret-du-geant-huawei.html

), a d'abord commencé par l'import d'équipements et de matériel pour moderniser les réseaux des télécommunications chinois. Dès les années 1990, le groupe s'est lancé sur les marchés internationaux en fabriquant son propre matériel avec une stratégie de prix agressifs et subventionnés. Ce qui a poussé ses concurrents, notamment Cisco Systems et Motorola (lien :

https://www.clubic.com/pro/legislation-loi-Internet/propriete-intellectuelle/actualite-354106-motorola-huawei-espionnage-industriel.html

) à l'accuser de « commercialisation de produits copiés » et même à le poursuivre en justice pour vol présumé de secrets commerciaux.


Aujourd'hui, Huawei est N.2 mondial des smartphones devant Apple et juste derrière Samsung. Il vient également de ravir la première place au suédois Ericsson en devenant le leader mondial des équipements de réseaux (modems USB, tablettes, équipement wifi et clés 4G distribuées, etc.). Mais ce n'est qu'un début : Huawei est d'ores et déjà en train de travailler sur les technologies d'avenir avec sa propre solution pour la 5G, les datacenters et l'intelligence artificielle.


Toutefois, cette progression insolente n'est pas au goût de tout le monde. Plusieurs pays l'ont écarté de leurs marchés 5G pour des questions de sécurité : les États-Unis et leurs alliés dans le renseignement (Canada, Royaume-Uni,

Australie

et Nouvelle-Zélande). Washington presse maintenant ses alliés européens, dont le Royaume-Uni, l'Allemagne et l'Italie, d'exclure Huawei de leurs réseaux 5G.


Pour les mêmes raisons, l'opérateur mobile japonais Softbank (lien :

https://www.lemondeinformatique.fr/actualites/lire-softbank-pret-a-evincer-huawei-de-ses-reseaux-4g-5g-73720.html

) prévoit de remplacer les équipements du fournisseur chinois dans ses infrastructures mobiles, face au risque de « portes dérobées » dans les matériels et de la fuite de données sensibles. Une perte estimée à 160 millions d'euros pour Huawei.


En France, Bouygues Telecom et Huawei viennent de signer un accord d'innovation (lien :

https://www.corporate.bouyguestelecom.fr/press-room/huawei-et-bouygues-telecom-signent-un-accord-dinnovation-conjointe-sur-la-5g-avec-un-premier-test-a-bordeaux-en-2018/

)conjointe sur la 5G. Orange a déclaré, selon le Financial times (lien :

https://www.ft.com/content/0531458a-fd6c-11e8-ac00-57a2a826423e

), qu'elle n'utiliserait plus l'équipement de Huawei pour la 5G dans son plan de test dans 17 villes européennes en 2019.


Le groupe Huawei risque ainsi de subir le même sort que son compatriote ZTE (lien :

https://www.zte.com.cn/global/

), d'abord privé d'accès aux technologies américaines, puis condamné à une lourde amende et mis sous un régime strict de surveillance pendant 7 ans.


Plus généralement, toute intervention des États dans ce domaine peut en effet paraître suspecte. Mieux vaut privilégier le droit de la concurrence en laissant les entreprises opérer sur un marché ouvert. Cette situation n'est pas unique et cache une dépendance grandissante dans laquelle se trouvent les opérateurs face à la montée de quelques équipementiers puissants.



Doit-on restreindre la concurrence ?



Restreindre la concurrence pour atteindre des objectifs politiques comme la sécurité est la définition même du monopole légal.


Récemment, le vice-président de la Commission européenne, Andrus Ansip, a alerté sur un risque sécuritaire en Europe (lien :

http://www.rfi.fr/europe/20181207-cybersecurite-ue-doit-inquieter-geant-chinois-telecoms-huawei

). Il pense « qu'il faut s'inquiéter de Huawei et d'autres entreprises », car ces entreprises sont contraintes de « coopérer avec leurs services de renseignement, et ce n'est pas bon signe quand les entreprises doivent ouvrir leurs systèmes à des services secrets ».


En 2013, les révélations (lien :

https://www.huffingtonpost.fr/2017/11/20/ce-qui-a-change-depuis-les-revelations-dedward-snowden-en-juin-2013_a_23271946/

) sur le système de surveillance de masse de la National security agency (lien :

https://www.lemonde.fr/technologies/article/2013/10/21/comment-la-nsa-espionne-la-france_3499758_651865.html

)américaine et la récolte massive de métadonnées des appels téléphoniques avaient suscité les mêmes craintes. Le Patriot Act (lien :

https://www.nouvelobs.com/monde/20060906.OBS0822/qu-est-ce-que-le-patriot-act.html

), avait enfoncé le clou en permettant le stockage de données de millions de citoyens américains avant d'être corrigé par le Freedom Act (lien :

https://www.lemonde.fr/pixels/article/2015/06/01/qu-est-ce-que-le-usa-freedom-act_4644663_4408996.html

).


Ces révélations ont poussé la Chine à investir des centaines de milliards d'euros de son budget dans les nouvelles technologies et dans la cybersécurité. La loi chinoise entrée en vigueur le 1áµ‰Ê³ juin 2017 (lien :

https://chinacopyrightandmedia.wordpress.com/2016/11/07/cybersecurity-law-of-the-peoples-republic-of-china/

), dans la lignée du programme chinois d'intelligence artificielle « 2030 AI » (lien :

https://multimedia.scmp.com/news/china/article/2166148/china-2025-artificial-intelligence/index.html

), renforce « le contrôle des infrastructures d'informations importantes ». Les données massives et le cloud entrent dans la liste des domaines concernés par la notion « d'infrastructure d'information ». Une des dimensions importantes de la loi est l'obligation de stocker les données importantes et à caractère personnel sur des serveurs localisés sur le territoire chinois.


Selon cette loi, les infrastructures d'hébergement étrangères situées sur le territoire chinois doivent être détenues par un acteur local afin d'obtenir les licences d'exploitation indispensables pour leur activité. Ce dispositif juridique impose également à toute entreprise chinoise de collaborer avec les autorités si ces dernières l'exigent.



Doit-on se réjouir d'une domination de Nokia et Ericsson ?



À qui profite la crise ? L'administration américaine a pris une série de mesures pour barrer l'accès de Huawei au marché américain et interdire l'utilisation du matériel de Huawei sur son territoire. Les entreprises sont ainsi accusées d'être liées au pouvoir chinois, même si aucune preuve n'a été donnée pour l'instant.


Cette crise redonne des perspectives à Nokia (lien :

https://www.boursorama.com/cours/1rPNOKIA/

) et Ericsson (lien :

https://www.boursorama.com/cours/1zERCB/

), dont les deux titres se sont envolés en bourse juste après l'arrestation de la directrice financière d'Huawei.


En Grande-Bretagne, l'entreprise British Telecom a annoncé qu'elle allait remplacer le matériel chinois par celui de Nokia ou d'Ericsson. SoftBank (lien :

https://www.lemondeinformatique.fr/actualites/lire-softbank-pret-a-evincer-huawei-de-ses-reseaux-4g-5g-73720.html

) au Japon va faire de même. Les contrats perdus par les deux groupes chinois ZTE et Huawei sont récupérés immédiatement récupérés par les deux équipementiers européens.


On peut craindre que cette situation de consolidation d'un duopole (lien :

https://www.larousse.fr/dictionnaires/francais/duopole/26995

), Nokia et Ericsson, se traduise par une moindre intensité concurrentielle. Les consommateurs européens en seraient alors les grands perdants.


[Image :

https://counter.theconversation.com/content/108849/count.gif?distributor=republish-lightbox-advanced

] ______


Par Michel Nakhla (lien :

https://theconversation.com/profiles/michel-nakhla-261620

), Centre de Gestion Scientifique-I3 UMR CNRS 9217, Mines ParisTech


La version originale (lien :

http://theconversation.com/

) de cet article a été publiée sur The Conversation (lien :

http://theconversation.com/

)


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006810.xml</field>
    <field name="pdate">2019-01-24T00:00:00Z</field>
    <field name="pubname">La Tribune (France), no. 6609</field>
    <field name="pubname_short">La Tribune (France), no. 6609</field>
    <field name="pubname_long">La Tribune (France), no. 6609</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Opinions</field>
  </doc>
</add>
