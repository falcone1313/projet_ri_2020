<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Les Grecs apprennent à compter sur eux-mêmes</field>
    <field name="author">DANCER Marie</field>
    <field name="id">eupr_005958.xml</field>
    <field name="description"><![CDATA[


Volos (région de Thessalie)


De notre envoyée spéciale



« Welcome to Volos. »

Le panneau de bienvenue trône au beau milieu d'un rond-point flambant neuf, à l'entrée de la ville. Quelques centaines de mètres plus loin, après avoir dépassé plusieurs immeubles en construction, des agents municipaux décrochent les décorations de Noël et la fête foraine plie bagage.


L'Épiphanie est passée, mais Volos tient toujours à se montrer sous son meilleur jour. Parée des atours d'une ville tranquille de 150 000 habitants, nichée entre la mer et ses îles, mais aussi le mont Pélion, qui attire les amoureux de nature, été comme hiver. Une Grèce en miniature avec son port, son université, son hôpital...


Sept ans après le premier reportage de

La Croix

sur place, Volos s'est embellie. Apprêtée. Les rues piétonnes, plus nombreuses, la promenade du front de mer comme la paisible place de la cathédrale Saint-Nicolas font naître chez le visiteur un sentiment de flânerie insouciante. De là à dire que Volos va mieux, comme semblent l'indiquer les dernières statistiques sur la Grèce

(lire les repères)

, il n'y a qu'un pas que Maria Kontogianni ne franchira pas. La directrice du bureau local de l'OAED (le Pôle emploi grec) assiste avec impuissance à l'inexorable hausse du chômage - passé de 16 % il y a sept ans à 19,5 % aujourd'hui en Thessalie, dont Volos fait partie.


Dans cette région marquée par une longue tradition industrielle, les usines ont fermé les unes après les autres depuis 2011.

« À chaque fois, plusieurs dizaines voire centaines de salariés sont restés sur le carreau »,

soupire la responsable.


Signe des temps, l'OAED a emménagé dans des locaux plus grands. Mais comme en 2011, les agents qui partent à la retraite ne sont pas remplacés et Maria Kontogianni doit faire avec. Seule la fusion avec deux organismes sociaux lui permet de s'appuyer sur un effectif de 35 personnes, contre 17 il y a sept ans.


Certes, quelques sociétés recrutent, dans l'agroalimentaire notamment. Sans oublier des emplois saisonniers dans le tourisme ou dans les magasins, comme actuellement pour les soldes. Mais cela reste une goutte d'eau et la colère des chômeurs, que Maria Kontogianni observait en 2011, s'est muée

« en désespoir »

. Chef cuisinier, actuellement en poste à l'hôtel Kipseli, Paragiodis Lytras alterne chômage et travail depuis sa première inscription à l'OAED en novembre 2011.

« Les salaires imposés par l'UE et le FMI sont trop bas. Je travaille 40 heures par semaine pour 457 € par mois. Avec cette somme, je peux seulement nourrir ma famille »,

proteste le quinquagénaire, venu en centre-ville pour déposer une demande d'aide publique au paiement de son loyer.


Depuis quatre mois, sa femme est infirmière dans l'Essex, en Grande-Bretagne, où elle gagne 3 000 livres (3 400 €) par mois. Ses deux aînés, étudiants, sont partis avec elle. Lui est resté avec les deux plus jeunes.


De plus en plus de familles sont ainsi séparées.

« Comment peut-on oser dire que le pays va mieux quand tout le monde le quitte?

, interpelle-t-il.

Cette crise est sans fin. Il ne faut plus accroître l'austérité. »

Alors que le premier ministre Alexis Tsipras (gauche radicale) fête ses trois ans au pouvoir et que de nouvelles coupes sont prévues dans les retraites en 2019 et 2020,

« plus personne ne croit dans le gouvernement, ni dans la politique »,

souligne-t-il.


À quelques mètres, midi sonne. Une salle attenante à la cathédrale Saint-Nicolas reçoit ses premiers visiteurs, des personnes défavorisées venues prendre un repas chaud, ou retirer un colis de nourriture. Car à Volos comme dans le reste du pays, la crise pénètre aussi dans les églises. Depuis 2009, les immigrés ne sont plus les seuls à solliciter leur solidarité. Des Grecs sont venus grossir les rangs.


De 500 repas quotidiens distribués avant la crise, le diocèse de Volos est passé à 1 300 en 2011 puis 2 000 aujourd'hui.

« La pauvreté est davantage présente malgré de légers signes d'amélioration,

assure le père Amphilochios Miltos.

Des gens font les poubelles, de plus en plus de magasins ferment - même s'il y en avait sans doute trop - les hôpitaux manquent de tout, y compris de pansements... »

Athina Bairami non plus ne trouve pas que Volos aille mieux. La jeune femme de 36 ans, qui projetait de partir en

Australie

voilà sept ans, est finalement restée dans sa ville natale

« parce que sans famille là-bas, il n'était pas évident d'obtenir des papiers »

. On la retrouve durant l'une de ses pauses: comme en 2011, la jeune femme cumule deux emplois, professeur d'anglais et serveuse dans un café le week-end, pour 30 € par jour. Ses revenus sont passés à 159 € pour 6 heures, contre 400 € par mois pour 20 heures de cours en 2011.


Athina est obligée de vivre chez ses parents, comme son frère de 31 ans.

« Cette fois, c'est décidé, je vais partir cette année, en France ou en Grande-Bretagne,

assure-t-elle,

pour simplement pouvoir vivre, fonder une famille, et offrir le meilleur de moi-même. Ici, je ne peux pas, tout marche encore au piston. »




« Au moins,

se console-t-elle,

j'ai eu la chance de grandir avant la crise, mes parents avaient les moyens de m'offrir des divertissements, des activités... Ce n'est plus le cas pour les jeunes d'aujourd'hui. »



Une situation que Diana Kakari, professeur de français à l'université de Thessalie, constate auprès de ses étudiants. Pourtant, cette quadragénaire regarde aujourd'hui la ville avec un oeil contrasté. Elle qui se disait, en 2011, tentée par un départ à l'étranger vient de rentrer après deux années à Londres, où elle a suivi son mari, fonctionnaire, le temps d'une mutation.



« À mon retour à Volos en septembre dernier, je me suis retrouvée seule enseignante titulaire, alors qu'en 2012, nous étions deux, appuyés par trois vacataires »,

se souvient-elle. En revanche, le personnel de ménage, qui avait été supprimé, a été réintroduit.

« Et nous avons aujourd'hui un recteur dynamique, qui fait construire ou aménager de nouveaux locaux pour les étudiants »,

constate-t-elle.


En sept ans, l'inquiétude pour l'avenir des étudiants n'a pas fléchi.

« C'est partout pareil en Europe. Mais ici, beaucoup ont dans l'idée qu'ils devront partir un jour. C'est une évidence, même si on n'en parle pas. »

Avec 45 % de perte de salaire depuis la crise, Diana Kakari ne relève

« aucune amélioration dans

(son)

quotidien, malgré une récente augmentation mensuelle de 20 €. On vivote plus qu'on ne vit... On paie moins d'activités à nos enfants, les gens ne dépensent plus ou presque, ils abandonnent progressivement leur voiture, certains quittent leur logement... On aura bientôt brûlé toutes nos économies. »



Comme la Grèce, Volos semble s'être installée dans une économie de la débrouille.Le salaire mensuel de Toula Kekatou, journaliste au quotidien régional

La Thessalie

, est passé de 2 400 € en 2011 à 900 €.

« Au début, cela fait un choc, ensuite on s'y habitue. »

La colère qu'elle exprimait en 2011 -

« envers les politiques et les citoyens qui ont profité du système clientéliste qui a conduit à la crise » -

est intacte

.

Une colère aujourd'hui mêlée

« de déprime et de résignation, voire de désarroi. Personne ne sait comment la Grèce peut s'en sortir, personne n'y répond ».



Ceux qui s'en sortent sont, à ses yeux, ceux qui créent une entreprise ou explorent de nouvelles voies. C'est le pari de Thrasivoulos Stavridopoulos, directeur du port de Volos depuis bientôt trois ans.

« Avant la crise, nous recevions très facilement des subventions. Maintenant nous devons compter sur nous-mêmes. Nous apprenons, dans la douleur, à être compétitifs »

, insiste-t-il. Après des années difficiles, les revenus du port se sont améliorés

« grâce à une politique de prix plus flexible, et à une meilleure qualité de service »

.


Les projets d'extension des infrastructures portuaires envisagés en 2011 vont être réactivés: créer plus d'espace pour les bateaux de croisière, les porte-conteneurs et les cargos de vrac...

« Et nous allons dès cette année connecter le port avec le réseau ferroviaire principal qui va d'Athènes à Thessalonique, grâce à des fonds européens et régionaux mais aussi,

espère-t-il,

des investissements étrangers. »

Il y va de l'avenir de la région, qui a besoin d'exporter ses produits agricoles et industriels. Face à la crise, l'ouverture vers l'extérieur est devenue un maître mot.

« Nous travaillons au sein de l'association des croisiéristes de Méditerranée pour promouvoir Volos dans de nouveaux circuits de croisières afin d'éviter de souffrir des problèmes géopolitiques au Moyen-Orient, comme l'an dernier,

explique Thrasivoulos Stavridopoulos

. Nous devons vivre ouverts, car s'isoler, c'est échouer. »



Volos et sa région ont malgré tout profité, comme tout le pays, d'un surcroît de fréquentation touristique depuis 2015.

« La saison 2017 a été particulièrement bonne» ,

se réjouit Sissy Topali, de la fédération des hôtels.

« Les autorités régionales ont fait un gros travail de promotion de notre région »,

souligne-t-elle. Restent de nouvelles voies à explorer:

« Notamment le tourisme religieux, avec les Météores, qui sont proches, pour prolonger la saison touristique. »

Après des années très difficiles, l'industrie locale commence aussi à envoyer quelques signaux positifs, insiste pour sa part Eleni Koliopoulou, présidente de l'association des industries de Thessalie et de Grèce centrale.

« Dans l'agroalimentaire, un secteur clé de la région, la production a doublé entre 2008 et 2015 et les exportations, tous secteurs confondus, sont en forte hausse depuis 2016 »

, se réjouit-elle.


La région ne manque pas d'atouts à ses yeux: une main-d'oeuvre bien formée, une tradition industrielle et des nouvelles sources de croissance à explorer, notamment dans le domaine du recyclage des déchets et, plus largement, du développement de l'économie circulaire.


Le début d'amélioration est perçu par la communauté albanaise, passée de 2 500-3 000 en 2011 à 1 700 personnes.

« Beaucoup envisagent de revenir désormais,

explique Florine Tgokai, président de l'association des Albanais de Volos.

La crise est positive pour les Grecs qui vont apprendre à en finir avec leurs excès et respecter l'argent. »



À la mairie, un imposant bâtiment blanc construit sur le modèle des habitations du mont Pélion, l'édile Achilles Beos a maintenu, depuis son élection en 2014, l'épicerie et la pharmacie sociales, comme la distribution gratuite de vêtements.

« C'est du travail qu'il faudrait donner aux citoyens mais malheureusement

, regrette-t-il,

le développement économique n'est pas du ressort de la municipalité. »



S'il a pu engager quelques travaux dans sa commune,

« c'est en économisant sur les frais de fonctionnement et en attribuant les appels d'offres aux moins-disants ».

Il a aussi commencé à recouvrer les dettes dues par certaines entreprises à la mairie, notamment des loyers impayés depuis parfois quinze ans.

« Il faut cesser de tout attendre du gouvernement ou de l'UE,

souligne-t-il.

Les Grecs doivent se prendre en main, produire eux-mêmes plutôt que d'importer, développer leurs atouts. »



Quant aux politiques,

« ils devraient se mettre d'accord en vue de l'intérêt général plutôt que de se déchirer,

estime ce maire sans étiquette.

Et les vieux politiciens devraient laisser la place aux jeunes ».



]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005958.xml</field>
    <field name="pdate">2018-01-25T00:00:00Z</field>
    <field name="pubname">La Croix, no. 41010</field>
    <field name="pubname_short">La Croix, no. 41010</field>
    <field name="pubname_long">La Croix, no. 41010</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Evénement</field>
  </doc>
</add>
