<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Les rouages du Système U</field>
    <field name="author">Thomas Pontiroli</field>
    <field name="id">eupr_005452.xml</field>
    <field name="description"><![CDATA[


U continue de creuser son sillon engagé avec Serge Papin, le médiatique PDG remplacé il y a bientôt un an par Dominique Schelcher. TBWA Paris, en qui la coopérative a renouvelé sa confiance, a repensé son système de communication.


/


Comment renouveler son discours quand on tient le bon depuis déjà plus de dix ans? La proximité, l'humain, le bien manger, la juste rémunération des producteurs... Autant de thèmes défendus de longue date par Système U. Mais voilà, depuis 2017, ils sont tous repris par ses concurrents - sauf E.Leclerc et Lidl focalisés sur le prix. Le premier à dégainer est Intermarché en mars 2017 avec son film «L'Amour», qui fera le succès de l'agence Romance. En septembre 2018, Carrefour lance son «Act for Food» avec l'agence Marcel (groupe Publicis), dont le propos est également de discréditer tous les engagements qui ont pu être pris dans les «pubs pour supermarchés» jusqu'alors. Promis, cette fois c'est vrai. Dans la foulée, Auchan y va de son «Manifesto» écrit avec l'agence ServicePlan. Quant à E.Leclerc, il rétorque que «quand on sait ce qu'on veut, c'est souvent plus cher»... Des engagements, des engagements et encore des engagements dans un marché où les beaux discours ont fait long feu, les consommateurs déconsomment et les priorités se polarisent entre bio et prix bas.


Une vision corporate


Face à cette armada publicitaire, la coopérative Système U a d'abord choisi de remettre en compétition son agence historique, TBWA Paris - la signature «Les nouveaux commerçants» a été conçue par Jean-Marie Dru, sous l'ère BDDP. Malgré le poids des années, l'agence l'a emporté en mars 2018. Face à elle, Altmann+Pacreau (finaliste), Marcel et Sid Lee Paris. Le vrai changement intervient en fait au niveau du groupe, avec la nomination en mai de Dominique Schelcher en remplacement du médiatique Serge Papin, patron de U de 2005 à 2018.


«Rechallenger l'agence lui a permis de prendre possession de la marque et de sa posture», pointe Sandrine Burgat, directrice de la communication depuis huit ans - et précédemment en charge du budget E.Leclerc chez

Australie

. Pour elle, «la vision du commerce portée par Serge Papin a été plus qu'une vision de communication, un projet d'entreprise». En réalité cette vision s'avère «assez corporate» et les 1563 points de vente ont du mal à se l'approprier en dehors du référencement des produits en magasin. Pour porter son discours, Système U doit donc d'abord unifier sa communication globale. Pour cela, TBWA a conçu tout un système de communication. Par exemple, l'agence a filmé des salariés expliquant comment mettre en place la nouvelle signalétique: ardoises noires pour la communication de marque nationale et blanches pour les messages locaux, tracts valorisant - tout de même - certains prix bas, affiches «manifeste»...


Cinq engagements


Sur le fond, Système U veut continuer à se singulariser. «Ce qui fait notre différence, c'est précisément la somme de nos différences», postule Sandrine Burgat. Une posture qui se retrouve dans la nouvelle signature: «Commerçants autrement». «Le mot "commerçant" désigne pour nous plus qu'un métier: derrière le terme, on trouve des hommes et des femmes», explique de son côté Anne Vincent, vice-présidente de TBWA Paris, soulignant le côté «humain» de l'enseigne.


Cet aspect est mis en avant dans la campagne TV du 13 janvier au 4 février. Cinq films de 15 secondes mettent en avant un boulanger et un aspirant apprenti, une caissière et un consommateur soucieux de la qualité des produits, un agent chargé du drive et un consommateur, et enfin deux clientes et un boucher. Alors que certains acteurs misent sur les caisses automatiques - dont le paroxysme est atteint par Le 4 Casino à Paris, magasin entièrement automatisé sur le modèle Amazon Go -, Système U met l'accent sur les métiers. Si le groupement compte 65 hypermarchés, ce n'est pas le coeur de son offre. L'essentiel du chiffre d'affaires (71,6%) en 2017, vient des supermarchés. «Nous défendons un modèle à taille humaine avec une intégration régionale et une préservation des territoires», appuie Sandrine Burgat. La proximité et la relation en magasin forment deux des cinq «grands engagements» de Système U. Les trois autres: la transparence, le côté responsable mais aussi l'innovation, incarnée par exemple en 2018 avec l'application «Y'A Quoi Dedans?», un Yuka-like.


Budget des petits


«Système U a déjà constitué tout un système de preuves qui ne demandaient qu'à être révélées», souligne Anne Vincent. «Carrefour et Auchan recherchent la proximité dans leurs campagnes car ce sont de gros acteurs centraux, mais ils sont taxés de non-transparence, alors que Système U est déjà local», appuie la publicitaire. «Nous ne nous lançons pas dans de grandes déclarations pour les années à venir. En réalité, nous faisons l'inverse des autres: au lieu de dire ce que nous allons faire, nous disons ce que nous faisons déjà», explique Sandrine Burgat, pour qui l'enseigne pèche parfois par excès d'humilité.


Dans la distribution, mieux vaut pourtant être armé jusqu'aux dents pour survivre... Avec un budget média de 151,9 millions d'euros en 2017, en hausse annuelle de 8,3 %, Système U (aidé par MyMedia) est le 36e annonceur français selon Kantar Media. «On joue dans la cour des grands avec le budget des petits», rappelle la directrice de la communication. Cinquième acteur avec 10,6 % de parts de marchés selon Kantar Worldpanel début 2019, Système U fait face à des concurrents bien plus offensifs en média: 392,6 millions pour Intermarché la même année, 412,9 millions pour Carrefour, 418,6 millions pour Auchan, et même... un demi-milliard pour Lidl, le troisième annonceur français. Comme le rappelle Sandrine Burgat, «il y a douze ans, nous parlions de commerce vertueux et de substances controversées. À l'époque, nous étions seuls au monde». Ou tout du moins en France.


Chiffres clés :


171,9 millions d'euros. Dépenses médias en 2017 (Kantar Media).


10,6%. Parts de marché GMS en 2018 (Kantar Worldpanel).


19,49 milliards d'euros. Chiffre d'affaires en 2017 (+1,4%).


Cet article est paru dans Stratégie (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005452.xml</field>
    <field name="pdate">2019-01-24T00:00:00Z</field>
    <field name="pubname">Stratégie (site web)</field>
    <field name="pubname_short">Stratégie (site web)</field>
    <field name="pubname_long">Stratégie (site web)</field>
    <field name="other_source">Stratégies</field>
    <field name="source_date">24 janvier 2019</field>
    <field name="section">Marques</field>
  </doc>
</add>
