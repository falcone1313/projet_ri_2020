<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">IX.- Horizons de Singapour</field>
    <field name="author">Robert GUILLAIN</field>
    <field name="id">eupr_006260.xml</field>
    <field name="description"><![CDATA[


Singapour ! Cette ville donne un choc. Ce grouillement de vie dans un éclatement de verdure, cette clameur d'une métropole moderne où s'entrecroisent les mots magiques du vingtième siècle - caoutchouc, dollars, pétrole - au milieu d'une nature primitive silencieusement occupée aux grands échanges entre le sol et le ciel; cette fièvre d'une cité concentrée sur son univers intérieur, fait des négoces et des plaisirs de sept cent mille jaunes et de huit mille blancs, et attentive cependant aux appels de l'Asie par-dessus l'horizon marin des détroits et des archipels: tout ici est action, vibration et force. Tout y est aussi volonté. Si les problèmes sont à peu près les mêmes que ceux de toute l'Asie du Sud-Est - montée du nationalisme, agitation communiste, fermentation sociale, difficultés économiques - ils se posent dans un climat où la volonté britannique est plus forte que toutes les forces de désagrégation.


Le retour des Anglais


Au bout de mon long voyage à la recherche de l'ordre perdu, de Karachi à Singapour, voici l'ordre retrouvé. L'Angleterre en Malaisie veut rétablir sa puissance et conserver son élan. Et, tandis que l'Inde de Kipling n'est plus, l'aventure de Raffles, marchand aventurier dont le coup d'oeil génial choisit cette île de marais et de jungle pour en faire un carrefour de l'empire, continue après cent trente années.


Pour combien de temps sont-ils revenus ? C'est la question qu'on se pose quand on a vu, comme j'ai pu le faire ces deux dernières années, Changhaï repris par le désordre chinois, Hanoi ravagé par la guerre, Saigon assiège par l'émeute; et plus récemment l'Inde sans les Anglais, et la Birmanie aux Birmans. Mais à Singapour voici les paquebots et les hydravions venant de Londres ou de Hong-Kong, les vapeurs dans le port, où les coolies chargent les lingots d'étain éclatants et les plaques de caoutchouc brut couleur de miel, les entrepôts s'ouvrant aux cargaisons de riz, les pipe-lines pompant le pétrole des bateaux-citernes. Voici les banquiers anglais dans leurs banques de Raffles Square, et les fonctionnaires de la couronne dans les bureaux gouvernementaux, le flot des voitures neuves dans lès ruelles où s'agitent les Chinois, les soirées du Tanglin Club et du Raffles-Hotel, le football devant le port des jonques. Les crieurs vendent le Strait Times; lai B. B. C. installée à Radio-Singapour monde de ses nouvelles le Sud-Est asiatique; six cents films étrangers -record du monde - passent par Singapour en un an. Le régiment de Churchill débarque de Londres; et la flotte d'Extrême-Orient, redescendue de Hong-Kong, embusque de nouveau ses cuirassés dans leur retraite entre l'ile Verte et la presqu'île, face au rideau de Jungle où débouchèrent, il y a quatre ans, les Japonais...


Pour combien de temps tout cela ? " Voilà encore une question de Français ! " me répond avec humour un fonctionnaire britannique. Et c'est bien une question que les Anglais ne se posent pas. Résoudre les problèmes au fur et à mesure qu'ils se présentent, dans l'empirisme et sans inutiles théories, c'est sans doute leur seul principe au point de départ. Mais attention, le point d'arrivée ne fait pour eux pas de doute : la domination anglaise en Malaisie ne prétend pas être éternelle. Les Britanniques savent parfaitement qu'elle doit avoir un terme, et que si le ou les nationalismes locaux sont sans exigence ni hâte excessives il faudra cependant leur céder un jour. L'Angleterre prend donc la voie qu'elle a suivie à Delhi, mais elle a la chance de pouvoir aller sans se presser, et de commencer le chemin avec quinze ans de décalage par rapport aux Indes. Le retard de la Malaisie et ses divisions raciales donnent le temps aux Anglais de préparer des équipes politiques et de former des alliances économiques, pour faire plus tard de ce pays un associé dans le Commonwealth. Mais enfin c'est un départ qu'ils ont en vue, et ils ne craignent pas de l'annoncer.


" Je vous donne ma parole - répète publiquement le premier personnage de la Malaisie, Malcolm MacDonald - et non pas la parole d'un fonctionnaire de passage qu'un autre remplacera, mais celle de l'Angleterre: nous voulons faire de vous un pays indépendant. Si vous avez des doutes- sur notre sincérité, je vous demande de considérer ce que nous avons fait depuis deux ans en Asie, et depuis bien plus longtemps ailleurs : Canada,

Australie

, Nouvelle-Zélande, Afrique du Sud; puis l'Inde, le Pakistan, la Birmanie, sans parler de l'Égypte ou de la Palestine: cette liste des pays que nous avons quittés est mon meilleur argument... " La Malaisie viendra-t-elle compléter la liste dans vingt ans, ou dans cinq ? Un fait est certain, c'est qu'elle s'apprête à y figurer un jour. D'ailleurs la date ne dépendra pas seulement de la politique britannique mais aussi de tout ce qui se passe autour de la Malaisie. Que l'Indochine et l'Indonésie viennent à être perdues par les Français et les Hollandais et l'ilot malais sera sans doute intenable. Mais en sens inverse Singapour, point de consolidation de la puissance britannique, peut devenir encore davantage : un centre de cristallisation de l'influence européenne. Et voilà pourquoi les Anglais s'y organisent pour avoir vue au delà de la Malaisie sur toute l'Asie du Sud-Est...


Un proconsul travailliste


Une véranda fleurie, sous une arche de granit vert; encadrant la fantasmagorie d'un coucher de soleil sur le détroit de Johore : coupée de forêts d'hévéas, la jungle ondule et roule vers la profondeur de l'horizon bleu. Une grande allée liquide s'enfonce dans son épaisseur comme une coulée d'étain fondu, où la silhouette aiguë d'une pirogue à voile s'éloigne dans le crépuscule en direction des lointains archipels. C'est ici qu'aime à se retirer chaque soir celui qui préside au destin de la Malaisie, Malcolm MacDonald. Il préfère à son bureau juché au sommet d'un gratte-ciel chinois, au-dessus du port de Singapour, cette retraite dans le moderne palais d'été du sultan de Johore. Âgé d'une cinquantaine d'années, ce fils de Ramsay MacDonald, affable et simple comme l'était son père, a été secrétaire d'État pour les dominions, ministre des colonies, et gouverneur général du Canada avant d'être envoyé en Malaisie.


La présence d'un tel personnage dit assez et l'importance du lieu, et l'intérêt que l'Angleterre travailliste attache à la politique qui s'y élabore. Devant cet horizon et dans cette sérénité laissons donc les problèmes purement malais - la nouvelle Constitution, la rébellion, et aussi d'autres questions délicates que sont le prix trop bas du caoutchouc, grand pourvoyeur des dollars de l'Angleterre, et la dangereuse concurrence du caoutchouc synthétique américain. Malcolm MacDonald est ici pour voir plus large encore, et plus loin.


Après avoir mis sur pied le statut de Singapour et de la Fédération malaise, il est allé à Londres, et le voici revenu avec un titre neuf et des pouvoirs que personne n'avait eus ici avant lui. Il est aujourd'hui haut commissaire, ou plus exactement commissaire général du Royaume-Uni dans l'Asie du Sud-Est. Il n'est donc pas seulement gouverneur général de la Malaisie, " coiffant " en cette qualité le gouverneur de Singapour, le haut commissaire de la Fédération de Malaisie et les gouverneurs du Bornéo britannique et de Sarawak. Il est une sorte de sur-ambassadeur de la Grande-Bretagne, qui, au point de rencontre de l'Asie méridionale et de l'Extrême-Orient, informe et conseille Londres dans toutes les questions politiques et diplomatiques qui concernent les deux façades asiatiques sur l'océan Indien et l'océan Pacifique. Il est encore celui qui coordonne la politique économique de l'Angleterre dans une région dont la paix devrait refaire une des plus riches du globe. Il est enfin le représentant dit ministère britannique de la défense, et, comme président du comité de coordination des forces navales, aériennes et militaires en Extrême-Orient, il conseille son gouvernement dans les questions stratégiques.


Londres a donc voulu fonder sur trois principes une politique d'ensemble qui déborde même le Sud-Est asiatique. Premier principe : décentraliser. Le " monsieur sur place ", comme écrivait autrefois Lyautey, décide mieux et plus vite que les bureaux aux bords de la Tamise. Deuxième principe : coordonner les solutions sans mélanger les problèmes. MacDonald et ses collaborateurs sent des gens qui circulent, et vers qui l'on vient de toute une partie du monde. Indonésie, Indochine, Birmanie, Ceylan, les Indes : voilà leur horizon, et plus loin encore la Chine et l'

Australie

. Mais dans chacun de ces secteurs on recherche la solution particulière, inspirée non pas des principes mais des nécessités pratiques du lieu et du moment. Troisième principe enfin : passer du colonial au politique, c'est-à-dire non seulement affirmer aux autochtones que l'ère coloniale est aujourd'hui périmée en Asie, mais faire cesser progressivement dans l'administration britannique le dangereux dualisme colonies-affaires étrangères, en repassant les responsabilités au seul Foreign Office. Nous connaissons ce problème en France - et en Indochine - où nous avons trop souvent dans une même affaire une " politique de la rue Oudinot " et une " politique du quai d'Orsay ".


Singapour et Saigon


Et ce n'est pas par hasard qu'est évoquée ici la politique française. Sans trahir les confidences de cette soirée devant le crépuscule théâtral du détroit de Johore, disons que les Anglais se désolent et s'inquiètent de l'énervante incertitude de notre politique indochinoise. Ils ont prêté à notre retour à Saigon, en 1945, un appui loyal et vigoureux. Ils seraient prêts à le continuer, à condition toutefois qu'il ne s'agisse pas d'une politique rétrograde. Mais s'il est vrai que nous désirons nous aussi une politique progressiste, nous restons désespérément à sa recherche, empêtrés entre les velléités de Paris et celles de Saigon, tiraillés entre les vues divergentes de nos services, paralysés par les querelles de partis et par les changements trop fréquents de nos envoyés en Indochine. Depuis trois ans nous n'avons pu aborder davantage avec l'Angleterre, à Singapour, que des généralités; et lorsque nos représentants prennent contact avec leurs collègues britanniques ils ne savent pas, Paris ne leur a pas dit, et pour cause, quelle politique est la nôtre, ni ce qu'ils peuvent chercher auprès des Anglais : nous n'avons rien à offrir, nous ne pouvons que poser des questions.


De Karachi à Singapour nous avons une chaîne de diplomates remarquables, sans doute la meilleure équipe que nous ayons jamais eue en Asie : ils ont nom Marchai. Daniel Lévy, Raoul-Duval, Gilbert. Guibaut. Mais ils sont tous embarrassés par la question indochinoise, dont les incidences entravent leur action, dont les malheureux problèmes hypnotisent. Paris. Tandis que les Anglais ont une politique asiatique, plus ou moins critiquable, mais qui existe, qui avance vers des solutions pratiques, et qui est aux dimensions de l'Asie, la France d'après guerre oublie qu'elle a été une grande puissance asiatique, qu'elle pourrait avoir encore un grand rôle dans la nouvelle Asie libérée. Pour avoir trop longtemps hésité entre une politique de conservation défendue avec faiblesse, et une politique de générosité, appliquée avec étroitesse, elle risque de retomber deux siècles en arrière, et de n'avoir plus en ces régions que quelques comptoirs sur des côtes hostiles...


Faut-il conclure de tout cela que l'Angleterre souhaiterait, à la faveur d'un redressement de notre politique, une sorte d'alliance dans l'Asie du Sud-Est qui serait quelque chose comme un prolongement asiatique du pacte occidental ? On croit à Singapour que ce serait aller trop loin et donner aux populations autochtones l'impression d'une sainte-alliance des colonialismes renaissants. Ce que l'Angleterre croit plus simplement, c'est que cette région forme un tout, où nos intérêts et nos méthodes doivent être parallèles; il importe pour Singapour que la tempête cesse de sévir dans le voisinage.


C'est avec le don typiquement britannique de l'under-statement - nous dirions en français l'art d'habiller de mots simples les paroles fortes - que Malcolm MacDonald, proconsul de l'Angleterre travailliste, résume l'entretien :


" On a suggéré que Londres est entièrement accaparé par les affaires de l'Occident. Je peux vous dire qu'il n'en est rien. J'y ai trouvé une attention extrême pour les choses de l'Asie du Sud-Est. Nos intérêts vitaux dans cette partie du monde ne se sont pas affaiblis. Nous sommes ici très actifs... "


FIN


(1) Voir le Monde des 22, 23, 25, 26-27, 28 septembre, 6, 7 et 8 octobre 1918.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006260.xml</field>
    <field name="pdate">1948-10-09T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
