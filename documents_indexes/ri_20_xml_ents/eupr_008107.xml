<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Ruée vers l’or vert</field>
    <field name="author">J CLEMENCOT</field>
    <field name="id">eupr_008107.xml</field>
    <field name="description"><![CDATA[


Poussé par deux moteurs surpuissants, le canot file en pleine nuit à l’assaut des côtes espagnoles. À son bord, plusieurs centaines de kilos de résine de cannabis Made in Morocco. En Méditerranée, ce type d’expédition est devenu banal. La majorité de la production rifaine part à destination de l’Europe via des filières clandestines. À la tête de ces trafics, des caïds comme Moufid Bouchibi, qui depuis sa planque algérienne continue d’être l’un des grands organisateurs du marché français.


Mais ces derniers ne sont plus les seuls à profiter du savoir-faire africain en matière de kif. En Amérique du Nord, des chefs d’entreprise ont commencé à écrire une autre histoire, légale celle-ci, du cannabis. Et le continent y joue déjà un rôle. L’an dernier, dans la perspective de l’autorisation de l’usage récréatif au Canada, après celle obtenue dans une dizaine d’États des États-Unis, plusieurs sociétés cotées en Bourse, comme Tilray, Canopy Growth, Supreme Cannabis, Aphria et Aurora Cannabis, ont vu leur valorisation crever le plafond, dépassant largement 1 milliard de dollars. Un engouement également alimenté par les utilisations médicales des molécules extraites de cette plante. Le THC (tétrahydrocannabinol), aux effets psychotropes, et le CBD (cannabidiol) aux vertus décontractantes, sont désormais largement reconnus pour leur action dans le traitement des douleurs, de la perte de l’appétit, des migraines, de l’insomnie… Un changement d’image radical pour le cannabis que les industriels ont aussitôt exploité en intégrant du CBD à des produits cosmétiques et alimentaires.


À l’avant-garde de ce mouvement, Constellation Brands, fabricant de la bière Corona, a investi plus de 4 milliards de dollars pour prendre 38 % du capital de Canopy Growth l’an dernier. De son côté, Heineken a lancé en Californie une boisson gazeuse à base de cannabis et entend désormais en faire autant au Canada, où le brasseur Molson Coors a lui aussi prévu de commercialiser des eaux au cannabis d’ici à décembre. Si aujourd’hui la bulle spéculative s’est en partie dégonflée (lire encadré ci-contre), le marché que toutes ces entreprises explorent n’en reste pas moins prometteur. Il est évalué à 150 milliards de dollars au niveau mondial et pourrait atteindre, selon Barclays, 272 milliards d’ici à 2028. La Colombie, l’Uruguay, la Thaïlande, Israël, pour ne citer que quelques pays, ont fait évoluer leur législation pour autoriser des utilisations, notamment médicales, ouvrant autant de nouveaux marchés.


Le potentiel de l’Afrique en la matière est loin de laisser les principaux acteurs de ce commerce insensibles. Le Lesotho, le Zimbabwe et l’Afrique du Sud participent à ce vaste mouvement. En cas de légalisation, ce secteur pourrait représenter, selon le cabinet Prohibition Partners, un marché de 7,1 milliards de dollars en 2023 à l’échelle du continent, dont près de 1 milliard pour le seul royaume chérifien, premier producteur mondial de résine de cannabis. « L’appréciation de la situation marocaine est très optimiste, car il est peu probable que la loi évolue aussi vite », juge néanmoins Khalid Tinasti, secrétaire exécutif de la Global Commission on Drug Policy.


Dans le royaume, le débat sur la légalisation du cannabis, nouvel or vert mondial, s’il « n’est pas tabou », insiste l’anthropologue Khalid Mouna, reste bloqué. Aucun gouvernement ne veut être celui qui fera passer cette culture, pratiquée dans le Rif depuis le VIe siècle, du bon côté de la loi. « Les partis politiques comme le PAM ou l’Istiqal utilisent cette question de manière opportuniste, mais rien ne bouge réellement, et le pouvoir craint de donner une tribune aux islamistes du PJD, car il faudrait trouver une justification religieuse à cette avancée », résume-t-il. Pourtant, environ 50 000 ha de cannabis seraient cultivés, principalement autour de la ville de Chefchaouen, faisant vivre plus ou moins directement 800 000 Marocains. Le conseil de Tanger-Tétouan-Al Hoceïma a bien voté, en juin, en faveur du financement d’une étude sur la culture du cannabis, mais ces travaux ne seront pas suffisants pour faire avancer le pays sur cette question, estiment les spécialistes interrogés. « En 2010, la gendarmerie royale avait déjà mené une étude sur la culture du chanvre, mais cette fois à fibre [c’est-à-dire très pauvre en substance psychotrope], en insistant sur son potentiel. Pour quelles avancées ? » constate un brin désabusé l’activiste Chakib el-Khayari, organisateur d’une journée d’étude parlementaire sur le cannabis à la fin de 2013. Dans ces conditions, aucun industriel n’ose ouvertement faire part de son intérêt pour ce secteur. « Ce sera comme pour la finance islamique. Quand le marché s’ouvrira, on aura des dizaines de projets », prédit Khalid Mouna, avant de conclure : « Nous payons aussi l’héritage de la colonisation. Il n’y a qu’à voir le retard pris par la France. Les pays anglo-saxons sont plus progressistes sur ces questions. »


De fait, c’est en Afrique australe qu’il faut chercher les avancées les plus importantes pour le développement de l’industrie du cannabis. Précurseur en ce domaine, le Lesotho, dirigé par le roi Letsie III, a adopté, dès 2008, une législation rendant possible la culture, la transformation et l’exportation de la

matekoane

(« cannabis », en langue sotho) pour un usage médical. À l’image du Maroc, le commerce du cannabis, bien qu’illégal, est depuis le XVIe siècle ancré dans les habitudes de ce petit pays de 2 millions d’habitants, et le trafic à destination du voisin sud-africain constitue un revenu complémentaire essentiel pour nombre d’agriculteurs.


En mai 2017, le gouvernement a attribué la première licence à la société Medi Kingdom, après lui avoir déjà donné, en février, une autorisation pour des travaux de recherche, inaugurant une nouvelle ère pour cette culture. Depuis, le Lesotho est devenu, en à peine plus de deux ans, une destination incontournable pour les investisseurs internationaux. Aujourd’hui, le pays se prévaut de la présence de plusieurs poids lourds cotés à la Bourse de Toronto. En juin 2018, le géant Canopy Growth, 6,4 milliards d’euros de capitalisation, annonçait l’acquisition pour environ 28,8 millions de dollars de la licence et des activités de la société Highlands, fondée par le Sud-Africain Jody Aufrichtig. Quelques semaines auparavant, Aphria, 1 milliard d’euros de capitalisation, créait une coentreprise avec la société Verve Dynamics, également détentrice d’une licence en échange d’un chèque de 2,7 millions d’euros. Toujours en 2018, le canadien Supreme Cannabis, dont la capitalisation atteint près de 200 millions de dollars, avait aussi acquis 10 % de l’entreprise locale Medigrow pour 10 millions de dollars. « C’est une énorme opportunité pour un petit pays comme le nôtre […]. Nous avons pour l’instant une dizaine d’entreprises opérationnelles sur le territoire, qui emploient 3 000 personnes », expliquait en septembre la vice-ministre de la Santé Manthabiseng Phohleli à l’AFP.


« Le faible coût de la main-d’œuvre, mais aussi le climat [trois cents jours d’ensoleillement par an, un taux d’humidité bas et un environnement peu pollué] sont autant d’arguments pour les entreprises qui investissent », détaille le journaliste Aurélien Bernard. À cela il faut ajouter une législation bienveillante pour les investisseurs étrangers en matière de taxes. D’ici à 2022, le gouvernement, qui a déjà octroyé 70 licences (vendues 30 000 dollars et renouvelables chaque année), espère la création de 30 000 emplois. Preuve de l’importance accordée à ce nouveau secteur, le souverain est venu en personne assister à l’inauguration des installations de Verve, au début d’août. Des plantations réalisées dans des serres où tous les employés portent blouses, masques et charlottes pour éviter les contaminations extérieures. « Ce sont quasiment des salles blanches, où l’environnement est contrôlé à presque 100 %. C’est essentiel pour obtenir les autorisations d’exportation vers le Canada, les États-Unis, l’Europe ou l’

Australie

», explique le consultant Benjamin-Alexandre Jeanroy. Un challenge que doit relever le Lesotho pour voir ce secteur, aujourd’hui uniquement tourné vers l’exportation, prospérer. La commercialisation locale, y compris pour un usage médical, n’est en effet pour l’heure pas encore prévue par la loi.


Ces exigences rendent très difficile l’accès de ce nouveau marché aux entrepreneurs locaux, condamnant tous les petits agriculteurs à rester dans l’illégalité. Grâce au canadien Supreme, Medigrow a pu investir 17,4 millions d’euros sur son site, qui compte actuellement 18 serres (5 000 m2). La société espère produire 1 000 litres d’huile de CBD, vendue entre 5 400 euros et 19 000 euros le litre. À terme, ses dirigeants prévoient un parc de 200 serres au bien nommé « royaume des cieux ». Si Canopy, face à la pression des investisseurs, a annoncé arrêter les acquisitions, d’autres poursuivent le mouvement. Cette année, l’américain Halo Labs a officialisé son intention d’intégrer les activités de Bophelo Bioscience &amp; Wellness, qui, sur 5 ha, compte produire 5 tonnes de cannabis, ce qui représente un chiffre d’affaires prévisionnel d’environ 46 millions de dollars. La première récolte est attendue pour le second trimestre de 2020.


Reste que le Lesotho ne gardera peut-être pas longtemps l’exclusivité de ce nouveau secteur. Au début de septembre, Harare a inauguré la première plantation de chanvre du pays. Et comme la patrie de feu Mugabe n’en est pas à une contradiction près, c’est dans l’enceinte de la prison que sont cultivés les premiers plants légaux du pays. Le projet est porté par une jeune dentiste, Zorodzai Maroveke, connue pour avoir milité ces dernières années pour une évolution de la législation. Les pieds cultivés seront utilisés pour leurs fibres. Le chanvre, pauvre en THC, peut servir à la fabrication de matériaux de construction, de vêtements, de papier, et même entrer dans la composition de batteries, selon des chercheurs de Clarkson University, à New York. Pour le Zimbabwe, la production de cannabis pourrait à terme représenter « une alternative à celle du tabac », l’une des principales sources de devises du pays, dont la consommation baisse, estime la ministre de l’Information, Monica Mutsvangwa. Parallèlement au chanvre, Harare aurait aussi approuvé en mars l’attribution de 37 licences vendues 50 000 dollars pour développer la production de cannabis à usage médical. Selon le rapport de Prohibition Partners, tous les projets doivent intégrer à leur tour de table un actionnaire local. En septembre 2018, la société britannique Nircam avait révélé avoir obtenu la première licence autorisant une culture industrielle à des fins thérapeutiques. Contactée par Jeune Afrique, elle n’a pas confirmé la poursuite de ce projet pour lequel elle disait avoir provisionné 10 millions de dollars.


Comme pour tous les secteurs innovants qui connaissent un fort développement, le cannabis, avec ses promesses d’argent rapide, constitue un terrain propice aux spéculateurs et aux aventuriers. Le consultant Nathan Emery constate aussi la prédation des élites proches du pouvoir. Au Lesotho, le Centre pour le journalisme d’investigation a révélé en août que deux fonctionnaires du ministère de la Santé, dont un directeur, avaient, en parallèle de leurs emplois officiels, accepté des fonctions exécutives au sein de l’entreprise britannique Afriag. Cette dernière, détentrice d’une licence au Lesotho, a par ailleurs fait entrer à son tour de table l’homme d’affaires controversé Paul de Robillard, accusé d’avoir été impliqué il y a une dizaine d’années dans un trafic de tabac en Afrique du Sud.


Au-delà des questions de gouvernance, Nathan Emery, interrogé par JA, estime que les producteurs africains auront des difficultés à se démarquer sur le segment de l’huile de cannabis en dépit des promesses de production à bas coût. « La culture sous serre restera chère, et personne ne connaît encore la taille du marché global. L’américain Aurora (3,6 milliards de capitalisation) vend actuellement à perte, car il est en surproduction. Au Lesotho, plusieurs sociétés comme Medigrow annoncent avoir obtenu des marchés au Canada ou en

Australie

. C’est invérifiable et ça sert avant tout à rassurer les marchés financiers », précise le consultant. Le spécialiste plaide pour le développement d’un marché africain approvisionné à partir de plantations en plein air, « mais structurer ces marchés prendra du temps, sauf peut-être en Afrique du Sud ». La nation Arc-en-Ciel et ses 57 millions d’habitants apparaît par conséquent comme le marché à ne surtout pas rater. D’ici à 2023, la libéralisation des usages (récréatifs et médicaux) pourrait générer localement un chiffre d’affaires de 1,8 milliard de dollars, selon Prohibition Partners. En 2018, la Cour suprême du pays a marqué les esprits en autorisant la production et la consommation récréative de cannabis. Les industriels attendent à leur tour une évolution de la législation.


Pour faire avancer leurs intérêts, les poids lourds du secteur comme Canopy n’hésitent pas à exercer un intense lobby auprès des membres de l’ANC ; l’écosystème se met en place, plusieurs cabinets d’avocats, comme les firmes Schindlers et Bowmans, proposant leurs services aux investisseurs. D’ici à 18 ou 24 mois, la loi pourrait évoluer en leur faveur, estimait en mars Nathan Emery. Depuis, quatre entreprises, dont l’américain Leafs Botanicals et les sud-africains Felbridge et House of Hemp (en partenariat avec Afriplex), ont reçu une licence leur permettant de cultiver du cannabis à usage médical, mais elles n’ont toujours pas l’autorisation de fabriquer des produits (huile, tablettes) à partir de leurs plantations. Elles pourraient le faire à partir de matières premières importées. Aujourd’hui, seuls les vendeurs de graines, de matériel de culture, et les brasseurs – en proposant des bières infusées au CBD – profitent de ce nouvel engouement.


Comme au Lesotho, les groupes canadiens sont dans les starting-blocks. Spectrum Therapeutics, filiale de Canopy Growth, a signé en juin un accord avec la ville du Cap pour s’installer sur 12 ha dans la zone économique Atlantis. L’entreprise prévoit d’y investir 38,5 millions d’euros pour faire pousser et transformer du cannabis. La Bourse de Johannesburg a elle aussi été rattrapée par cette fièvre cannabique. Le groupe Labat, qui y est coté, a annoncé en septembre l’acquisition de Knuckle Genetic, qui produit de l’huile et des fleurs de cannabis au Lesotho, ainsi que d’une usine pharmaceutique en Afrique du Sud pour fabriquer des produits à base de CBD.


D’autres pays comme la Zambie, dont la loi permet la culture pour un usage médical, mais qui n’a pas attribué de licence, le Kenya et l’Ouganda, où le débat sur la légalisation est ouvert, devraient participer eux aussi au développement du secteur. Plus au nord, les États se montrent encore réticents. Mais les idées progressistes fleurissent parfois là où on les attend le moins. Au début d’octobre, la presse a révélé que le groupe African Seeds, dont les promoteurs restent anonymes, avait reçu en 2017 et en 2018 l’autorisation du ministère malien de la Sécurité de cultiver du chanvre dans le pays en vertu d’une loi datant de 2001.



Julien Clémençot




L’Afrique, un marché potentiel de 7,1 milliards de dollars dès 2023




Le Lesotho, nouvelle terre promise des investisseurs internationaux




La Bourse de Johannesburg gagnée par la fièvre cannabique



]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008107.xml</field>
    <field name="pdate">2019-10-27T00:00:00Z</field>
    <field name="pubname">Jeune Afrique, no. JA3068</field>
    <field name="pubname_short">Jeune Afrique, no. JA3068</field>
    <field name="pubname_long">Jeune Afrique, no. JA3068</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Économie</field>
  </doc>
</add>
