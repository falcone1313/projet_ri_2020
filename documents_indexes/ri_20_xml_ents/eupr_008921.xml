<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">L'ALLEMAGNE DEVIENDRA-T-ELLE UN PAYS AGRICOLE ?</field>
    <field name="author">René Lauret.</field>
    <field name="id">eupr_008921.xml</field>
    <field name="description"><![CDATA[


Ce n'est pas sans surprise qu'on a pu lire dans une partie de la presse mondiale qu'à la suite des décisions de Potsdam l'Allemagne deviendrait une nation essentiellement agricole. Le communiqué final de la conférence ne dit rien de tel. On y trouve seulement cette petite phrase : " L'accord prévoit le développement de l'agriculture et des industries domestiques. -


Faut-il admettre qu'on aurait déduit cette conclusion de l'ensemble des dispositions concernant l'Allemagne? Elles prévoient des mesures sévères contre l'industrie, tant en raison du désarmement qu'au titre des réparations. L'U.R.S.S., qui a déjà fait des prélèvements sur l'outillage industriel dans sa zone, aura droit à 26 0/0 de celui des zones occidentales. Les États-Unis, le Royaume-Uni et les autres pays ayant droit aux réparations pourront également prélever sur les mêmes zones, dans une mesure qui n'est pas encore fixée, mais devra l'être dans six mois. Comme d'autre part l'industrie allemande a subi des destructions massives, et que l'accord de Potsdam prévoit " l'élimination ou le contrôle de toute industrie pouvant être utilisée à des fins militaires ", il y a lieu de penser que la puissance industrielle de l'Allemagne va se trouver fortement réduite.


Cette réduction coïncidera avec une diminution considérable de la main-d'oeuvre : des millions d'Allemands ont été tués ou grièvement blessés, d'autres travaillent à l'étranger comme prisonniers de guerre, des millions devront être employés à la reconstruction. Mais c'est là une situation temporaire : évaluons-la, approximativement, à dix ans (le terme est d'ailleurs sans importance). Quand les ruinée seront relevées dans les autres pays et en Allemagne, de nombreux travailleurs redeviendront libres. Il s'y joindra des classes nouvelles, également -nombreuses, intactes, qui atteindront alors l'âge adulte. Bref, la quantité de main-d'oeuvre disponible en Allemagne se rapprochera sensiblement du chiffre de 1938, avant l'annexion de l'Autriche : la plus grande partie de la population des provinces de l'est, annexées à la Pologne, reflue en effet vers l'ouest; il faudra même y ajouter celle des Sudètes, que les Tchèques s'apprêtent à expulser. Au total une dizaine de millions d'immigrants.


Quelle était, en 1938, la densité de population de l'Allemagne, la proportion de sa population ouvrière ? L'ancien Reich comptait 140 habitants au kilomètre carré (la France 75), dont 50 % se consacraient à l'industrie, 230/0 à l'agriculture, le reste aux autres professions (transports, commerce, etc.). Ces chiffres vont se trouver modifiés par les déplacements de frontières. Les provinces de l'est, beaucoup moins peuplées que les autres, étant enlevées à l'Allemagne, qui héritera toutefois une partie de leurs habitants, la moyenne kilométrique va se trouver fortement relevée : elle approchera sans doute de 200. L'Allemagne, Jusqu'à présent surpeuplée, le sera encore davantage.


Comment, dans ces conditions, pourrait-elle transférer de la main-d'oeuvre de l'industrie à l'agriculture? Les immigrants comprennent précisément un grand nombre d'agriculteurs qu'il faudra déjà caser; et l'on ne voit pas bien comment on pourra les caser, car il y a peu de place sur la terre allemande.


Le communiqué qui nous annonce " le développement de l'agriculture en Allemagne - semble ignorer que dès avant l'outre guerre elle y était plus développée que dans n'importe quel pays d'Europe, sauf, peut-être, le Danemark et la Hollande. Les rendements à l'hectare du blé, du seigle, de la pomme de terre, de la betterave étaient très supérieurs à ceux de pays plus fertiles, comme la France, la Hongrie ou la Roumanie. Les méthodes scientifiques de culture et l'emploi des engrais avaient presque atteint le maximum. Le nazisme, qui avait inscrit à son programme le retour à la terre et l'augmentation de la production agricole, enregistra dans ce domaine son plus grand échec, malgré des efforts désespérés : à la veille de la guerre la production avait légèrement baissé, la population agricole avait diminué de 1.200.000 âmes.


Il s'agit là de faits élémentaires, si notoires qu'on a quelque honte à devoir les rappeler. Il en résulte que dans une dizaine d'années l'Allemagne comptera 60 à 70 millions d'hommes, dont une proportion encore moins forte qu'en 1938 pourra s'employer dans l'agriculture, puisque la campagne allemande était déjà saturée, et que sa superficie sera plus petite. Que conclure, sinon qu'une main-d'oeuvre égale ou supérieure à celle d'alors - la moitié de la population allemande - serait sans emploi, si elle ne pouvait être occupée dans l'industrie?


C'est là le point capital, le coeur même de la question. L'Observer, adoptant la thèse de l'" Allemagne agricole ", déclare que l'économie allemande reposera désormais sur l'agriculture, la production du charbon et les industries légères: en somme elle serait comparable à celle du Danemark, l'Allemagne deviendrait " un Danemark multiplie ".


Notre confrère anglais oublie que le Danemark à 83 habitants au kilomètre carré, l'Allemagne 140 : là est toute la différence. L'Allemagne a en conséquence un excédent de main-d'oeuvre que l'agriculture ne peut utiliser; le Danemark n'a pas cet excédent. Le Danemark exporte des produits agricoles et paye, avec cette exportation, les matières premières qui lui manquent (textiles, pétrole, métaux, etc.). L'Allemagne a un déficit de produits alimentaires et ne peut payer ceux qui lui manquent, ainsi que ses matières premières, qu'en exportant des produite industriels.


Rien ne peut modifier cet état de choses, si ce n'est un changement radical de la situation démographique allemande. Donc l'industrie allemande sera reconstruite, elle renaîtra, à moins que ces dizaines de millions d'Allemands ne soient exterminés, ou transportés en Amérique, en

Australie

ou en Russie.


Il est vrai que cette reconstruction comporte une redoutable alternative; l'industrie allemande s'orienterait nécessairement vers les fabrications de guerre, ou vers l'exportation. En 1938 elle travaillait pour la guerre, l'Alternative n'exportait plus que pour 5.249 millions de marks. En 1929 elle travaillait pour l'exportation, l'Allemagne exportait pour 13.483 millions de marks. Qui sait si cette baisse de la concurrence allemande sur les marchés étrangers n'encouragea pas tel ou tel à fermer les yeux sur les armements de Hitler ?


Il faudra cependant prendre une décision. Si l'on veut démilitariser l'Allemagne, il faudra que les nations mêmes qui s'apprêtent à forcer leurs ventes à l'extérieur - en première ligne l'Angleterre et les États-Unis - acceptent un jour sa concurrence commerciale. Le problème qui se pose n'est pas tellement celui de la suppression de l'Industrie allemande que celui de son orientation; si elle s'oriente vers la paix, elle s'orientera en même temps vers les marchés mondiaux.


On préfère aujourd'hui n'y pas penser. Ayant quelques années devant soi, on aime mieux recourir à des slogans, comme l'" Allemagne pays agricole ". Mais on ne changera pas les réalités en refusant de les voir. Ceux qui préconisent cette politique étant, au fond, des réalistes, on aime à croire qu'ils n'ont pas dit leur dernier mot.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008921.xml</field>
    <field name="pdate">1945-08-21T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
