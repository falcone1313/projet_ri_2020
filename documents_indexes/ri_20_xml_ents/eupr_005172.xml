<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Ce qu'il faut retenir des Mondiaux de natation : des records et des remous</field>
    <field name="author">Elisabeth Pineau, Chloé Ripert</field>
    <field name="id">eupr_005172.xml</field>
    <field name="description"><![CDATA[


Les Championnats du monde de natation viennent de prendre fin à Gwangju, en Corée du Sud. Si la superstar américaine Caeleb Dressel a encore plus assis sa domination sur la discipline, avec six titres, il y a eu d'autres coups d'éclat... et quelques éclaboussures. Si vous n'avez rien suivi, pas de panique, on vous fait un résumé.



Sun Yang fait des vagues



A Gwangju, ce n'est pas dans les bassins mais en dehors qu'on a vu les plus gros remous. L'image, dès la deuxième journée des championnats, a fait le tour des médias du monde entier : l'Australien Mack Horton refuse de monter sur le podium pour recevoir sa médaille d'argent sur le 400 m nage libre, puis se tient à l'écart lors des séances photo. Et manifeste ainsi sa défiance à l'égard du vainqueur, Sun Yang (champion du monde sur la distance pour la quatrième fois d'affilée), au coeur d'une polémique après un contrôle antidopage rocambolesque.


Le nageur chinois,

déjà suspendu trois mois pour dopage en 2014,

est soupçonné d'avoir détruit à coups de marteau un échantillon sanguin au cours d'un contrôle inopiné, en septembre 2018. Un coup de sang qui ne lui a pas valu de sanction de la part de la Fédération internationale de natation (FINA), en raison d'un vice de forme, sidérant de nombreux nageurs comme Jérémy Stravius :



« C'est inadmissible de voir un nageur concourir et gagner alors qu'il s'est passé toutes ces choses avant, qu'il a un passé et un présent qui sèment le doute

.

Horton nous a montré l'exemple, ça va être un déclic. »



Le « feuilleton Sun » s'est prolongé toute la semaine. Mardi, victorieux sur 200 m nage libre après la disqualification du Lituanien Danas Rapsys, le Chinois s'est encore fait snober sur le podium. Cette fois, c'est le Britannique Duncan Scott, médaillé de bronze, qui a refusé de lui serrer la main, provoquant sa colère.


Ironie de l'histoire, la FINA n'a pas apprécié l'initiative d'Horton et de Scott, et leur a adressé une « lettre d'avertissement ».


Sun Yang, lui, doit être entendu en septembre par le Tribunal arbitral du sport, l'Agence mondiale antidopage ayant fait appel. Il risque la suspension à vie.



La nouvelle reine des bassins, c'est Titmus



On attendait Katie Ledecky, on a eu Ariarne Titmus. L'Australienne de 18 ans a réussi un exploit : mettre un terme au règne hégémonique de la quadruple championne olympique de Rio. Depuis les Mondiaux de 2013, jamais l'Américaine n'avait été battue sur 400 mètres nage libre dans un championnat majeur. C'était avant Titmus.


L'Australienne a devancé Ledecky en finale de plus d'une seconde (3'58''76 contre 3'59''97), après avoir réalisé une course épatante. En tête dans les deux premiers 100 mètres, elle a ensuite été reprise et dépassée par Ledecky. Jusqu'aux 350 mètres, tout donnait l'Américaine vainqueure. C'était sans compter la dernière longueur de Titmus. Après son virage, l'Australienne revenait à la hauteur de Ledecky et la dévorait dans les derniers mètres.


Ledecky devait nager le 1500 mètres, dont elle détient le record du monde, et le 200 mètres nage libre, mais elle a déclaré forfait sur les deux courses pour

« raisons médicales »,

selon sa fédération. Le 200 a été remporté par la patronne Federica Pellegrini, déjà triple championne du monde de la distance et qui avait décidé de raccrocher en 2017, devant nulle autre que Titmus.


Mais sur le 800 mètres, Katie Ledecky a pris sa revanche et rappelé qu'il ne fallait pas l'enterrer trop vite. L'Américaine s'est imposée devant l'Italienne Simona Quadarella. Ariarne Titmus a complété sa collection de médailles, en prenant le bronze, seul métal qui lui manquait sur ces championnats du monde.



Deux records du monde de Phelps tombés... à l'eau



Ce n'est pas un, mais bien deux records du monde de la légende américaine Michael Phelps qui ont été battus. Le premier, c'est le Hongrois Kristof Milak qui l'a fait tomber sur le 200 mètres papillon. Devancé par Chad le Clos dans le premier 100 mètres, Milak s'est envolé dans la dernière longueur. Il a réalisé un chrono de 1'50''73, larguant ses poursuivants à plus de trois secondes et battant le record de Phelps, qu'il détenait depuis 2009, de 78 centièmes ! Il est devenu le premier nageur à casser la barre des 1'51''.


Milak a reçu les éloges de Phelps, qui a réagi dans le


New York Times


:


« Aussi frustré que je sois de voir ce record s'effondrer, je ne pourrais pas être plus heureux en voyant de quelle manière il l'a fait. Les 100 derniers mètres de ce gamin étaient incroyables. Il a réalisé un super 200 m papillon du début à la fin. [...] Je lui tire mon chapeau. »


Mais Phelps n'avait pas fini de tomber des nues. Deux jours plus tard, Caeleb Dressel battait un autre de ses records : il nageait la demi-finale du 100 mètres papillon en 49''50, soit 32 centièmes de moins que Phelps à Rome en 2009 !


On a aussi vu, pour la première fois, un brasseur casser la barre des 57 secondes sur le 100 mètres : le Britannique Adam Peaty a nagé en 56'88'' en demi-finales avant de réaliser le doublé sur 50 et 100.


L'

Australie

, avec Ariarne Titmus, a battu le record du monde du relais 4 x 200 m nage libre en 7'41''50 devant les Américaines... et Katie Ledecky. Ce sacre met fin à leur règne, puisque les Américaines avaient été titrées à tous les Mondiaux depuis 2011.


Cinq autres records sont tombés : Chupkov sur le 200 m brasse en 2'06''12, la toute jeune Regan Smith (17 ans) sur le 200 m dos en 2'03''35, le relais mixte américain sur le 4 x 100 mètres en 3'19''40 et les Américaines sur le 4 x 100 4 nages en 3'50''40. Au passage, Regan Smith en a profité pour établir le nouveau record du monde du 100 m dos en 57''57.



Caeleb Dressel, nageur supersonique



Un record du monde de Phelps pulvérisé et six titres. Le bilan de l'Américain Caeleb Dressel est impressionnant,


et presque aussi bon qu'aux Mondiaux de 2017 à Budapest, où il avait remporté sept médailles d'or.


A Gwangju, il a remporté les titres sur 50 mètres nage libre, 100 m nage libre, 50 m papillon, 100 m papillon, en relais 4 x 100 nage libre et en relais mixte 4 x 100 nage libre. Il a aussi pris l'argent en 4 x 100 4 nages mixte et 4 x 100 4 nages.


S'il n'a pas battu le record du monde du 100 m nage libre, Dressel a tout de même accompli une performance incroyable : l'Américain a conservé son titre sur la distance reine en 46''96, devenant le troisième homme à passer sous la barre mythique des 47 secondes, le premier sans combinaison ! Il a devancé le champion olympique Kyles Cahlmers et Russe Vladislav Grinev.



Les Bleus boivent la tasse



Avec seulement 11 qualifiés (sept nageurs et quatre nageuses), l'équipe de France avait débarqué à Gwangju avec peu d'attentes, poursuivant sa reconstruction.


Mehdy Metella, médaillé de bronze mondial sortant du 100 m nage libre et double médaillé européen en 2018 (argent en 100 m papillon, bronze en 100 m nage libre), incarnait la meilleure chance de podium. Mais, le nageur de 27 ans a calé en demi-finales du 100 m nage libre. Deuxième meilleur performeur mondial du 100 m papillon derrière la fusée Caeleb Dressel (50''36), il n'a finalement pris que la cinquième place en finale.


Freinée par une blessure à l'épaule au printemps, Charlotte Bonnet, championne d'Europe en titre sur 200 m nage libre, a dû se contenter de la 7e place, loin derrière la reine Pellegrini.


Après avoir réalisé le troisième temps des demi-finales, Marie Wattel a pris la cinquième place du 50 m papillon en finale. Elle rate le podium pour trois petits centièmes, malgré un nouveau record de France (25''50).


Il a fallu attendre la 5e journée pour voir le compteur débloqué grâce à David Aubry, médaillé de bronze sur 800 m. L'élève de Philippe Lucas, 22 ans, a aussi obtenu la semaine précédente sa qualification olympique sur le 10 km en eau libre, tout comme Marc-Antoine Olivier.


Clément Mignon, Mehdy Metella, Charlotte Bonnet et Marie Wattel sont allés chercher la deuxième médaille française, du bronze, sur le relais mixte 4 x 100 nage libre derrière les relais américain et australien.


Deux ans après les deux médailles récoltées aux Mondiaux de Budapest,


la moisson est donc similaire à un an des Jeux olympiques. D'ici là, Florent Manaudou aura fait son retour dans l'équipe. Après quasiment trois ans sans compétition et une parenthèse handball, c'est pour la grand-messe tokyoïte que le champion olympique 2012 du 50 m nage libre a replongé.


Cet article est paru dans Le Monde (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005172.xml</field>
    <field name="pdate">2019-07-28T00:00:00Z</field>
    <field name="pubname">Le Monde (site web)</field>
    <field name="pubname_short">Le Monde (site web)</field>
    <field name="pubname_long">Le Monde (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
