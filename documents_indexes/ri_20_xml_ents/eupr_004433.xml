<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Paris : Le boss de la boutargue, le « caviar méditerranéen », officie depuis 30 ans à Ivry</field>
    <field name="author">Romain Lescurieux</field>
    <field name="id">eupr_004433.xml</field>
    <field name="description"><![CDATA[


GASTRONOMIE
Gérard Memmi, 62 ans, à la tête de l'entreprise familiale Memmi, sort cette semaine le livre « Boutargue » (Flammarion). Il a reçu « 20 Minutes » dans son usine de fabrication à Ivry-sur-Seine (Val-de-Marne) entre souvenirs, oeufs de mulets et alcool de figue


Cette poche d'oeufs de mulets, salés et séchés, enrobée d'une couche de cire, surnommée le « caviar méditerranéen » vit un retour de hype. Gérard Memmi à la tête de l'entreprise familiale Memmi basée à Ivry, sort cette semaine le livre « Boutargue » (Flammarion). Alors que certains s'énervent sur chocolatine/pain au chocolat, d'autres se prennent la tête sur l'orthographe de la boutargue. Boutargue ? Poutargue ? Gérard Memmi tranche.


« Lehaïm, comme on dit ». Il lève son verre de boukha (eau-de-vie de figues), trinque, puis s'affaire à couper au-dessus de son bureau boisé, de fines tranches orangées de boutargue. La coupe est nette, chirurgicale. « C'est tout un art », lâche-t-il, cigarillo en bouche et lunettes bleues teintées. A 62 ans, Gérard Memmi est à la tête de l'entreprise familiale Memmi, basée à Ivry-sur-Seine (Val-de-Marne) et spécialisée dans un mets raffiné : la boutargue. Cette poche d'oeufs de mulets, salés et séchés, enrobée d'une couche de cire, est surnommée le « caviar méditerranéen ».


C'est le métier, la passion de Gérard Memmi, qui sort cette semaine son livre Boutargue (Flammarion) d'ores et déjà prix Archestate 2019. S'il en parle avec passion et dévore son sujet, c'est qu'il baigne depuis qu'il est gamin dans ce produit mystérieux et ancestral dont les premières traces remontent au VIIe siècle avant Jésus-Christ. Avec son orthographe toujours débattue, la boutargue (poutargue ?) vit d'ailleurs un sacré retour de hype.


« En ce moment, le produit est très à la mode »


La boutargue, c'est la nouvelle toquade des chefs cuistots. « Pourquoi les chefs sont obsédés par la poutargue », titre l'Obs en juin dernier. « Recette : un oeuf à la boutargue pour changer de la truffe », publie en septembre Glamour. Produit haut de gamme, culturel, historique, cette poche d'oeufs habituée des tables de fêtes juives se retrouve de plus en plus dans les assiettes de restaurants. « Ça a toujours suscité curiosité et fantasme. Déjà le nom, puis l'emballage. Enfin le goût ne laisse pas indifférent. En ce moment le produit est très à la mode. Notre activité s'est beaucoup développée ces cinq dernières années », analyse Gérard Memmi.


Gérard Memmi et sa boutargue - R.LESCURIEUX / 20Minutes


Memmi est l'un des plus anciens fabricants de boutargue de la capitale. Chaque année, il sort vingt tonnes de ce produit de son usine, avec un prix moyen de vente à 140 euros le kilo. Et ce, à destination des distributeurs, des restaurants, des épiceries fines, des poissonneries, de Rungis. Et dans le monde entier. Connue à Martigues, Bastia, Murcie, Izmir, en Sardaigne, très appréciée aussi en Asie (Japon et Taïwan), chacun déguste la boutargue à sa sauce.


« Râpée, en toast, accompagnée de pommes de terre, ou à l'apéro avec un alcool de figue pour les Tunisiens, la boutargue est aussi consommée avec de la manzana en Espagne », détaille Gérard Memmi, fier. Sur son fond d'écran d'ordinateur, une photo de boutargue s'affiche. Au mur, des prix, des trophées, et toujours une boutargue sous le coude. A tout moment, il en dégotte une au fond d'un tiroir de son beau bureau d'Ivry. « C'est beau mais ça n'a pas toujours comme ça », claque-t-il avec sa gouaille.


« A l'époque, on ne trouvait pas de boutargue à Paris »


Quand Elie Memmi, champion tunisien de water-polo débarque en 1967 à Paris avec femme et enfants, c'est la galère. Pour Gérard Memmi, c'est un souvenir de « froid, de grisaille, de pauvreté, d'abandon, d'immensité ». Le gamin de Tunis alors âgé de 10 ans est « perdu dans Paris, cette grande ville que je ne connaissais pas ». « On était fauchés. Je faisais la fin des marchés avec ma mère pour ramasser les invendus », se souvient-il. Son père tente de fabriquer de la harissa. En vain.


Un jour lors d'un salon alimentaire, il voit des oeufs de mulet congelés. « On ne trouvait pas de boutargue à Paris alors que c'était très consommé en Tunisie », rembobine Gérard Memmi. Avec l'aide financière de la famille, il achète 200 kilos d'oeufs de mulet venant d'

Australie

et se précipite chez un ami musulman, qui a un magasin oriental, porte de la Chapelle (18e), pour faire ses boutargues dans l'arrière-salle. Bingo. La sauce prend, les ventes s'envolent notamment auprès des juifs séfarades qui débarquent alors dans la capitale.


Elle est séchée durant deux à quatre jours - R.LESCURIEUX / 20Minutes


Puis, en 1989, la famille Memmi passe le périph'et débarque à Ivry-sur-Seine dans une ancienne serrurerie. De son côté Gérard Memmi plaque tout et rejoint l'entreprise familiale pour en prendre la tête en 1994 et devenir le boss de la boutargue (poutargue ?). Alors que certains s'énervent sur chocolatine/pain au chocolat, d'autres se prennent en effet le chou sur l'orthographe de la boutargue. Boutargue ? Poutargue ? En provençal boutargo. En arabe batlowdotarikh. Questionné sur le sujet, Gérard Memmi tranche. « En France c'est connu sous le nom de poutargue, mais les puristes disent boutargue. C'est le même produit. Mais moi je dis boutargue », flingue-t-il.


« La boutargue est une poésie »


Dans son usine, on fabrique « à l'ancienne », « avec les normes », mais « à l'ancienne ». « On fait tout à la main », dit Gérard Memmi, dans les allées de sa production. Là, près de 15 salariés s'affairent. Décongélation, salage, dessalage, nettoyage... la boutargue est ensuite dénervée puis séchée entre deux et quatre jours, plongée dans la cire, emballée et expédiée. Prête à déguster. Prête à faire voyager. « J'ai déjà vu des gens pleurer en mangeant de la boutargue car ça leur rappelait leurs grands-parents, les jours de fête »


15 salariés travaillent dans l'usine d'Ivry - R.LESCURIEUX / 20Minutes


« La boutargue est une poésie. Une sainte poésie. C'est une promesse de partage, même lorsqu'on en consomme seul ; c'est une promesse de fête même si c'est un jour normal ; c'est une promesse de rêve même si ce n'est que du poisson », écrit dans la préface du livre de Gérard Memmi, Haïm Korsia, grand rabbin de France. Parfois Gérard Memmi ne mange pas de boutargue pendant vingt jours et va soudain s'en taper une entière dans le canapé, sans remords, ni regrets. La boutargue, c'est sa vie, c'est son père. « A travers ce livre, je voulais transmettre l'héritage fabuleux de mon père et lui rendre hommage », sourit-il, un peu crispé par les émotions mais les yeux toujours cachés par ses lunettes bleues teintées.


Cet article est paru dans 20 Minutes (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004433.xml</field>
    <field name="pdate">2019-10-27T00:00:00Z</field>
    <field name="pubname">20 Minutes (site web)</field>
    <field name="pubname_short">20 Minutes (site web)</field>
    <field name="pubname_long">20 Minutes (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Paris</field>
  </doc>
</add>
