<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">VIII.- Le redressement britannique en Malaisie</field>
    <field name="author">ROBERT GUILLAIN</field>
    <field name="id">eupr_009065.xml</field>
    <field name="description"><![CDATA[


Quelque part dans une Asie verte, toute mousseuse de végétation tropicale, une petite ville miraculeuse. On est au fond de la jungle, mais les Anglais roux jouent à leur cricket paresseux sur l'éblouissant tapis d'émeraude des gazons rasés, tandis qu'applaudit joyeusement un public d'indigènes bruns en sarongs. Dans les ruelles de la ville, propres comme celles d'un village du comté de Kent, Chinois et Malais semblent faire bon ménage. Des clubs normands font face à des palais hindous ou mauresques servant de gare ou de Parlement; des cinémas américains voisinent avec les boutiques chinoises. Tout autour, décors d'un Hollywood tropical, lacs coupés de cascades, collines couronnées de palmes. Au plus haut, dans un jaillissement de verdure, la résidence du haut commissaire, hissant sur le ciel fantasmagorique un grand drapeau anglais. En ville les tireurs de pousses se rangent et s'arrêtent quand passe, cabossée d'une énorme couronne royale d'or, la limousine lente du représentant de Sa Majesté. Et l'invité, qui se rend à la résidence, honoré du titre d' " hôte d'État ", ordonne à son chauffeur : " A la maison du roi ! "


C'est le jeu presque désuet de l'heureuse colonie, reconstitué ou prolongé à Kuala-Lumpur, capitale de la Fédération malaise. Nous sommes à moins d'un jour d'avion de la Birmanie, où l'incendie a éclaté, de Saigon, où les choses ont tourné au tragique, de Java, où anciens maîtres et sujets libérés ne s'entendent que pour se détester. Ce cricket au fond de la jungle, affirmant tranquillement la solidité britannique, est aussi éloquent sans doute que le spectacle de Singapour, où nous terminerons le voyage, de Singapour qui proclame orgueilleusement par ses croiseurs et ses troupes, par son mouvement et son argent, le retour de l'Angleterre.


Chinois et Malais


Mieux que Singapour Kuala-Lumpur montre l'envers du décor. Comme ailleurs dans l'Asie du Sud-Est le Japon est passé par ici, " politisant " les couches supérieures de la population. La Malaisie a cessé d'être le pays sans histoire et sans politique. On y découvre un monde de partis divers, et c'est à Kuala-Lumpur qu'il faut venir pour pénétrer dans ce dédale. C'est à " K. L. " aussi qu'il faut être, en ce mois de juin où commencent dans les bois d'hévéas et là jungle proche les coups de force et les assassinats de planteurs, pour mesurer le danger de la rébellion communiste et comprendre son véritable caractère : une opération de sabotage.


Les deux races qui se côtoient ici, Malais et Chinois, auxquels s'ajoute une minorité de travailleurs indiens dans les plantations, font de la Malaisie un " pays sans nation ". Sur tous les terrains, les autochtones malais sont en état d'infériorité sur les immigrés. Ils sont seulement 2.500.000, alors que les Chinois sont 2.600.000, et les Indiens 600.000. Leur éducation et leur formation politiques sont en retard. Il a fallu attendre l'Angleterre travailliste d'après la guerre pour qu'on s'avise d'utiliser ce peuple aimable mais indolent. Les conservateurs d'autrefois, en quête de bénéfices rapides, ont préféré importer des Chinois, laborieux et vifs : ceux-ci ont pris tout le commerce, petit et grand, la banque et l'argent, et se sont installés dans les régions les plus riches et à Singapour, façade sur la mer, tandis que les Malais étaient relégués dans les kampongs (villages de paillotes) de l'Est pauvre et sauvage. Et malgré les efforts de certains nationalistes après la guerre pour inventer une nation et une citoyenneté " malaisiennes " unissant Malais et Chinois, ces derniers en fait de sentiment national ne connaissent que leur appartenance à la Chine; Ils en reproduisent d'ailleurs exactement dans leur communauté les luttes intestines entre Kouomintang et communistes.


S'il est un lieu d'Asie où la Grande-Bretagne peut encore imposer son arbitrage c'est bien la Malaisie. Dès 1945 elle y revient résolument avec ses troupes et ses fonctionnaires. Trois raisons lui interdisent la démission qu'elle prépare au même moment aux Indes. Raison de prestige : il lui faut effacer le souvenir de la déroute de 1942 le long de ces belles routes de la jungle. Raison économique : les grandes trouées rouges des mines d'étain d'alentour et les forêts artificielles d'hévéas, qui saignent aujourd'hui assez de latex pour fournir plus de la moitié du caoutchouc du monde, c'est pour l'Angleterre la plus riche source de dollars de toute la zone sterling. Raison stratégique enfin : Singapour ne redevient pas seulement la porte commerciale de l'Extrême-Orient, il est plus que jamais le Gibraltar asiatique, indispensable à la vieille Angleterre, et plus encore à la neuve

Australie

, terre d'avenir du monde britannique...


La nouvelle Constitution


Neuf sultans sont venus diner chez le gouverneur de la Fédération. Les aides de camp écossais et malais annoncent leurs beaux noms, qui sont ceux de leurs États : Selangore et Perlis, Johore. Trengganu, d'autres encore. Orchestre au dehors, sur les pelouses. Pleine lune. Sur les sombres soies des sarongs portés par les hommes et les femmes, éclairs des ors brodés et des bijoux. Des sultanes aux yeux noirs - mais l'une est une blonde Hongroise - caressent les orchidées dans les vases de cristal. C'est dans un anglais impeccable que ces princes et leurs ministres discutent avec les hauts fonctionnaires anglais sur la conférence des sultans qui vient de s'ouvrir à " K. L ". Diner de quarante couverts, s'achevant par un toast au roi George. Mais c'est en parfait langage malais qu'un Anglais prononce un discours : car ceux que l'Angleterre travailliste envoie depuis la guerre ont commencé par s'asseoir un an, à Londres, sur les bancs de l'école des langues orientales. Tout dit ici l'effort britannique pour se concilier la faveur des Malais et s'appuyer sur eux dans la nouvelle Constitution qu'ils viennent de promulguer.


C'est en effet au début de 1948, après deux ans de tâtonnements, que prend corps le régime politique qui va donner à Kuala-Lumpur et à l'arrière-pays malais une importance accrue. Deux unités séparées sont créées. Singapour est confirmé dans son ancien statut de colonie - dernière colonie anglaise en Asie, et symbole de la permanence britannique. Les neuf sultanats de la péninsule forment d'autre part la Fédération de Malaisie, capitale Kuala-Lumpur. Étant maintenant séparés de Singapour, qui compte 700.000 Chinois, les Malais retrouvent la majorité sur les Chinois dans la Fédération. L'Angleterre accorde à celle-ci, avec un conseil fédéral élu, des franchises politiques, qui sont à peu près celles de l'Inde de 1935, et l'achemine vers le statut de dominion. Avant d'en arriver là, elle veut évidemment profiter du fait que le relèvement des Malais par rapport aux Chinois est pour le moment rendu plus facile par la faiblesse de la Chine, ravagée par la guerre civile.


La rébellion


Tout cela explique la rébellion qui vint soudain, au début de juin, troubler la paix britannique en Malaisie. Elle procéda pour une part du mécontentement des Chinois, et d'une infime minorité malaise, contre le nouveau statut politique. Elle est en même temps un règlement de comptes entre Chinois : le parti communiste malais, qui en fait recrute presque exclusivement parmi les Chinois, a perdu au profit du Kouomintang local la direction de la communauté chinoise qu'il avait prise pendant l'occupation japonaise lorsqu'il était l'animateur de la résistance. Encouragé par les succès de Mao Tsé Toung en Chine, il déclenche la guerre civile chez les Chinois de Malaisie. Il s'agit enfin et surtout de contribuer à la lutte internationale du communisme, en sabotant dans le domaine politique le redressement britannique et la cristallisation de l'influence occidentale autour d'une Malaisie prospère, et dans le domaine économique l'exploitation d'une source de matières premières indispensables à l'Angleterre, à l'Europe occidentale et aux États-Unis.


S'il entre enfin dans la rébellion une part de mécontentement contre les difficultés économiques, car les coolies pauvres se plaignent de la pénurie de riz et de l'insuffisance des salaires, il est clair que ce sentiment est exploité par les meneurs communistes dans une aventure où les travailleurs ont tout à perdre. La réaction énergique de l'Angleterre, qui met le parti communiste hors la loi, fait venir des troupes de Hong-Kong et de Londres et des armes d'

Australie

, qui dissout ceux des syndicats où les extrémistes avaient pris le dessus, ne peut laisser aucune illusion. Et le parti lui-même doit subir des pertes graves dans ses rangs.


Il a pour lui la jungle et cette autre jungle impénétrable qu'est la population chinoise. Il se révèle bien organisé pour l'action : larges groupes de guérillas fortement armés pour le combat de front contre la police ou les troupes; petites unités de tueurs et de saboteurs; réseau d'agitateurs, à la tête desquels on cite trois chefs - tous Chinois - Wou Tien Wang, Tchang Hing Tcheng, le plus fanatique, et Liou Yit Fun : ce dernier, qui assista à l'automne 1947 à la conférence communiste du Commonwealth à Londres, aurait été abattu dès le début de l'insurrection.


Mais des forces importantes, entraînées progressivement au combat de jungle, sont déployées par les Anglais contre les rebelles. Les troupes de l'insurrection ne comptent guère, selon les évaluations faites à Kuala-Lumpur, plus de cinq mille à six mille partisans. Ceux-ci sont loin enfin d'avoir le soutien populaire qui caractérise les mouvements nationalistes du Vietnam ou de l'Indonésie : encore une fois, il ne s'agit pas ici d'une révolte nationaliste. Les Malais savent bien que la victoire du mouvement ferait de la Malaisie un État chinois et communiste où leur race aurait définitivement perdu la partie. La grande ligue des partis de gauche (le Council of Joint Action) dont l'élément dominant était composé de Chinois non communistes se désagrège. Et la majorité de la communauté chinoise reste neutre : la fermeté britannique a d'ores et déjà écarté la menace de son ralliement à la cause des rebelles. En définitive, parce que les Anglais ont réagi immédiatement, l'affaire est beaucoup moins difficile à régler pour eux que nous l'affaire d'Indochine; lentement, mais sûrement, l'ordre doit être rétabli d'ici l'été prochain.


Travaillisme contre communisme


Il ne faut pas croire enfin que les Britanniques se satisfassent en Malaisie d'un recours à la force. Ici comme ailleurs en Asie l'Angleterre travailliste a donné à sa politique à l'égard des peuples de couleur une orientation nouvelle et hardie. Son principe est non seulement de donner des responsabilités croissantes aux autochtones et de former des équipes pour l'administration, le gouvernement... et les affaires, mais d'encourager même la naissance d'une opposition organisée et constitutionnelle. En dépit des résistances des gros intérêts capitalistes et conservateurs de Singapour et de l'arrière-pays, qui l'accusaient de faiblesse, elle a laissé le champ libre aux partis indigènes. Un de ses efforts les plus intéressants a été d'aider la formation d'un mouvement syndical échappant à la tutelle communiste. Les trade-unions britanniques ont elles-mêmes envoyé en Malaisie leurs délégués pour guider et éduquer un syndicalisme authentique selon les méthodes travaillistes, et veiller à ce qu'il ne dégénère pas en un mouvement officieux et " jaune ". Enfin c'est par des méthodes modernes, où la radio et le film ont un grand rôle - des " unités mobiles " portent le cinéma jusqu'au fond de la jungle - qu'elle a organisé l'information et l'éducation de la population chinoise et malaise.


En face des mouvements d'opposition l'Angleterre veut éviter les erreurs commises ailleurs dans l'Asie du Sud-Est - erreurs dans lesquelles l'Inde libérée est elle-même menacée de tomber. Elle a pensé qu'il fallait avant tout éviter de rejeter dans le camp des extrémistes ceux qui ne voulaient pas collaborer avec elle, et compris quel péril comporte une politique qui laisse au communisme le monopole des revendications économiques, politiques et sociales. Elle a jugé enfin que le problème fondamental dans cette région était de savoir distinguer, quand il en était encore temps, entre le nationalisme indigène et le communisme international, pour être libérale avec le premier, intraitable avec le second. Et ses représentants reconnaissent d'ailleurs qu'ils ont été éclairés par l'expérience indochinoise, où les hésitations et la crainte des solutions neuves ont abouti à forger l'alliance des nationalistes et des communistes, à transformer en communistes définitifs - voir le cas de Ho Chi Minh - des adversaires qui avaient été longtemps " rattrapables " - et ne le seront plus désormais...


(A suivre.)


(1) Voir le Monde des 55, 23, 25 , 26-27, 28 septembre, 6 et 7 octobre 1948.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_009065.xml</field>
    <field name="pdate">1948-10-08T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
