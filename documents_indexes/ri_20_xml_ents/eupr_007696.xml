<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Tim Flannery «L'Europe a été le noeud crucial de l'évolution des espèces et de la naissance du monde moderne»</field>
    <field name="author">Sonya Faure, Thibaut Sardier</field>
    <field name="id">eupr_007696.xml</field>
    <field name="description"><![CDATA[



Auteur d'un ouvrage sur l'histoire naturelle du Vieux Continent, le paléontologue australien explique quelle fut l'importance et la richesse de la faune sur ce territoire. Une immense biodiversité aujourd'hui disparue. Il milite pour le «réensauvagement» de l'espace européen.



Des girafes tendent leur cou vers le ciel, mais gardent un oeil sur ce troupeau d'éléphants aux trompes semblables à celles d'un tapir. A quelques mètres de là, on croise des cervidés, des éléphants encore, ou... des chiens-ours. Et des animaux encore plus bizarres, comme les chalicothères, mammifères à la tête de cheval et au corps de gorille. Ce drôle de bestiaire, on l'imagine façon Roi Lion dans une savane africaine fantasmée. Pourtant, c'est bien en Europe qu'on aurait pu l'observer, si on avait vécu au miocène, de 23 à 5,3 millions d'années avant notre ère. Le continent a depuis perdu cette immense biodiversité. Le paléontologue australien Tim Flannery nous rafraîchit la mémoire avec


Supercontinent. Une histoire naturelle de l'Europe (Flammarion, 2019). Dans un voyage au fil des 100 derniers millions d'années, il montre que l'Europe a toujours joué un rôle de carrefour, accueillant des espèces nombreuses et surprenantes. Il invite à renouer avec ce passé en «réensauvageant» nos territoires... ou en ressuscitant des espèces disparues.



Si vous deviez ressusciter un animal disparu, ce serait l'anisodon, un drôle de quadrupède de 1,50 m au garrot. Pourquoi lui ?



Parce que cet animal de la fin du miocène est une énigme : rien de semblable n'existe aujourd'hui ! Imaginez une créature dont la démarche est similaire à celle d'un grand singe, avec une tête de cheval, un cou d'Okapi et des bras de gorille ! J'aimerais le voir et comprendre ce qu'il faisait d'un tel corps.



Pourquoi est-il important, pour penser la biodiversité actuelle, d'observer l'Europe des millions d'années en arrière ?



Nous devons comprendre l'histoire des continents pour apprécier pleinement les forces qui continuent d'agir sur nous. Vous vivez sur un continent qui est le carrefour du monde... depuis 100 millions d'années ! L'Europe s'est alors individualisée sous la forme d'un archipel qui a servi de passage entre trois grandes masses terrestres : l'Afrique, l'Asie et l'Amérique du Nord. Les espèces qui ont migré de l'Asie à l'Amérique du Nord sont passées par l'Europe dont elles ont modifié les paysages. Ce fut la même chose après la «grande extinction» qui a mis fin aux dinosaures il y a 66 millions d'années. Quelques millions d'années après cette crise, les ancêtres des mammifères les plus courants dans le monde d'aujourd'hui sont venus d'Asie vers l'Europe. C'est là qu'ils se sont transformés en chevaux ou en chameaux avant de migrer en Amérique du Nord... et de revenir 50 millions d'années plus tard en Europe et en Asie ! L'Europe a bien été le noeud crucial de l'évolution des espèces et de la naissance du monde moderne. Son écologie très particulière a aujourd'hui encore des conséquences sur les êtres humains qui la peuplent, sur leur agriculture, leurs matériaux de construction et même sur la physionomie des villes.



Le continent abrite aujourd'hui encore des espèces primitives et pourtant largement sous-estimées, comme le loir...



Cet animal est un véritable fossile vivant, qui appartient au plus ancien lignage de mammifères européens. Pensons aussi au triton et au crapaud accoucheur, les plus vieux vertébrés européens : leurs ancêtres ont vécu au temps de l'hatzegopteryx [un prédateur volant de 10 mètres, ndlr]. Ces espèces survivent depuis 100 millions d'années, sans doute parce qu'elles sont amphibiennes : lors de l'extinction des dinosaures, l'environnement aquatique a fourni un refuge pour de nombreuses espèces. Mais comment ont-ils réchappé à chaque grand changement climatique ou environnemental comme l'âge de glace, la domestication des espèces, le déboisement ? C'est un mystère.



Vous expliquez que l'Europe a vu se développer une grande biodiversité avec l'éocène, il y a trente-quatre millions d'années. Cette ère est marquée par une augmentation du carbone atmosphérique et donc une augmentation des températures...



C'est juste. L'augmentation de la température a ouvert des brèches à de très hautes latitudes qui ont permis aux créatures d'accéder à un environnement plus chaud. A ces conditions climatiques se sont ajoutés des facteurs tectoniques qui ont fait émerger des régions continentales et créé des ponts de terre entre ces continents auparavant séparés par l'eau.



A l'aune de cet exemple, avons-nous raison d'être inquiets du réchauffement climatique ?



Pour étudier des évolutions géologiques si lointaines, nous ne nous attachons pas à une année, un siècle ou un millénaire. Nous analysons de très longues périodes de 100 000 à un million d'années, qui n'ont rien à voir avec les temporalités humaines. Il se peut donc très bien que d'ici un million d'années, la présence des animaux en Europe prenne des formes tout à fait intéressantes. Mais pour les individus que nous sommes, le réchauffement que nous expérimentons aujourd'hui aura un impact négatif.



Quelle est pour vous la dernière grande rupture de l'histoire de l'Europe en matière de biodiversité ?



La fin de la dernière avancée glaciaire, il y a près de 12 000 ans, est un grand changement car une nouvelle Europe se révèle alors, libérée de la glace. La faune et la flore peuvent à nouveau s'y développer. Mais, à mon sens, le continent s'est vraiment transformé quand le nombre d'humains a commencé à augmenter au néolithique, et surtout depuis les deux derniers siècles. Il n'y a aujourd'hui quasiment plus un centimètre carré de terre arable qui ne soit pas utilisé en Europe. Le sol européen est devenu avant tout un artefact humain.



C'est ce qui frappe à la lecture de votre livre : très tôt, les humains ont eu un impact majeur sur leur environnement...



Les êtres humains et leur niche écologique ont sans cesse pris plus de place. Les Néandertaliens coexistaient avec un grand nombre d'autres prédateurs comme les lions ou les léopards. Mais lorsque Sapiens et Néandertal cohabitent en Europe et s'hybrident, vers -40 000 environ, les populations animales se mettent à diminuer réellement. Ces chasseurs semblent plus efficaces que tous les autres grands carnivores. L'Europe perd alors ses hyènes et ses lions, mais aussi ses grands herbivores chassés du continent. Les humains ont éliminé les grands mammifères de manière très systématique. Le dernier boeuf musqué européen est mort en Suède il y a environ 9 000 ans, puis après une longue pause, une nouvelle vague d'extinctions s'est abattue sur l'Europe au milieu du XVII e siècle : l'auroch et les chevaux sauvages européens n'y ont pas survécu. L'humain entraîne donc une simplification de l'écosystème. Mais dans le même temps, il amène de nouveaux organismes, plantes ou insectes, parfois même mammifères et oiseaux. Et de nouvelles hybridations apparaissent. Le moineau cisalpin est un bon exemple : cet hybride apparaît avec la domestication en Italie il y a 10 000 ans.



Quelle est l'influence de la domestication sur l'hybridation des espèces ?



La domestication est arrivée en Europe avec un nouveau groupe d'humains venus d'Asie occidentale. Ils ont importé leurs aurochs pour y former des troupeaux domestiques, mais nous savons aujourd'hui que ces bêtes se sont reproduites avec des aurochs sauvages européens. Les recherches ADN ont montré qu'un peu des gènes des aurochs britanniques sauvages ont été incorporés dans les troupeaux domestiques. Idem pour les cochons. Un peu de la vieille Europe a survécu, cachée dans les gènes de certains organismes.



Les récents progrès de la recherche sur l'ADN ont révolutionné l'étude des écosystèmes anciens. En quoi cela a-t-il changé notre regard sur le passé ?



L'étude de l'ADN ancien a beaucoup progressé ces cinq dernières années et les chercheurs européens ont été à la pointe de ces avancées. Elle a notamment montré que l'hybridation entre les espèces a été essentielle dans le succès de l'évolution. On sait désormais que les Européens sont nés d'une hybridation entre des peuples humains africains et des Néandertaliens. Ou que les grands mammifères européens comme le bison d'Europe sont des hybrides entre les bisons des steppes et les aurochs. Les recherches ADN mettent en lumière les détails des migrations, des rencontres, des partages de gènes qui ont permis aux espèces de s'adapter à l'évolution de leur environnement. Elles nous donnent une version totalement nouvelle de l'histoire de l'Europe.



Aujourd'hui, faut-il recréer des espèces disparues grâce aux manipulations génétiques ?



Je suis un peu aventurier. J'aime penser à une Europe plus sauvage et fantastique. Nous pourrions par exemple utiliser les gènes cachés dans les corps de notre bétail actuel ou dans l'ADN des ours pour faire revivre des créatures disparues ! Mais avant cela, il faudrait réintroduire tous ces animaux qui ne sont pas éteints, mais qui ont disparu du continent, comme les lions, les léopards, les hyènes et même les éléphants qui ne survivent aujourd'hui que dans les forêts d'Afrique de l'Ouest.



Faut-il réensauvager l'Europe ?



Le réensauvagement est intéressant car c'est un processus naturel. De vastes lieux ont d'ores et déjà été abandonnés en Europe, comme les monts métallifères de Toscane, près de Sienne et Grossetto. Là-bas, la nature commence à se reconstituer elle-même, formant un maquis et une forêt très diversifiée de chênes, de houx et de châtaigniers. Le sous-étage de cette forêt qui renaît nourrit les cerfs, les chevreuils et les daims. Mais on voit aussi les limites de ce que la nature peut faire quand certaines espèces manquent : en l'absence de lynx, les cervidés sont trop nombreux et ravagent la flore. Seul le genévrier subsiste... si rien n'est fait, un environnement incroyablement divers et complexe sera perdu. Les humains ont donc un rôle à jouer, car réensauvager ne peut se limiter au fait d'abandonner ces terres.



Les Européens pourraient vivre auprès d'animaux sauvages ?



En voyageant, j'ai vu des Africains complètement à l'aise avec des lions à proximité de chez eux. Ils sont conscients du danger, font attention, mais ils comprennent que des grands mammifères partagent leurs paysages. En

Australie

, nous devons vivre avec des crocodiles et des requins. Je suis persuadé que l'idée que les Européens aient un jour à coexister avec des lions ou des hyènes est possible.



C'est tout de même mal parti quand on voit, en France, la difficile cohabitation entre les éleveurs et le loup...



Mais combien d'Européens sont effectivement tués par des loups ? Le problème est dans votre tête, pas dans la réalité.



Est-il possible de voir aujourd'hui encore apparaître de nouvelles espèces ?



A l'échelle d'une vie humaine, nous avons vu s'étendre en Europe le chacal doré. Ce grand carnivore, qui est arrivé en Europe quelque part dans les années 50, a maintenant atteint la Belgique. Nous avons aussi vu apparaître les chiens viverrins. Donc oui, de nouvelles espèces peuvent apparaître sur le continent. La nature est très difficile à anéantir, tout spécialement en Europe. Elle est tenace!


ARCHIVES: La galerie de paléontologie et d'anatomie comparée est un des établissements du Muséum national d'histoire naturelle français (MNHN). Elle se trouve dans le Jardin des plantes de Paris, France, à côté de la gare d'Austerlitz. Le bâtiment a été conçu entre 1892 et 1898 par l'architecte Ferdinand Dutert. Le 13 Mai 2007. ARCHIVES: Paleontology Gallery and comparative anatomy is one of the institutions of the French National Natural History Museum. It is located in the Jardin des Plantes in Paris, Facebook Twitter.


Cet article est paru dans Libération (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007696.xml</field>
    <field name="pdate">2019-12-30T00:00:00Z</field>
    <field name="pubname">Libération (site web)</field>
    <field name="pubname_short">Libération (site web)</field>
    <field name="pubname_long">Libération (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
