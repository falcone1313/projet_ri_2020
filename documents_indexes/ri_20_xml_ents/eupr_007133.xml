<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Hsieh Su-wei, la grâce du papillon</field>
    <field name="author">LAURENT FAVRE</field>
    <field name="id">eupr_007133.xml</field>
    <field name="description"><![CDATA[


6019


, MELBOURNE @LaurentFavre


Enchâssés au milieu de la quinzaine, les huitièmes de finale constituent le moment du tournoi où les rêveurs cèdent la place aux ambitieux. C'est l'heure du rappel à l'ordre. En rang deux par deux les enfants, les grandes personnes sifflent la fin de la récré.


À l'Open d'

Australie

, après avoir éliminé la tête de série numéro 3, Garbiñe Muguruza, et l'ancienne numéro 2 mondiale Agnieszka Radwanska, la Taïwanaise Hsieh Su-wei (32 ans) risquait fort l'élimination dans le tableau féminin. Face à elle, pas moins que la meilleure joueuse actuelle. Angelique Kerber n'est certes que tête de série numéro 21 et seizième au classement technique de la WTA mais l'Allemande est invaincue cette saison (12 victoires consécutives, le titre à Sydney) et sa condition physique est impressionnante. Nous devions donc en rester là.



Des points inouïs à la pelle



Et ce fut le cas. Hsieh Su-wei a perdu. Une défaite en trois manches (6-4 5-7 2-6), le genre à ne valoir qu'un entrefilet que seuls les carnivores de l'information auraient lu ou une simple mention dans un papier sur « la qualification dans la douleur d'Angelique Kerber » . Ce n'était somme toute qu'une défaite de plus, l'élimination d'une fille inconnue, asiatique, dans un huitième de finale du tableau féminin (circonstance aggravante?), au beau milieu de la nuit européenne, dans un match que personne n'a vu...


A quel moment a-t-on dit non? A quel moment s'est-on promis de parler d'elle, quoi qu'il advienne? A 4-4, 15-30 dans la deuxième manche, quand Hsieh Su-wei fit monter Angelique Kerber au filet, pour la lober puis lui servir une nouvelle amortie? Il y eut tellement de points incroyables... A la réflexion, ce fut sans doute à l'instant où, après 80 minutes d'un tennis éblouissant, il devint évident qu'elle allait tout de même perdre, ramenée à la raison par la puissance et l'endurance de son adversaire. Tant de grâce et de beauté ne pouvaient rester ignorées. Vous deviez savoir.



La liberté à l'état pur



Savoir que, le lundi 22 janvier 2018 sur la Rod Laver Arena de Melbourne, Hsieh Su-wei joua ( « composa » serait plus juste) le plus beau moment de tennis vu depuis des années, hommes et femmes confondus. Pas forcément le plus efficace, mais le plus pur, le plus inventif, le plus inspiré, le plus libre. Un jeu tout en toucher, en sensations, en variations, en intelligence tactique. Un jeu de géographe, qui redéfinit en permanence les dimensions du court et invente des angles qui n'existent qu'au ping-pong. Des coups à peine frappés, parfois par en dessous, des smashs amortis, des retours amortis, des revers amortis, à gauche, à droite, devant, derrière, de nouveau devant.


Oui, durant plus d'une heure, Angelique Kerber ne fut qu'une balle de chiffon entre les pattes d'une chatte. L'Allemande faillit en perdre la raison. Lorsque, au prix d'un effort harassant, elle parvenait à faire le point, le hurlement qui sortait de sa gorge tenait plus de la crise de nerfs que du cri de guerre. La raquette de la Taïwanaise semblait être un filet à papillons d'où les balles sortaient, si légères, si flottantes dans l'air de Melbourne, que pendant une demi-seconde, le public ne savait jamais si elles seraient in ou out. Elles étaient in et chaque impact à l'intérieur du court saisit le stade de ravissement.


Durant plus d'une heure, nous eûmes ainsi l'illusion de voir jouer Suzanne Lenglen, Billie Jean King ou Martina Hingis. Hsieh Su-wei les égalait dans l'élégance et le talent, avec la difficulté supérieure d'être confrontée à un tennis autrement plus rapide, physique et puissant que ce qu'avaient connu ces légendes. Comment fit-elle pour résister ainsi, avec son service anémique et ses bras trop maigres? D'où sortait-elle cette capacité à annihiler la force de l'adversaire? C'est un mystère qui survit à son exposition, même si le timing, le placement et l'équilibre du corps doivent y être pour quelque chose.


En dépit de son âge, Hsieh Su-wei a le visage creusé d'une adolescente. Elle porte une drôle de tenue dépareillée, avec de larges échancrures au niveau des aisselles qui dévoilent son body et ses côtes, et l'allure efflanquée d'une coureuse de fond éthiopienne. Aucun muscle apparent. Droitière, elle joue revers à deux mains mais aussi coup droit à deux mains. En fait, à bien y regarder, elle joue revers des deux côtés puisque la main opposée est toujours au bout du manche. Un style de jeu qu'elle tient d'une (mauvaise) habitude héritée de l'enfance. « J'ai débuté le tennis très jeune mais je n'avais qu'une raquette d'adulte. Alors je la tenais à deux mains pour qu'elle me paraisse moins lourde. Plus tard, mon père a essayé de me faire changer de prise mais je n'ai pas voulu: j'étais à l'aise comme ça. »



Sans coach ni plan de jeu



A Melbourne, cette joueuse habituée à écumer les tableaux de double et les compétitions ITF sur le continent asiatique (elle fut finaliste début novembre à Hua Hin face à Belinda Bencic) a débarqué sans coach ni plan de jeu. « Mon boyfriend avait regardé le précédent match de Kerber ce matin à la vidéo mais j'ai oublié de lui demander comment elle jouait, donc, en fait, je n'avais pas de plan en entrant sur le court. J'ai juste essayé de jouer mon Su-wei style », expliqua-t-elle en conférence de presse.


Ce qui nourrit évidemment immédiatement une autre question. « Le Su-wei style? C'est du tennis libre. Si je n'ai pas de plan, je peux faire ce que je veux. Quand la balle arrive, je décide au dernier moment où je vais frapper. Parfois, mes adversaires me disent qu'elles ne savent pas où ma balle va partir, mais moi non plus. » Angelique Kerber, elle, savait qu'elle allait devoir courir beaucoup et s'énerver le moins possible. « Je l'avais déjà rencontrée il y a quelques années et je m'attendais à un match compliqué », raconta-t-elle, lorsque ce fut son tour de se présenter à la presse.



L'atout jambes



La plupart des questions qui furent adressées à l'Allemande concernaient son adversaire. Elle ne s'en formalisa pas, comme si elle-même était encore sous le choc. « Parfois, je me disais: « Mais c'est impossible comment elle frappe la balle! » Elle arrivait tout le temps à avoir la meilleure réponse. C'était impensable, incroyable. Je ne jouais pas mal mais je ne pouvais rien faire. Je me suis efforcée de rester calme, d'oublier ce qui venait de se passer, de me concentrer sur le point suivant. Heureusement, j'ai pu gagner le deuxième set. En fin de match, j'avais de meilleures jambes qu'elle et c'est ce qui a fait la différence dans la troisième manche. »


Les jambes d'Angelique Kerber la porteront peut-être jusqu'au titre samedi. On se souviendra d'elle, peut-être déjà plus de Hsieh Su-wei, qui joua pourtant le plus beau tennis du monde. « Mais elle était du monde où les plus belles choses/ Ont le pire destin/Et rose elle a vécu ce que vivent les roses/L'espace d'un matin. » (Malherbe).


Durant plus d'une heure, Angelique Kerber ne fut qu'une balle de chiffon entre les pattes d'une chatte. L'Allemande faillit en perdre la raison


FAMILIARITÉFederer, sympa mais pas tropTout va bien pour Roger Federer, tranquille vainqueur du Hongrois Marton Fucsovics en huitième de finale (6-4 7-6 6-2). Après quatre tours, il demeure le dernier qualifié à n'avoir pas lâché un set. « Cela ne veut rien dire. Je ne serais pas plus fatigué après des matches en quatre sets. Et un set gagné 7-6 ou un autre gagné 6-0, ce n'est pas la même chose. » Il affrontera mercredi le Tchèque Tomas Berdych, un habitué des quarts de finale.Effusions hors de proposLorsqu'il est détendu, le Bâlois se laisse un peu aller en conférence de presse. Interrogé sur le manque de sororité du tennis féminin, il avoua que la franche camaraderie affichée dans le vestiaire masculin lui semblait souvent surjouée. « Je trouve que parfois ça va trop loin. Les joueurs s'embrassent comme s'ils ne s'étaient pas vus depuis dix ans. Le fair-play est important et je trouve normal que l'on respecte son adversaire mais il ne faut pas devenir trop copains. Pour le public et pour le sport, il est bon que deux adversaires soient un peu portés par une rivalité. Avec Rafa, nous avons prouvé que respect et rivalité étaient possibles. » L. FE


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007133.xml</field>
    <field name="pdate">2018-01-23T00:00:00Z</field>
    <field name="pubname">Le Temps</field>
    <field name="pubname_short">Le Temps</field>
    <field name="pubname_long">Le Temps</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Sport</field>
  </doc>
</add>
