<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Les espoirs flottants des Rohingyas</field>
    <field name="author">The Hindu; Suvojit Bagchi</field>
    <field name="id">eupr_007363.xml</field>
    <field name="description"><![CDATA[


Le mois dernier a été horrible !" dit le

majhi

(capitaine) Rafiuddin en regardant la chaîne des monts de l'Arakan, située de l'autre côté de la frontière. Vers ce pays natal qui n'est plus sa patrie. Alors qu'il était adolescent, il a été obligé de quitter son village en Birmanie. Il est arrivé au Bangladesh, il y a vingt-cinq ans, à la faveur de l'exode des Rohingyas en 1991-1992. Ces temps-ci, il gagne sa vie du fait d'un autre exode. Celui déclenché par une armée birmane encore plus dure et qui a rendu le trajet beaucoup plus dangereux. Pour les désespérés qui s'entassent sur les plages de Birmanie, il est une sorte de Charon, le fameux nocher au service d'Hadès. Il a été investi du pouvoir de les tirer de l'enfer en les prenant à bord de son embarcation pour les ramener vers la vie.


Le fleuve sur lequel le

majhi

exerce son métier est la Naf. Il marque la frontière entre le Bangladesh et la Birmanie et va se jeter dans le golfe du Bengale. Pour les Rohingyas, il est le Styx qui sépare le monde de la vie du royaume de la mort.

"La dernière fois que je suis allé en Birmanie, j'étais arrivé sur la plage de Naik-Kon Dia depuis à peine une demi-heure quand j'ai vu un hélicoptère approcher,

raconte Rafiuddin,

il y avait environ 2 000 personnes agglutinées comme des fourmis. Elles ont peut-être pensé qu'elles allaient être bombardées... Je ne sais pas. Mais l'hélicoptère a disparu, et peu après, trente hommes en uniforme ont surgi de la forêt."



Rafiuddin s'exprime de manière hésitante, en observant de longues pauses pour donner à sa soeur le temps de traduire.

"Les soldats ont isolé de la foule cinq hommes portant de longues barbes. Ils les ont emmenés de force dans la forêt, à une centaine de mètres de l'endroit où on était. Puis ils ont décapité un à un les cinq hommes avec des machettes. Juste en face de nous et sous les yeux de leurs familles. La foule regardait en silence."

Bien que Rohingya lui-même, Rafiuddin raconte sans trahir beaucoup d'émotion :

"Je me suis agenouillé et j'ai prié, mais sans me couvrir le visage et sans bouger la tête ni les mains, car je ne vou-lais pas attirer l'attention. J'ai fermé les yeux et j'ai prié pour qu'ils meurent rapidement et sans souffrir."



Les corps ont ensuite été jetés dans les marécages. Par chance, Rafiuddin n'a pas été pris pour cible.

"Ils ont vu que j'avais un bateau bangladais et m'ont épargné",

explique-t-il. Une fois les soldats partis, la foule pétrifiée a repris vie.

"Des centaines de femmes et d'enfants ont voulu monter sur mon bateau en même temps, mais je ne pouvais en prendre qu'une vingtaine. C'était compliqué pour moi de décider qui je devais prendre à bord et qui je devais laisser sur place !"

Rafiuddin s'interrompt pour répondre à un appel téléphonique. Il s'ensuit une conversation très tendue, dans laquelle le mot "police" revient deux fois. Il finit par se tourner vers moi en disant :

"Je suis recherché par la police (bangladaise); je compte sur vous pour leur parler."



Selon Human Rights Watch, dans les trois régions côtières de Maungdaw, Rathedaung et Buthidaung, de la province birmane de l'Arakan, au moins 288 villages rohingyas ont été détruits par l'armée birmane entre le 25 août et le 25 septembre 2017. Des hommes comme Rafiuddin ont joué un rôle crucial pour permettre à ces fuyards d'arriver sains et saufs au Bangladesh. Installées sur et autour de l'île de Shahpori, la dernière parcelle de territoire bangladais, là où la Naf entre dans le golfe du Bengale, les compagnies de pêcheurs convoient les Rohingyas de la Birmanie vers le Bangladesh à trois kilomètres en amont de la rivière.


Ma première rencontre avec Rafiuddin se déroule à l'extérieur de mon hôtel à Cox's Bazar, sur la route menant à Teknaf, une minuscule ville côtière située côté bangladais. Vêtu d'un débardeur sale et en lambeaux, il n'a pas dû se raser depuis plusieurs semaines comme en témoigne sa barbe hirsute.

"C'est bon. Allons-y !"

me répond-il quelques minutes après avoir fait ma connaissance.


Trafic. Il me conduit jusqu'à une forêt derrière mon hôtel par une route en pente raide. Tout en marchant, il me parle un peu de lui. Il est arrivé au Bangladesh en provenance du village birman de Pirindaung, situé dans la circonscription de la ville de Rathedaung, en bord de mer.

"À l'époque, nous n'étions pas attaqués contrairement à ceux qui traversent aujourd'hui

, explique-t-il,

nos problèmes étaient différents. Nous devions demander des permissions à l'Ã%tat pour tout : pour se marier, pour déménager dans un autre village, pour aller pêcher, pour acheter une chèvre ou une vache. Même pour la terre, nous devions demander une permission aux autorités locales. Mon père en a eu marre et a décidé de partir."



Depuis 1978, on a enregistré cinq grandes vagues d'exode de Rohingyas de Birmanie vers le Bangladesh. La plus récente, qui a débuté le 25 août, est la plus importante de toutes.


Nous atteignons les rives de la Naf au bout de vingt minutes. De l'autre côté du fleuve s'élèvent les monts de l'Arakan. Nous voilà de retour chez Rafiuddin, dans sa maison de deux pièces, faite en bambou et terre séchée. Deux fillettes nous observent en silence tandis que nous nous installons. L'une d'elles me tend une tasse de thé noir, puis s'assoit entre Rafiuddin et moi.

"Je serai votre interprète",

dit-elle. C'est la plus jeune soeur de Rafiuddin, qui est née au Bangladesh. Rafiuddin va récupérer des réfugiés sur trois des six plages situées à l'embouchure de la Naf. En août et septembre, il est allé sept fois en bateau en Birmanie pour ramener des réfugiés et les déposer sur l'île de Shahpori. Mais maintenant, ce n'est plus possible, du moins sans risquer d'être jeté en prison.

"Je suis poursuivi pour trafic d'êtres humains"

, précise-t-il.


Rafiuddin me raconte qu'il a commencé sa vie de réfugié en travaillant comme aide d'un patron pêcheur dont il réparait et assurait l'entretien des bateaux. Il a obtenu le titre de

majhi

il y a quelques années. Ces cinq dernières années, il a convoyé des centaines de membres de cette ethnie depuis l'Arakan vers Teknaf.

"Au début, ce n'était pas très difficile de les faire passer d'un pays à l'autre. On a souvent transporté des gens gratuitement, quand on apprenait qu'ils étaient coincés sur une plage."

En tant que pêcheurs, il leur était relativement facile de naviguer sur la rivière et en mer.


Mais la situation a changé. Contrairement à 2012 [violences intercommunautaires] ou même 2016 lorsque des combats entre militants rohingyas et l'armée en octobre ont provoqué un premier exode, Rafiuddin n'a plus eu son mot à dire sur les réfugiés qu'il embarque. Tout a été décidé en fonction d'un processus compliqué de transfert d'argent.

"Je reçois seulement un tiers du prix payé par chaque passager de plus de 10 ans, soit entre 2 000 et 10 000 takas bangladais [1 taka = 0,01 euro]",

explique Rafiuddin, qui affirme également avoir pris des femmes et des enfants

"sans leur faire payer le moindre centime".




"En août, on demandait entre 2 000 et 3 000 takas par passager parce qu'il n'y avait aucune restriction. Mais maintenant, depuis que c'est interdit, on prend 7 000 à 10 000 takas par adulte"

, précise-t-il

.

En général, le propriétaire de l'embarcation (celui qui se cache derrière l'expression "la compagnie") encaisse la moitié de la recette, tandis que l'autre moitié est répartie entre le

majhi

et ses assistants, le barreur et son aide recevant 50 % de la somme revenant au

majhi

.


Refuge. Selon les Nations unies, plus de 800 000 réfugiés sont arrivés dans le sous-district de Teknaf et dans le district de Cox's Bazar depuis 1978. Nombre d'entre eux, dont Rafiuddin, reçoivent régulièrement des vidéos sur leur téléphone mobile montrant d'horribles scènes de violence se déroulant dans les villages rohingyas de Birmanie. Rafiuddin tient à m'en montrer. Sur l'une d'entre elles, on voit le corps d'une fille vêtue d'une chemise rouge et d'une jupe orange reposant sur un drap bleu et blanc.

"J'avais vu cette fille à Dong Khali, au nord de Maungdaw

, raconte Rafiuddin,

elle avait cherché désespérément à monter sur mon bateau, mais j'avais déjà quitté la rive. Il pleuvait beaucoup et je l'avais rapidement perdue de vue."

Je dis à Rafiuddin que je voudrais voir par moi-même ce que les militaires birmans sont capables de faire et lui demande 18 de me faire passer clandestinement de nuit dans en Arakan, il me regarde alors comme si je venais de dire quelque chose de monstrueux

. "Je ne le ferai pas même si vous me donnez 100 000 takas !

rétorque-t-il fermement,

c'est suicidaire !"



Un matin, je pars en direction du nord vers Cox's Bazar avec Shafique, mon guide local. La région, située de part et d'autre de la Naf, dispose d'énormes réserves de gaz naturel. Beaucoup d'ailleurs ont établi un lien entre le déplacement forcé des Rohingyas à l'intérieur du pays et la découverte de ces ressources en gaz, car les villages des Rohingyas musulmans se trouvent juste au-dessus d'importantes réserves d'hydrocarbures.


Des spécialistes mondiaux comme Azeem Ibrahim, un chercheur de la Kennedy School of Government, de l'université d'Harvard, voient clairement un rapport entre le "génocide" (rohingya) et la

"découverte en mer d'importantes sources d'approvisionnement en gaz et en pétrole",

qui a suscité l'attention de

"grandes sociétés... originaires de Chine, d'Inde, d'

Australie

et de Corée du Sud",

certaines ayant déjà obtenu

"des permis de prospection de la part de l'entreprise publique birmane, la MOGE (Myanmar Oil and Gas Enterprise)."



Alors que nous approchons d'Unchiprang, un campement provisoire de réfugiés entre Teknaf et Cox's Bazar, les effets de ce "génocide" deviennent visibles. Des enfants à moitié nus, vêtus de haillons, et des femmes en burqa (sans le voile sur le visage) se tiennent au bord des routes. On dirait qu'on a attribué un arbre à chaque famille pour s'y mettre à l'abri.


Les enfants accourent au-devant de notre tuk-tuk [tricycle à moteur], dont les vitres sont grillagées pour tenir les passagers éloignés des mendiants. Unchiprang offre un tableau si sombre à nos yeux que Shafique, pourtant habitué à rendre visite à ces camps, passe un coup de fil chez lui. Il veut savoir si ses enfants vont bien.


Le camp constitue un refuge provisoire, installé dans une zone boisée. Un millier d'arbres ont été abattus pour pouvoir installer les tentes de 2,5 mètres sur 3 en bâches plastiques. Les abris s'étendent de part et d'autre de la route dans des mares de boue où l'on s'enfonce jusqu'aux chevilles. Les excréments humains empuantissent l'air. L'eau potable et les denrées alimentaires saines font ici cruellement défaut. Malgré tous les efforts des ONG et de la Commission chargée de l'aide et du rapatriement des réfugiés (RRRC), les ordures flottent à la surface de flaques d'eau stagnante autour des cabanes de fortune. Nous décidons de passer la nuit sur place.



"Mais Sadaullah veut vous rencontrer",

me rappelle Shafique. Sadaullah a été le client d'un batelier dont il a payé les services pour faire venir de Birmanie sa soeur et ses quatre enfants. Il pourrait m'éclairer sur l'aspect financier du processus qui permet le passage des Rohingyas. Nous prenons donc la direction de la ville de Teknaf. Nous y rencontrons notre homme dans un petit restaurant bon marché où chacun s'affaire autour d'un plat de soupe aux lentilles accompagné de pain blanc. Sadaullah fait un peu penser à [l'acteur indien] Amitabh Bachchan, dans les années 1970. Après s'être présenté comme un

"médecin à temps partiel"

, il commence par dissiper certaines idées que l'on peut avoir sur ce supposé

"trafic d'êtres humains"

.



"Les associations humanitaires, le gouvernement, la police et la presse qualifieraient cela de trafic d'êtres humains, mais ma mère s'est évanouie sous le choc quand ma soeur l'a appelée pour lui dire qu'ils étaient sur la plage de Go Zon Dia en Birmanie et qu'ils avaient épuisé tous leurs stocks de nourriture et d'eau. Je n'ai pas eu d'autre choix que de les faire traverser clandestinement le fleuve",

explique-t-il.


Argent. Il lui a d'abord fallu trouver une "compagnie" disposée à faire sortir un bateau, surtout en période d'interdiction de pêche, puis il a dû négocier pour avoir un tarif acceptable. Finalement, il a conclu un accord verbal par lequel il s'engageait à verser 42 000 takas pour douze personnes.

"Le problème est que je n'avais pas tout cet argent. J'ai donc demandé à un parent en Arabie Saoudite de financer le voyage, ce qu'il a accepté."

Mais l'argent n'est pas arrivé dans les délais prévus, et Sadaullah a été prié de ne pas quitter le "bureau de la compagnie" durant cette nuit fatidique du 10 octobre.



"Pendant ce temps, le batelier a rejoint la plage de Go Zon Dia, mais avant de laisser ma soeur et ses enfants monter à bord, il a téléphoné à la 'compagnie' pour demander si l'argent avait bien été viré sur son compte. Il ne l'avait pas été. Il a alors menacé de quitter la plage avec ceux qui pouvaient payer tout de suite. La nuit tombait et les militaires birmans étaient dans les parages",

se souvient Sadaullah. Désespéré, il a passé de multiples coups de fil pour obtenir de l'aide.


Finalement, sa femme s'est débrouillée pour réunir la somme.

"Je ne sais toujours pas comment elle a fait...,

dit Sadaullah,

l'argent a été viré via bKash, un service très apprécié de transfert d'argent en ligne. Ma soeur et ses enfants ont été autorisés à monter à bord une fois seulement que la 'compagnie' et le batelier se sont assurés du bon virement des fonds."

Quelques heures plus tard, quand il a retrouvé sa soeur sur l'île de Shahpori après vingt-cinq ans de séparation, celle-ci est tombée dans les pommes.

"Elle n'aurait jamais pensé pouvoir atteindre le Bangladesh en traversant ce fleuve agité au bord de la mer."



Sadaullah explique avoir en gros puisé dans un

"réseau humanitaire privé"

bien rodé, avec des membres en Europe et en Asie de l'Ouest, qui s'organisent en sous-main pour réunir les fonds nécessaires au transport de femmes et d'enfants principalement de Birmanie vers le Bangladesh.


Survivre. Le réseau regroupe une poignée de bénévoles qui paient la "compagnie" pour qu'elle mette des bateaux à leur disposition. L'argent court-circuite les circuits bancaires traditionnels au profit de

hundi

[lettres de change] ou d'autres instruments financiers alternatifs. Les bénévoles versent les sommes au Bangladesh conformément aux instructions données par la personne finançant de l'extérieur du Bangladesh le voyage en bateau. Des officiers de police nous ont confié être au courant de tels

"engagements"

, mais ne pas pouvoir faire grand-chose.



"Pour moi, ce n'est pas du blanchiment d'argent ni un trafic. C'est une action humanitaire de la part de personnes conscientes du problème à une époque où le monde entier néglige la question des Rohingyas",

souligne Sadaullah. Il ne nie par pour autant les risques liés à ces bateaux qui transportent trois fois plus de passagers que leur capacité normale.


Nous nous sommes rendu compte de la justesse de ses inquiétudes lorsque nous sommes allés de Gholapara à une importante madrasa (école coranique), la Jamia Ahmadiyya Baharul Uloom, sur l'île de Shahpori. Sur un lopin de terre circulaire à l'est de la madrasa se trouve le plus grand cimetière de Shahpori.


Au moment de notre arrivée, des élèves du séminaire sont en train de mettre un corps en terre. Un des enseignants, maître Jasimuddin, nous montre une photo sur son téléphone : le corps d'un jeune homme âgé de 25 ans environ. Il a été repêché dans la Naf après le chavirage d'une embarcation le 9 octobre.

"C'est lui que nous enterrons maintenant"

, nous explique le maître.


Le lendemain, Rafiuddin me téléphone, pour la quatrième fois en cinq jours. Il est déçu que je n'aie pas parlé à la police.

"Je suis en fuite parce que j'ai sauvé des gens. Moi, personne ne veut m'aider. Maintenant, vous aussi, vous partez !"

se plaint-il.


L'hiver va être long et difficile pour Rafiuddin. Tant que des accusations de trafic pèseront sur lui, il ne pourra pas aller pêcher sur le fleuve. Ils sont nombreux dans son cas sur l'île de Shahpori.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007363.xml</field>
    <field name="pdate">2018-01-25T00:00:00Z</field>
    <field name="pubname">Courrier international, no. 1421</field>
    <field name="pubname_short">Courrier international, no. 1421</field>
    <field name="pubname_long">Courrier international, no. 1421</field>
    <field name="other_source">Courrier International (site web)</field>
    <field name="source_date">26 janvier 2018</field>
    <field name="section">d'un continent à l'autre. asie</field>
  </doc>
</add>
