<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">L'année 2019 dans le monde</field>
    <field name="author">afp</field>
    <field name="id">eupr_007525.xml</field>
    <field name="description"><![CDATA[


Des milliers de femmes dansent sur une chorégraphie créée par un collectif féministe pour dénoncer les violences et l'oppression dont elles sont victimes, le 4 décembre 2019 à Santiago / AFP


Contestations tous azimuts, canicules exceptionnelles mais aussi le feuilleton du Brexit ou la procédure de destitution contre Donald Trump: rappel des événements marquants de 2019 dans le monde.


- Crises en Amérique latine


En janvier, l'opposant vénézuélien Juan Guaido se proclame président par intérim, réclamant le départ de Nicolas Maduro dont il conteste la réélection, dans un pays en proie à un effondrement économique et une grave crise migratoire. Il est reconnu par une cinquantaine de pays, dont les Etats-Unis. Soutenu par l'armée, Maduro reste en poste.


A Haïti, plusieurs dizaines de personnes sont mortes depuis mi-septembre dans des manifestations réclamant la démission du président Jovenel Moïse sur fond de pénurie de carburant.


En octobre, l'Equateur est paralysé presque deux semaines après la suppression de subventions pour les carburants, finalement annulée.


Le leader de l'opposition au Venezuela Juan Guaido, proclamé président par intérim, appelle à une manifestation à Caracas le 9 mars 2019 / AFP


Au Chili, le Parlement décide mi-novembre de lancer un référendum pour réviser la Constitution héritée de la dictature de Pinochet après un mois d'une violente contestation des inégalités socio-économiques (une vingtaine de morts, des milliers de blessés).


La Bolivie annule le 24 novembre la réélection contestée du président Evo Morales, après quatre semaines de protestation qui ont fait plusieurs dizaines de morts. Lâché par la police et l'armée, le premier président indigène du pays avait démissionné le 10 novembre, s'exilant au Mexique et dénonçant un coup d'Etat.


En Colombie, le président de droite Ivan Duque est lui aussi confronté depuis le 21 novembre à une contestation inhabituelle marquée par des manifestations massives.


- Révoltes arabes, émeutes en Iran


Un Algérien agite un drapeau national lors d'une manifestation contre la classe dirigeante dans la capitale Alger le 27 septembre 2019, pour le 32e vendredi consécutif depuis le début de la contestation / AFP


Le 22 février commencent en Algérie des manifestations massives contre la candidature à un cinquième mandat d'Abdelaziz Bouteflika, très affaibli depuis un AVC en 2013. Le 2 avril, le président démissionne sous la pression de la rue et de l'armée.


Mais les Algériens continuent de manifester, réclamant le démantèlement du «système» qui dirige le pays depuis l'indépendance en 1962 et le départ de tous ses représentants, dont fait partie le nouveau président Abdelmadjid Tebboune, élu le 12 décembre lors d'un scrutin marqué par une abstention record.


Le 11 avril, au Soudan, Omar el-Béchir, au pouvoir depuis 30 ans, est destitué par l'armée après quatre mois d'un mouvement populaire, déclenché par le triplement du prix du pain. La répression du soulèvement fait plus de 250 morts selon les manifestants. Un conseil de transition est mis en place en août, un Premier ministre civil installé en septembre.


En Irak, une contestation débute le 1er octobre contre la corruption, le chômage et la déliquescence des services publics, avant de dégénérer en grave crise politique. Le 1er décembre, le Parlement accepte la démission du gouvernement. Près de 460 personnes sont tuées et 25.000 blessés dans la répression par les forces de l'ordre.


Au Liban, l'annonce le 17 octobre d'une taxe - vite annulée - sur les appels via la messagerie WhatsApp suscite une vive réaction populaire, entraînant la démission du Premier ministre Saad Hariri. Un universitaire soutenu par le puissant mouvement chiite Hezbollah, Hassan Diab, est désigné pour le remplacer mais cette nomination n'a pas calmé les contestataires dans un pays au bord de la faillite économique.


Et l'Iran a été le théâtre mi-novembre de plusieurs jours d'émeutes après une hausse du prix de l'essence. Selon Amnesty International, 304 personnes ont été tuées en trois jours durant la répression du mouvement, un bilan qualifié de «mensonge» par les autorités. Au moins 7.000 personnes ont été arrêtées, d'après le Haut-Commissariat de l'ONU aux droits de l'Homme.


- Fin du «califat», Baghdadi tué


Fin mars, les Forces démocratiques syriennes (FDS), coalition soutenue par Washington et dominée par les forces kurdes, s'emparent de Baghouz, dernier bastion syrien du groupe jihadiste Etat islamique (EI), scellant la fin du «califat», instauré en 2014 sur un vaste territoire à cheval entre la Syrie et l'Irak.


Le 27 octobre, Donald Trump annonce la mort du chef de l'EI, Abou Bakr al-Baghdadi, lors d'une opération militaire américaine dans le nord-ouest de la Syrie. Considéré comme responsable de multiples exactions et atrocités en Irak et en Syrie et d'attentats sanglants, Baghdadi s'est fait exploser dans le village où il se cachait.


Le chef du groupe Etat islamique (EI), Abou Bakr al-Baghdadi, dans une vidéo publiée par le media Al Furqan le 29 avril 2019, et tué lors d'un assaut américain en octobre 2019 / AL-FURQAN MEDIA/AFP/Archives


Mais l'EI conserve des cellules dormantes et continue de revendiquer des attentats meurtriers.


- Boeing dans la tourmente


Mi-mars les avions 737 MAX du constructeur américain Boeing sont cloués au sol, après deux crashs d'appareils de Lion Air et Ethiopian Airlines, totalisant 346 morts. Le système automatique censé empêcher l'avion de partir en piqué est mis en cause.


La crise a déjà coûté une dizaine de milliards de dollars au constructeur, visé par des plaintes de victimes et des enquêtes des autorités américaines.


Mi-décembre l'arrêt jusqu'à nouvel ordre de la production est annoncée. Le 23, le patron de Boeing Dennis Muilenburg démissionne.


- Grande-Bretagne: la saga du Brexit


La sortie du Royaume-Uni de l'Union européenne, décidée par les Britanniques par référendum en 2016, initialement prévue le 29 mars 2019, est reportée trois fois, faute d'accord entre Britanniques sur les conditions du divorce.


Les députés rejettent un accord conclu avec l'UE par la Première ministre Theresa May, puis un second texte négocié par son successeur Boris Johnson. Après une victoire aux législatives anticipées en décembre, Boris Johnson obtient un premier vote de soutien de la nouvelle Chambre des communes à son accord de Brexit. Il espère une adoption finale le 9 janvier, pour une sortie de l'UE le 31 janvier 2020.


- Notre-Dame de Paris en flammes


Le 15 avril, la toiture et la charpente de la cathédrale Notre-Dame de Paris sont ravagées par un incendie.


Incendie à Notre-Dame de Paris le 15 avril 2019 / AFP/Archives


Les pompiers parviennent à sauver l'édifice gothique. Une chaîne humaine extrait la quasi-totalité des oeuvres et reliques.


L'incendie d'un des monuments les plus visités d'Europe suscite une émotion planétaire, générant 922 millions d'euros de promesses de dons pour sa reconstruction, qui prendra des années. Pour la première fois depuis 1803, Notre-Dame ne connaîtra pas de messe de Noël.


- Image d'un trou noir


En avril, une équipe internationale de scientifiques révèle la première image d'un trou noir: un rond sombre au milieu d'un halo flamboyant, au centre de la galaxie M87, à environ 50 millions d'années-lumière de la Terre.


- Escalade avec l'Iran


En mai, Téhéran commence à se désengager de l'accord international de 2015 sur son programme nucléaire, en riposte au retrait des Etats-Unis en 2018 et au rétablissement de sanctions.


La pression monte entre Washington et Téhéran après des sabotages et attaques de navires dans le Golfe, imputées à l'Iran qui dément.


Le 14 septembre, des attaques contre des infrastructures pétrolières en Arabie saoudite sont revendiquées par les rebelles yéménites Houthis, soutenus par Téhéran.


Téhéran a dépassé en six mois le stock d'uranium enrichi prévu par l'accord sur le nucléaire, le taux d'enrichissement et la quantité d'eau lourde autorisés, et modernisé ses centrifugeuses.


- Manifestations à Hong Kong


Hong Kong connaît depuis juin sa crise la plus grave depuis sa rétrocession à la Chine en 1997, avec des manifestations quasi quotidiennes, de plus en plus violentes, contre l'ingérence jugée grandissante de Pékin.


Le mouvement est né de l'opposition à un projet de loi, depuis abandonné, autorisant les extraditions vers la Chine. Les manifestants ont élargi leurs revendications, réclamant un véritable suffrage universel et une enquête indépendante sur les violences policières.


Le 24 novembre, les candidats prodémocratie remportent une écrasante victoire aux élections locales, démentant l'affirmation des autorités selon laquelle la majorité silencieuse s'opposerait aux manifestants.


- La planète chauffe


Juillet 2019 est le mois le plus chaud jamais mesuré, avec des records de températures en Europe comme au Pôle Nord. En août, l'Islande dit adieu à son premier glacier disparu, tandis que 400 autres sont menacés.


Des tiges de blé brûlées par la vague de chaleur en Allemagne le 26 juillet 2019 / AFP


En août et septembre des incendies ravagent des régions entières d'Amazonie, en raison de la déforestation, suscitant de vives critiques de la politique du président brésilien d'extrême droite Jair Bolsonaro. L'

Australie

affronte en novembre et décembre des incendies sans précédent et bat également des records de chaleur.


La jeune militante écologiste suédoise Greta Thunberg, élue «personnalité de l'année» par le magazine Time, rallie des millions de personnes à son mouvement «Fridays for Future».


- Désengagement américain


Début août, les Etats-Unis sortent du traité sur les armes nucléaires de portée intermédiaire (INF), conclu pendant la Guerre froide avec Moscou.


Au nom de «l'Amérique d'abord», Donald Trump multiplie les désengagements de la scène multilatérale: retrait du Nord-Est syrien, officialisation du retrait de l'accord de Paris sur le climat, lancement d'une guerre commerciale sans merci avec ses partenaires, notamment l'Union européenne et la Chine dont l'économie ralentit fortement.


Pékin et Washington annoncent à la mi-décembre une trêve dans leur guerre commerciale en dévoilant un accord commercial qui doit être signé début janvier.


- «Impeachment»


Les démocrates de la Chambre des représentants lancent le 24 septembre une procédure de destitution («impeachment») de Donald Trump, soupçonné d'abus de pouvoir pour avoir demandé à l'Ukraine d'enquêter sur le démocrate Joe Biden, adversaire potentiel à la présidentielle de 2020.


Donald Trump à Philadelphie, en Pennsylvaniele 14 décembre 2019 / AFP


Le président américain est mis en accusation le 18 décembre par la Chambre, contrôlée par les démocrates, qui vote en faveur de son «impeachment» pour «abus de pouvoir» et "entrave à la bonne marche du Congrès.


Il appartient désormais au Sénat de le juger, sans doute en janvier. Mais les républicains, qui contrôlent la chambre haute, ont la ferme intention d'acquitter leur président.


- Syrie: offensive turque


La Turquie déclenche le 9 octobre une offensive contre la milice kurde des Unités de protection du peuple (YPG), alliées des Occidentaux dans la lutte antijihadiste, mais qualifiée de «terroriste» par Ankara en raison de ses liens avec le Parti des Travailleurs du Kurdistan (PKK).


L'opération, lancée après l'annonce par Donald Trump d'un retrait de troupes américaines du Nord-Est syrien, suscite un tollé international.


Ankara met fin deux semaines plus tard à son offensive, après deux accords négociés séparément avec les Etats-Unis et la Russie qui prévoient le retrait des YPG de la plupart de leurs positions frontalières.


- Crise sociale en France


Depuis le 5 décembre la France est confrontée à un important conflit social contre un projet de réforme des retraites, avec une grève reconductible des transports publics.


Le gouvernement veut fusionner en un système «universel» les 42 régimes de retraites existants, supprimant les «régimes spéciaux», dont ceux du métro parisien et de la compagnie ferroviaire SNCF. Le projet inclut une forte incitation à travailler jusqu'à 64 ans, contre 62 ans pour l'âge «légal» de départ en retraite.


Cette crise, qui se poursuit en période de Noël, intervient dans la foulée du mouvement des «Gilets jaunes» qui secouait le pays depuis la fin 2018, contestant parfois violemment la politique sociale et fiscale du gouvernement. Onze personnes au total ont trouvé la mort en marge des manifestations qui ont fait quelque 2.500 blessés parmi les manifestants et 1.800 chez les forces de l'ordre.


- Les «Gafa» sous pression


Aux Etats-Unis et en Europe, les géants d'internet Google, Apple, Facebook et Amazon, critiqués sur la protection des données personnelles ou sur des positions dominantes (marché publicitaire, recherche en ligne, commerce électronique...), font l'objet d'enquêtes, menaces de démantèlement et amendes (5 milliards de dollars pour Facebook aux Etats-Unis).


Un bras de fer oppose des géants du web aux médias sur la rémunération des contenus de presse qu'ils reprennent, captant l'essentiel des revenus publicitaires en ligne. En France, Google et Facebook refusent de verser à la presse des «droits voisins», un mécanisme de partage des revenus permis par la transposition d'une directive européenne, adoptée fin mars.


Cet article est paru dans La Croix (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007525.xml</field>
    <field name="pdate">2019-12-26T00:00:00Z</field>
    <field name="pubname">La Croix (site web)</field>
    <field name="pubname_short">La Croix (site web)</field>
    <field name="pubname_long">La Croix (site web)</field>
    <field name="other_source">AFP Infos Françaises</field>
    <field name="source_date">26 décembre 2019</field>
    <field name="section">Monde</field>
  </doc>
</add>
