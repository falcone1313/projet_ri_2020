<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">LES CARAÏBES PRÈS DE CHEZ NOUS</field>
    <field name="author">Sophie Ouimet</field>
    <field name="id">eupr_005134.xml</field>
    <field name="description"><![CDATA[



UNE JOURNÉE À L'ÎLE FLOWERPOT



Pour les Québécois en quête d'une destination de vacances qui se compte en dollars canadiens, la péninsule Bruce peut s'avérer une destination abordable. Et elle n'a pas grand-chose à envier aux Caraïbes ou même à la Côte d'Azur, du moins en ce qui concerne ses plages limpides aux reflets turquoise bordées de cailloux.


Un peu plus loin que Sandbanks (il faut compter neuf bonnes heures de voiture à partir de Montréal), les paysages époustouflants de la péninsule Bruce valent bien les quelques heures supplémentaires de voyagement. Et d'ailleurs, à entendre notre accent résonner un peu partout, on se dit que nous ne sommes pas les premiers à avoir découvert le secret

L'une des attractions vedettes de la péninsule Bruce se trouve au-delà de la pointe, au confluent de la baie Géorgienne et du lac Huron : l'île Flowerpot, célèbre pour ses deux piliers de pierre détachés du rivage. On s'y rend pour admirer ces structures en forme de pots de fleurs et, bien sûr, se baigner dans les eaux turquoise qui les bordent.


Chaque jour, pendant la saison touristique, des traversiers déversent un flot de vacanciers qui viennent y passer la journée. En effet, on accède à la petite île de 2 km2 par bateau seulement, à partir du village de Tobermory, lui-même situé au bout de la péninsule.


Inhabitée, l'île fait partie du parc marin national Fathom Five. On peut y planter sa tente pour une nuit, sur l'un des six emplacements de camping disponibles, mais aucun service n'est offert. La plupart des gens préfèrent donc effectuer l'aller-retour dans la même journée.


En effet, on explore généralement les lieux en quelques heures, durant lesquelles on a le temps de voir les « flowerpots », de faire une petite randonnée, de pique-niquer et de piquer une tête dans l'eau, si on n'est pas frileux.


Quand on débarque du bateau, le plus simple est d'emprunter le sentier qui longe la côte et qui mène aux « flowerpots ». Ce sont en fait des piliers de pierre qui ont été détachés du rivage par l'érosion. Il y en avait autrefois un troisième, mais il s'est effondré au début des années 1900. Ce n'est pas un phénomène géologique unique, puisqu'on en trouve des semblables ailleurs, notamment au Nouveau-Brunswick (les rochers Hopewell) ou encore en

Australie

(les Douze Apôtres).


Ceux-ci valent toutefois largement le détour. Le long du sentier, les gens s'arrêtent ici et là pour pique-niquer ou se baigner  une baignade glaciale, mais ô combien revigorante ! À noter qu'il n'y a pas de plage en tant que telle dans l'île, mais on peut s'installer un peu partout sur les grands rochers plats, qui en font une plateforme idéale pour se jeter à l'eau à condition d'en avoir le courage.


En continuant la randonnée, on arrive au site du phare de l'île, qui a été remplacé depuis par une tour d'acier. Mais on peut toujours visiter la maison de l'ancien gardien, qui a été transformée en petit musée, et où l'on peut également acheter quelques rafraîchissements et du chocolat.


Pour finir, il est possible de faire une boucle par un autre sentier afin de retourner au quai d'où partent les bateaux, ou simplement revenir sur nos pas pour pouvoir lézarder un peu plus longtemps au bord de l'eau.


DES ÉPAVES


Les bateaux qui font la navette entre Tobermory et l'île Flowerpot offrent de faire un arrêt dans une baie, tout près du phare de Tobermory, pour voguer au-dessus de deux épaves de bateaux.


Pour la petite histoire, l'un des vaisseaux avait pris feu en 1907, alors que l'autre, endommagé, avait coulé avant d'avoir pu être réparé en 1885. Puisque l'eau est peu profonde et transparente à cet endroit, elle permet de discerner les détails des épaves avec une précision étonnante. Certains traversiers sont même dotés d'un fond de verre pour mieux admirer les coques ensevelies sous l'eau.


D'ailleurs, le parc marin Fathom Five compte une vingtaine d'épaves en tout et s'avère très prisé des plongeurs, grâce à ses eaux claires et limpides.


RENSEIGNEMENTS PRATIQUES


COMMENT S'Y RENDRE


De nombreux bateaux font la liaison entre Tobermory et l'île Flowerpot tous les jours. En haute saison, il est recommandé d'acheter son billet en ligne à l'avance, pour l'aller et le retour. Deux entreprises privées font le circuit (Blue Heron Cruises et Bruce Anchor Cruises) et offrent différentes options. Nous avons choisi un bateau à fond de verre de Blue Heron Cruises pour l'aller, et un de ses bateaux express pour le retour. Dans les deux cas, les embarcations étaient complètement remplies.


Aussi, puisque l'île fait partie d'un parc national, on doit se procurer un permis d'une journée pour le visiter ou pour y camper. On peut facilement l'acheter en même temps que nos billets de traversier, en ligne.


MANGER ET BOIRE


Même si l'île compte un petit dépanneur, mieux vaut emporter de la nourriture et des boissons pour pique-niquer parce qu'il n'est pas toujours ouvert et que l'offre est très limitée. L'épicerie Foodland, juste à côté du port à Tobermory, prépare plusieurs sandwichs le matin pour les voyageurs qui partent en excursion.



EXCURSION À LA « GROTTO »



L'une des attractions les plus populaires de la péninsule Bruce est, ironiquement, bien cachée : la grotte. Intriguée par cet endroit dont tout le monde parlait, nous avons tenté l'expérience. Et nous ne l'avons pas regretté, même si nous avons bien failli passer tout droit !


En pleine saison, pour visiter la grotte (que tout le monde appelle la « grotto », même en français), mieux vaut être bien préparé. Afin d'éviter qu'un trop grand nombre de touristes ne s'y rende chaque jour, Parcs Canada a instauré récemment un système pour limiter le nombre de places. La façon de garantir un accès au moment voulu est de réserver son stationnement.


Chaque journée est divisée en créneaux de quatre heures, et il faut respecter son rendez-vous, sous peine d'être refusé à l'entrée du parc. Toutefois, les gens qui campent déjà au lac Cyprus, situé à proximité, ont accès gratuitement à la caverne et son bassin d'eau bleue.


On se rend facilement à la grotte, située dans le parc national de la Péninsule-Bruce, à partir du village de Tobermory. Après quelques kilomètres seulement sur l'autoroute, il suffit d'emprunter le joli Cyprus Lake Road, qui serpente dans le parc national jusqu'au stationnement. C'est là qu'on délaisse la voiture pour les jambes : une randonnée d'une trentaine de minutes est nécessaire pour arriver à destination.


On se retrouve finalement à l'anse Indian Head et sa plage rêvée garnie d'immenses roches calcaires plates, dont l'eau est complètement transparente.


Avec les reflets du soleil et du ciel bleu cette journée-là, l'effet en est presque irréel. Plusieurs baigneurs sont déjà bien immergés dans l'eau claire, malgré sa température glaciale et l'heure plutôt matinale.

L'excursion peut s'arrêter là, mais les plus aventureux voudront continuer quelques mètres plus loin En poursuivant sur le sentier, on passe d'abord une arche naturelle, où l'on peut apercevoir l'eau cristalline à travers les rochers. C'est joli, mais on n'est pas encore arrivé à la grotte : il faut continuer sur le sentier.


On débouche alors sur une esplanade qui surplombe une falaise. En chemin, aucune pancarte n'indique l'entrée de la fameuse « grotto », ce qui contribue à entretenir son petit côté mystérieux. Convaincue que nous sommes passée tout droit, nous envisageons de faire demi-tour jusqu'au moment où un passant nous pointe l'entrée de la grotte. On l'aperçoit alors enfin, dans une fente à peine visible entre deux roches. Le trou est si bien camouflé que nous venions de l'enjamber sans même nous en rendre compte.


C'est là qu'il faut se faufiler dans l'espace étroit dans le roc, baptisé à juste titre la « cheminée ». On peut facilement s'y sentir claustrophobe pendant quelques secondes. Mais une fois à l'intérieur, la perspective s'ouvre et on découvre enfin la fameuse caverne et son bassin d'eau bleue.


Les reflets de l'eau chatoient de toutes leurs couleurs, surtout à l'endroit où la pierre est percée par un tunnel immergé qui mène de l'autre côté du bassin. Cette ouverture crée une tache de couleur turquoise plus claire. Même s'il fait assez sombre et humide à l'intérieur, des jeunes sautent dans l'eau et leur « plouf » résonne entre les murs de la grotte.

Après être remonté à la lumière du jour  en faisant attention aux gens qui y descendent en même temps ! , on peut retourner à l'anse Indian Head pour un bain dans les eaux turquoise. Il faut juste garder la montre à proximité pour s'assurer d'avoir quitté le stationnement à temps.


RENSEIGNEMENTS PRATIQUES


RÉSERVER SA PLACE


Si l'on ne campe pas dans le secteur, il faut réserver son espace de stationnement pour s'assurer d'avoir une place. Il n'est pas rare que des vacanciers soient refusés à la barrière et doivent rebrousser chemin. Le coût du stationnement est de 11,70 $, auquel il faut ajouter les frais de réservation de 6 $ et la taxe.


SE RENDRE


Au départ de Tobermory, emprunter la Highway 6, puis tourner à gauche sur le Cyprus Lake Road. On se trouve alors dans le parc national de la Péninsule-Bruce. Continuer sur cette route jusqu'au stationnement P1. La randonnée part de cet endroit.



LES VILLAGES DE LA PÉNINSULE



TOBERMORY,  Ontario  La péninsule Bruce est surtout reconnue pour ses paysages sauvages. Elle est néanmoins traversée par quelques agglomérations : Tobermory, village portuaire situé complètement à la pointe, Lion's Head, au beau milieu, sans oublier Wiarton et son style un peu western, à la base.


Ces villages sont reliés par la seule route qui traverse la langue de terre au complet, la Highway 6. En ligne droite, l'autoroute est entourée de chaque côté d'un paysage triste et sec, de stations d'essence souvent fermées et d'une quantité étonnante de magasins Home Hardware. Au point où l'on se demande un peu ce qu'on est venu faire ici


Mais il faut persévérer parce que la récompense, quand on arrive à la pointe de la péninsule, rachète largement tous ces kilomètres d'asphalte. Au détour d'une jolie courbe, on découvre le charmant petit village de Tobermory, dont le port animé est entouré de quelques restaurants, d'une bibliothèque municipale et de magasins de souvenirs.


Les amateurs de plein air s'y rendent par milliers pendant la saison touristique pour respirer son air frais, admirer son phare et ses couchers de soleil, faire des randonnées, mais aussi pour profiter de ses côtes.

Si on fait rapidement le tour de Tobermory, il s'agit néanmoins d'un arrêt agréable vers d'autres destinations. Certains s'y rendent pour prendre le traversier vers l'île Manitoulin, sur la rive nord de la baie Géorgienne, alors que de nombreux vacanciers profitent de leur passage à Tobermory pour aller visiter l'île Flowerpot et son paysage unique.


À un jet de pierre du village, accessible à pied, se trouve le centre d'accueil du parc national de la Péninsule-Bruce et du parc marin national Fathom Five. Sa visite peut s'avérer intéressante, car au-delà des informations d'usage, il comporte une tour d'observation haute d'environ 18 m qui surplombe le parc national et les îles. Une belle façon d'entamer son séjour.


C'est là aussi que part la Bruce Trail (sentier Bruce), qui débute à Tobermory et qui traverse l'Ontario jusqu'à Niagara, à l'extrémité sud-est de la province. Nul besoin d'en parcourir les quelque 900 km, mais on en marchera inévitablement des bouts en randonnant dans les parcs nationaux de la péninsule. Partout, les sections de la piste sont indiquées par des traces de peinture blanche sur les arbres, et ces segments passent parfois dans des endroits inusités comme des terrains privés.


Il faut juste faire attention où on met les pieds : on peut croiser des serpents dans les sentiers de la péninsule.


BONNES ADRESSES À TOBERMORY


OÙ RESTER ?


Point de vue hébergement, il ne faut pas s'attendre à du quatre-étoiles. Dans la péninsule Bruce, on retrouve surtout des motels, quelques « inns » ou alors du camping dans les parcs nationaux.

Nous avons réservé au Blue Bay Motel, un établissement de Tobermory au style vintage assumé, qui s'est avéré très mignon et confortable, tout cela à prix bien raisonnable.


OÙ MANGER ?


Pour déjeuner : le Craigie's


Il ne faut pas se fier aux apparences : ce restaurant qui semble déposé dans une boîte carrée au milieu d'un stationnement est finalement bien sympathique. On y sert avec une rapidité déconcertante café, œufs, bacon et crêpes. On peut facilement s'en tirer pour moins de 10 $ par personne. Parfait pour un arrêt rapide avant une excursion à l'île Flowerpot.


Pour souper : Tobermory Brewing Co.


On mange beaucoup de fish and chips dans la région, mais si on veut varier un peu, on peut mettre le cap sur la brasserie du village. Entouré d'une belle terrasse, le pub compte accessoirement un bon chef. On y mange bien, et le personnel est avenant.


Pour une gâterie : The Sweet Shop


The Sweet Shop est l'une des premières adresses sur lesquelles on tombe en faisant une recherche sur Tobermory. Et comme de fait, c'est une véritable institution qui attire les foules en quête d'une crème glacée, d'un fudge ou d'une autre gâterie. Nous y avons d'ailleurs mangé une délicieuse glace à la pêche de Niagara.


Pour un bon café : The Coffee Shop


À côté du Sweet Shop se trouve le Coffee Shop (ce n'est pas une blague). On y sert de très bons lattés, des pâtisseries et quelques repas un peu plus consistants comme des sandwichs déjeuners.


LION'S HEAD


Situé plus ou moins à la moitié de la péninsule, Lion's Head est considérablement moins loin que Tobermory. Le village compte une sympathique plage municipale pour les familles, un joli phare, et il peut être agréable d'y faire du kayak. Mais autrement, l'offre est un peu limitée, autant pour les hébergements que pour les restaurants. Le Lion's Head Inn, toutefois, offre quelques chambres et un très sympathique restaurant entouré d'une terrasse. Il semble constituer le cœur de l'endroit, rien de moins (et nous y avons pris presque tous nos repas).



ESCAPADE À SOUTHAMPTON



On dit que ce sont les Hamptons de l'Ontario. Après la péninsule Bruce et son côté un peu plus rustique, il peut s'avérer agréable de passer par Southampton, une petite ville chic et mignonne sur les berges du lac Huron.

Elle comporte tout le nécessaire d'une station balnéaire typique : une longue plage de sable, une jolie rue principale, ainsi que quelques bonnes adresses pour manger. Côté hébergement, on peut opter pour de jolis inns ou des cottages à louer. On y trouve également un motel rétro à la façade turquoise, qui possède aussi un comptoir de gelato !


Si les berges du lac Huron ne sont pas aussi impressionnantes que celles de la baie Géorgienne, elles ont le mérite d'être tranquilles. Fait non négligeable : la température de l'eau y est légèrement plus supportable ! Il fait donc bon s'y baigner, ou y suivre un cours de yoga.


Il règne aussi une atmosphère agréable sur la rue principale, High Street, qui s'élance perpendiculairement à la plage. L'artère a des allures de grand boulevard, avec son drapeau canadien planté à son extrémité et ses jolies maisons de chaque côté. On y trouve quelques cafés, des glaciers et des magasins de toutes sortes : vêtements, décoration, accessoires Dans la petite échoppe The Cook's Cupboard, où quelques étalages ont été disposés devant, on tombe d'ailleurs sur des accessoires de table signés Ricardo.

En face du bureau de poste, la Walker House occupe tout le coin de rue avec son imposante façade de briques. On y sert une réconfortante cuisine de pub, qu'on peut déguster dans la salle à manger ou sur la magnifique terrasse fleurie bien cachée derrière.


Southampton est l'un de ces endroits où il fait bon vivre. En chemin vers la maison, on s'y arrête donc volontiers, ne serait-ce que pour prendre quelques butter tarts (ces tartelettes populaires dans la région) avant de repartir, pour rendre la route plus agréable.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005134.xml</field>
    <field name="pdate">2019-07-27T00:00:00Z</field>
    <field name="pubname">La Presse+</field>
    <field name="pubname_short">La Presse+</field>
    <field name="pubname_long">La Presse+</field>
    <field name="other_source">La Presse (site web)</field>
    <field name="source_date">27 juillet 2019</field>
    <field name="section">INSPIRATION</field>
  </doc>
</add>
