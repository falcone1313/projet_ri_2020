<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Les MCN, nouveau terrain de jeu pour les chaînes télé</field>
    <field name="author">Didier Si Ammour</field>
    <field name="id">eupr_007352.xml</field>
    <field name="description"><![CDATA[


Les groupes audiovisuels historiques investissent de plus en plus les MCN [multi-channel networks], à l'image de TF1 avec Studio71 ou M6 avec BroadbandTV, avec l'idée d'adopter les codes qui vont séduire les millennials... et les marques.


/


Pour les fêtes de fin d'année, des milliers d'internautes pouvaient charger Coach Danielle de laisser un message personnalisé à leurs amis les incitant à ne pas prendre le volant après avoir bu. Résultat : 4 millions de 18-24 ans touchés et 2,5 millions de vidéos partagées sur Facebook. Un succès pour cette campagne virale de prévention construite autour d'une personnalité rappelant vaguement Jackie Sardou, inconnue des téléspectateurs mais populaire sur les réseaux sociaux avec 1,2 million de fans sur Facebook, plus de 460 000 abonnés sur Twitter et 265 000 sur YouTube. Cette opération a été réalisée pour l'assureur MMA par Finder Studios, l'un de ces fameux MCN (multi-channel network, réseau de chaînes YouTube en français), prisés des jeunes générations, qui privilégient l'écran de poche mobile au traditionnel téléviseur du salon.


Finder studios est l'une des composantes de Studio71 France, label du groupe TF1. Pour autant, ce type d'opération aurait tout aussi bien pu être menée par Amaury et Quentin, duo de Golden Network, l'entité du groupe M6 consacrée à la vidéo pour millennials, ou par Monsieur Poulpe, tête d'affiche de Studio Bagel, filiale de Canal+, qui assure déjà l'interview hebdomadaire virale Autopromo, diffusée depuis la plateforme digitale Détours, créée pour Seat par le groupe de TV payante.


Déferlante


Déjà concurrents sur le marché de la télévision, les groupes audiovisuels rivalisent maintenant via ces chaînes du web, où M6 et TF1 ont accentué leur présence en 2017. «On a assisté à une déferlante avec différents accords et prises de participation», constate Laurent Douard, directeur général de Carat France.


Depuis novembre, le groupe M6 représente en France BroadbandTV, un géant mondial également contrôlé par RTL Group et qui permet désormais à Golden Network d'atteindre les 600 millions de vidéos vues par mois en France, juste derrière Talent Web, le mastodonte aux 700 millions de vues par mois du groupe Webedia. Cet accord s'ajoute à celui conclu plus tôt avec StyleHaul, autre filiale internationale de RTL Group sur la mode et la beauté, qui revendique dans le monde plus de 2 milliards de vues par mois. Ces partenariats ont permis à M6, pionnier dans les MCN en lançant dès 2012 Golden Moustache, de damer le pion à TF1.


Le groupe leader sur le marché TV, après avoir pris en régie Finder Studios dès 2015 et fait l'acquisition de MinuteBuzz fin 2016, est entré début 2017 à hauteur de 6,1 % dans le capital de Studio71, filiale de ProSiebenSat.1, qui compte plus de 6 milliards de vidéos vues par mois dans le monde. Suite à cette prise de participation, TF1 a lancé en septembre Studio71 France, affichant 400 millions de vidéos vues par les internautes chaque mois, grâce à des contenus internationaux (Lilly Singh, The Rock...), à ceux des influenceurs de Finder studios (Jigmé, Seb La Frite, Jenesuispasjolie) et du trio Lolywood.


Une audience rajeunie de dix ans


Le groupe TF1 distance ainsi largement Canal+, qui avait pourtant, dès 2014, acquis Studio Bagel, un opérateur totalisant chaque mois plus de 50 millions de vidéos vues. NRJ Group a aussi fait ses premiers pas dans le monde des MCN en entrant dans le capital de Share Fraiche, qui revendique 15 millions de vues par mois.


« Le phénomène est récent et pas encore tout à fait intégré dans les stratégies audiovisuelles », analyse Raphaël Pivert, responsable études et recherches de GroupM. Pourtant, « l'objectif est de s'adapter à de nouvelles écritures, de nouvelles cibles et de nouveaux usages », explique Marine Adatto, directrice entertainement et content de TF1 Publicité. Ce que Raphaël Pivert traduit par « une façon de se réinventer pour toucher des cibles qui regardent peu la TV, qu'elles ont désertée en cinq à dix ans. » De son côté, le dirigeant de Carat France note que « l'audience entre la TV en direct et la catch-up rajeunit de 10 ans. Et quand le délinéarisé est consommé sur un autre écran, elle rajeunit encore de 10 ans. » En conséquence, « il ne faut pas vouloir rajeunir l'audience du linéaire, prévient Laurent Douard, mais plutôt aller là où sont les audiences jeunes, et leur parler dans leur langage, donc via les MCN. »


M6 perçoit, lui, Golden Network « comme de la R&D, avec de la création de contenus pour toucher les millennials, explique le directeur général de la régie, David Larramendy, et un laboratoire dans l'écriture pour ce qui pourra aller en TV plus tard ». Longtemps rattachée à M6 Web, l'activité relève d'ailleurs des programmes depuis un an. Et fin 2015, l'équipe de Golden Moustache a été mise à l'antenne sur W9, une démarche adoptée deux ans plus tôt par le groupe Canal+ avec Studio Bagel.


« La vocation à aller sur les autres médias est naturelle », selon Marine Adatto. Des talents comme les Lolywood, qui avaient fait la vidéo la plus consommée pendant l'Euro, passent maintenant sur NT1 (bientôt TFX). Pourtant, des risques existent. Laurent Douard cite en exemple le programme Presque adultes, qui associait trois stars de YouTube - Natoo, Norman et Cyprien - dans une série courte diffusée sur TF1 à 20h40 durant l'été 2017. Une initiative perçue comme « un manque de créativité, selon le directeur général de Carat France, et qui décrédibilise la marque média et ces personnalités. »


En revanche, leur utilisation en publicité poserait moins problème. Carat utilisait dès 2014 les services de Golden Moustache pour Granola et en 2017 ceux de Finder studios pour Oreo, avec plus de 2,5 millions de vidéos vues à la clef. « Cela permet de parler à une cible par des outils qu'elle affectionne », se réjouit David Larramendy. Simplement, « la problématique pour les marques consiste à laisser de la liberté aux talents, qui peuvent être disruptifs, constate la représentante de TF1 Publicité. Il faut rester dans leur ligne éditoriale et l'expliquer aux abonnés. » Et cela fonctionne : les deux vidéos pour le lancement de la dernière version du jeu Assassin's Creed ont dépassé les 2 millions de vues. « Si c'est maquillé, cela ne marche pas, pointe David Leclabart, directeur général de l'agence

Australie

. Les millennials sont ceux qui détestent le plus la publicité sur le digital et ils savent décrypter les subterfuges marketing. »


Cet article est paru dans Stratégie (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007352.xml</field>
    <field name="pdate">2018-01-25T00:00:00Z</field>
    <field name="pubname">Stratégie (site web)</field>
    <field name="pubname_short">Stratégie (site web)</field>
    <field name="pubname_long">Stratégie (site web)</field>
    <field name="other_source">Stratégies</field>
    <field name="source_date">25 janvier 2018</field>
    <field name="section">Médias</field>
  </doc>
</add>
