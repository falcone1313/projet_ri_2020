<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">"L'opération de police" d'Indonésie devant l'opinion mondiale La presse américaine déplore le nouvel accroc fait an prestige des Nations unies</field>
    <field name="author">SERGE KARSKY.</field>
    <field name="id">eupr_006269.xml</field>
    <field name="description"><![CDATA[


Nous aurions aimé consacrer la dernière revue de presse de cette année à un sujet plus réconfortant qu'une guerre, ne serait-ce qu'une " opération de police ". Mais, tributaires que nous sommes de la triste actualité, nous nous voyons obligés de nous occuper de l'Indonésie qui retient aujourd'hui l'attention de l'opinion mondiale.


Aux États-Unis la réaction a été pour une fois unanime et même violente. Quelle que soit la crainte du communisme en Asie, la totalité de la presse, traditionnellement anticolonialiste a mis l'accent sur l'agression et sur la violation des engagements pris envers les Nations unies. Des journaux de la chaîne Scripps-Howard comme le WASHINGTON NEWS qui accuse la Hollande d'avoir " fait remonter la fièvre internationale ". au libéral NEW YORK STAR qui n'hésite pas à comparer l'action des Pays-Bas à celle de Hitler se jetant sur la Tchécoslovaquie en 1938 ou sur la même Hollande en 1940 et qui réclame que l'aide du plan Marshall soit retirée non seulement à l'Indonésie mais également aux Pays-Bas, la presse est unanime dans sa condamnation.


Le conservateur WASHINGTON POST parle d'un " coup au-dessous de la ceinture administré aux Nations unies par le gouvernement des Pays-Bas ", tandis que la PHILADELPH1A INQUIRER, indépendant, craint que " la poursuite de la guerre marque un nouveau fléchissement de l'autorité des Nations unies..., fléchissement dont le monde, Pays-Bas compris. sera la victime ".


Le NEW YORK TIMES déplore l'aménité de la résolution du conseil de sécurité et la qualifie de " cadeau de Noël " au gouvernement de La Haye,


" Il est évident, écrit l'éditorialiste, que le prestige du conseil de sécurité a déjà été ébranlé, ainsi que le prestige de l'homme blanc dans toute l'Asie et celui des nations démocratiques dans le monde entier. "


C'est le CHRISTIAN SCIENCE MONITOR qui, tout en admettant que les Hollandais ont peut-être eu quelque difficulté à s'entendre avec les républicains, exprime la raison essentielle de la réaction américaine : " Ils devraient comprendre, écrit-il, que la condamnation universelle de leur " blitz " repose sur quelque chose de plus solide qu'une sympathie émotive à l'égard de la république indonésienne. Elle repose sur l'engagement pris par la Hollande envers les Nations unies de ne pas recourir à la force sans que la commission des bons offices en soit complètement informée. Ceci l'emporte sur toute autre considération. "


Le temps aidant, les commentaires de ces derniers jours deviennent cependant plus nuancés. Ainsi le SAN FRANCISCO CHRONICLE du 23 décembre écrit :


" Quelle que soit l'action entreprise maintenant, elle doit être menée en vue de colmater la brèche et non pas de l'élargir. "


Enfin le NEW YORK HERALD TRIBUNE, qui reconnaissait déjà à la veille de Noël que les démocraties occidentales feraient mieux de s'abstenir de toute intervention si celle-ci ne se révélait pas décisive, écrivait le 29 décembre :


" Il est péniblement évident que gouverner le monde à l'aide d'ordres successifs d'armistice n'est pas une solution, ni à Java ni en Palestine. "


Les Anglais n'oublient pas l'intérêt de l'empire


Ainsi qu'on pouvait s'y attendre l'opinion britannique s'est montrée moins unanime que celle des États-Unis. Les organes travaillistes seuls sont restés fidèles à l'idéologie anticolonialiste et ont tous flétri l'action des Pays-Bas. Ainsi le NEW STATESMAN AND NATION écrivait la semaine dernière :


" Sous avons le droit et le devoir de dire aux Hollandais, dans les termes les plus explicites, que le fait pour nous de faire partie de l'Union occidentale ne signifie pas que nous devions éviter toute friction en refusant de nous critiquer les uns les autres. Au contraire nous devrions bien expliquer que si nous voulons assurer le succès de la " manière de vivre démocratique ", tous les empires occidentaux devraient se mettre d'accord sur une nouvelle politique de coopération avec les peuples qui ne sont pas disposés à accepter le statut de races soumises. "


Mais déjà le NEWS CHRONICLE, bien que traditionnellement progressiste, nuançait rapidement sa première condamnation pour rappeler l'Impuissance des républicains Indonésiens à affermir leur propre autorité et pour affirmer que les Hollandais ont choisi la seule voie possible. Et le journal libéral conclut :


" Il est possible de faire quelque chose en étendant à l'océan Indien un arrangement comparable au pacte de Bruxelles. Un pacte régional assurant l'indépendance de tous ses membres peut offrir une solution constructive à la situation actuelle. "


Quant aux organes conservateurs, ils prennent résolument la défense de la politique choisie par le gouvernement de La Haye. Le TIMES estime que " l'Angleterre pourrait bien se montrer plus conciliante à l'égard d'un de ses partenaires du Benelux ", le DAILY TELEGRAPH qualifie de " regrettable " la tendance générale " à condamner à tort ce qui est en fait et par nature, une opération de police ", et le DAILY MAIL rappelle à ses lecteurs le triste exemple de la Birmanie.


Enfin l'ECONOMIST, fidèle à sa recherche de synthèse, élargit le débat : " Depuis 1946, écrit-il, il est devenu de plus en plus évident que bon nombre de communautés de l'Asie sud-orientale ne sont pas en mesure de se gouverner elles-mêmes, que les formes occidentales de l'autorité mises en pratique par des Asiatiques s'effondrent rapidement, et que l'anarchie prend leur place. Cette anarchie est à son tour un terrain propice pour le communisme, qui prospère dans une combinaison de propagande utopique et de terrorisme. Loin de s'engager dans la voie de l'indépendance, ces communautés menacent de devenir des épaves incapables de faire face aux besoins économiques de leurs populations, ou même de se laisser entraîner dans une nouvelle dépendance envers la sphère russe...


" Le libéralisme d'aujourd'hui exige de la part de la politique coloniale des idées neuves sur le progrès social, idées qui mettent l'ancien libéralisme, davantage politique, dans l'ombre. Si les nations occidentales veulent seulement faire un effort pour bâtir en commun en Asie sud-orientale une politique de coopération à la mesure de leur expérience et de leurs ressources, il se trouvera qu'à longue échéance la tradition libérale et la défense de la liberté nationale elle-même seront mieux assurées par des apports de civilisation, de richesses et de main-d'oeuvre - ce qui implique des sacrifices - que par des discours au conseil de sécurité. "


Nous aurions aimé recueillir ici l'opinion de l'

Australie

, dont le délégué au conseil de sécurité a pris une position on ne peut plus nette. Mais l'

Australie

est un pays dont l'opinion publique ne se reflète à travers sa presse que d'une façon tronquée, car malgré sa majorité et son gouvernement travaillistes, il n'y existe que des journaux conservateurs qui forment pour la plupart un grand monopole commercial.


On ne s'étonnera donc pas que tous les journaux australiens aient pris la défense des Hollandais, et que le SIDNEY DAILY MIRROR puisse déclarer résolument que " la défense de la communauté hollandaise en Indonésie est la défense de l'

Australie

". Tous les journaux attaquent violemment la position prise aux Nations unies par le docteur Evatt, position que le SIDNEY SUN estime " refléter ses propres vues, et non celles de la plupart des Australiens ", et que le MELBOURNE AGE qualifie de " démonstration héroï-comique " et de " déclarations forcenées ".


L'opinion hollandaise n'est pas absolument unanime


Le vote de confiance du Parlement de La Haye qui a suivi la déclaration du gouvernement sur son action en Indonésie a pu faire croire que seuls les communistes n'appuyaient pas cette action. Les quelques organes de la gaucho socialiste montrent cependant qu'un certain malaise se fait jour dans les rangs du parti travailliste. Ainsi le HET PAROOL rappelle le proverbe hollandais qui dit que " là où il y a deux antagonistes il y a deux fautes ", et écrit :


" Nombreux sont ceux qui ont sérieusement pensé à la possibilité d'un arbitrage, mais la rapidité des événements en a disposé autrement... Don nombre de socialistes ont vu que cette affaire entraînerait un déclin du parti travailliste. "


L'hebdomadaire DE GROENE écrit de son côté :


" La question la plus importante est de savoir... comment la politique hollandaise devra être amendée afin que le peuple se donne de meilleurs gouvernants que ceux qui l'ont conduit dans un telle impasse. "


Cependant le HET VRIJE VOLK, officiel du parti, s'en prend à la soldatesque... indonésienne :" Si l'on peut réussir à écarter la soldatesque qui par son emprise sur le gouvernement de Djokdjakarta a eu le pouvoir alors la coopération avec la République sera remise à l'ordre du jour. L'armée républicaine doit être débarrassée de son influence sur le gouvernement républicain. "


Tous les autres organes de presse, le DE WAARHEID, communiste, excepté, approuvent entièrement le gouvernement. Le catholique DE MAASBODE dit même que ce dernier a agi " plutôt trop tard que trop tôt ", et le ELSEVIERS, hebdomadaire de droite, affirme que " la République de Djokdjakarta ne fut rien d'autre que la chimère d'un groupe de potentats ".


Quant à la réaction de l'opinion mondiale, l'éditorialiste du NIEUWE ROTTERDAMSE COURANT, libéral, s'en console aisément :


" L'attitude de solidarité de la Belgique à notre égard est une consolation, écrit-il. L'esprit du Benelux est plus fort que jamais, c'est une compensation qui est d'une grande signification pour notre avenir. Partenaire au pacte des Cinq, la France également s'est tenue aux côtés de la Hollande. "


Du moins au conseil de sécurité.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006269.xml</field>
    <field name="pdate">1949-01-01T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
