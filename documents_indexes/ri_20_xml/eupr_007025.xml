<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Facebook : voici ce que Mark Zuckerberg a oublié de dire dans sa tribune</field>
    <field name="author">L'Obs,</field>
    <field name="id">eupr_007025.xml</field>
    <field name="description"><![CDATA[


Mark Zuckerberg va fêter en février les 15 ans de Facebook, et à cette occasion il publie une tribune dans "le Monde", le "Wall Street Journal" et plusieurs titres européens. Ces journaux auraient plutôt dû lui vendre une page de publicité, tant ce texte de 8.000 signes, titré dans "le Monde" "Je souhaite clarifier la manière dont Facebook fonctionne", est un tissu d'omissions intéressées et d'approximations.


Pour le fondateur du réseau social, qui a connu de 2016 à 2018 des années agitées, cet exercice de com' est autrement confortable que de devoir répondre à des questions, par exemple sous le feu des parlementaires américains.


Plus subtil que Trump, Zuckerberg ne ment pas frontalement mais biaise en permanence, arrivant ainsi lui aussi à des "faits alternatifs" au rapport modéré avec la réalité. Revenons sur trois sujets : le filtrage publicitaire, les données personnelles et le RGPD.


Pubs ciblées : du bon oubli de la discrimination


Tout le texte publié ce vendredi veut justifier son modèle économique, gratuit et fondé sur la publicité. Il explique donc :


"Les gens nous disent que s'ils doivent voir des publicités, celles-ci doivent être pertinentes pour eux. Pour cela, nous devons comprendre leurs centres d'intérêt. Nous créons donc des catégories - par exemple, 'personne aimant le jardinage et vivant en Espagne' - à partir des pages qu'ils aiment ou encore des contenus sur lesquels ils cliquent.


Puis, nous facturons les annonceurs pour qu'ils puissent montrer leurs publicités à ces catégories de personnes. (...) Internet permet aussi d'offrir une plus grande transparence et un plus grand contrôle sur les publicités que vous voyez, par rapport à la télévision, la radio ou la presse écrite."


Cette ode à la transparence et au contrôle oublie un point clé : ce filtrage par catégories est régulièrement dénoncé pour ses effets pervers, ni transparents ni contrôlés - sinon a posteriori quand ils ont été dénoncés de l'extérieur.


Revenons à Facebook et au ciblage publicitaire des antisémitesCes nombreuses options de filtrage offertes aux annonceurs se sont en effet plusieurs fois avérées permettre des discriminations, notamment racistes.


En octobre 2016, une enquête du journal en ligne ProPublica révélait que le système d'achat de publicités permettait aux annonceurs de cibler leurs destinataires de façon à exclure des catégories d'internautes selon des critères de race ou d'origine, entre autres.


On pouvait choisir qu'un message publicitaire ne soit pas vu de personnes ayant une affinité avec les Afro-Américains, les Américains d'origine asiatique ou les hispaniques. Ce qui pouvait amener à des discriminations illégales aux Etats-Unis en matière de logement par exemple, interdisant de discriminer selon de tels critères. Facebook a réagi en promettant de mettre fin à cette situation, et en février 2017 il a annoncé un système automatique pour détecter les publicités problématiques.


Raté : en septembre 2017, ProPublica montrait que des publicités pouvaient cibler des utilisateurs affichant leur antisémitisme. En novembre de la même année, ProPublica débusquait de nombreuses possibilités de filtrage discriminant : ses journalistes ont pu acheter des publicités invisibles aux "Afro-Américains, mères de lycéens, personnes intéressées par des fauteuils roulants, Juifs, expatriés d'Argentine et personnes de langue espagnole".


Facebook réagissait en admettant "un raté technique" auquel il allait mettre bon ordre. En vain semble-t-il, puisqu'en août 2018 le ministère américain du Logement portait plainte contre l'entreprise, accusée d'autoriser des pratiques discriminatoires. Cette fois, Facebook a annoncé supprimer plus de 5.000 options de filtrage publicitaire pour éviter des publicités discriminatoires.


La plupart de ces options permettaient d'identifier et d'exclure des groupes ethniques ou religieux. Ainsi en bloquant les personnes intéressées par "Passover" (en anglais, la fête juive de Pessah), un annonceur pouvait empêcher des internautes juifs de voir ses publicités. Une annonce de location immobilière excluant les personnes intéressées par la culture amérindienne pouvait empêcher des Amérindiens de louer, etc.


Des incidents techniques malheureux... ou la conséquence répétée d'un système ? En 2017, Olivier Ertzscheid, maître de conférences en sciences de l'information et de la communication, auteur entre autres de "L'appétit des géants" (C&F Editions), commentait :


"Je ne crois pas un seul instant que ce ménage sera effectué de manière pérenne et que ces biais, sortis par la porte de l'opinion publique effarouchée, ne reviendront pas par la fenêtre de l'intérêt commercial supérieur."


Données personnelles : Cambridge Analytica, vous connaissez ?


C'est peut-être le sujet le plus brûlant pour Facebook : la montagne d'informations personnelles que le réseau social accumule sur ses utilisateurs.


Des chercheurs estiment depuis des années pouvoir extrapoler l'âge, l'orientation politique, sexuelle, la religion et d'autres données individuelles en se basant sur les "j'aime" ("like") sur le réseau social. Selon une équipe de Cambridge, sur la base de 68 "j'aime" d'un utilisateur, ils pouvaient prédire la couleur de peau (à 95% d'exactitude), l'orientation sexuelle (à 88% d'exactitude), et l'affiliation aux Républicains ou aux Démocrates (à 85%). Et d'autres traits pouvaient aussi être estimés, comme la religion, la consommation d'alcool, de tabac ou de drogues...


Enquête sur l'algo le plus flippant de FacebookZuckerberg écrit :


"Parfois, les gens en déduisent que nous faisons des choses que nous ne faisons pas. Par exemple, nous ne vendons pas les données des gens, contrairement à ce qui est souvent rapporté. En réalité, vendre les données des utilisateurs aux annonceurs irait à l'encontre de nos intérêts commerciaux, car cela réduirait la valeur intrinsèque de notre service pour ces derniers. Il est donc dans notre intérêt de protéger les données personnelles et d'empêcher qu'elles soient accessibles à d'autres."


La tribune compte 1.369 mots, mais n'y figurent pas ces deux-ci : Cambridge Analytica. C'est pourtant avec la révélation en mars 2018 par le "New York Times" et le "Guardian" du rôle de cette entreprise de marketing politique dans la victoire de Trump, pour lequel elle disait avoir profilé la totalité de la population américaine, que les ennuis de Facebook sont devenus importants.


Après la victoire du milliardaire, le PDG de Cambridge Analytica, Alexander Nix, publiait ce communiqué triomphaliste (supprimé ensuite, mais archivé là) :


"Nous sommes ravis que notre approche révolutionnaire de la communication, fondée sur les données, ait joué un rôle si important dans la victoire extraordinaire du président élu Trump."


Cambridge Analytica, détenue par le discret milliardaire ultra-conservateur Robert Mercer et alors dirigée par Steve Bannon, qui sera ensuite directeur de campagne puis conseiller spécial de Trump (avant d'être viré avec fracas), a récolté des données sans autorisation, en 2014, auprès de dizaines de millions d'utilisateurs Facebook, profitant d'une fonctionnalité, supprimée ensuite par le réseau social.


A la source de ces révélations, un scientifique des données ("data scientist"), Christopher Wylie, a expliqué :


"Nous avons utilisé Facebook pour récolter des millions de profils de gens. Et construit des modèles pour exploiter ce que nous savions d'eux et cibler leurs démons intérieurs. C'était la base sur laquelle a été construite toute la compagnie."


Christophe Wylie a été alors bloqué sur Facebook, de même qu'Instagram et WhatsApp (qui appartiennent à Facebook). La transparence a ses limites.


Just to sum up. 1) Facebook broke the law. 2) Cambridge Analytica broke the law. 3) Vote Leave broke the law. 4) LeaveEU broke the law. 5) Brexit and Trump were both won through breaking the law. 6) Facebook let it all happen and covered it up.

https://t.co/CAOrP5rKry



-- Christopher Wylie ?????????? (@chrisinsilico) 11 juillet 2018


En avril 2018, une autre ex-employée, Brittany Kaiser, a estimé devant des parlementaires britanniques que des informations personnelles de citoyens du Royaume-Uni ont été utilisées abusivement pendant la campagne pour le référendum sur le Brexit, et que le nombre d'utilisateurs de Facebook dont les données avaient été siphonnées par CA était bien plus grand que les 87 millions admis par le réseau social.


"Nous ne vendons pas les données des gens", écrit Mark Zuckerberg aujourd'hui : nuance... il les a échangées pendant des années avec d'autres géants du Web comme Amazon ou Microsoft, selon une enquête en décembre 2018 du "New York Times".


Le RGPD, pour mémoire


Puisqu'il est question de données personnelles, le PDG pouvait difficilement faire l'impasse sur le RGPD, ce règlement européen  qui oblige les acteurs du Net, même hors-UE, à améliorer leurs pratiques. Ce qui donne dans la tribune :


"Lorsque, en accord avec le Règlement général sur la protection des données (RGPD), nous avons demandé aux gens la permission d'utiliser ces informations pour améliorer les publicités qu'ils voient, la grande majorité a donné son accord, car ils préfèrent voir des réclames plus pertinentes."


Le RGPD, c'est simple, Zuckerberg n'en pense quasiment que du bien, d'ailleurs sa compagnie affirmait en avril 2018 "Facebook se conforme au RGPD et offre de nouvelles protections à tous, partout dans le monde".


Facebook va mettre hors de portée de la loi européenne 1,5 milliard d'utilisateursSauf que... le réseau social s'est empressé avant l'entrée en vigueur, le 25 mai 2018, du RGPD, de mettre hors de portée d'application 1,5 milliard de ses utilisateurs (ou plus exactement de ses comptes - certains internautes en ayant plusieurs - sur les 2 milliards atteints fin 2018). Auparavant rattachés à Facebook Irlande, une filiale créée en 2008 pour profiter de la fiscalité locale très tempérée, 1,5 milliard d'utilisateurs de Facebook d'Afrique, Asie,

Australie

et Amérique latine, ont opportunément été "rapatriés" aux Etats-Unis.


Mark Zuckerberg avait argumenté en citant la concurrence chinoise (où Facebook, comme Twitter ou Google parmi d'autres, est toujours interdit) en déclarant devant le Congrès américain, en avril 2018 :


"Il y a un équilibre extrêmement important à trouver quand il faut donner un consentement spécial pour des choses sensibles comme la reconnaissance faciale. Mais ne faites pas... - mais nous devons toujours faire en sorte que les entreprises américaines puissent innover dans ces domaines. Ou sinon, nous allons passer derrière les concurrents chinois et d'autres dans le monde qui ont des régimes différents pour de nouvelles fonctionnalités comme celle-là."


Pourquoi attaquer Facebook ?

https://t.co/6FhqAA8heO



Nous détaillons aujourd'hui la surveillance de masse imposée par Facebook.#RGPD #ContreLesGafam pic.twitter.com/fsZDnN2H6D


-- La Quadrature du Net (@laquadrature) 19 avril 2018




Facebook, Google... Tous fichésPar ailleurs, si "la grande majorité a donné son accord" comme le dit dans sa tribune ce vendredi Zuckerberg, ont-ils le choix ? Comment leur sont présentées les conditions (sur la reconnaissance faciale, par exemple, glissée dans un recoin) ? La Cnil a justifié sa condamnation de Google cette semaine (50 millions d'euros, un montant record permis par le RGPD, et qui aurait pu aller jusqu'à 4% du chiffre d'affaires mondial) par la complication qui empêche l'internaute de régler facilement ses exigences de vie privée.


Voir aussi les griefs de la plainte collective déposée par l'association La Quadrature du Net (qui s'est appuyée sur le RGPD pour lancer sa campagne contre les GAFAM, dont le premier effet a été la condamnation de Google). Cette plainte est du ressort de l'Irlande ; l'autorité locale équivalente de la Cnil sera-t-elle convaincue par les arguments de Zuckerberg sur cette merveille de transparence que serait Facebook ?



Thierry Noisette



Cet article est paru dans L'Obs (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007025.xml</field>
    <field name="pdate">2019-01-25T00:00:00Z</field>
    <field name="pubname">L'Obs (site web)</field>
    <field name="pubname_short">L'Obs (site web)</field>
    <field name="pubname_long">L'Obs (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Economie</field>
  </doc>
</add>
