<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">« Tout le paysage est comme une allumette » : récit d’une semaine dans l’enfer des feux australiens</field>
    <field name="author">Christophe Ayad</field>
    <field name="id">eupr_004011.xml</field>
    <field name="description"><![CDATA[


La fin du monde sera grise. Grise comme le ciel de Melbourne, de Canberra ou de Sydney, obscurci par la fumée des incendies. Elle sera rouge aussi, comme le soleil australien qui semble ne plus savoir distinguer le jour de la nuit. Elle sera noire, enfin, comme les forêts ravagées par les incendies qui brûlent sans discontinuer depuis des semaines, voire des mois et pour encore des semaines, voire des mois.


Ces incendies, qui ont détruit 80 000 km2 de forêts (la taille de l’Irlande) depuis septembre, ont causé jusqu’à présent la mort de 28 personnes et détruit 2 000 habitations et propriétés. Un bilan humain et matériel relativement faible par comparaison avec les surfaces ravagées, qui représentent plus de trois fois celles qui ont brûlé l’été dernier au Brésil, et dix fois celles de Californie en 2018. La perte pour la biodiversité est incommensurable : plus d’un milliard d’animaux auraient péri dans les incendies ; plus de 80 % de la forêt d’eucalyptus des montagnes Bleues aurait disparu, 50 % de la forêt tropicale de Gondwana. L’île Kangourou, refuge de la principale colonie de koalas sains (l’espèce est fragilisée par une maladie provoquant la cécité), est à moitié détruite.


Les incendies en

Australie

ont déclenché une vague de solidarité sans précédent mais aussi beaucoup d’indignation. Récit d’une semaine dans un pays meurtri, choqué, partagé entre la honte et la colère.



Peur sur les Snowy Mountains



Vendredi 10 janvier, alerte rouge sur les Snowy Mountains, dans le sud-ouest de la Nouvelle-Galles du Sud. Les prévisions météo sont très mauvaises : grosse chaleur (de 30 °C à 40 °C) et vents violents. Tout est réuni pour un nouveau week-end catastrophique. Mais cette fois, les autorités ont pris les devants, la radio ne cesse de marteler : « Prenez vos dispositions pour défendre votre maison, ou alors partez maintenant ! Quand le feu sera là, il sera trop tard. »


C’est toujours le même dilemme : rester – et risquer de perdre la vie si les choses tournent mal – ou partir – et risquer de perdre sa maison, faute d’avoir été là pour éteindre les départs de feu. Face aux flammes, il y a deux catégories d’Australiens : ceux qui restent défendre leur maison et ceux qui partent. Sans qu’aucun groupe ne blâme l’autre. Dans ce pays grand comme un continent, bâti par des colons et des pionniers, c’est à chacun de se prendre en main. Même les pompiers sont volontaires. Ici, on n’attend pas grand-chose de l’Etat.


En fait, il n’y a plus grand monde à évacuer en ce vendredi de fournaise. La plupart des touristes avaient déserté la région le samedi 4 janvier, lorsque les flammes s’étaient dangereusement déjà rapprochées du village d’Adaminaby (60 km au sud-est de Canberra), au pied des Alpes australiennes. Ceux qui sont restés sont les fermiers du coin et les habitants qui vivent ici à l’année.


Les résidences secondaires sont vides, tout comme le village de vacances, déserté. La saison estivale est finie pour ce village de montagne en lisière des parcs nationaux de Namadgi et du Kosciuszko, qui abrite le plus haut sommet du pays (mont Kosciuszko, 2 228 m). Cet immense ensemble de parcs nationaux forme le deuxième poumon de la Nouvelle-Galles du Sud, avec les montagnes Bleues, à l’ouest de Sydney.



Abrogation de la taxe carbone



C’est le branle-bas de combat au local des pompiers, tous volontaires, d’Adaminaby. Les hommes du Rural Fire Service (RFS) ont reçu des renforts de toute la région, jusqu’à Sydney. Casquettes et pantalons bleus, chemises jaunes, les hommes, de tous âges, arborent un écusson à l’effigie de leur unité d’origine. Mais un quidam attire l’attention plus que les autres. Les oreilles décollées, le teint couperosé par la forte chaleur, la bouche en accent circonflexe, les orbites profondément enfoncées, il n’y a pas de doute, c’est Tony Abbott, premier ministre de 2013 à 2015 et chef de file des climatosceptiques du Parti libéral d’

Australie

(conservateur, au pouvoir).


Pendant son court passage au pouvoir, il avait abrogé la taxe carbone, réduit les investissements publics dans les énergies renouvelables de 70 %, supprimé le ministère des sciences, l’Autorité du changement climatique et la Commission pour le climat. Il avait aussi autorisé le rejet dans la Grande Barrière de corail des déchets de dragage d’un port, destiné à l’exportation du charbon.


La presse locale tente sa chance, mais il décline au nom du respect dû à ses collègues pompiers : « Je ne suis qu’un parmi d’autres, ici. » A 62 ans, Abbott n’est pas pompier volontaire pour la galerie. Il est fréquent de le croiser sur des incendies. Au Monde, il accorde trois minutes et deux questions, « parce que vous êtes venus de loin » :


« L’

Australie

est-elle entrée dans une nouvelle ère où les catastrophes climatiques vont faire partie du quotidien ?


– Ces incendies sont exceptionnels en Nouvelle-Galles du Sud, oui, mais, au niveau national, je ne dirais pas que l’

Australie

traverse une situation exceptionnelle. Elle a toujours été sujette à la sécheresse, aux incendies et aux inondations, c’est dans notre histoire.


– Le gouvernement ne doit-il pas changer d’approche après cette saison catastrophique d’incendies ?


– Si vous voulez m’amener sur le terrain du changement climatique, je vous répondrai que toutes les enquêtes sur le phénomène des incendies ont montré que le paramètre essentiel était une meilleure gestion des résidus combustibles en milieu forestier. On s’en occupe pendant un temps, puis la négligence l’emporte. »


L’important est dans le non-dit, entre les lignes : le réchauffement n’est pas une réalité, la sécheresse est une fatalité, s’il faut changer quelque chose, c’est l’entretien des forêts, pas la politique du pays en matière d’émissions de carbone.


Le capitaine local des pompiers fait un dernier briefing : « Attention, ce sera une très longue journée. Vous aurez sûrement l’impression qu’il ne se passe rien, mais les conditions vont changer avec le vent et tout peut rapidement tourner mal. Gardez à l’esprit que vous êtes dans une région montagneuse. Le feu peut sauter de crête en crête, vous risquez alors de vous retrouver isolés dans une vallée. »


Dernière vérification des fréquences radio et des pointeurs GPS. Il est midi, le vent se lève. Abbott embarque dans son camion et disparaît dans la montagne. Sur l’application Fires near me, que tous les Australiens ont téléchargée sur leur téléphone, les icônes bleues (feu sous contrôle) passent les unes après les autres au jaune (foyer actif et en mouvement). Quand elles deviennent rouges, c’est qu’il s’agit d’un feu virulent et incontrôlé.


En face de la caserne, Linda n’est pas tranquille. La vieille dame sort de chez elle jeter un coup d’œil à l’horizon devenu soudain flou. On ne voit déjà plus les sommets environnants, noyés dans une fumée diffuse. « Le problème, c’est cette sécheresse terrible qui dure depuis trois ans. Tout le paysage est comme une allumette. Un rien peut l’embraser. » Mais elle non plus ne croit pas au réchauffement climatique : « C’est la nature, cela a toujours été comme ça. D’ailleurs, le climat change sans cesse, ça ne veut rien dire le changement climatique. Il faudrait construire plus de barrages, c’est tout. »


Elle n’a « pas confiance dans les politiciens pour régler les problèmes. Ils sont incompétents. Les fermiers, eux, savent ce qu’il faut faire. » Ironie du sort, Adaminaby est déjà bordé par un grand lac de barrage, destiné à fournir de l’électricité et de l’eau aux éleveurs de bétail. Mais le niveau de l’eau est tellement bas qu’il ne suffit plus aux besoins de la région.


Zak, le jeune mécanicien de la station-service, a lui aussi son idée sur la question des incendies : « Le changement climatique, c’est un cycle : sécheresse, incendies, inondations, c’est normal. Les derniers grands incendies dans la région remontent à 2003, la boucle est bouclée. Mais ce qui se passe en ce moment est la faute des écologistes : ils interdisent toute intervention dans les parcs nationaux au nom de la préservation des espèces. Je le sais bien, j’ai grandi à Tumut, en bordure d’un parc national. Avant, on brûlait régulièrement les résidus forestiers et les Aborigènes faisaient ça avant nous. On devrait s’inspirer d’eux. Mais le lobby écologiste interdit tout. »


C’est paradoxalement à la campagne que l’on rencontre le plus de climatosceptiques


Lorsqu’on lui fait remarquer que les Verts ne sont pas dans la coalition au pouvoir, il explique d’un air entendu qu’ils sont « une minorité qui a beaucoup d’influence ». C’est une opinion pourtant souvent entendue et partagée dans le bush et chez les fermiers. Quant à la technique du brûlis, le réchauffement climatique, justement, la rend trop périlleuse à mettre en œuvre, au risque de déclencher des feux majeurs.


Dans cette partie du pays, c’est paradoxalement à la campagne que l’on rencontre le plus de climatosceptiques, même si la sécheresse persistante de ces dernières années a fait évoluer les mentalités des agriculteurs. Les arbres de la campagne autour d’Adaminaby sont tellement secs qu’on les croirait calcinés avant même que le feu les touche. Certains s’effondrent d’eux-mêmes sous leur propre poids, leurs racines ne les retenant même plus au sol.


Le climatoscepticisme est largement partagé chez les pompiers également. La vision du monde de Tony Abbott correspond bien à la philosophie prométhéenne propre aux combattants du feu : l’homme et la nature se sont toujours combattus, souvent il gagne, parfois il perd. C’est sur cette foi que s’est construit le pays depuis l’arrivée du premier contingent de peuplement européen, le 26 janvier 1788, et il est très difficile de faire évoluer un mythe fondateur.


« Quand j’entends les gens comme Tony Abbott parler de fatalité, je suis furieux. Evidemment que le climat se réchauffe » Peter Fogerty, retraité


Au fur et à mesure que la journée avance, la fumée se fait de plus en plus épaisse. Mais le front des feux est difficile à discerner. Un hélicoptère et deux avions tournent dans le ciel sans qu’il soit possible de les voir. La caserne de Shannons Flat, quelques kilomètres plus à l’est, est à la pointe avancée du dispositif anti-incendie.


Le capitaine Bob Killip jongle entre son talkie-walkie, son portable, une carte détaillée et un tableau des opérations en cours. « C’est une région de hameaux et de fermes isolées. Nous nous concentrons sur la protection des biens, des animaux et des personnes, nous n’avons pas les moyens d’empêcher le feu de brûler la forêt. Notre second objectif, c’est d’empêcher que le feu s’étende au Territoire de la capitale australienne. » Canberra, la capitale fédérale, forme en effet une région autonome. Gravement affectée par les incendies de 2003, elle est, cette année, soumise à des fumées toxiques.


A ses côtés, Peter Fogerty, un retraité installé dans la région, a préféré passer la journée avec les pompiers que chez lui. « Je suis trop exposé. La semaine dernière, ma propriété a échappé aux flammes par miracle. Nous étions littéralement bombardés par les braises. Le feu allait gagner ma maison quand la température a soudain chuté et la pluie s’est déclarée. Quand j’entends les gens comme Tony Abbott parler de fatalité, je suis furieux. Evidemment que le climat se réchauffe, il n’y a aucun doute là-dessus et nous en sommes grandement responsables. Ici, on voyait de la neige sur les sommets trois mois par an il y a vingt ans. Maintenant c’est dix jours, grand maximum. »


La discussion s’engage sur les incendies : chacun a son diagnostic, sa solution au problème. Un peu comme les Français et le vin, ou la politique. Un pompier plaide pour la réduction des parcs nationaux protégés : « Ce sont des bombes à retardement que nous avons fabriquées nous-mêmes par amour idéalisé de la nature et par pingrerie, en refusant de payer pour les entretenir. » Un autre pompier est de l’avis contraire : seules les forêts, selon lui, nous sauveront du réchauffement.


Pendant ce temps, le vent retombe peu à peu, l’incendie restera contenu, du moins de ce côté du parc national du Kosciuszko car, côté occidental, à cheval entre l’Etat de Nouvelle-Galles du Sud et celui de Victoria, deux foyers ont fusionné pour former un « méga feu ». Le feu, finalement, on le rencontrera le lendemain, sur une route isolée au sud de Bombala, à une centaine de kilomètres au sud d’Adaminaby. C’est un bushfire typique, un feu de broussaille en train de dégénérer. Pour le contenir, les pompiers allument un contre-feu.


« On ne peut pas éteindre ce genre d’incendie, explique le capitaine de pompiers Max Stewart. Pour les combattre, il faut les contenir, délimiter un périmètre. On le fait soit en traçant des tranchées dans la forêt au bulldozer, soit en allumant des contre-feux qui éliminent le “carburant” de l’incendie alentour. » Après avoir pris quelques photographies, l’un de ses hobbys, il contemple son œuvre avec mélancolie : « Invoquer les pratiques des Aborigènes, c’est de la pensée magique. Ce pays est composé essentiellement d’urbains. Dans les années à venir, nous allons devoir totalement repenser notre approche et nos priorités. Il va falloir consacrer de l’argent et des moyens à nos forêts. Notre survie en dépend. »



Désolation à Mallacoota



Mallacoota, à l’extrémité sud-est du pays, ne se trouve qu’à 60 km de Bombala. Mais la route forestière est coupée. Il faut remonter au nord, rejoindre la côte et la longer vers le sud en passant par Eden. Cette station balnéaire porte aujourd’hui mal son nom. Elle est le dernier arrêt avant l’enfer. Quelques kilomètres au sud d’Eden, en effet, la route est fermée. Il suffit de contourner le panneau d’interdiction en faisant un signe entendu pour passer, mais presque personne ne s’y risque dans un pays où les gens sont plutôt respectueux des règles et des interdits.


Le long de l’autoroute des Princes, presque tout a brûlé. La forêt qui recouvre toute la région jusqu’à Mallacoota a flambé comme une allumette. Les arbres sont toujours debout, tels des cierges noircis vacillant sur leur base. Un souffle pourrait les renverser. On traverse cette cathédrale lugubre le souffle coupé et la gorge serrée. De temps à autre, un arbre s’abat dans un amas de cendres. L’armée et les services forestiers ont entrepris de raser tous ceux restés encore debout de part et d’autre de la route pour éviter les accidents. Par endroits, il a fallu entièrement refaire le bitume, qui a fondu.


A l’approche de Mallacoota, le feu semble avoir été encore plus intense. Une épaisse couche de cendres tapisse le sol, comme de la poussière lunaire. Parfois, une portion de deux ou trois kilomètres de prairie a été épargnée. On aperçoit des fermes intactes entourées de forêt calcinée. Même les étendues d’eau semblent mortes.


Mallacoota est la « ville martyre » de cette vague estivale d’incendies. Le 31 décembre, le mur de flammes qui a dévalé vers la baie dans un grondement d’avion au décollage a contraint touristes et habitants, paniqués, à se rassembler sur la plage pour échapper au danger. Cette même plage sur laquelle la marine de guerre australienne a envoyé des barges semblables à celles déployées en Normandie pendant la seconde guerre mondiale pour embarquer ceux désirant quitter la ville dans les jours suivants.


L’image n’est pas anodine dans l’imaginaire australien. La plage symbolise la douceur de vivre de ce pays, où le sport et le culte du corps tiennent lieu de culture nationale. Elle est aussi la porte d’entrée de cette île-continent : c’est là qu’ont débarqué les colons européens. C’est au nom d’une nouvelle « invasion » fantasmée du pays que le gouvernement de Tony Abbott avait mis en place l’opération « Frontières souveraines » (aussi appelée « Stopper les bateaux ») visant à empêcher toute arrivée de migrants par la mer. Cette évacuation de quelque 1 500 résidents de Mallacoota par la mer, c’est un peu comme si l’

Australie

moderne refermait la boucle de son histoire : ses occupants, arrivés par la mer, y sont comme rejetés par les incendies qu’ils ont contribué à allumer.



« Qui va vouloir venir en vacances ici ? »



« Les médias racontent que c’était le chaos et la panique, ce n’est pas vrai du tout, s’insurge Carla Todd, une professeure de sport en vacances avec son mari et ses deux filles. Les trois évacuations se sont passées dans le calme et la sérénité. » Malgré l’asthme de sa fille cadette, malgré les conditions de vie difficiles et la fumée permanente qui l’oblige à porter un masque filtrant, Carla Todd a tenu à rester sur place jusqu’au mardi 14 janvier, par solidarité avec les habitants et par amour pour cette région où elle revient chaque année depuis son enfance.


Mallacoota est un site sans pareil. Entouré de forêts, le village est bordé par un lac d’un côté et la mer de l’autre. Plus d’une centaine de maisons y ont brûlé. C’est beaucoup à l’échelle d’une toute petite communauté. Certains quartiers ont été comme bombardés : une maison sur deux y est détruite par les flammes. Surtout, il n’y a aucun espoir que le paysage retrouve rapidement la même apparence. « Je ne sais pas qui va vouloir venir en vacances ici désormais », se désole Grant Cockburn, 57 ans, loueur de bateaux.


Son équipement est intact, mais c’est en janvier qu’il effectuait d’habitude 25 % de son chiffre d’affaires annuel. « J’espère juste que les habitués vont continuer à nous soutenir. C’est ce qu’ils pourraient faire de mieux. » Mais il se doute que l’avenir sera sombre : « Quand la pluie reviendra, toute la cendre va partir dans le lac, qui sera pollué et perdra ses poissons. Où que je regarde, je ne vois pas de solution. » L’océan gronde non loin de là, indifférent aux arbres calcinés qui s’étendent jusqu’au bord de son rivage.



L’économie du pays affectée



Le gouvernement a annoncé un plan d’urgence de 2 milliards de dollars australiens (1,2 milliard d’euros) mais il y a de grandes chances que cette somme soit largement insuffisante. Jamais l’économie du pays n’avait été touchée à une telle échelle. Cette fois-ci, le secteur agricole n’est pas le seul affecté. A Mallacoota, outre le tourisme, la principale source d’emplois est l’usine de conditionnement d’ormeaux, un mollusque local. Elle a entièrement brûlé.


Pour le moment, l’aide d’urgence garantit 1 000 dollars australiens (620 euros) par adulte à ceux qui ont perdu un proche ou leur domicile, ou encore ont été gravement blessés, et 400 par enfant. Une indemnité de perte d’activité qui peut monter jusqu’à treize semaines de revenus est prévue, mais elle ne suffira probablement pas.


Une aide de 50 millions a aussi été débloquée pour la sauvegarde de la faune en danger, la moitié pour mener une étude complète des besoins et l’autre pour envoyer des premiers secours. Mais, là aussi, il faudra beaucoup plus pour sauver les animaux qui peuvent l’être alors que les évaluations des scientifiques locaux font désormais état d’un milliard d’animaux morts dans les incendies. Mallacoota est un bon exemple. La ville est parsemée de panneaux jaunes mettant en garde les automobilistes sur la présence de koalas. C’était en effet l’une des rares villes d’

Australie

où l’on pouvait croiser cet animal, particulièrement craintif, en pleine localité.



« Un vrai crève-cœur »



Jack Bruce, un jeune ingénieur de 31 ans revenu d’Europe au pays pour les vacances de Noël, a entrepris avec deux autres personnes de recueillir les koalas blessés par le feu. « C’est un vrai crève-cœur, raconte-t-il. J’en ai trouvé des dizaines de morts des suites de leurs brûlures dans le petit bois derrière la maison de mon père. Ceux qui ont survécu ne savent pas où aller pour se nourrir, leur environnement naturel a complètement disparu. »


Les eucalyptus sont calcinés et n’ont plus de feuilles sur des dizaines de kilomètres à la ronde. Mardi, il a relâché Dora, une jeune femelle recueillie le 1er janvier, dans un rare bosquet d’arbres intacts. L’animal, particulièrement traumatisé, a été soigné par un groupe de vétérinaires dépêchés par les autorités par hélicoptère. « Nous avons sauvé 70 koalas à ce jour, se réjouit Jack. Mais ce qui est rageant, c’est que nous sommes seuls à chercher. Les gardes forestiers sont occupés à déblayer les routes et ne nous aident pas. »


A ce jour, Mallacoota reste coupée de Melbourne, la capitale de l’Etat de Victoria, auquel elle est rattachée. Vers le nord, en direction de Sydney, les autorités organisent des convois accompagnés pour permettre à ceux qui veulent partir de quitter la ville. Après l’adrénaline des premiers jours qui ont suivi la catastrophe, la ville est guettée par une forme de dépression. Ses habitants réalisent qu’un retour à la normale prendra des années.


Que le paysage ne sera peut-être plus jamais le même. Les médias qui défilent, les rumeurs de pillage des propriétés abandonnées mettent les nerfs à vif. Les routes coupées engendrent un syndrome d’enfermement. L’arrivée de la pluie est accueillie avec soulagement après des jours passés dans une fumée étouffante. Mais les pompiers mettent désormais en garde contre les glissements de terrain ou la pollution des sols et des retenues d’eau.


Après la solidarité des premiers jours face à l’adversité, quand il fallait partager la nourriture, l’eau potable et les générateurs d’électricité, les différends commencent à réapparaître. Tony ne décolère pas. L’homme, qui se décrit comme un « activiste », est connu dans le coin : « Ils me détestent ici parce que je les empêche de faire leurs petites affaires en rond. On m’a menacé de mort parce que je protestais contre la destruction d’un bois destinée à dégager une vue sur la mer pour les golfeurs. Il n’y a que l’argent qui les préoccupe. D’ailleurs, le jour de l’incendie, si on a perdu autant de maisons, c’est parce que les pompiers étaient occupés à sauver les touristes. Nous sommes une ville de 1 000 habitants et nous accueillons 7 000 touristes sans aucun plan contre les incendies. C’est fou. »


Cette partie de la côte australienne est en effet de plus en plus urbanisée. Il y a peu, il a fallu détruire à l’explosif une partie des rochers bordant la plage de Mallacoota pour installer une rampe d’accès destinée aux bateaux de plaisance. Depuis l’incendie, Tony héberge son meilleur ami, dont la maison héritée l’année dernière de son père a brûlé dans l’incendie : « Je dois frapper en entrant chez moi pour savoir si je ne dérange pas », rigole-t-il amèrement, avant de repartir avec son chien en promenade.



Solidarité à Cobargo



Comme Mallacoota, Cobargo, à une centaine de kilomètres plus au nord, est un village à part. Y cohabitent des fermiers traditionnels et une communauté hippie punk attirée par le grand air, un festival de musique, une vie bon marché et une ambiance sans façons. Plusieurs artistes et musiciens se sont installés dans le coin, ainsi que des retraités venus de Sydney et de Canberra, souvent des intellectuels de gauche. Un mélange de Normandie et de Larzac avec, en prime, la mer à 10 km. L’incendie qui a frappé la ville entre Noël et le Nouvel An a mis en exergue sa singularité. Dès le lendemain, des bénévoles locaux ont installé un camp de secours pour ceux ayant perdu leur logis sur un terrain communal. Alfredo La Caprara, dit « Alfie », un descendant d’immigrants siciliens, le gère de main de maître. Il a l’habitude : il est aussi l’organisateur du festival folk local.


C’est là, juste devant ce centre, que Cobargo est devenu célèbre. Le premier ministre, Scott Morrison, y a effectué, jeudi 2 janvier, une visite mouvementée qui a tourné au désastre. Violemment critiqué pour son absence – il était en vacances à Hawaï avec sa famille – lorsque les incendies ont pris une tournure dramatique avant Noël, en frappant les villes de la côte sud-est en pleine saison estivale, celui que le public surnomme « Scomo » a saisi l’invitation lancée par Tony Allen, ancien maire et vieux militant local du Parti libéral (conservateur, au pouvoir), sa formation. Tony Allen est un fermier à l’ancienne, qui élève quelques centaines de vaches laitières. Il pensait bien faire en offrant à son leader une occasion de se rattraper sur le terrain. Aujourd’hui, il ne veut pas s’étendre sur l’incident : « Je regrette ce qui s’est passé. Mais c’est fini, maintenant, il faut regarder vers l’avenir ».


L’incident ? En fait, la visite a tourné au fiasco. D’abord, Morrison a rencontré un pompier qui lui disait être épuisé après avoir travaillé quarante-huit heures sans manger et avoir perdu sa ferme. « Bon, ben vous avez à faire, je ne vous retiens pas ! », lui a répondu le premier ministre, en lui tapotant l’épaule. Morrison, qui vient du monde du marketing, est ensuite arrivé vers les habitants avec un grand sourire : « Comment allez-vous ? » Quand une jeune femme, Zoey, lui dit « Je ne vous serrerai pas la main si vous ne nous envoyez pas plus de secours », il lui tourne le dos. Il attrape la main d’un pompier qui retire la sienne. Enfin, il est pris à partie par un couple de punks du coin, Johnny et Danielle, qui lui crient : « Où étiez-vous quand nous avions besoin de vous ? Pourquoi n’avions-nous que quatre camions de pompiers pour défendre toute cette région ? »


La vidéo, d’à peine une minute et demie, a plus fait pour détruire la réputation de « Scomo » que des années d’inaction. Il y apparaît comme un leader sans charisme, sans empathie, un homme dépassé. Il n’a pas arrangé son cas en se félicitant que les incendies n’aient tué personne lors d’une visite éclair sur l’île Kangourou, alors que deux pompiers locaux avaient perdu la vie. Morrison est, de fait, un leader de compromis entre les durs du Parti libéral, dirigés par Tony Abbott, et les progressistes, longtemps incarnés par Malcolm Turnbull.


En 2015, Turnbull a renversé Abbott dans un putsch interne, puis, en 2018, c’est Abbott qui a défait Turnbull. Scott Morrison s’est retrouvé premier ministre par défaut. A la surprise générale, il a remporté les législatives l’année suivante, et le voilà leader d’un parti sans tête, Turnbull ayant quitté la vie politique et Abbott ayant perdu son siège de député. Il n’est pas climatosceptique comme Abbott, mais suffisamment probusiness pour laisser les coudées franches au lobby du charbon, qui est à l’

Australie

ce qu’est le nucléaire à la France.


Tout en promettant de réduire les émissions carbone du pays, il freine toute législation dans ce sens et passe son temps à ergoter sur les quotas de réduction agréés à Paris lors de la COP21, en 2015, en voulant y inclure des crédits carbone remontant au protocole de Kyoto (1997). En revanche, il réprime durement le mouvement Stop Adani, qui s’oppose au projet de plus grande mine de charbon du monde à Carmichael, dans le Queensland.


Cette visite désastreuse à Cobargo a fait chuter Morrison dans les sondages, où il a perdu 9 points, passant de 43 % à 52 % d’opinions défavorables en un mois. Il a dû reconnaître « avoir fait des erreurs » et promettre de « faire plus pour réduire les émissions [carbone] », mais sans dire quand ni comment. En revanche, elle a donné aux habitants de Cobargo une forme d’énergie et de fierté qui leur sert de moteur jusqu’à ce jour. Lorsque, deux jours après la visite du premier ministre, la police est arrivée pour demander que les sinistrés installés dans le refuge de Cobargo aillent rejoindre le centre mis en place par les autorités à Bega, 30 km plus au sud, pas un habitant n’a accepté de bouger.


« Pas question de fermer notre refuge, s’exclame un habitant. Ici, il y a de la solidarité, de l’entraide, tout le monde se connaît et se tient les coudes malgré les divergences. » Le vice-premier ministre, Michael McCormack, en visite de rattrapage à Cobargo quelques jours plus tard, a reçu le même accueil que « Scomo » et la police, mais sans caméras. « Au moins, notre clash a servi à nous situer sur la carte de l’

Australie

et du monde », rigole Danielle, l’ex-juriste qui avait pris Morrison à partie. Il est vrai que, depuis, les donations en faveur de Cobargo ont fait un bond spectaculaire.



Cagnotte Facebook



L’entrepôt de Cobargo déborde de nourriture, de vêtements arrivant tous les jours par camions. « Tous ces dons, c’est contre-productif, ça coule le commerce local, soupire Seona, une humanitaire bénévole. Ce dont les gens ont vraiment besoin, c’est d’argent liquide. » Cobargo est devenu un tel symbole que les villages alentour, parfois plus détruits mais moins connus, comme Mogo, en souffrent. C’est aussi le cas de Quaama, à 5 km plus au sud, mais qui n’a pas la chance d’être sur la route principale. Peu importe, donner à Cobargo, c’est afficher son appartenance à la nation. C’est peu ou prou ce qu’explique candidement Jasmeet Singh, responsable d’une organisation caritative sikh, Turbans for Australia : « C’est important que nous soyons présents dans ces circonstances. Et j’espère que cela changera le regard de certains sur nous. »


Au niveau national, Celeste Barber, comédienne et humoriste, a déclenché une avalanche de dons en ouvrant une cagnotte Facebook qui a récolté quelque 50 millions de dollars australiens depuis le 2 janvier. Depuis, tout le monde s’y est mis. Dans les commerces, des troncs servent à recueillir les dons. Le capitaine de l’équipe nationale de cricket a mis sa casquette aux enchères : elle est partie pour un million de dollars. Des stars du tennis présentes en vue de l’Open d’

Australie

prévu à Melbourne la semaine prochaine ont versé les recettes d’un match de gala, etc.



Mélange de kermesse et de veillée funèbre



Cette solidarité se traduit au quotidien par le fait qu’il n’y a presque plus de sans-logis dans le centre de secours de Cobargo. La plupart se sont installés chez des amis ou de la famille. « Mais il faut voir comment cela va se passer dans plusieurs mois, s’inquiète Siona. Avec le temps et la promiscuité, des tensions risquent d’apparaître. Il y a aussi ceux qui n’avaient pas les moyens d’assurer leur maison. Il y en a plus qu’on ne le croit, c’est une région où le revenu moyen représente un tiers de celui à Sydney [la capitale économique du pays, à 350 km au nord]. Ceux-là ont vraiment tout perdu. » Et il y a ceux qui savent aussi qu’au village voisin de Tathra seules cinq maisons sur les soixante-dix qui avaient brûlé en mars 2018 ont été reconstruites.


Dimanche 12 janvier, le pub de Cobargo organisait un grand barbecue pour que les habitants puissent se retrouver pour la première fois depuis les incendies. C’était un mélange de kermesse et de veillée funèbre, un moment d’une infinie tendresse où l’on se touche, s’embrasse et se donne l’accolade, les yeux humides et la voix étranglée. Chacun avait besoin de parler, de raconter sa frayeur, le crève-cœur des vaches blessées qu’il a fallu achever d’une balle dans la tête puis enterrer dans une tranchée creusée au bulldozer, le lait répandu par terre car il n’y a plus les moyens de le stocker, les vignes brûlées sur leurs ceps et les raisins réduits à la taille d’une tête d’épingle, le cri des koalas et des kangourous blessés à mort par les flammes.


Il y a ceux qui ont perdu leur maison mais conservé leur outil de travail, et il y a le contraire. Il y a aussi cet aveu étonnant de franchise de Paul : « J’ai passé quatre nuits dans un centre de secours à Bega. Il y avait tout le confort possible, et depuis je peux imaginer ce que c’était d’être un réfugié coincé dans un camp de rétention à Manus, en Papouasie-Nouvelle-Guinée, pendant deux ans, sans savoir de quoi sera fait le lendemain, sans aucune prise sur son destin. » Surtout, il y a un père et son fils, avalés par les flammes dans la vallée de Wandella. Et ce troisième mort, qui n’est toujours pas nommé. On se touche, comme pour se persuader que l’on est bien vivant. On a bu, chanté et dansé.


Le seul politicien autorisé à prendre la parole est Andrew Constance, élu dans la circonscription de Bega. C’est un libéral, du même parti que Morrison, mais qui a gagné le respect en défendant sa maison la lance à incendie à la main. « Nous avons été abandonnés à nous-mêmes », se plaint un pompier. « Vive la République indépendante de Cobargo », s’exclame le patron du pub. Constance évite le piège de la polémique, déplacée ce jour-là. Mais, dans les médias, il n’a pas eu de mots assez durs pour le leadership défaillant du premier ministre. Il représente l’avenir d’un parti sclérosé et décrédibilisé par la crise des incendies ; il ne serait pas étonnant qu’il perce à l’avenir sur la scène nationale. C’est en effet au sein du Parti libéral, au pouvoir pour les trois années et demie à venir, que vont se dessiner les évolutions des politiques menées. Scott Morrison paraît pour le moins forcé de faire des concessions tant sa situation est fragile.



Colère à Sydney



Mercredi 15 janvier, il est 18 heures sur le célèbre quai circulaire du port de Sydney. Une petite foule est rassemblée devant l’ancienne Maison de la douane, bâtie en 1845. Le célèbre opéra, dont le toit en forme de voiles est devenu l’emblème de la ville, n’est pas loin : samedi, à la nuit tombée, on y a projeté des images de pompiers pour rendre hommage aux héros de tout un pays. Ce mercredi, l’heure n’est pas aux hommages mais à la colère. L’association des Etudiants pour la justice climatique a appelé à un rassemblement destiné à dénoncer les choix procharbon du gouvernement. Avec le mouvement Stop Adani et Extinction Rebellion, ils sont en pointe de la mobilisation locale. « Fuck Scott Morrison », s’époumone un lycéen sous les yeux des passants goguenards.


La foule n’est pas au rendez-vous : 200 personnes tout au plus. Le timing n’est pas bon : une manifestation a déjà rassemblé quelque 30 000 personnes, vendredi 10 janvier. Un grand meeting est prévu le dimanche 26, jour de la fête nationale. « Le pays est encore en vacances scolaires et, ici, on ne manifeste pas facilement comme chez vous les Français », justifie Lilly, une organisatrice, avec un mélange d’ironie et d’envie. Mais la manifestation a beau être de faible ampleur, elle s’inscrit dans un contexte d’agitation permanente. Les autorités, particulièrement nerveuses, poursuivent systématiquement les activistes d’Extinction Rebellion et de Stop Adani lorsqu’ils s’en prennent à des investisseurs locaux ou étrangers comme l’allemand Siemens, partenaire du projet minier Adani. Lilly, qui a été arrêtée en octobre et a passé vingt-huit heures en garde à vue, s’apprête à être inculpée.


Ce mercredi, les banderoles évoquent pêle-mêle les koalas, le lobby du charbon et Rupert Murdoch, le magnat australien des médias. Ses titres et chaînes de télé sont accusés de délibérément exagérer le nombre d’incendiaires arrêtés pour minimiser le rôle du réchauffement climatique, auquel il ne croit pas. Quant au premier ministre, « Scomo », son surnom a été transformé en « Smoko », un jeu de mot mélangeant son acronyme et le mot « smoke » (« fumée »).



L’Open d’

Australie

de tennis



Le sujet n’est pas pris à la légère. L’une des principales nouveautés de cette vague d’incendies sans précédent est la fumée qui a envahi les grandes villes depuis des semaines. Une vieille dame est morte peu après le réveillon pour insuffisance respiratoire à Canberra. Autre cas, celui de Courtney Partridge-McLennan, une adolescente décédée en novembre d’une crise d’asthme à Glen Innes, entre Sydney et Brisbane. Partout les urgences respiratoires sont débordées, et le port de masque filtrant est devenu chose courante dans les rues. Mais quand on fait remarquer aux Australiens que leurs mégapoles sont plus polluées que Pékin ou Shanghaï, ils marquent un instant de stupéfaction.


Le grand sujet du moment est l’Open d’

Australie

de tennis, prévu du 20 janvier au 2 février à Melbourne. Pourra-t-il se tenir normalement alors que les qualifications, mardi 14 janvier, ont vu une joueuse contrainte d’abandonner, au bord de l’asphyxie ? La gestion du problème par le directeur du tournoi n’a rien à envier à celle du premier ministre : nous avons suivi l’avis des experts, tout va bien, circulez, il n’y a rien à voir ! Dans un pays où le sport tient lieu de culture, l’enjeu n’est pas mince. L’honneur national est en jeu.


Or, depuis que la question des incendies est devenue un enjeu médiatique international, une gêne transparaît. Celle qui consiste à être désignés comme les gardiens négligents d’un des plus beaux conservatoires de la nature mondiale. « Nous n’en pouvons plus d’être représentés par des élus climatosceptiques sur la BBC ou sur Fox News, explique Alex, comptable. Ces gens font passer les Australiens pour des clowns ou des arriérés. Or, nous sommes un pays riche et développé, nous devrions montrer l’exemple, même si cela ne suffirait pas à changer l’avenir de la planète. Nous avons besoin d’une perspective, de croire en l’avenir. »


Croisé dans le quartier d’affaires de Sydney, Alex regarde avec sympathie les manifestants, même s’il n’est pas question pour lui de participer à une telle action. Question de culture et d’éducation. Les étudiants, accompagnés par un imposant cordon policier, effectuent un bref sit-in devant le siège d’AGL Energy, au 200 George Street, qui participe au projet Adani.



La « tyrannie du carbone »



« Je suis optimiste, ajoute Alex. Les choses bougent, le secteur des affaires va prendre en main la question sans attendre le gouvernement. » Vœu pieux ? Il est vrai que plus grand monde, même la centrale patronale, n’ose publiquement mettre en doute le réchauffement climatique. Emma, par exemple, qui rentre de son jogging dans le quartier d’affaires : « C’est un problème compliqué, je n’ai pas d’avis car je ne suis pas spécialiste », esquive-t-elle quand on lui demande son avis sur la manifestation.


Signe des temps, Tony Abbott a perdu son siège de député aux élections générales de mai 2019 face à une candidate indépendante, une sportive ayant fait campagne sur le thème du climat et de l’environnement. Abbott a perdu, certes, mais Morrison et son parti ont gagné en faisant campagne pour les baisses d’impôt face au travailliste Bill Shorten, qui promettait de réduire les émissions de carbone et d’accélérer la transition énergétique… sans supprimer d’emplois dans l’industrie du charbon. Finalement, les électeurs ont choisi de ne pas payer « the bill » (« la facture »), obéissant au jeu de mot des libéraux, qui leur enjoignaient de ne pas régler la facture tout en rejetant les travaillistes.


Lilly, elle, ne fait confiance à aucune force politique représentée au Parlement pour mettre fin à la « tyrannie du carbone », pas même les Verts. Pour sa génération, c’est dans la rue que se décident les choses, pas sur la scène politique, corrompue par essence. « Ces incendies seront notre Tchernobyl climatique, prédit le grand écrivain Richard Flanagan, interrogé par Le Monde. Dans cinq ans, l’

Australie

sera soit un leader mondial dans la lutte contre le réchauffement, soit une dystopie autoritaire que ses dirigeants obligeront au suicide. » Rien de moins.


Cet article est paru dans Le Monde (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004011.xml</field>
    <field name="pdate">2020-01-17T00:00:00Z</field>
    <field name="pubname">Le Monde (site web)</field>
    <field name="pubname_short">Le Monde (site web)</field>
    <field name="pubname_long">Le Monde (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">planete</field>
  </doc>
</add>
