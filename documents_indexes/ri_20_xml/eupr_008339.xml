<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Blanco : « Ma vie c'est comme un match de rugby »</field>
    <field name="author">Recueilli par Laurent FRÉTIGNÉ.</field>
    <field name="id">eupr_008339.xml</field>
    <field name="description"><![CDATA[


Serge Blanco c'est une certaine idée du rugby. L'incarnation du panache et d'une époque révolue. C'est aussi le modèle d'une après-carrière réussie. Au moment où se disputent les joutes finales de la Coupe du monde, Blanco c'était une évidence. Entretien.


Pourquoi ces mémoires ?


Ça a été une nécessité après mes deux pontages. Quand j'étais sur mon lit d'hôpital je me suis dit que ce serait peut-être bien de raconter ma vie, pour ne pas toujours laisser cela aux autres.


Vous avez décidé de vous livrer de façon intime...


Je voulais montrer qui est le véritable Serge Blanco. Les gens ne se rendent pas trop compte car ils voient quelqu'un sur un stade ou dans les médias, alors que j'ai une vie tout à fait normale.


Pas tout à fait quand même, avec cette naissance au Venezuela, ce père que vous n'avez pas connu, votre carrière...


Je pense que, dans mon malheur, j'ai eu la chance de vivre quelque chose d'unique avec ma mère, mes grands-parents. Ma mère est la pierre angulaire de ma vie. Elle n'est plus en vie, mais ça résonne toujours dans mon coeur et me permet de continuer à vivre normalement.


« On partageait nos vies »


Grâce à elle vous avez eu une enfance et une jeunesse heureuses ?


Elle a tout fait pour que je ne manque de rien malgré ses moyens limités. J'ai eu une jeunesse exceptionnelle. On peut vivre certaines situations et ne pas être malheureux. Au contraire. J'ai aussi la chance d'avoir fait les bonnes rencontres, d'avoir vécu à une époque bénie où il y avait beaucoup de possibilités.


C'est l'époque où vous travaillez chez Dassault.


J'ai fait partie du monde ouvrier. À l'époque, il y avait 1 600 personnes qui y travaillaient. Il y avait une synergie et une solidarité fortes. C'est ça qui, à mon avis, faisait la différence. Il y avait du partage et une forme d'insouciance.


Venons-en au rugby, quel est votre plus beau souvenir ? L'essai de la demi-finale en 1987 ?


C'est un superbe souvenir collectif. Cet essai, c'est l'aboutissement de sept années de partage avec des gars qui sont devenus des amis, avec cet entraîneur exceptionnel qu'était Jacques Fouroux. Après, il y a des essais personnels, comme en

Australie

, quand je prends le ballon sur ma ligne d'en-but et que, d'un seul trait, je vais marquer l'essai 100 m plus loin.


Les joueurs qui vous ont plus marqué ?


Morne Du Plessis, le capitaine des Springboks en 1980, le All Black Graham Moorie, l'Australien Campese. On est une grande famille. J'ai eu l'occasion de côtoyer Jean-Pierre Rives qui est mon ami, Philippe Sella, Laurent Rodriguez, Pierre Berbizier, Patrice Lagisquet... Le plus important, ce sont les souvenirs avec eux. On partageait nos vies, presque. On était tous concernés par le bonheur ou le malheur d'un coéquipier. C'était un élément moteur. Mais j'imagine que les gars sont toujours solidaires. Il n'y a pas de raison pour qu'humainement ça ait changé, malgré le professionnalisme.


Quel est votre regard sur l'évolution du rugby ?


On ne peut pas faire une analyse du rugby sans une analyse de la société. Il y a eu une évolution, une accélération incroyable. Tout a été bouleversé. Tout ce modernisme nous est tombé dessus, et le rugby a suivi. Aujourd'hui, ça nous dérange un peu. Le raté, c'est que le rugby est passé de la case plaisir à la case bénéfice.


Est-ce que le professionnalisme a aussi tué le

french flair

?


Le

french flair

ne disparaîtra jamais. Je crois à la vertu des joueurs à trouver du plaisir, à s'approprier le jeu. Même si aujourd'hui tout est étudié, analysé, programmé. L'entraîneur dit aux joueurs : « Attention, il va attaquer comme ça, ils ont l'habitude de faire ça... » Or, lorsqu'un joueur sort du plan avec l'envie de se faire plaisir, cela crée des situations exceptionnelles. Il faut que les joueurs continuent à garder leur personnalité.


Quels joueurs peuvent incarner cela ?


Il y en a, mais il y a moins de spontanéité. J'ai parfois l'impression que les joueurs sont mis au second plan. En revanche, quand je vais voir du rugby amateur, je retrouve le jeu d'antan. Les joueurs s'amusent, prennent du plaisir, font des choses techniques superbes. Cette Coupe du monde le montre aussi. Il y a des matches surprenants. Le Japon prouve qu'on peut jouer. Les Blacks, bien sûr. Il y a des nations qui sont capables de montrer des mouvements magnifiques.


Pas les Bleus ?


On a besoin de se reconstruire, d'avoir une vision différente du rugby. Mais ce n'est pas uniquement l'équipe de France. Notre problème c'est la formation des jeunes comme des éducateurs. Je suis désolé quand je vois des joueurs qui n'arrivent pas à faire correctement une passe...


Que faut-il revoir ?


Il faut un rééquilibrage de la formation. Ça m'agace, par exemple, de voir des gamins jouer avec un casque, alors qu'au final c'est sans doute plus dangereux car ils se croient protégés. Il vaudrait mieux leur apprendre à se baisser, à plaquer en mettant la tête du bon côté, en serrant les bras. Il faut remettre la technique au premier plan.


« Des cadavres au bord de la route »


C'est le sens de votre engagement auprès de Florian Grill pour les prochaines élections à la FFR ?


Nous pensons qu'une mauvaise voie a été prise pour le rugby français. Le bénévolat est en train de se perdre, comme l'authenticité qui permettait au rugby d'être reconnu. Il y a aussi la formation, le rapport entre rugby pro et amateur. Les clubs sont désemparés par la création des Ligues régionales qui ont supprimé le service de proximité dont ils bénéficiaient. Il fallait conserver les comités territoriaux.


Pourtant Bernard Laporte se dit proche du rugby amateur ?


Je reste convaincu qu'il avait moyen d'orchestrer et réaménager la Fédération autrement. Et non de laisser des centaines de cadavres sur le bord de la route. On ne peut pas toujours écarter des gens. En suivant Florian Grill, je suis persuadé qu'on a des solutions pour relancer nos clubs amateurs.


Vous qui étiez capables de suspendre le temps, vous dites qu'il file trop vite ?


Oh oui ! Je n'ai pas eu assez de temps pour faire tout ce que j'avais envie, même des choses banales. Il faut entreprendre ce qu'on peut faire, chacun à sa façon. Ne pas les laisser passer. Quand j'ai eu 50 ans, je me suis dit merde, qu'est-ce que j'ai fait ? Ici, ils sont tous avec des planches de surf. Ils sont heureux. Ils veulent faire du surf, qu'ils fassent du surf.


Quel est votre rebond le plus favorable ?


Le rugby. Le rugby, c'est ma vie. Ma vie c'est comme un match de rugby, avec des rebonds qu'on ne maîtrise pas. Des rebonds qui vont en arrière, alors qu'on voulait qu'ils aillent devant. J'ai pris beaucoup de plaisir dans le rugby, comme j'en ai aussi pris dans la vie.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008339.xml</field>
    <field name="pdate">2019-10-20T00:00:00Z</field>
    <field name="pubname">Ouest-France Vannes</field>
    <field name="pubname_short">Ouest-France</field>
    <field name="pubname_long">Ouest-France Vannes</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Autres Sports</field>
  </doc>
</add>
