<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Des batteries pour voitures électriques ? La solution se trouverait au Portugal</field>
    <field name="author">Benedikt Fuest</field>
    <field name="id">eupr_008761.xml</field>
    <field name="description"><![CDATA[


Reportage


Benedikt Fuest


Un vendredi après-midi tranquille à la fin du mois mai, Carlos Filipe Camelo Miranda de Figueiredo, « presidente » de la municipalité de Seia, une ville de l’arrière-pays de Porto, entend soudain scander des chants devant sa mairie. Plusieurs dizaines de manifestants se sont rassemblés et brandissent des pancartes portant des inscriptions comme « Não Lithio – Seia Verde » (Non au lithium – Seia vert !) ou encore, barré : « Seia, capitale du lithium ». Ils lui crient : « Le lithium, ça vous parle, Monsieur le maire ? »


Nichée dans la Serra da Estrela au milieu d’une ligne droite reliant la côte atlantique à la frontière espagnole, Seia est une tranquille agglomération de montagne de tout juste 6.000 habitants. Des manifestations, il n’y en a encore jamais eu ici.


Et pourtant, ces derniers mois, cette petite ville, ses habitants et toute la région montagneuse dans le nord-est du Portugal ont soudain été propulsés au cœur de la révolution climatique, de la politique économique européenne et du boom mondial des matières premières. Car la roche de ses collines verdoyantes recèle des gisements de lithium. Pour ce métal alcalin, composant essentiel des batteries, la demande est forte comme jamais auparavant en raison de l’engouement pour les voitures électriques.


Dans ce vert arrière-pays du Portugal, les géologues supposent des gisements de lithium d’au moins 60.000 tonnes, peut-être les plus importants de toute l’Europe. Les Portugais seraient ainsi encore très loin derrière les réserves du Chili, de la Chine et de l’

Australie

, pays leaders sur le marché mondial avec des réserves d’exploitation sur des sites connus estimées à 8 millions (au Chili) et 1,5 million de tonnes (en

Australie

).


Mais les imbroglios de la politique économique, des progrès techniques concernant les batteries et la demande mondiale en forte hausse poussent de grandes compagnies minières comme le Fortescue Minerals Group à penser qu’il pourrait être intéressant d’obtenir l’autorisation d’explorer les montagnes aux alentours de Seia. Le groupe minier australien prévoit de sonder plus de 260 km 2 au total autour du parc naturel national Serra da Estrela.


Un peu plus au nord, près de Covas do Barroso, dans la province de Trás-os-Montes, l’entreprise britannique Savannah Ressources a déjà entamé des prospections. Dans des communiqués enthousiastes adressés à ses investisseurs, l’entreprise fait part de hautes concentrations de lithium et dresse désormais avec la Mina do Barroso des plans pour concrétiser ce qui serait la première production de lithium dans toute l’Europe.


Il y a deux façons d’obtenir du lithium : en Amérique du Sud, après avoir pompé à la surface la saumure des gros dépôts salins du désert de l’Atacama, le lithium qu’elle contient est dissocié du sel. En

Australie

, en Amérique du Nord, en Chine et en Europe, on le trouve dans ce qu’on appelle des pegmatites – des filons ayant jadis transporté du magma à l’état liquide, jailli il y a des centaines de millions d’années des profondeurs de la Terre avant de venir se cristalliser dans les failles de la roche quelques centaines de mètres en dessous de la surface.


« Dans ces filons, le lithium est présent dans des concentrations allant d’un peu moins de 1 % à un peu moins de 1,5 % », explique Dirk Harbecke, chef de l’entreprise Rock Tech Lithium de Vancouver au Canada, qui se spécialise dans l’exploration du lithium. Pour obtenir 1 kg de lithium, il faut donc traiter environ 100 kg de pierres. « Pour l’extraire, il faut déjà creuser – soit à ciel ouvert, soit dans le sous-sol – pour atteindre les pegmatites. »


Au Portugal, les entreprises prévoient des mines à ciel ouvert ; pour accéder à un gisement de lithium, des puits de mine d’un diamètre allant jusqu’à 800 mètres doivent être creusés et la roche détachée à l’explosif. Les gisements se trouvent à une profondeur de 100 à 300 mètres sous terre.


« C’est une vision d’horreur », s’exclame Dieter Seitz. Cet expatrié allemand s’occupe de son oliveraie à proximité de Seia et vit dans une ancienne ferme. En quête de tranquillité, il s’est installé dans la région il y a plus de 30 ans. Maintenant, le voici confronté au présent. « C’est arrivé tout d’un coup sans qu’on s’y attende. Au départ, personne ici n’était au courant que des permis avaient été attribués pour des forages de prospection. »


Ceux qui, comme lui, ont quitté leur pays pour une nouvelle vie sont le cœur d’un mouvement local de protestation, car ils craignent pour l’avenir écologique et économique de toute la région autour du patrimoine naturel qu’est le parc de Serra da Estrela : « Nous misons ici sur l’écotourisme, nous vendons du fromage de la serra, de l’huile d’olive de la serra – la région tout entière vit de sa réputation de paradis naturel. »


Rien que pour les forages exploratoires près de Cobas do Barroso, qui descendent à plus de 100 mètres, les prospecteurs de lithium ont imprimé leurs tracés dans la nature, effectué des coupes claires dans la forêt et aplani des collines. Vu les techniques d’extraction hautement mécanisées, les puits de mine géants ne créent pas beaucoup de nouveaux emplois. Par contre, ils entraînent avec eux le déboisement de très grandes surfaces, la circulation de camions, le bruit et la poussière des explosifs et des eaux usées potentiellement toxiques, sans oublier une baisse des niveaux des nappes phréatiques. C’en serait fini de l’écotourisme pour des décennies.


Dieter Seitz et ses compagnons à Seia ont la ferme intention de ne pas en arriver là et prévoient de nouvelles actions de protestation. Ils veulent déjà commencer par empêcher les forages d’exploration. Mais M. de Figueiredo, le « presidente » local qu’ils avaient interpellé en mai devant sa mairie de Seia, n’est pas non plus en position de les renseigner. « Lui-même n’a aucune influence sur ce qui se passe », se désole Seitz.


Les licences sont attribuées par le gouvernement à Lisbonne, explique Nik Völker, un programmeur allemand néo-rural à mi-temps qui documente, pour les opposants à l’extraction du lithium, les différents projets sur une carte interactive. C’est la Direction générale portugaise de l’énergie et de la géologie, en coopération avec le ministère de l’Environnement et de la Transition écologique, qui est responsable et qui décide aussi des zones à explorer.


Dans ces secteurs, les projets miniers peuvent être accélérés et autorisés, même si on s’y oppose sur le plan local. C’est le volet d’une initiative pour les matières premières qui est vue d’un bon œil par les responsables politiques de l’UE et qui avait déjà été exposée dans une stratégie baptisée « Improving framework conditions for extracting minerals for the EU ».


En effet, cela fait longtemps que Bruxelles a compris que la production de batteries est une compétence clé que les constructeurs automobiles européens ne devraient pas abandonner aux seuls leaders mondiaux chinois et japonais. Dans une « Alliance européenne des batteries » mise en place en 2017, l’UE encourage la production de cellules de batteries selon des standards européens élevés et vise à créer « une chaîne de valeur complète », donc aussi une filière d’approvisionnement en matières premières.


Pour Michael Schmidt, expert en lithium à l’Agence allemande pour les matières premières, « il s’agit d’une volonté politique d’établir une production de cellules de batteries moderne en Europe. Par conséquent, cela a du sens de rechercher quelles sont les matières premières dont nous disposons. Du lithium, nous en avons. Nous pourrions vraisemblablement l’extraire en restant compétitifs, et c’est cela qui suscite l’intérêt de l’industrie à s’approvisionner en Europe. »


Dans la réalité, les projets de lithium dépendent avant tout de la rentabilité d’exploitation pour dépasser le stade des sondages. « En ce moment, le lithium n’est coté sur aucune Bourse des métaux ; les prix sont matière à négociation entre le fournisseur et l’acquéreur », explique Michael Schmidt.


Selon Benchmark Minerals Intelligence, les analystes londoniens de la branche, le prix d’achat de la tonne de carbonate de lithium dans la qualité requise pour les batteries est passé de 5.000 à plus de 16.000 dollars les cinq dernières années. Dès lors, en Europe, même les projets dont les coûts d’exploitation sont estimés à au moins à 6.000 dollars la tonne sont intéressants.


Mais toutes les sortes de lithium ne se valent pas, fait remarquer Michael Schmidt : « Le lithium n’est pas un produit minier typique. Il est vendu en tant que produit chimique sous la forme de carbonate de lithium ou d’hydroxyde de lithium. » Et ce faisant, cela dépend extrêmement de qui achète où : « La pureté chimique, la taille des cristaux, les impuretés – les carbonates de lithium ne sont pas tous à mettre sur le même plan. Pour atteindre une qualité bien précise et stable sur le long terme, il faut un certain savoir-faire. »


Dirk Harbecke, de Rock Tech Lithium, s’attend à ce que la demande pour ce métal explose dans les années à venir et pense que la production dans les déserts de sel de l’Amérique du Sud ne pourra pas monter en puissance sans que cela s’accompagne de fluctuations dans la qualité.


D’après les prévisions de Morgan Stanley, les besoins mondiaux en carbonate de lithium, le produit final, s’élèveront à un million de tonnes par an en 2025. À présent, la production est d’environ 350.000 tonnes. « Aujourd’hui, tous les grands constructeurs automobiles observent qu’ils ont besoin de leur propre production de cellules et donc d’un accès spécifique à leurs propres réserves de matières premières », précise Michael Schmidt. Volkswagen a par exemple annoncé début mai la construction d’un site de production de batteries à Salzgitter, et pour cela l’entreprise devra assurer son propre approvisionnement en matières premières en conformité avec des standards élevés d’extraction minière et de qualité du matériau.


« L’électromobilité est censée être l’alternative la plus écologique à la voiture fonctionnant aux carburants fossiles », dit Schmidt. « Or, si l’on considère où et comment les matières premières destinées aux batteries sont extraites ainsi que l’organisation et les investissements nécessaires pour le transport et la transformation ultérieure, c’est vraiment encore perfectible. »


En particulier, l’extraction du cobalt en Afrique a lieu dans des conditions que les fabricants ne peuvent pas contrôler. C’est pourquoi la recherche tente de créer des batteries qui nécessitent le moins de cobalt possible. Mais ces nouvelles batteries ont besoin de plus de lithium sous la forme d’hydroxyde de lithium à plus forte valeur ajoutée. En revanche, il peut être beaucoup plus facile à produire à partir de gisements de minerai comme au Portugal ou au Canada, explique l’investisseur Harbecke. « Les réserves que recèle la saumure des dépôts salins d’Amérique du Sud ne sont pas aussi faciles à exploiter pour obtenir une qualité constante puisqu’elles comportent en grande partie du magnésium qu’il faut éliminer. »


Des facteurs aussi bien politiques que techniques semblent donc indiquer que l’exploitation en Europe peut s’avérer profitable. À côté des projets au Portugal, d’autres projets viennent d’être lancés, comme en Serbie, dans la vallée du Jadar, et en Suède, avec le projet « North volt », auquel VW participe aussi.


Le gouvernement portugais est fermement décidé à établir sa propre production de lithium dans le pays et a déjà annoncé une procédure de vente aux enchères des licences d’extraction, procédure qui aurait déjà dû être lancée en mai dernier. Or, le lancement des enchères a été reporté sans en indiquer les motifs. « En octobre prochain, aura lieu l’élection d’un nouveau Parlement au Portugal. Peut-être que certains craignent un mouvement de contestation », commente Dieter Seitz.


En tout cas, il a déjà fait sa provision de nouveaux tracts, il distribue des flyers d’information et appose des autocollants portant l’inscription « Non au lithium ». L’objectif du mouvement protestataire est de faire en sorte que, jusqu’aux élections, chaque électeur dans le nord-est du Portugal soit au courant des plans du gouvernement en ce qui concerne le lithium et leurs possibles conséquences pour le paysage et le tourisme.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008761.xml</field>
    <field name="pdate">2019-07-27T00:00:00Z</field>
    <field name="pubname">Le Soir GENERALE</field>
    <field name="pubname_short">Le Soir</field>
    <field name="pubname_long">Le Soir GENERALE</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">LENA</field>
  </doc>
</add>
