<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">« La maison Windsor toxique et machiavélique »</field>
    <field name="author">Stefanie Bolzen</field>
    <field name="id">eupr_006562.xml</field>
    <field name="description"><![CDATA[


Stefanie Bolzen


Alors que, la semaine dernière, les deux tourtereaux ont annoncé leur décision de se mettre en retrait de la famille royale britannique, la crise autour du prince Harry a maintenant atteint une ampleur comparable au drame qu’a connu Diana au milieu des années 90. Les parallèles entre le jeune homme de 35 ans et sa mère, décédée en 1997 dans un accident de voiture survenu dans un tunnel parisien – sa limousine était traquée par des paparazzis –, sont inévitables.


Ce chapitre tragique de la saga Charles et Diana fut précédé par la décision de la princesse d’utiliser la presse, qu’elle haïssait tant, pour défendre ses propres intérêts et donner sa vision personnelle de ce qui se passait derrière les murs épais du palais. Diana avait d’abord coopéré secrètement avec le journaliste Andrew Morton pour sa spectaculaire biographie parue en 1992. Par la suite, en 1995, elle avait accordé à la BBC une interview de plusieurs heures au cours de laquelle elle avait créé le scandale en se livrant à des révélations intimes sur sa relation tumultueuse avec Charles. Quelques semaines plus tard, la reine envoyait au couple une lettre les priant de divorcer. Pour elle, c’était « la meilleure solution ».


Tout comme Harry et Diana sont liés par les événements traumatiques que leur a fait vivre l’impitoyable presse britannique, aujourd’hui ils sont également liés par son instrumentalisation. Dans le cas de Harry, il s’agit de son amitié avec le journaliste de télé Tom Bradby. En sa qualité de présentateur du journal de 10 heures sur ITV News, l’homme de 53 ans est un visage bien connu de la télévision britannique. Si, en tant que journaliste politique, Bradby connaît bien les zones de crise de notre planète, il a également été le « Royal Correspondent » de sa chaîne pendant plusieurs années et était donc responsable de la rédaction d’articles sur la famille royale.


Contrairement à d’autres représentants des médias, vis-à-vis desquels tant Harry que le prince William nourrissent une méfiance générale, Bradby a su tisser de bons rapports avec les deux frères, d’abord avec l’aîné, puis avec le cadet. Si bien que l’année dernière dans le cadre d’un documentaire ITV exclusif consacré au voyage du couple princier en Afrique, Bradby a pu récolter quelques déclarations explosives. Et encore aujourd’hui, il a à nouveau été choisi pour être le porte-parole de Harry et son épouse Meghan.


Les articles de Bradby permettent de suivre la triste évolution des relations du couple princier avec le reste de la famille royale et l’ensemble de la cour. Mais ce n’est pas tout : les commentaires de Bradby sonnent comme une mise en garde ; si le Palais ne s’était pas montré suffisamment compréhensif à l’égard de Harry et Meghan lors de la réunion de Sandringham, la crise aurait pu s’aggraver davantage.


La « maison Windsor moderne est devenue toxique et machiavélique », écrit Bradby dans le

Sunday Times

, et Harry et Meghan sont ses victimes. C’est pourquoi le journaliste émet un avertissement on ne peut plus clair qui risque de choquer le Palais : « Il est clair que nous ne savons pas ce qui nous attend. Alors que leur collaboration pour le documentaire ITV a déjà été qualifiée de franche , qu’est-ce que ce sera s’ils décident de vider complètement leur sac ? »


Un commentaire apparemment lancé comme une petite remarque anodine, mais qui ne manque pas de sel. D’autant plus que lundi, le tabloïd britannique



The Sun

a rapporté que Harry et Meghan envisageaient d’accorder une interview à la célèbre animatrice de talk-show Oprah Winfrey. La présentatrice américaine est une amie de Meghan Markle et faisait partie des hôtes de marque présents au mariage du couple en mai 2018, au château de Windsor.


D’après le journal, les conseillers en relations publiques du couple se sont déjà mis en contact avec des chaînes de télévision américaines, et plus concrètement avec Oprah Winfrey, pour étudier la possibilité d’organiser « une interview sans fard ». Les deux tourtereaux aimeraient « raconter leur version de l’histoire » – une formulation qui fait inévitablement penser à la princesse Diana.


Il y a 25 ans, les propos tenus par Diana, qui évoquait la froideur et l’ignorance stoïque avec lesquelles l’avait traitée la reine, avaient causé des dommages irréversibles à l’image de la monarque – et à celle de son fils Charles.


Aujourd’hui, les déclarations publiques de Harry et Meghan pourraient bien donner du fil à retordre aux Windsor pendant un bon moment. D’autant plus que leur histoire a quelque chose de particulier. « Meghan se sent condamnée au silence, mais elle n’est plus disposée à se taire », cite le

Sun

. Et d’ajouter : « Harry et elle ont le sentiment que la famille royale s’est comportée de manière raciste et sexiste. »


Si elle a motivé la décision du couple, cette accusation possède également sa propre dimension. A presque 94 ans, Elisabeth II tente de maintenir non seulement la cohésion de son Royaume-Uni, mais également des quinze autres pays dont elle est la cheffe d’Etat. Les Etats du Commonwealth, avec leurs sociétés modernes et multiculturelles, s’étendent du Canada à l’

Australie

et à la Nouvelle-Zélande, en passant par les Caraïbes. Pour la dynastie royale britannique « blanche », cela représente un énorme défi au XXI e siècle. L’intérêt et l’enthousiasme des minorités pour la famille royale sont plutôt limités.


La reconnaissance internationale de la famille royale est un élément clé qui rend l’institution si précieuse pour l’Etat britannique, assure le journaliste télé Tom Bradby. « Le fait est que Harry et Meghan ont du succès auprès d’un groupe cible jeune, progressiste et multiculturel, des valeurs qui ne sont pas vraiment représentées au sein du reste de la famille. Aujourd’hui, la royauté risque non seulement de perdre ce groupe cible, mais également de se le mettre à dos. »


Les commentaires de Tom Bradby vont-ils inciter le prince Charles, son fils William et la reine à trouver une solution convenant à Harry et Meghan en ces jours de crise ? Qui sait. S’ils avaient suivi le travail de Bradby de plus près avant, la réunion de Sandringham n’aurait peut-être pas eu de raison d’être.


Si seulement ils avaient prêté plus d’attention à la réponse donnée par Meghan à une question posée par Bradby pour son documentaire en octobre dernier. « Merci de me le demander. Car peu de gens me demandent si je vais bien. Mais ce que je traverse actuellement, ma vie de jeune mariée et de mère d’un jeune enfant, c’est très réel et ça se joue dans les coulisses », avait répondu la duchesse de Sussex. « Par conséquent, peut-on dire que c’est un combat ? », avait alors demandé Bradby. « Oui », avait répondu Meghan presque en chuchotant. La fragilité se lisait sur son visage.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006562.xml</field>
    <field name="pdate">2020-01-18T00:00:00Z</field>
    <field name="pubname">Le Soir GENERALE</field>
    <field name="pubname_short">Le Soir</field>
    <field name="pubname_long">Le Soir GENERALE</field>
    <field name="other_source">Le Figaro (site web)</field>
    <field name="source_date">15 janvier 2020</field>
    <field name="section">LENA</field>
  </doc>
</add>
