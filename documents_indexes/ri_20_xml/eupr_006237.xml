<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">PÉRIPLE SUD-AMÉRICAIN VI.- UN PAYS HEUREUX : L'URUGUAY</field>
    <field name="author">Edmond DELAGE</field>
    <field name="id">eupr_006237.xml</field>
    <field name="description"><![CDATA[


Il existe de par le monde si peu de pays heureux que je ne laissai pas échapper l'occasion d'en visiter un : l'Uruguay. La " mission Mermoz " n'avait pu y faire qu'une brève escale, à son passage à Montevideo, marqué par une belle réception à l'ambassade de France. L'Uruguay est, comme une autre heureuse nation d'Europe, la Suisse, un " petit " pays - pour l'Amérique latine. Il n'a que 196.925 kilomètres carrés et a la forme d'un coeur enchâssé entre les deux masses brésilienne et argentine. En 1937 il était peuplé de 2.093.000 habitants au lieu de 1.378.000 en 1916. C'est d'ailleurs la densité la plus forte du continent sud-américain : 12 habitants au kilomètre carré.


Sa capitale, Montevideo, est à une heure d'avion de Buenos-Aires. Mais le moyen le plus pratique de l'atteindre est de traverser l'estuaire de la Piata, en une nuit, sur de confortables paquebots aménagés pour le sommeil. Le soir de mon embarquement une foule ardente et sonore accompagnait sur le quai de ses vivats et banderoles quelques amis en partance pour la " Bande orientale " : c'était le nom que portait l'Uruguay au moment de sa lutte pour l'indépendance de 1825 à 1830.


Lorsque, guidé par le fidèle Parisien qu'est notre confrère Barbagelata, syndic de la presse sud-américaine à Paris, je gravis la colline couronnée par le Cerro - l'ancienne forteresse transformée en petit musée militaire, lieu de pèlerinage et de promenade dominicale - j'embrassai du regard la rade et une belle cité de 800.000 âmes.


Elle aussi commence à édifier d'immenses buildings, entre autres un hôtel de ville d'aspect monolithique, d'une bonne vingtaine d'étages : la " gratte-ciélite " est décidément une maladie de l'urbanisme sud-américain. La bonne ville de 1896, qui ne comptait alors que 200.000 habitants, s'est rapidement transformée. Aux quartiers populeux de l'Aguada, aux jolies places qui rappellent celles de la province méridionale française, se sont ajoutés de superbes quartiers percés de larges avenues comme le boulevard Artigas, de vastes parcs comme celui de José Battle y Ordonez, entouré de riches villas, de luxueux hôpitaux, stades, piscines. La promenade préférée des Montévidéens est restée le parc Rodo qui aboutit à la plage Ramirez.


Au centre de ce dernier a été tout récemment élevé un émouvant monument au penseur national Rodo (1871-1917), sorte de Renan uruguayen, auteur d'un Ariel contre Caliban. Son buste est entouré de magnifiques groupes allégoriques en bronze. Un autre monument très populaire, d'un réalisme touchant, - la Charrette, - évoque en grandeur naturelle la carriole, les chevaux et les vaches des premiers défricheurs. La métropole se prolonge au bord de la mer par une série de jolies plages, Ramirez, Positos, Carrasco. Sur des kilomètres c'est une succession de villas neuves, de tous styles, enfouies parfois dans des bois et bosquets odorants de pins et d'eucalyptus. Heureux les peuples sans guerre : ils bâtissent...


La campagne uruguayenne est en Amérique du Sud celle qui se rapproche le plus de celle de nos contées. Quand on roule sur une route cimentée, vieille de dix ans a peine, de Montevideo à Punta del Este (160 kilomètres), le Deauville du pays - bientôt doublé d'un Hossegor immense, - on est frappé, comme l'est encore plus l'Argentin, las de la platitude de sa pampa, par de charmants vallonnements, par les alternances de cultures d'aspect méditerranéen, de fermes semblables à des jouets d'enfants tout neufs, de champs de mais, de pacages et de hauts plateaux à pâturages dominés par des montagnettes arides aux contours provençaux. Le trafic intense d'énormes camions du dernier type se marie à celui de petites charrettes paysannes capables de traverser les fondrières. Sur les bas-côtés galopent sur leur selle en peau de mouton les gauchos, fièrement protégés d'un puncho immaculé, - souvent aussi de rieuses jeunes filles.


Punta del Este, au delà de la petite ville de Maldonado, proche des bois charmants de Punta Ballina tous plantés en essences rares par la main de l'homme, fait dès maintenant une concurrence redoutable à Mar dal Plata dont les Argentins sont si fiers. 125.000 touristes ont traversé cette année l'estuaire pour venir jouir de la fraîcheur et de l'air sec uruguayen. Sur cette pointe se dresse un énorme casino de jeux (on joue beaucoup en Amérique du Sud, terre bénie des loteries); ses deux plages, l'une battue des flots, l'autre mieux protégée et abritant un port de plaisance pour des centaines de yachts - souvent argentins, - sont en pleine saison le rendez-vous d'élégantes - et plantureuses - Sud-Américaines...


Tout ici donne l'impression de la prospérité et du bonheur paisible : il n'y a pas une seule boîte de nuit dans toute la capitale. La population, entièrement blanche, en majorité de race espagnole, avec des apports italiens, germaniques, basques français (du second Empire), s'enrichit d'une immigration modérée et choisie (25.000 en 1911, mais de 4.000 à 6.000 seulement depuis 1932). Peu de capitalistes richissimes, mais une aisance largement répandue. Beaucoup de fortunes moyennes, solides, sagement gérées, sans éclat...


La plupart des Uruguayens sont agriculteurs. 70 % du territoire sont occupés par des prairies naturelles. Avec ses 8.250.000 bovins, ses 17.950.000 ovins, c'est le pays dont la densité en bétail est la plus forte du monde. Il bat jusqu'à la Nouvelle-Zélande et l'

Australie

. Il n'a pas de charbon, mais ses ressources hydroélectriques croissent rapidement grâce aux barrages du rio Negro : nos hydrauliciens - en tête M. Antoine, inspecteur général de l'Électricité de France - sont très cotés. Grâce à sa viande, à ses laines (60.000 tonnes exportées), à ses cuirs, à ses produits laitiers, à ses arachides, à ses vins, à ses fruits (pêches et oranges), ce doux pays, où il ne fait jamais moins de +12 et que de belles nappes d'eau rafraîchissent, a connu jusqu'à ces tout derniers temps une prospérité inouïe. Son peso est solide, couvert à 100 %; les maisons s'amortissaient en sept ou huit ans (les loyers sont fort chers).


Pour la première fois depuis 1942 sa balance fut pourtant l'an dernier déficitaire : 167 millions de dollars à l'importation, 116 à l'exportation. Est-ce le début d'une crise ?


Sur ce total la part de la France est bien modeste : 9 millions de dollars d'achats, 2 millions 800.000 seulement de ventes (champagnes, liqueurs, autos, tuyauterie - une de nos grandes exportations en Amérique du Sud, - tissus).


Après la poussée presque irrésistible de l'économie nord-américaine depuis 1945 l'Uruguay semble vouloir se dégager et revenir progressivement à la situation d'avant guerre : en 1947 les exportations et importations vers l'Europe ont repris avec une vigueur nouvelle (57 % et 22 %). Malgré la popularité dont jouit la France sa situation commerciale y est encore très faible : elle n'est que le vingt et unième fournisseur de l'Uruguay.


Peut-on s'y intéresser violemment à la politique ? Il ne le semble pas. Le parti au pouvoir est celui qu'a fondé dans la seconde moitié du dix-neuvième siècle l'illustre Battle : c'est celui des rouges (les colorados). Mais ils sont bien roses et peu éloignés des blancs (blancos), les Herristes. Quand, en juillet 1947, mourut subitement le président " rouge ", l'éminent M. Berreta, il fut remplacé par le jeune et charmant vice-président Luis Battle-Berre. De larges et intelligentes concessions ont été faites par lui à l'opposition.


Le pays est catholique mais sans bigoterie, et laïc, nous dirions presque " radical modéré ". La législation sociale est avancée; de nombreux monopoles sont réservés à l'État (carburants, alcools, pêche, etc.). L'instruction est partout répandue : pas d'analphabètes. C'est plaisir que de voir sortir de classe, si propres en leurs tabliers tout blancs et uniformes, les petits écoliers. Nos professeurs, nos savants sont recherchés, mais trop peu nombreux : tout récemment M. Tréfouel, directeur de l'institut Pasteur, a été reçu avec empressement pour donner une série de conférences à Montevideo.


La France est peu représentée dans la population : ses nationaux s'y sont, hélas ! fondus en peu de générations (comme dans le reste de l'Amérique). Son prestige reste pourtant très grand : grâce pour une bonne part au superbe lycée français où enseignent plusieurs de nos jeunes agrégés. La diffusion des articles de nos meilleurs journalistes par nos services de l'information était très appréciée dans la presse uruguayenne. Mais la " hache " et la " guillotine " l'ont dangereusement compromise. En Uruguay comme au Brésil et en Argentine notre pays a encore une belle partie à jouer - et à gagner. Le voudra-t-il ?


FIN


(1) Voir le Monde des 10, 11-12, 13, 15 et 10 avril 948.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006237.xml</field>
    <field name="pdate">1948-04-17T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
