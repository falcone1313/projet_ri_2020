<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Pourquoi les Français aiment dire "non"</field>
    <field name="author">Marie Gathon</field>
    <field name="id">eupr_007716.xml</field>
    <field name="description"><![CDATA[


Répondre "non" par défaut à une question semble être le défaut national des Français selon les observations de Sylvia Sabes, correspondante pour la BBC à Paris.


Olivier Giraud, comédien français qui partage depuis plus de dix ans son regard sur la culture française avec son one man show, "Comment devenir parisien en une heure", explique ce réflexe en disant : "répondre 'non' vous donne la possibilité de dire 'oui' plus tard; [c'est] le contraire quand vous dites 'oui', vous ne pouvez plus dire 'non' ! Il ne faut pas oublier que les Français sont un peuple de protestation, et une protestation commence toujours par un non".


En effet, les Français protestent plus ou moins sans arrêt depuis que les Parisiens ont pris d'assaut la prison de la Bastille en 1789. Ces premières protestations ont lancé la Révolution française, mettant fin à plus de 900 ans de règne monarchique. Ils ont été remplacés aujourd'hui par les gilets jaunes, descendus dans la rue en novembre 2018 pour protester contre une hausse de la taxe sur le carburant et ont continué à protester - parfois violemment - depuis lors, défilant pour de nouvelles causes chaque samedi et démontrant que la protestation est un passe-temps national.


Pourtant, selon la journaliste, même un "non" peut se transformer en un "oui". "Comme je l'ai découvert au fil des ans, le "oui" se cache souvent dans le contexte de ce qui est dit", explique-t-elle.


Le contexte comprend le ton, le langage corporel, le cadre et la situation; essentiellement, tout ce qui n'est pas expressément exprimé en mots. Dans son livre' The Culture Map : Breaking Through the Invisible Boundaries of Global Business ', Erin Meyer, professeur à l'INSEAD Business School, identifie huit échelles pour montrer comment les différentes cultures sont liées. La première échelle traite du rôle du contexte dans la communication. Des pays comme les États-Unis et l'

Australie

sont des cultures à faible contexte où les gens disent généralement ce qu'ils veulent dire et pensent ce qu'ils disent. Cependant, la France, comme la Russie et le Japon, a tendance à être une culture à contexte élevé, où "une bonne communication est sophistiquée, nuancée et stratifiée. Les messages sont à la fois parlés et lus entre les lignes", écrit-elle.


Erin Meyer soupçonne que l'un des facteurs à l'origine de ce fossé se trouve dans les chiffres : selon son livre, il y a 500 000 mots en anglais, mais seulement 70 000 en français. Cela signifie que les anglophones sont plus susceptibles d'avoir le mot exact pour dire ce qu'ils veulent, alors que les francophones doivent souvent enchaîner une série de mots pour communiquer leur message. Non seulement cela oblige les Français à être plus créatifs avec la langue, mais cela leur permet aussi d'être plus ambigus avec ce qu'ils veulent dire. Par conséquent, "non" en France ne signifie pas toujours "non".


Ce recours au "non" ne signifie pas non plus que les Français sont un peuple fondamentalement négatif. Leur approche commence en partie à l'école. Les enfants français apprennent à argumenter une thèse, une antithèse et une synthèse lors de la préparation des essais, ce qui leur apprend à argumenter leur point de vue, à argumenter contre leur propre argument, puis à développer un résumé. Meyer écrit : "Par conséquent, les hommes d'affaires français conduisent intuitivement leurs réunions de cette manière, considérant les conflits et les dissonances comme mettant en lumière des contradictions cachées et stimulant une réflexion nouvelle". En fait, le "non" français est souvent une invitation à débattre, à s'engager et à mieux se comprendre, ce qui a favorisé le développement d'un bouquet de "non" différents, utilisé dans diverses situations.


Le premier et le plus important des non est celui qui signifie vraiment "je ne sais pas" ou le "je n'ai pas d'idée". Barlow et Nadeau estiment que près de 75 % des non qu'ils ont rencontrés visaient à dissimuler un manque de connaissance. Cela vient probablement de la peur du ridicule. C'est une peur que les élèves français rencontrent pour la première fois à l'école primaire, où les notes individuelles sont partagées en classe, ce qui crée un environnement d'humiliation et de vulnérabilité.


La peur est encore plus grande lorsque les adolescents passent le Baccalauréat, une série d'examens de fin d'études secondaires. Les résultats sont féroces, avec une note de 12 sur 20 qui permet d'obtenir une mention honorable et une note de 20 qui est pratiquement impossible. Les résultats sont affichés en ligne pour que le monde entier puisse les voir. Après 13 ans d'angoisse, les survivants de ce système sont soulagés d'offrir un "non" discutable, plutôt qu'un "oui" erroné.


Le non le plus facile à gérer est peut-être le "non-flirt". Accompagné d'un clin d'oeil et d'un sourire, c'est une invitation au dialogue utilisée par n'importe qui, depuis le boucher qui fait jouer ses clients qui demandent un morceau de viande, jusqu'au jeune enfant qui espère une sucrerie. Dans son aspect le plus inoffensif, le "non-flirt" peut séduire les clients qui retournent au même café chaque après-midi pour discuter avec leur sympathique serveur. Parfois, comme tous les jeux, cela devient fatigant.


Le non autoritaire est plus difficile à gérer. Barlow et Nadeau suggèrent que le non utilisé par beaucoup de Français vient d'une obsession de ne pas se faire reprocher d'avoir tort. Et bien que cela soit vrai dans tous les milieux, les fonctionnaires de France en ont fait un système complexe qui semble archaïque et inefficace.


Le "non réflex" est peut-être le plus intime de tous les non et est courant chez les amis et au sein des couples. Polly Platt, auteur de French or Foe, a fait part de sa stratégie pour obtenir un oui de son mari français, à Sylvia Sabes. Pour ses projets de vacances d'été, par exemple, elle proposait une destination qui ne l'intéressait pas et que son mari n'accepterait jamais - peut-être un endroit trop chaud ou proche de la belle-famille, comme Marrakech ou Philadelphie. La deuxième suggestion poserait une autre série de problèmes. Au moment où elle a suggéré son premier choix d'aller dans leur résidence secondaire en Dordogne, son "oui" est venu facilement. Elle savait que si elle avait commencé avec l'idée de rester près de chez elle, il aurait dit non sans réfléchir.


Cet article est paru dans Le Vif (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007716.xml</field>
    <field name="pdate">2019-12-30T00:00:00Z</field>
    <field name="pubname">Le Vif (site web)</field>
    <field name="pubname_short">Le Vif (site web)</field>
    <field name="pubname_long">Le Vif (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
