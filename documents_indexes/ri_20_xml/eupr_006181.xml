<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">LA HAUTE COUTURE FRANÇAISE doit lutter pour reconquérir ses marchés extérieurs</field>
    <field name="author">ALPHONSE BRISSON</field>
    <field name="id">eupr_006181.xml</field>
    <field name="description"><![CDATA[


La haute couture française attire depuis fort longtemps la riche clientèle étrangère. Après la guerre de 1914-1918, elle ne s'est plus contentée d'exportation invisible; elle a pris place résolument sur les marchés extérieurs, non seulement de l'Europe centrale, mais aussi de l'Amérique.


La grande crise économique de 1929 qui a débuté, on se le rappelle, aux États-Unis, a malheureusement paralysé cet élan : d'acheteurs qu'ils étaient, les Américains sont devenus producteurs; ils ont eu leurs couturiers qui se sont évertués à copier les modèles français. Les maisons parisiennes ont alors non seulement perdu un débouché de premier ordre, mais il leur a fallu, de plus, se défendre contre les espions commerciaux qui s'introduisaient chez elles et renseignaient la couture américaine sur les créations en cours et celles qui se préparaient.


La guerre de 1939-1945 a été pour la haute couture française une nouvelle épreuve. Plus d'exportation possible Outre-mer. Plus de matière première. Les grandes maisons parisiennes ont pourtant fait face avec courage et ingéniosité à cette menace d'asphyxie. Elles ont obtenu des dérogations qui leur ont permis de se procurer le textile dont Elles avaient besoin, et elles ont maintenu leur activité sur le marché intérieur où la clientèle ne leur a pas fait défaut. Dans la détresse générale, en effet, des fortunes s'étaient édifiées, et les objets de luxe trouvèrent preneurs comme auparavant, à tel titre que beaucoup de maisons de couture, de moyenne importance naguère, prirent alors une grande extension, en dépit de la loi de 1939 qui avait interdit la création de nouveaux fonds de commerce.


Les difficultés du temps de paix


Mais avec le retour de la paix, la haute couture française s'est trouvée aux prises avec d'autres difficultés.


Difficultés de main-d'oeuvre, d'abord. Bien que les ouvrières n'aient pas été, dans leur ensemble, déportées, un certain nombre, soit pour servir la Résistance, soit par crainte des bombardements, soit pour des raisons d'ordre social, avaient quitté Paris et abandonné leur métier.


Absorbées dans la suite par d'autres activités elles ne sont pas revenues à l'atelier. Or il faut six ans pour former une couturière, et cette pénurie de main-d'oeuvre constatée au lendemain de la libération subsiste encore aujourd'hui.


Trop souvent, en effet, cédant a la nécessité du moment, des parents orientent leurs enfants vers des secteurs momentanément plus rémunérateurs au détriment d'un apprentissage sérieux. Il s'ensuit que la coulure ne parvient à recréer ni les cadres ni les éléments qui lui sont indispensables, et manque, de ce fait, de personnel qualifié.


D'autre part, la pénurie de matières premières sévit toujours. Les métiers d'art obtiennent cependant de modestes contingents de textile, mais ils doivent réserver les produits fabriqués, dans la mesure où les prix pratiqués le permettent, à l'exportation afin de faire entrer en France le plus possible de devises. Une telle politique risque d'ailleurs d'être inefficace; une collection n'est achetée par l'étranger que dans là mesure où elle est consacrée par Paris.


Présentement la couture dispose de textiles de remarquable qualité, mais trop rares puisque bien souvent il n'existe que trois répétitions pour un modèle.


L'aspect économique du problème n'est pas moins préoccupant. Le marché intérieur se resserre depuis la libération. On impute ce fait à la volonté des fortunes acquises pendant la guerre de ne pas se faire remarquer. Mais cela provient beaucoup plus, à notre sens, de la baisse du pouvoir d'achat et de la hausse des prix qu'ont entraînées les améliorations de salaires.


Les débouchés extérieurs


La couture devrait pouvoir compter sur les débouchés extérieurs; or ceux-ci, inexistants en 1944, ne s'ouvrent que difficilement. L'insuffisance des communications retient dans leur pays les riches étrangères. Au départ nos prix étaient trop élevés. La dévaluation Pleven a permis de travailler avec les marchés d'exportation en supprimant avant même qu'elles fussent réellement appliquées les primes de péréquation prévues pour les écarts de change. Mais la dernière augmentation de salaire de 25 % a relevé de façon notable les prix de revient, annulant ainsi l'unique avantage de la dévaluation.


La haute couture doit encore faire face à d'autres difficultés. Partout, à l'étranger, se sont créées des industries concurrentes qui prétendent se passer de la France. Les différents marchés possibles nous sont maintenant fermés. L'Angleterre n'importe pas de produits de luxe. L'Amérique paraissait offrir de grandes possibilités de vente, mais une récente mesure de l'office des prix de Washington, étendue aux importations de l'étranger, apporte à nos exportations une entrave sérieuse en limitant le métrage des étoffes entrant dans la confection des vêtements, et en interdisant toute fantaisie.


L'Amérique latine pourrait être un débouché intéressant; nos services aériens permettent en effet dès maintenant la venue à Paris des élégantes d'outre-atlantique. Il y a la une espérance. D'autre part, il est indispensable de réhabituer l'oeil étranger à " voir français " après nos cinq années d'absence sur les marchés mondiaux. Si l'on veut que nos exportations reprennent, il sera nécessaire de réorganiser le tourisme.


La chambre syndicale agit de son côté au delà de nos frontières par la présentation de mannequins ou de poupées, tant en Europe qu'en Amérique et en

Australie

. Ces manifestations reçoivent partout un accueil très favorable, et cette politique réussit pleinement; chaque fois, en effet, on constate quelques mois après un nouvel afflux d'acheteurs étrangers à Paris.


Telle est la situation. Du moins pouvons-nous souligner le dynamisme réconfortant, qui ne cesse d'animer ce secteur de notre économie. Les couturiers, bien qu'individualistes, se sont organisés pour mener une politique cohérente et résoudre en commun leurs difficultés. Leurs heureuses initiatives nous ont déjà permis d'acquérir une part des devises indispensables à notre économie.


Relevons aussi au sein de cette profession de prestige international et si typiquement française, l'inexistence de problèmes sociaux du fait de la compréhension patronale, mais aussi et surtout grâce à l'activité d'un personnel extraordinaire qui a conservé l'amour de son travail, et auquel on peut tout demander parce qu'il s'associe pleinement à l'existence de " la maison ".


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006181.xml</field>
    <field name="pdate">1947-01-03T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
