<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">L'hypocrisie de la réunion de Davos</field>
    <field name="author">Jeanne Morinay</field>
    <field name="id">eupr_006452.xml</field>
    <field name="description"><![CDATA[


Warren Buffet s'amusait à dire que Davos, "

ce sont des milliardaires qui expliquent à un parterre de millionnaires la façon dont il faut réformer la classe moyenne

". Cette année, la réunion annuelle phare du Forum économique mondial portera sur la façon de construire un monde "

plus cohésif et plus durable

", mais cette année encore, il est permis de douter sur la traduction réelle des idéaux des élites économiques et politiques. D'autant plus lorsque l'on sait que la majorité des grands patrons des banques et institutions financières se rendront en Suisse en jet privé, comme l'a décrié Greenpeace mercredi.


Dans une tribune publiée aujourd'hui sur Project Syndicate et à quelques jours de la tenue du Forum, Alexander Friedman, Jerry Grinstein, Larry Hatheway et Charles C. Krulak, des intellectuels reconnus aux postes clés, réfléchissent à des propositions pour mettre le modèle capitaliste dominant sur une meilleure voie. Premièrement, "

il est temps de réviser le code fiscal américain pour réduire les inégalités structurelles de richesse

". Beaucoup de dirigeants présents à Davos ont soutenu le projet fiscal du Président Donald Trump en 2017, qui a réduit considérablement les impôts des entreprises et des milliardaires et contribuera à faire perdre à des millions de ménages de la classe moyenne leur assurance santé. L'argent, pour l'essentiel, n'a pas servi l'investissement comme le proclamaient les soutiens de Donald Trump, mais a servi la rentabilité des titres. Comme le mentionnent les auteurs, le steepep-up cost basis, permet encore à la classe supérieure d'échapper à l'impôt au moment de léguer leur patrimoine.


Deuxièmement, il y a le sujet des prêts étudiants, perçus par des millions d'Américains comme une condamnation à vie, étant donné qu'il n'est plus rare de commencer sa vie professionnelle avec une dette de 100.000 euros. C'est aussi un enjeu pour l'élection présidentielle de 2020 puisque le sénateur Bernie Sanders a déjà promis d'effacer la totalité des plus de 1.500 milliards de dollars de dette supportés par 45 millions d'Américains, quand la sénatrice progressiste Elizabeth Warren veut le faire totalement ou partiellement pour 95% d'entre eux. Même en tenant compte de la multitude de bourses et de financements disponibles, le coût des études aux États-Unis est tel que la plupart des emprunteurs ne parviennent pas à rembourser leurs prêts dans les délais. Ils sont obligés de rééchelonner cette dette sur vingt ou vingt-cinq ans, via des programmes fédéraux définissant des "

mensualités abordables

" par rapport à leurs revenus.


Troisièmement, "

nous devons modifier les rapports des entreprises afin d'encourager une réflexion plus durable à long terme, la première étape consistant à mettre fin à l'obsession des bénéfices trimestriels

". Les auteurs plaident pour une révision des échéances sur lesquelles sont jugées les entreprises, la période de trois mois étant trop courte pour qu'elles soient incitées à mettre en place des stratégies de long terme. Sur ce même thème, "

les rachats d'actions méritent une attention plus critique [...]


Les sociétés du S&P 500 utilisent maintenant couramment les bénéfices ou l'argent emprunté pour racheter leurs actions, au lieu d'investir dans de nouvelles usines, de nouveaux secteurs d'activité

". Oui, les grands dirigeants américains ont opté en août l'an dernier pour un modèle d'entreprise multipartite, qui au-delà de l'actionnaire, doit satisfaire l'employé, le fournisseur ou encore l'environnement. Mais la charte n'a rien de contraignant et c'est pourquoi "

il faudrait élargir les rapports d'entreprise pour y inclure les mesures telles que le taux de satisfaction des clients, l'empreinte carbone [...] ou encore l'écart de rémunération entre les cadres supérieurs et les employés moyens",

proposent les auteurs.


Quatrièmement, il y a le sujet de la taxe sur les transactions financières. C'est en 1972 que la première taxe sur les transactions financières (TTF) a vu le jour, sous le nom de

"taxe Tobin

" suggérée par le prix Nobel d'économie James Tobin. Près d'un demi-siècle plus tard, le chantier est loin d'être achevé. Elle est au coeur d'un arbitrage polémique entre les moyens considérables qu'elle peut dégager - grâce à un impôt de l'ordre de 0,1% - et la pénalité qu'elle fait subir au secteur financier - surtout à l'heure où la City redistribue ses pions. Selon le Congressional Budget Office (CBO), une taxe sur les transactions de 0,1% pourrait produire environ 1.000 milliards de dollars de recettes supplémentaires, au moment où la relance budgétaire est sur toutes les lèvres, après que les politiques monétaires modernes ont montré leurs limites.


Cinquièmement, les pays doivent augmenter les salaires minimums et les indexer sur l'inflation, toujours selon les autres auteurs du rapport. Selon la Banque de réserve fédérale de Chicago, ces mesures augmenteraient également la demande globale dans la plus grande économie du monde. Sixièmement, il faudrait rapidement revoir la comptabilisation nationale de la richesse des pays. La croissance du PIB en tant que telle n'a plus de sens pour mesurer le bien-être des nations quand l'on sait que les réparations d'un tsunami, en boostant le secteur du bâtiment, font mécaniquement augmenter le produit national. "

Le revenu national devrait inclure les coûts des externalités telles que la dégradation de l'environnement ou les émissions de gaz à effet de serre (GES)

[...]

En outre, tous les pays devraient convenir de normes communes pour intégrer d'autres mesures du progrès social. Celles-ci pourraient inclure l'espérance de vie, la mortalité infantile, la détection, la prévention et le traitement des maladies courantes

", proposent les auteurs.



Last but not least

, il faut prendre d'urgence des mesures pour faire face aux changements climatiques d'une manière qui permette de partager équitablement les coûts au sein des pays et entre les générations. La politique climatique doit avoir des avantages tangibles aujourd'hui si elle veut être politiquement acceptable. Cela suppose des taxes, mais aussi des subventions pour "

les communautés actuelles productrices de charbon, de pétrole et de gaz, ainsi que les familles à faible ou modeste revenu touchées par une taxation régressive".

Dans l'actualité récente, et au-delà du mouvement des Gilets jaunes dont la mèche s'est allumée après la hausse prévue de la taxe carbone, le Zimbabwe a été le terrain de manifestations meurtrières au mois de janvier l'an dernier après que le Président Emmerson Mnangagwa a annoncé le doublement des prix des carburants pour tenter d'enrayer la plus grave pénurie de pétrole dans le pays depuis dix ans. "

Les élites parlent de fin du monde, quand nous, on parle de fin du mois

". Selon le Comité mixte américain sur la fiscalité et le CBO, une taxe sur le carbone de 25 dollars par tonne avec des augmentations annuelles de 2% ajustées en fonction de l'inflation permettrait de réunir 1.000 de dollars aux États-Unis sur une décennie.


Voilà de vrais sujets sur lesquels les chefs d'entreprise qui se rendront la semaine prochaine au Forum de Davos devraient se pencher. Rappelons que la dernière conférence sur le climat de l'ONU, la COP25, qui s'est tenue à Madrid en décembre n'a débouché sur aucune avancée significative malgré l'urgence, et pendant que des incendies dantesques dévastaient l'

Australie

.


Cet article est paru dans WanSquare (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006452.xml</field>
    <field name="pdate">2020-01-17T00:00:00Z</field>
    <field name="pubname">WanSquare (site web)</field>
    <field name="pubname_short">WanSquare (site web)</field>
    <field name="pubname_long">WanSquare (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Environnement</field>
  </doc>
</add>
