<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">lautre mark</field>
    <field name="author">U R Par ARNAUD GONZAGUE PhotosAUDOIN DESFORGES</field>
    <field name="id">eupr_004995.xml</field>
    <field name="description"><![CDATA[


En décembre dernier, Thierry Marx a participé à la remise des diplômes de l'une des nombreuses écoles qu'il parraine (voir p. 21) - celle-ci est dédiée à la photographie culinaire. Le raout se déroulait au septième étage du ministère de l'Economie et des Finances, mais l'important est le nom de cette formation : Media Social Food. Les médias, le social et la cuisine, un résumé parfait de ce qui travaille, en profondeur, le chef parisien de 59 ans. Marx a besoin de séduire les médias pour mieux faire la promotion de ses formations auprès du grand public; besoin de la cuisine pour attirer la lumière et recréer du lien social; besoin de social pour donner un peu de sens à la grande quincaillerie médiatique.


XOn sent que chez lui, tout se tient étroitement. L'ego et l'envie d'être utile, l'engagement sincère et l'habileté du bon storyteller, la capacité de serrer la main aux politiques comme aux géants de l'agroalimentaire, mais aussi de prêter une oreille bienveillante à M. Tout-le-Monde. Et d'intervenir aussi bien dans les centres pénitentiaires et les écoles que sous les ors d'un ministère. « Bien sûr que c'est quelqu'un qui a envie de faire partie des grands, de ceux qui laissent une trace, décrypte Raphaël Haumont, le physico-chimiste avec lequel il a fondé le Centre français d'Innovation culinaire à l'université d'Orsay (Essonne). Mais c'est aussi une personnalité authentique, ouverte et généreuse. Les gens qui travaillent avec lui ne claquent jamais la porte. » C'est cet alliage qui fait qu'avec Thierry Marx, on a moins envie de parler de jus de viande ou d'émulsions de légumes que de la société française comme elle va. Pour lui, elle va mal d'ailleurs, comme la planète en général. Mais l'homme a le sens du consensus : il tacle la malbouffe à longueur de colonnes, mais dit que l'industrie agroalimentaire « évolue beaucoup »; il fait comprendre que l'école républicaine lui a été un calvaire, mais n'incrimine aucun enseignant. Une philosophie : au lieu de vitupérer contre des coupables, mieux vaut penser aux solutions. Depuis 2012, il a porté sur les fonts baptismaux une cascade de formations qui soutiennent (gratuitement) les projets professionnels de publics très éloignés de l'emploi (voir p. 26). « Thierry Marx est ce qu'on appelle un "destinateur". C'est-à-dire quelqu'un qui donne une mission aux autres pour les faire grandir, avance Ivan Darrault-Harris, professeur émérite et membre du Centre de Recherches sémiotiques de l'université de Limoges. C'est pour cela qu'on a tant envie de le suivre et de l'écouter. » Il y a quelque chose de physique dans cet attrait. Marx, qui s'est initié à la méditation zen et s'est perfectionné dans les arts martiaux au Japon, a l'allure du sage oriental - crâne lisse, toge immaculée (la veste de cuisine s'apparentant chez lui au kesa du moine bouddhiste), sourire de celui-qui-sait. Malgré sa carrure à la Bruce Willis, sa voix reste d'une extrême délicatesse et son regard d'une vraie douceur.


Surtout, pour Ivan Darrault-Harris, « il est un homme arrivé au sommet après un long et difficile parcours ». Dans les interviews qu'il donne, il se raconte. « Mais pas comme le font les chefs traditionnels, qui parlent du terroir de leur enfance et de la tarte de leur grand-mère qui continue de les inspirer », note Raphaël Haumont. Thierry Marx parle de l'homme qu'il fut : il vient d'un milieu dénué de racines. Fils d'une modeste famille du quartier de Belleville, à Paris (papa ouvrier du bâtiment, maman laborantine), il atterrit à 12 ans dans la cité HLM d'une banlieue pas facile (Champigny-sur-Marne). Au collège, le jeune garçon décroche, fréquente les petits voyous, se bagarre, mais échappe aux pires vicissitudes de la rue grâce à la discipline du judo.


Enfant, il rêvait de finir « cuisinier-pâtissier sur un bateau », mais la conseillère d'orientation - un modèle de perspicacité - lui dit : « La cuisine, ce n'est pas pour vous. » On le place en CAP mécanique générale. Il s'y soustrait grâce à son grand-père qui le recommande chez les Compagnons du Devoir et décroche un CAP de pâtisserie. Mais à Champigny, il n'y a rien à faire, personne ne l'attend. Exit les millefeuilles. Marx travaille comme manutentionnaire, convoyeur de fonds, garde du corps, s'enrôle dans l'armée pendant trois ans. Il officie en tant que Casque bleu au Liban, revient en France et parvient à décrocher un CAP de cuisine, un BEPC et même un bac général. « Ceux qui ont le savoir ont le pouvoir », lui répétait sa grand-mère. Il se met à la lecture, prenant une revanche sur le passé humiliant de celui qui ne savait pas écrire correctement « Bon anniversaire » sur un gâteau.


Le jeune homme comprend que lorsqu'on n'a pas de réseau, il faut du culot. Il rencontre alors l'immense Bernard Loiseau dans son restaurant, à Saulieu, qui discute longuement avec lui, sans lui donner de travail. Le lendemain, il a rendez-vous avec Claude Deligne. « D'où tu viens? » lui demande le grand maître du Taillevent.


« De chez Loiseau », répond Marx du tac au tac. Semi-mensonge, qui démontre déjà un sens aigu de la story. Ça marche : il est embauché comme commis, puis file chez Joël Robuchon, puis chez Alain Chapel, part se perfectionner à Sydney, à Tokyo, à Singapour. Il revient en France et, en 1988, il décroche sa première étoile au Michelin pour son restaurant de Montlouissur-Loire. La deuxième arrive en 1999 pour ses bons offices au Relais & Châteaux de Cordeillan-Bages, en Gironde. Le voilà lancé pour de bon.


« Son parcours est celui d'un héros, au sens mythologique du terme, explique Ivan Darrault-Harris. Un personnage en rupture avec les codes de son milieu, qui entreprend un long voyage pour échapper à son destin. Le Japon, l'

Australie

, le Liban Le héros Marx s'est forgé une identité ailleurs, avant de revenir en maître. Notez qu'il gère aujourd'hui la brasserie de la tour Eiffel. Quoi de plus français? » Ulysse a fait un beau voyage, mais il continue pourtant de détonner dans le petit monde des chefs français. D'abord, il est de ceux qui n'élèvent jamais la voix (ou rarement) en cuisine, lieu où, traditionnel-lement, les ordres sont impitoyablement aboyés. Il a même claqué la porte de l'émission « Top Chef » en 2014 parce que, a-t-il dit, M6 exigeait de lui un comportement d'adjudant des arrière-salles, ce qu'il n'est pas. Ses trouvailles culinaires elles-mêmes tranchent avec les standards français. « Il privilégie l'épure extrême : trois gestes, trois ingrédients et un assaisonnement, point. On est loin des plats noyés de sauce, de crème et de champignons, sourit Raphaël Haumont. C'est un expérimentateur. Lui, il aime d'abord la science et les innovations. » Ce scientisme avide, cette curiosité envers la nouveauté ont un côté très anglo-saxon - ne pas oublier qu'il a étudié le management à Berkeley au début des années 1990 - tout comme l'est son amour sans complexe pour l'ambition professionnelle.


Et s'il y a eu bien d'autres chefs français qui ont accepté de frayer avec les géants de l'agro-industrie, on n'en connaît pas qui se reconnaissent aussi volontiers chefs d'entreprise que lui, et qui citent aussi régulièrement le businessman américain Warren Buffett. Cela ne lui attire pas forcément les suffrages de la profession. Pour autant, difficile de trouver un critique gastronomique qui ose en dire du mal. « Je n'ai pas très envie de m'exprimer sur lui. J'espère que vous comprendrez », s'excuse l'un d'entre eux contacté par nos soins. Les autres ne nous ont pas répondu, à l'exception de Philippe Couderc, plume au magazine « Challenges ». « Marx, c'est de la cuisine qui épate peu le bourgeois, ce qui n'empêche pas qu'il a un grand talent. Mais c'est un homme protée : il est tellement partout, tellement dans une course à la réussite Cela provoque un peu de lassitude. » Thierry Marx, homme trop pressé? En 2018, l'un de ses partenariats a valu à ce dézingueur de malbouffe un carton rouge, distribué par l'association Foodwatch : certains parfums de la barre alimentaire bio qu'il a concoctée avec l'entreprise Feed étaient notés « C » et « D » au Nutri-Score - le baromètre alimentaire des autorités sanitaires. En cause, notamment, la présence riquiqui de fruits lyophilisés (3% de l'ensemble) et l'ajout de maltodextrine, un dérivé ultra-transformé du sucre, 100% industriel. Thierry Marx est partout : il n'est pas anormal qu'il se trouve aussi, quelquefois, là où il est dispensable.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004995.xml</field>
    <field name="pdate">2019-07-25T00:00:00Z</field>
    <field name="pubname">L'Obs</field>
    <field name="pubname_short">L'Obs</field>
    <field name="pubname_long">L'Obs</field>
    <field name="other_source">L'Obs (site web)</field>
    <field name="source_date">27 juillet 2019</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
