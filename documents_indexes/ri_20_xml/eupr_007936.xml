<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Paul Audi, Agnès Desarthe, Didier Cahen, Lucien Jerphagnon, Sergi Pàmies… Les brèves critiques du « Monde des livres »</field>
    <field name="author">Collectif</field>
    <field name="id">eupr_007936.xml</field>
    <field name="description"><![CDATA[


PHILOSOPHIE. « Curriculum. Autour de l’esth/éthique », de Paul Audi


Paul Audi poursuit l’élaboration d’une œuvre philosophique singulièrement originale et diverse, aux points de croisement de l’esthétique et de l’éthique. En rassemblant, sous le titre Curriculum, des analyses conduites dans le sillage de Créer. Introduction à l’esth/éthique, maître-livre paru chez Verdier en 2010, le philosophe, né en 1963, approfondit le concept de création au fil de rencontres et dialogues avec Nietzsche, Sartre, Lacan, Derrida, Foucault. Les textes, de tonalités différentes, entrelacent humour et théorie, rigueur et poésie, concepts et littérature. Ils fournissent également, à la fin du volume, l’occasion d’un bilan d’étape, sous la forme d’un tableau du chemin parcouru. L’auteur de L’Ethique mise à nu par ses paradoxes, même (PUF, 2000) ou du Pas gagné de l’amour (Galilée, 2016) y esquisse les lignes de force et la logique interne d’un ensemble riche de plus de trente volumes. R.-P. D.


« Curriculum. Autour de l’esth/éthique », de Paul Audi, Verdier, 328 p., 22 €.


HISTOIRE. « Les Gnostiques », de David Brakke


Le gnosticisme, qui n’a peut-être pas existé, n’était pas ce que l’on croyait. Tel est à peu près l’état de la recherche sur ce courant de pensée multiforme, face auquel s’est bâtie l’orthodoxie chrétienne, d’Irénée de Lyon (v. 130-202) aux pères conciliaires du IVe siècle. Par bonheur, David Brakke offre une lumineuse synthèse de ce que l’on peut tout de même savoir, en forme d’introduction à « la diversité sociale et culturelle qui rend les débuts du christianisme si fascinants ». Fl. Go


« Les Gnostiques. Mythe, rituel et diversité au temps du christianisme primitif » (The Gnostics. Myth, Ritual and Diversity in Early Christianity), de David Brakke, traduit de l’anglais (Etats-Unis) par Marie Chuvin, Les Belles Lettres, 200 p., 23 €.


ESSAI. « Trois pères. Jabès, Derrida, Du Bouchet », de Didier Cahen


Quel point commun entre les poètes Edmond Jabès (1912-1991), André du Bouchet (1924-2001) et le philosophe Jacques Derrida (1930-2004) ? La mémoire de Didier Cahen, poète et écrivain, qui leur rend hommage dans un livre singulier, entre récit, critique et témoignage. Ces rapprochements inattendus, Didier Cahen les affectionne. Notamment dans « Le Monde des livres » où il tient, sous la forme du collage, la chronique « Trans|Poésie ». Il n’a jamais entretenu un rapport d’allégeance à ces « trois mages ». « La dimension biographique l’emporte clairement sur l’analyse critique », explique-t-il en préambule. Cela n’exclut évidemment pas une lecture pensive de ces auteurs réputés complexes. Mais ce qui rend surtout le livre attachant, c’est ce parti pris de ne jamais séparer la parole poétique ou philosophique de l’expérience humaine. Il autorise l’auteur à évoquer de petites et grandes choses sous forme de souvenirs, de fragments, d’aperçus. Il est question de la poésie qui devrait, selon lui, s’écrire aujourd’hui contre « l’emprise du verbe » ; mais aussi de la judéité et de la blessure originelle, chez Jabès ; de Derrida, qui aurait aimé écrire de la poésie si le démon de la théorie n’était pas toujours revenu « empiéter ». Au terme de ce beau livre de questions, on comprend que la vocation d’un poète ne peut venir de nulle part. A. d. C.


« Trois pères. Jabès, Derrida, Du Bouchet », de Didier Cahen, Le Bord de l’eau, 168 p., 16 €.


ROMAN. « La Chance de leur vie », d’Agnès Desarthe, Points, 310 p., 7,40 €.


Dans La Chance de leur vie, Agnès Desarthe envoie une famille française, les Vickery, aux Etats-Unis, afin que le père, Hector, philosophe et poète, enseigne dans une fac de Caroline du Nord. Pendant ce temps, le fils adolescent perfectionnera son anglais, qui n’en a guère besoin. Et Sylvie, la mère ? Eh bien, elle ne fera rien, fidèle au « dogme taoïste du non-agir » auquel elle déroge rarement. La Chance de leur vie est un roman d’une grande simplicité apparente et d’une profondeur immense. Il y est question de la France, des Etats-Unis, du couple, du lien entre parents et enfants, de la soif de spiritualité, des âges de la vie… Ce livre splendide et drôle exalte l’amour du secret et croit en la vérité des gestes et des corps. R. L.


« La Chance de leur vie », d’Agnès Desarthe, Points, 310 p., 7,40 €.


PHILOSOPHIE. « Aimons-nous encore la liberté ? », de Michel Erman


Tocqueville parlait de « passions ennemies ». Michel Erman, qui s’inscrit dans son sillage, préfère évoquer une « fatigue », voire une « léthargie ». Mais il s’agit du même combat entre « l’envie de rester libres » et « le besoin d’être conduits » qu’a défini, dès 1840, De la démocratie en Amérique – nuancé, chez le philosophe contemporain, d’un sentiment de défaite. Aimons-nous encore la liberté ? est en effet, à l’instar de son titre, un questionnement angoissé sur les formes contemporaines de contrainte sociale – parfois décrites à l’emporte-pièce – mais, plus encore, et de manière plus intéressante, sur la complaisante placidité avec laquelle nous les acceptons. Fl. Go


« Aimons-nous encore la liberté ? », de Michel Erman, Plon, 156 p., 16 €.


ESSAI. « Chroma. Un livre de couleurs », de Derek Jarman


Ludwig Wittgenstein a passé la fin de sa vie à écrire des Remarques sur les couleurs (1977). Derek Jarman (1942-1994) a-t-il voulu imiter le philosophe, alors qu’il venait d’achever son film Wittgenstein (1993) ? C’est affaibli par le sida, presque aveugle, sûr de sa mort prochaine, que le cinéaste et écrivain se lance dans son propre « Livre de couleurs ». Ce Chroma, enfin réédité, tient à la fois d’un « à la manière de » baroque – intuitions zigzagantes, rapprochements inattendus, phénoménologie sauvage, lancés aux trousses d’une introuvable logique des couleurs – et d’un flamboyant et déchirant adieu à la beauté du monde. Fl. Go


« Chroma. Un livre de couleurs », (Chroma. A Book of Colour), de Derek Jarman, traduit de l’anglais par Jean-Baptiste Mellet, L’Eclat, « Poche », 256 p., 8 €.


PHILOSOPHIE. « L’Absolue simplicité », de Lucien Jerphagnon


Selon ses propres mots, il aimait à « mettre le chambardement dans les têtes ». Ancien prêtre devenu l’élève puis l’assistant de Vladimir Jankélévitch, historien de l’Antiquité et philosophe, spécialiste reconnu de saint Augustin, Lucien Jerphagnon (1921-2011) fut toujours un inclassable, qui préféra l’enseignement à la renommée. L’importance de son œuvre n’en apparaît pas moins, peu à peu, dans toute son évidence. Ce troisième volume dans la collection « Bouquins », après Les Armes et les Mots (2012) et L’Au-delà de tout (2017), réunit ses cours, des articles mais surtout quelques-uns de ses textes fondamentaux, dont le bouleversant Julien, dit l’Apostat (1986), sur cet empereur du IVe siècle, petit-fils de Constantin, qui tenta de rétablir les anciens dieux avant de mourir dans une expédition guerrière en Mésopotamie. Jerphagnon, après Voltaire et tant d’autres, était fasciné par cet intellectuel et brillant général qui, comme lui, rêvait d’une synthèse entre néoplatonisme et christianisme. Dans la préface, Michel Onfray rend hommage à celui qu’il salue comme son « vieux maître ». Leurs oppositions sont évidentes mais il reconnaît tout ce qu’il lui doit. Lucien Jerphagnon confiait volontiers : « Je n’ai pas formé des clones et c’est la preuve que j’ai été finalement un bon professeur. » M. Se.


« L’Absolue simplicité », de Lucien Jerphagnon, Robert Laffont, « Bouquins », 1 190 p., 32 €.


ROMAN. « Seiobo est descendue sur terre », de Laszlo Krasznahorkai


En dix-sept récits, le grand écrivain hongrois Laszlo Krasznahorkai étudie la possibilité d’un choc esthétique à l’ère moderne, provoqué par le détail d’une toile, un masque nô à l’effigie de la déesse Seiobo… Malgré leur autonomie apparente, les histoires convergent en montrant comment le prosaïsme cynique du présent n’a pas annihilé toute rencontre avec la beauté. Laszlo Krasznahorkai joue admirablement du contraste entre un quotidien saccagé par la laideur des métropoles, les cohortes de touristes jetées à l’assaut des chefs-d’œuvre qu’ils écrasent de leur masse et ce moment à peine perceptible où une œuvre arrache un flâneur à la foule et fait basculer son destin. N. W.


« Seiobo est descendue sur terre », (Seiobo jart odalent), de Laszlo Krasznahorkai, traduit du hongrois par Joëlle Dufeuilly, Babel, 460 p., 9,80 €.


ROMAN. « Le Sillon », de Valérie Manteau


Le Sillon commence comme le récit d’une liaison vacillante pour se transformer en précis de décomposition d’un pays, la Turquie. Installée sur la rive asiatique de la ville, la narratrice, amoureuse d’un Turc, marche, met ses pas dans ceux du journaliste d’origine arménienne Hrant Dink, assassiné en 2007, s’assoit pour fumer une cigarette, milite en faveur des journalistes et écrivains poursuivis après la tentative de coup d’Etat de juillet 2016… L’écriture de Valérie Manteau possède une grâce et une légèreté qui lui permettent d’entremêler l’évocation de ce qui se passe dans la tête de la jeune Française avec la description de ce qui advient dans les rues et dans le pays. Ce livre étonnant et beau a reçu le prix Renaudot en 2018. R. L.


« Le Sillon », de Valérie Manteau, Le Tripode, « Météores », 236 p., 9 €.


ROMAN. « Maunten », de Drusilla Modjeska


Penser l’histoire ou croire qu’elle n’a aucun sens : telle est la ligne qui divise les personnages de Maunten, premier roman traduit de l’Australienne Drusilla Modjeska, dont le titre signifie « montagne » en tok pisin, créole et langue officielle de Papouasie-Nouvelle-Guinée. En 1968, Léonard, documentariste anthropologue, y débarque avec son épouse, Rika. La jeune Néerlandaise est photographe. Son travail la lie rapidement à d’autres universitaires étrangers et à de jeunes Papous. Sept ans plus tard, ce territoire, qui fut protectorat britannique, colonie de l’Empire allemand, puis occupé et administré par l’

Australie

, deviendra indépendant. Tandis que la vague des indépendances africaines galvanise les militants, les tensions raciales montent. Mais là-haut, dans cette montagne creusée de fjords, vivent des communautés isolées qui perpétuent des traditions ancestrales comme la confection du tapa, tissu fait d’écorce, que Léonard veut filmer. Roman passionnant, entre documentaire et thriller, Maunten s’ouvre en 2005, quand la génération d’après décide de revenir sur les lieux pour comprendre ce que la montagne a fait de leurs aînés. Gl. Ma.


« Maunten » (The Mountain), de Drusilla Modjeska, traduit de l’anglais (

Australie

) par Mireille Vignol, Au vent des îles, 564 p., 21 €.


BIOGRAPHIE. « Madeleine Riffaud. L’esprit de résistance », d’Isabelle Mons


A 95 ans, Madeleine Riffaud n’a rien oublié du temps où elle s’appelait « Rainer », son nom de code de résistante, choisi en hommage à Rilke. L’universitaire Isabelle Mons a pu s’en rendre compte lors de leurs entretiens. Encore fallait-il trouver le souffle pour restituer la force du témoignage de celle qui fut une héroïne, quoi qu’elle s’en défende. La biographe y est parvenue en maintenant un équilibre entre propos recueillis et souvenirs tirés des archives et des œuvres de son interlocutrice.


Avec sensibilité, elle raconte l’histoire de cette jeune fille qui entre dans la résistance quand, après la défaite de juin 1940, un Allemand lui flanque un violent coup de pied. A l’été 1944, elle exécute un officier de l’armée d’occupation en plein Paris, pour venger son camarade « Picpus », tombé sous les balles ennemies, et les victimes d’Oradour-sur-Glane. Arrêtée, torturée, elle répète : « Je ne sais rien. » Elle en réchappe à la faveur d’un échange de prisonniers, et participe aux combats pour la libération de Paris.


Après la guerre, elle se fait un nom comme poète et devient l’amie de Picasso, Eluard, Aragon, et des époux Aubrac. Devenue journaliste à L’Humanité, elle réalise des reportages en Algérie, au Vietnam, où elle révèle l’horreur des bombardements américains, puis dans les hôpitaux parisiens. Mille vies en une, qu’Isabelle Mons remet en ordre, tout en donnant à voir la richesse de l’œuvre politique et littéraire. A. Fl.Média d’appel


« Madeleine Riffaud. L’esprit de résistance », d’Isabelle Mons, Payot, 368 p., 22,90 €.


NOUVELLES. « L’Art de porter l’imperméable », de Sergi Pàmies


Ce recueil de nouvelles ressemble à un cristal. Il reflète toutes les couleurs de l’amour. Tous ses stades aussi : l’émerveillement de la rencontre, la fragilité d’un couple, l’amour filial d’un homme refusant d’annoncer à sa mère nonagénaire que son éditrice ne publiera pas son dernier texte… Taciturnes et pusillanimes, les personnages partagent un penchant pour la désespérance qui donne au livre son charme aigre-doux. Le Catalan Sergi Pàmies décrit même ses propres parents, militants politiques exilés en France sous Franco, et qui, au côté d’Albert Camus ou de Jorge Semprún, trouvent place dans son panthéon mythifié de dissidents de gauche. Mais alors que tous étaient de fiers « porteurs d’imperméables », lui s’entête à arborer ce vêtement sans parvenir à toucher du doigt le grand absent du recueil : l’amour de soi. An. Fr.


« L’Art de porter l’imperméable » (L’art de portar gavardina), de Sergi Pàmies, traduit du catalan par Edmond Raillard, Jacqueline Chambon, 130 p., 15 €.


PHILOSOPHIE. « Récit et reconstruction », de Claude Panaccio


Quel est le lien – si ténu qu’il est parfois difficile de les distinguer l’une de l’autre – entre la philosophie et son histoire ? Selon Claude Panaccio, il tient au fait que « la philosophie (…) ne peut miser sur aucun consensus qui l’autoriserait à reléguer son propre passé au rang de ce qui est définitivement révolu ». Le chercheur québécois s’attache dès lors à expliquer ce qui fait de l’histoire de la philosophie un exercice proprement philosophique. Son argumentation, formidablement érudite et d’une exemplaire clarté, repose sur la notion de reconstruction. Faire de l’histoire de la philosophie, c’est en effet toujours reconstruire, dans son discours propre, celui d’un auteur ou d’une école passée, ce qui « présuppose certaines continuités entre les discours philosophiques de nos devanciers et ceux d’aujourd’hui ». S. Be.


« Récit et reconstruction. Les fondements de la méthode en histoire de la philosophie », de Claude Panaccio, Vrin, « Analyse et philosophie », 230 p., 24 €.


HISTOIRE. « Histoire mondiale de la guerre froide. 1890-1991 », d’Odd Arne Westad


Quand la guerre froide a-t-elle commencé ? Pour Odd Arne Westad, il faut remonter à la première crise du capitalisme, dans les années 1890, qui entraîne une radicalisation du mouvement ouvrier et la montée en puissance de la Russie et des Etats-Unis, préfigurant la double bipolarité, intellectuelle et systémique, qui marquera le XXe siècle – socialisme contre capitalisme, Etats-Unis contre Union soviétique. La thèse est audacieuse. Elle aurait gagné à être davantage soutenue. Plus intéressante, dans cette étude qui demeure d’une grande richesse, est l’ouverture sur les mondes asiatique, africain et latino-américain. La guerre froide, on le sait, a plutôt été une succession de guerres périphériques qu’un affrontement direct entre les deux géants, l’arme nucléaire interdisant un tel choc. G. M.


« Histoire mondiale de la guerre froide. 1890-1991 » (The Cold War. A World History), d’Odd Arne Westad, traduit de l’anglais (Etats-Unis) par Martine Devillers-Argouarc’h, Perrin, 350 p., 27 €.


Cet article est paru dans Le Monde (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007936.xml</field>
    <field name="pdate">2019-10-24T00:00:00Z</field>
    <field name="pubname">Le Monde (site web)</field>
    <field name="pubname_short">Le Monde (site web)</field>
    <field name="pubname_long">Le Monde (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">critique-litteraire</field>
  </doc>
</add>
