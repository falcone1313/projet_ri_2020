<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Bordeaux, une fascination chinoise</field>
    <field name="author">Hubert Prolongeau</field>
    <field name="id">eupr_005568.xml</field>
    <field name="description"><![CDATA[


bordeaux envoyé spécial - Un nom, c'est bien souvent tout ce qui pour nous reste de quelqu'un, pas seulement mort, mais de son ­vivant. » Sans doute l'entrepreneur chinois Chi Keung Tong n'avait-il pas relu Proust ­depuis longtemps quand il décida de ­rebaptiser les quatre châteaux bordelais qu'il a achetés entre août 2016 et septembre 2017 : Larteau est devenu ­Lapin impérial, Tour Saint-Pierre s'appelle désormais Château Lapin d'or, ­Senilhac est rebaptisé Antilope tibétaine et Clos Bel-Air répond à la douce appellation de Grande Antilope.


Ces changements de nom indignent le milieu viticole de la région. En étant ­affichés au grand jour, ils symbolisent les achats chinois de châteaux du Bordelais, amorcés en 2008 et multipliés ­depuis 2011 avec l'aide de l'agence ­immobilière Maxwell-Baynes et d'une jeune Chinoise, Li Lijuan, chanteuse quadrilingue devenue l'interlocutrice privilégiée des entrepreneurs de l'empire du Milieu. Rien qu'en 2018, cinq d'entre eux ont été ­acquis : L'Hermitage Lescours et Mongiron en octobre, ­Lagorce en avril, Segonzac en février, Bellevue en janvier et le très emblématique Bellefont-Belcier fin 2017.


Aussi la vieille peur du « péril jaune » ­revient-elle encore en fin de banquet, quand les vignerons se lâchent. « Ils vont tout acheter », « On n'est plus chez nous », « Bientôt, le bordeaux n'aura plus le même goût ... On a presque l'impression que Fu Manchu est aux commandes. Cette inquiétude n'est pas partagée par tous. C'est le cas de Christophe Chateau, directeur de la communication au comité interprofessionnel du vin de Bordeaux : « Les Chinois apportent de l'argent, rénovent les propriétés et jouent le maintien de la qualité. De quoi nous plaindrions-nous? »


Peut-on pour autant parler d'invasion? Oui, répondent nombre d'experts tels que César Compadre, spécialiste du vin au journal Sud Ouest : « Je considère qu'il y a eu deux choses déterminantes pour le ­vignoble bordelais dans ce premier quart de XXIe siècle : l'envolée des prix des grands crus et l'arrivée des Chinois. » Mais d'autres relativisent le mouvement et parlent plutôt d'une modeste avant-garde pour l'instant : 150 châteaux ont été achetés, soit 3 % du vignoble. Pour mémoire, les Belges en ont acheté une cinquantaine, sans ­susciter de ­fantasmes.


Et puis il faut regarder la nature des propriétés achetées. On ne trouve aucun grand cru prestigieux. Des vins de qualité quand même, mais qui n'atteignent pas des prix stratosphériques à l'hectare. « Les Chinois ont compris que le premier sou ­gagné est celui qu'on ne dépensait pas. Ils n'ont aucun intérêt à acheter des grands crus et sont allés vers les petites propriétés, où l'hectare se vend entre 15 000 et 25 000 euros », explique César Compadre. Par comparaison, un hectare à Pauillac vaut 2 millions d'euros. Seule exception, Bellefont-Belcier, grand cru classé de saint-émilion (mais pas un premier grand cru classé A ou même B). En 2012, ce château a été acquis pour 26 millions d'euros par le métallurgiste Quang Wang, qui l'a vendu en 2015 à un investisseur chypriote, avant de finalement repasser il y a plus d'un an dans les mains d'un propriétaire chinois, Peter Kwok, dont la société Vignobles K ­possède huit châteaux.


Mettre la main sur un petit vin plutôt qu'un grand renvoie à la vision de la ­région par les Chinois : ce qui est prestigieux, c'est le nom générique de « Bordeaux », un sésame magique quelles que soient l'appellation et la qualité du vin, bien plus que telle ou telle propriété. Un prestige que les autres régions viticoles françaises n'ont pas. Une seule propriété en Bourgogne et une dizaine d'autres entre l'Anjou et le Vaucluse sont passées sous pavillon chinois.



Différences culturelles



Dans la grande majorité des cas, les Chinois font du vin à Bordeaux pour le vendre ensuite en Chine, où ils habitent. Ils gèrent leurs propriétés à distance, faisant appel à des vignerons sur place. Mais leur approche est en train de changer. Vers la qualité. Déjà 3 500 étudiants chinois sont venus à Bordeaux pour s'initier au marketing et à la communication, et beaucoup se retrouvent à travailler dans les châteaux. « On sent que les Chinois deviennent de plus en plus connaisseurs, estime Laurence Lemaire, auteure de Le Vin, le Rouge, la Chine (Sirène Production Edition, 13e édition parue en septembre 2018). Ils n'achètent plus n'importe quoi juste parce que c'est à Bordeaux. Ils montent en gamme, se tournent vers le fronsac, le ­médoc et le saint-émilion, et ils possèdent aujourd'hui quatorze crus bourgeois. » Autre évolution : « Il y a de moins en moins de simples particuliers parmi les investisseurs. Les propriétaires actuels ont des ­réseaux importants de distribution en Chine et ils y exportent beaucoup. » Parmi eux, Naijie Qu, patron du groupe Haichang, Franck You, grosse fortune de la pharmacie, ou le milliardaire Jack Ma, fondateur du site marchand Alibaba. Les huit principaux investisseurs chinois totalisent plus de 60 % des propriétés acquises.


Il y a peu de poésie dans la démarche de Jinbao Huang, patron du groupe pharmaceutique Yofoto. Il a acheté en 2012 le château de Lugagnac en profitant de désaccords successoraux dans la famille du précédent propriétaire. Il a ­ensuite gardé aux commandes le fils de la maison, François-Thomas Bon, mais en lui adjoignant une contrôleuse de ­gestion chinoise. Toute la production (700 000 bouteilles par an) part en Chine, achetée directement par le groupe pharmaceutique. Une décision traumatisante pour les employés, qui ont vu d'un coup réduits à néant quinze ans d'efforts pour tisser des liens avec des clients en France et dans le monde. Risque commercial, ­déchirement humain...


« Jinbao Huang ne parlait pas anglais, n'avait pas de culture européenne... Nous avons eu peur. Mais il s'est finalement ouvert à notre façon de faire », avoue M. Bon. Même si les différences culturelles se sont fait jour : la Chine est un pays autoritaire, et les rapports entre patron et employés très formalistes. Là aussi, il a fallu des ajustements. « Le groupe fait des efforts pour s'adapter. Mais il est ­difficile de voir plus loin qu'une dizaine d'années. Le marché chinois n'est pas ­encore mûr pour des produits plus ­difficiles à vendre, comme le rosé ou nos blancs... », ajoute-t-il.


Le château La Rivière (Fronsac) est, lui, devenu tragiquement célèbre quand le nouveau propriétaire chinois, Lam Kok, son fils de 12 ans et l'ancien propriétaire français, James Grégoire, meurent dans un accident d'hélicoptère en décembre 2013. En survolant ce vignoble. C'est sa veuve, Mme Lau, qui a repris le domaine. Marquée par le drame, elle est restée en Chine mais a laissé en place l'équipe française. « Je n'ai pas le sentiment que grand-chose ait changé, sinon qu'il me faut un traducteur pour parler avec ma patronne », explique Xavier Buffo, directeur général. Un représentant de Mme Lau, un Franco-Chinois qui vit à Bordeaux, vient une fois par semaine. Le château, l'un des plus beaux de la région, a aussi continué de s'ouvrir au tourisme : dégustation, ­visites et restaurant.


« L'aspect touristique est important pour les Chinois, commente César Compadre. Ils achètent aussi des châteaux qui en jettent... » De 4 000 visiteurs en 2013, La ­Rivière est passé à 15 000 cette année. « Quand je vais en Chine, je sens cette fierté du château, de la belle bâtisse, des tours et des jardins à la française », ­raconte Xavier Buffo. Un cinquième de la production de La Rivière part en Chine, le reste se divise entre la France et d'autres pays.


Jusqu'où ira cette fièvre d'achat? Le mouvement semble aujourd'hui se tasser. Les premiers qui ont vendu du vin de Bordeaux en Chine, sans vraiment de concurrence, ont gagné beaucoup ­d'argent. Les autres se sont aperçus que faire du vin n'était pas si facile... Le vendre non plus, d'autant que la concurrence se durcit avec l'

Australie

et le Chili, qui bénéficient d'accord de libre-échange avec la Chine, dont la France est privée. L'Etat chinois surveille aussi de près les investissements à l'étranger. En juin 2018, dix des vingt-quatre châteaux acquis par le groupe Haichang ont fait l'objet d'une enquête en France par le Parquet national financier « à cause ­d'infractions dans les montages financiers (blanchiment, faux...) ayant permis leur acquisition », selon la police judiciaire de Bordeaux. Un coup de tonnerre dans un ciel jusque-là serein, dont tout le monde ici se demande à voix basse s'il est annonciateur d'orages.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005568.xml</field>
    <field name="pdate">2019-01-25T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Le Monde (site web)</field>
    <field name="source_date">26 janvier 2019</field>
    <field name="section">Spécial</field>
  </doc>
</add>
