<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">ALENA: le Japon s'invite à la table</field>
    <field name="author">Maxime Bergeron</field>
    <field name="id">eupr_007172.xml</field>
    <field name="description"><![CDATA[


Le Canada a créé la surprise au premier jour de la sixième ronde de renégociation de l'ALENA, hier, en confirmant sa participation à un vaste traité de libre-échange avec 10 pays de la région Asie-Pacifique. Un pied de nez à Donald Trump, estiment certains, qui risque de teinter le reste des pourparlers prévus jusqu'à lundi à Montréal.


Le gouvernement de Justin Trudeau a annoncé hier matin s'être entendu avec le Japon, le Mexique et d'autres pays réunis au sein du Partenariat transpacifique global et progressiste (PTPGP). Pendant un discours au Forum économique de Davos, M. Trudeau a fait valoir que cet accord allait « dans le bon sens » au moment où règne « beaucoup de scepticisme sur le commerce dans le monde à l'heure actuelle ». Il a fait l'apologie du libre-échange, sans nommer Donald Trump.


Plusieurs analystes ont assimilé ses commentaires à une rebuffade envers le président américain, qui a retiré les États-Unis des discussions du PTPGP peu après son entrée en poste. Donald Trump est aussi à l'origine de la renégociation forcée de l'ALENA, qu'il a souvent qualifié de « pire accord commercial jamais conclu ».



10 200 MILLIARDS



Le PTPGP vise à éliminer plus de 95 % des tarifs douaniers entre les 11 pays signataires dès l'entrée en vigueur de l'entente. La signature d'un accord final ne devrait pas avoir lieu avant mars prochain, et la date d'une éventuelle mise en oeuvre reste inconnue. Le partenariat regroupe l'

Australie

, le Canada, le Chili, le Japon, la Malaisie, le Mexique, la Nouvelle-Zélande, le Pérou, Singapour, Brunei et le Viêtnam.


Cette zone économique de 494 millions d'habitants compte un produit intérieur brut (PIB) combiné de 10 200 milliards US, soit 13,6 % du PIB mondial. L'accord donnera aussi au Canada un accès privilégié au Japon, troisième économie en importance dans le monde. Ottawa dit s'être finalement joint au PTPGP grâce à des concessions qui garantiront la souveraineté culturelle du Canada et la protection de son secteur automobile.



APPLAUDISSEMENTS ET COLÈRE



L'annonce a été reçue de façon hautement contrastée par les différentes industries du Canada. Les producteurs agricoles du Québec, par exemple, ont salué les nouveaux débouchés que ce partenariat ouvrira en Asie pour les exportations québécoises. Ils croient aussi que cet accord permettra au Canada de ne rien céder aux Américains sur la gestion de l'offre pendant la renégociation de l'ALENA.


Les syndicats de l'automobile ont plutôt dénoncé un affaiblissement catastrophique de l'industrie canadienne. Jerry Dias, président du syndicat Unifor, qui regroupe plus de 300 000 travailleurs, croit de plus que cette annonce viendra réduire notablement le pouvoir du Canada aux tables de négociation de l'ALENA.


Le négociateur en chef du Canada pour l'ALENA, Steve Verheul, n'appréhende aucun impact négatif sur les pourparlers. « Nous ne nous attendons pas à cela », a-t-il dit en marge des négociations, à l'Hôtel Bonaventure.



«

Les États-Unis seront libres de rejoindre le PTPGP s'ils le souhaitent dans le futur. Je ne crois pas qu'ils soient très inquiets de cela. Cela n'a été soulevé d'aucune façon à la table.

» - Steve Verheul



Entouré d'une foule compacte de journalistes, Steve Verheul a aussi fait valoir que les négociations de l'ALENA et celles du PTPGP sont « séparées, avec des acteurs différents et des enjeux distincts ». « L'ALENA a une dynamique qui lui est propre. On va défendre nos positions sur la gestion de l'offre, sur l'industrie automobile, comme on l'a fait depuis le début, et le PTPGP ne fera aucune différence. »


Un point de vue partagé par Daniel Schwanen, vice-président à la recherche à l'Institut C.D. Howe et expert du libre-échange. Il estime que l'accès qu'obtiendront le Canada et le Mexique au marché japonais - qui représente 6 % du PIB mondial - viendra réduire leur dépendance envers les États-Unis.


« Ça donne plus de poids aux propositions canadiennes dans les négociations de l'ALENA, il n'y a aucun doute là-dessus », a-t-il dit à La Presse.



TRUMP POSITIF



Le président américain, dont les commentaires sur l'ALENA fluctuent au gré de ses humeurs, a adopté un ton plutôt positif hier pour décrire la sixième ronde de négociations qui se déroule à Montréal, rapporte l'AFP.


« L'ALENA avance plutôt bien, a déclaré Donald Trump depuis le Bureau ovale. Il se trouve que je pense que si cela ne fonctionne pas, nous y mettrons fin, mais je pense que cela se passe plutôt bien, nous verrons. »


Une dizaine de sujets seront abordés aujourd'hui aux tables de négociation à l'Hôtel Bonaventure, dont l'environnement et la propriété intellectuelle.


Même si le président américain décidait de se retirer de l'ALENA demain, l'accord est en place pour au moins deux ans encore, estime le négociateur en chef pour le Québec, Raymond Bachand. Le président doit donner un préavis de six mois, a-t-il expliqué hier lors d'un entretien avec La Presse. Ensuite, le Congrès doit se prononcer, et ce sera contesté. « Ça va aller devant les tribunaux et jusqu'en Cour suprême. Alors on en a pour au moins deux ans. »


Le Québec est la seule province canadienne à avoir désigné un négociateur pour participer au renouvellement de l'ALENA. Raymond Bachand n'est pas présent aux tables de négociation, mais il est tenu au courant quotidiennement de l'évolution des discussions. Il relaie ensuite cette information aux élus, mais il réserve ses commentaires publics pour la fin de la ronde, prévue lundi. « Ça ne sert à rien de commenter la partie entre deux périodes », fait-il valoir.


L'ancien ministre estime que les discussions dureront probablement plus longtemps que ce qui a été prévu, avec une dernière réunion à Washington au printemps. « Ça pourrait prendre beaucoup plus de temps », estime-t-il. Le temps joue en faveur de la renégociation, croit Raymond Bachand. Le Canada a indiqué clairement à l'automne qu'il n'était pas prêt à accepter n'importe quel accord, ce qui a donné un choc aux milieux économiques, explique-t-il.


De plus, l'opinion est en train de basculer aux États-Unis, selon lui. « Le milieu agricole américain, le territoire de Trump, est en train de se mobiliser », souligne-t-il.


- Hélène Baril, La Presse


« Dans le contexte où le dialogue sur la renégociation de l'ALENA s'est durci, il s'agit d'un excellent coup de la part du Canada, qui vient réaffirmer notre position de premier plan dans le domaine du commerce international. » -

Yves-Thomas Dorval

, président-directeur général du Conseil du patronat


« Les 10 partenaires du Canada représentent plus de 460 millions de consommateurs et un PIB combiné de 8,7 billions de dollars américains. La demande dans ces pays est particulièrement forte dans des secteurs clés de l'économie de la métropole tels que les technologies de l'information et des communications, les services financiers, le génie et les infrastructures ainsi que les technologies propres. » -

Michel Leblanc

, président et chef de la direction de la Chambre de commerce du Montréal métropolitain


« Nous nous sommes tenus debout pour nos industries culturelles et nous avons obtenu l'accord unanime des pays membres pour garantir la protection de notre culture. Nous avons réussi à conserver notre flexibilité pour appuyer à travers des programmes et des politiques la création, la distribution et le développement de l'art canadien, notamment dans l'espace numérique. » - La ministre du Patrimoine canadien

Mélanie Joly

, saluant les clauses touchant la culture incluses dans le PTPGP


« En ratifiant le PTPGP, le gouvernement canadien peut légitimement fermer la porte à toute concession additionnelle dans le cadre de la renégociation de l'ALENA. Si les États-Unis souhaitent profiter d'un accès accru au marché canadien, ils n'ont qu'à réintégrer le PTPGP. » -

Marcel Groleau

, président de l'Union des producteurs agricoles, qualifiant de « bonne nouvelle » la ratification du PTPGP, qui permettra d'exporter plus facilement des produits vers le marché japonais.


« D'un côté, le gouvernement du Canada [affirme] qu'il veut un secteur laitier en croissance, fort et dynamique qui crée de l'emploi et génère des investissements ; de l'autre côté, il continue de rogner des parts du marché canadien des produits laitiers, d'abord avec l'AECG, puis maintenant avec le PTPGP. Le gouvernement doit comprendre qu'en continuant à faire des concessions de la sorte, il met le secteur laitier canadien en péril. » -

Pierre Lampron

, président des Producteurs laitiers du Canada


- Propos recuiellis par Maxime Bergeron et Hélène Baril


La possibilité de détaxer et de dédouaner les achats en ligne, qui fait partie des négociations en cours sur le renouvellement de l'Accord de libre-échange nord-américain (ALENA), est « une recette parfaite pour un beau désastre », dit Paul-André Goulet, propriétaire de la franchise Sports Experts de la rue Sainte-Catherine à Montréal.


« Je ne suis pas contre l'ALENA, mais j'ai investi 10 millions [dans mon magasin] avec un engagement de 30 ans. Si les règles changent, ce sera fortement dommageable pour mon entreprise et pour les emplois qui en dépendent », a-t-il dit hier dans un point de presse convoqué dans son magasin.


Le Canada subit la pression des États-Unis pour augmenter de 20 $ à 800 $ le seuil d'exemption des biens achetés sur l'internet. Même si les négociateurs canadiens sont bien au fait de l'impact qu'aurait une telle augmentation, les détaillants craignent que leur cause ne pèse pas bien lourd à côté notamment de la gestion de l'offre, a expliqué Léopold Turgeon, PDG du Conseil québécois du commerce de détail (CQCD).


Au total les pertes en taxes et droits de douane pourraient atteindre de 700 millions à 1,6 milliard, un manque à gagner que le gouvernement pourrait être tenté de récupérer par des hausses de taxes.


Selon une étude réalisée par l'économiste Pierre Emmanuel Paradis pour le CQCD, 86 % des produits achetés sur l'internet ont une valeur inférieure à 200 $.


- Hélène Baril, La Presse


Cet article est paru dans La Presse (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007172.xml</field>
    <field name="pdate">2018-01-23T00:00:00Z</field>
    <field name="pubname">La Presse (site web)</field>
    <field name="pubname_short">La Presse (site web)</field>
    <field name="pubname_long">La Presse (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">International</field>
  </doc>
</add>
