<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Permis d'exercice national pour les professionnels de la santé au Canada: est-ce possible ?</field>
    <field name="author">Marco Laverdière</field>
    <field name="id">eupr_004140.xml</field>
    <field name="description"><![CDATA[


Cela signifie qu’en raison du manque de personnel spécialisé, les femmes prêtes à accoucher doivent être redirigées vers un autre hôpital qui se trouve à un peu plus d’une heure de route. Cela s’ajoute à une pénurie de médecins de famille en Outaouais.


Diverses causes peuvent être à l'origine de cette situation et il n'y a probablement pas de solution magique. Mais on peut se poser cette question: devrait-on dans un tel cas faire en sorte qu’un médecin ontarien exerçant de l’autre côté de la Rivière des Outaouais puisse venir faire un remplacement du côté québécois, sans avoir à se soumettre à diverses exigences administratives pour obtenir un permis d’exercice du Québec, avec les coûts et les délais qui peuvent en découler ?


Plusieurs médecins semblent penser que oui, si l’on en juge par un sondageréalisé cet automne par l’Association médicale canadienne auquel 7 000 de ses membres ont répondu. Ainsi, « neuf médecins sur dix appuient l’instauration d’un permis d’exercice national et les trois quarts estiment que cela améliorera l’accès aux soins de santé ».


D’ailleurs, plusieurs autres organismes canadiens dans le secteur médicalapparaissent favorables à une telle idée, en invoquant notamment la nécessité de faciliter les remplacements (locum) de médecins dans les régions mal desservies et la téléconsultation interprovinciale.


Mais est-ce que l’instauration d’un permis d’exercice national est vraiment possible dans le contexte canadien ? Car il est bien établi au plan constitutionnel que la réglementation des professions relève généralement des provinces.


Étant engagé depuis plus de vingt ans dans diverses activités académiques et professionnelles liées au droit et aux politiques de la santé, notamment la réglementation des professions, cette question m’est apparue intéressante considérant l’actualité des dernières semaines et, aussi, en fonction de certains développements jurisprudentiels récents en matière constitutionnelle.



S’inspirer de la réglementation des valeurs mobilières



Un jugement de 2018 de la Cour suprême du Canada concernant la réglementation des valeurs mobilières laisse envisager ce que pourraient être les bases juridiques d’un éventuel permis d’exercice national pour les médecins.


Bien qu’il s’agisse d’une matière de compétence provinciale, le gouvernement fédéral et les gouvernements de certaines provinces ont proposé d’instaurer un régime de coopération législative concernant les valeurs mobilières. Celui-ci inclut une loi type pouvant être adoptée par les législatures des provinces et territoires pour régir les aspects courants du commerce des valeurs mobilières.


Pour l’essentiel, et malgré les contestations de certaines provinces, la Cour suprême a indiqué qu’un tel régime respecterait les compétences provinciales, puisqu’aucune d’entre elles ne serait forcée d’y adhérer. Elle a également conclu que l’intervention du gouvernement fédéral serait justifiée, considérant ses propres compétences et les enjeux de dimension nationale en cause.


En fait, ce régime de coopération législative est très semblable à celui mis en place en

Australie

, en vue de créer un permis d’exercice national pour les médecins et les autres professionnels de la santé, suivant un accord de coopération entre le gouvernement fédéral, les états et les territoires. Avec certaines adaptations, il ne serait donc pas impensable que le Canada soit également en mesure de mettre en place un tel système.



Un « passeport professionnel »



Une autre solution, moins bien intégrée, mais peut-être aussi moins ardue d'un point de vue politique et juridique, pourrait consister à instaurer un « passeport professionnel national », en cherchant à faire évoluer les actuelles dispositions sur la mobilité de la main-d’œuvre de l’Accord de libre-échange canadien (ALEC).


Avec certaines exceptions, cet accord entre les gouvernements fédéral, provinciaux et territoriaux engage ceux-ci à mettre en place des mesures permettant aux personnes exerçant des métiers ou professions réglementés de travailler partout au Canada, sans exigence supplémentaire significative de formation, d’expérience, d’examens ou d’évaluation.


Ceci dit, l’ALEC permet toujours que certaines exigences provinciales et territoriales soient maintenues, par exemple, au plan linguistique (maîtrise suffisante du français ou de l’anglais), des frais d’inscription et d’obtention de permis, de l’assurance responsabilité professionnelle et des examens portant sur les aspects déontologiques.


L’idée d’un passeport national, reposant sur l’élimination ou la réduction de ces derniers obstacles, ainsi que sur une plus grande coordination administrative entre les autorités réglementaires provinciales, semble être favorisée par la Fédération des ordres des médecins du Canada. Celle-ci évoque même une sorte de « carte Nexus » permettant aux médecins de satisfaire rapidement aux exigences administratives des provinces et territoires pour être autorisés à y exercer. Cette approche s’apparente également à ce que les Américains cherchent à mettre en place depuis quelques années, sans y être parvenus totalement, avec l’« Interstate Medical Licensure Compact ».



Et la protection du public dans tout ça ?



Quoique complexe, l’instauration d’un permis d’exercice ou d'un passeport national pour les médecins pourrait être juridiquement réalisable, pour peu évidemment qu’il y ait une volonté politique suffisante. Ce serait bien sûr aussi le cas pour d’autres professionnels de la santé, comme les pharmaciens, infirmières, dentistes, etc.


Certains obstacles particuliers devraient toutefois être surmontés, comme au Québec par exemple, alors que la délivrance de permis à des professionnels qui n’ont pas une maîtrise suffisante du français poserait problème suivant la Charte de la langue française, à moins de miser sur des permis temporaires ou, encore, sur des autorisations spéciales.


De manière réaliste, il faut bien admettre que la collaboration fédérale-provinciale requise pour aplanir de tels obstacles n’est pas acquise si on considère les réticences qui subsistent dans le dossier des valeurs mobilières, dont celles du Québec. La province cherche traditionnellement à faire prévaloir son autonomie mais aussi, à préserver son expertise dans ce domaine. Des obstacles politiques du même ordre sont observables en ce qui concerne le projet d’instaurer un programme national d’assurance médicaments.


Par ailleurs, si on allait de l’avant avec un tel projet, il faudrait éviter d’éroder les mécanismes de protection du public qui découlent de la réglementation professionnelle, notamment en ce qui concerne l’accès aux recours auprès des ordres professionnels provinciaux ainsi qu'aux responsabilités civiles en cas de préjudice.


L’exercice d’un recours contre un professionnel soulève déjà un certain nombre de difficultés pour les patients. Il faudrait prévoir des règles afin que ceux-ci puissent toujours s’en remettre à l’autorité réglementaire locale pour formuler une plainte à l’égard d’un professionnel d’une autre province qui intervient dans le cadre d’un remplacement ou d’une téléconsultation.


Il faudrait aussi pouvoir miser sur une coopération plus étroite entre les différentes autorités réglementaires, non seulement pour l’harmonisation des normes de pratiques, mais également pour les enquêtes et les cas de discipline.


Bref, l’instauration d’un permis d’exercice national constituerait certainement un sérieux défi sur les plans juridiques et politiques. Pour assurer le succès d'une telle initiative, il faudrait la concevoir d’abord et avant afin de répondre aux besoins du public et non pas seulement pour réduire les exigences administratives applicables aux professionnels.



* * * * *




Ce texte est d'abord paru


sur le site franco-canadien


de The Conversation. Reproduit avec permission.



<img src="

https://counter.theconversation.com/content/127963/count.gif?distributor=republish-lightbox-basic

" alt="The Conversation" width="1" height="1" style="border: none !important; box-shadow: none !important; margin: 0 !important; max-height: 1px !important; max-width: 1px !important; min-height: 1px !important; min-width: 1px !important; opacity: 0 !important; outline: none !important; padding: 0 !important; text-shadow: none !important" /><


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004140.xml</field>
    <field name="pdate">2020-01-18T00:00:00Z</field>
    <field name="pubname">Le Quotidien (Saguenay, QC) (tablette)</field>
    <field name="pubname_short">Le Quotidien (Saguenay, QC) (tablette)</field>
    <field name="pubname_long">Le Quotidien (Saguenay, QC) (tablette)</field>
    <field name="other_source">Le Soleil (Québec, QC) (tablette)</field>
    <field name="source_date">18 janvier 2020</field>
    <field name="section">Santé</field>
  </doc>
</add>
