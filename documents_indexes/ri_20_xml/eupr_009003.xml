<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Avant la conférence internationale DES CÉRÉALES</field>
    <field name="author">RENÉ CERCLER.</field>
    <field name="id">eupr_009003.xml</field>
    <field name="description"><![CDATA[


La conférence internationale des céréales qui va s'ouvrir, a pour objet de rechercher, sur le plan mondial, les meilleurs moyens de répartition et de distribution de la récolte 1947-1948, et, à cette fin, son travail sera divisé en quatre commissions, l'une spécialement chargée des questions d'approvisionnement, l'autre de la collecte, la troisième des céréales secondaires, et la dernière des modalités du rationnement.


Consacrés en principe à la prochaine campagne, les débats de Paris dépasseront cependant certainement, au moins dans les conversations privées des personnalités présentes, ce cadre limité. Aussi n'est-il pas inutile, à cette occasion, d'examiner les recommandations générales faites, en matière de blé, par la commission préparatoire de la F.A.O., qui s'est réunie à Washington, du 28 octobre 1946 au 24 janvier 1947, pour étudier les propositions relatives à l'alimentation suggérées à Copenhague par Sir John Boyd Orr. Le copieux rapport de cette commission - rapport auquel nous nous référons - - vient en effet d'être récemment publié.


Il faut rappeler qu'avant guerre il existait d'une façon chronique des excédents mondiaux de blé, et tous les efforts consistaient alors à stabiliser les prix en limitant la production. Il est symptomatique de constater qu'entre 1930 et 1940 les prix payés aux producteurs nationaux de pays comme la France, l'Allemagne ou l'Italie étaient parfois le double ou le triple des prix c.a.f. du blé importé. Pendant les premières années de la guerre, par suite de la fermeture du marché européen, les stocks mondiaux des quatre grands pays exportateurs - Canada, Argentine, États-Unis et

Australie

- atteignirent des chiffres records : 47 millions de tonnes en 1943. Depuis ces stocks ont baissé jusqu'à l'exsanguité, et en dépit des excellentes récoltes américaines la pénurie est aujourd'hui mondiale. Quant aux prix, ils sont encore fort différents, même entre pays exportateurs; en 1946 par exemple le blé américain valait 2 dollars 18 le bushel, alors que le blé canadien variait entre 1 dollar 55 (pour l'Angleterre) et 2 dollars 24 (pour les autres pays), que le blé australien s'établissait à i dollar 77 et l'argentin à 3 dollars.


Dans l'état actuel des choses, la commission de Washington affirme que le premier but doit être d'augmenter à la fois la production et la consommation sans compromettre le bien-être du producteur, et ensuite d'accumuler suffisamment de réserves pour garantir le monde contre les mauvaises récoltes et le retour de la famine.


Ces stocks sont également nécessaires pour garantir la stabilité des prix. La commission est persuadée que la solution du problème mondial du blé doit être recherchée dans le sens d'un accroissement constant de la consommation plutôt que dans une diminution de la production. A son avis les principes directeurs à suivre en vue d'un accord international sur le blé sont d'abord d'obtenir une plus grande stabilité des prix du blé et d'établir un système coordonné de réserves mondiales, puis d'encourager une plus grande consommation par des programmes de nutrition et d'assurer la sécurité des producteurs.


Quant aux prix, les commissions de Washington recommandent la fixation d'un prix maximum et d'un prix minimum, ces plafonds étant obligatoires dans les transactions des parties contractantes. Toutefois elles prévoient des ventes à des prix spéciaux, inférieurs au prix minimum, lorsqu'il y aurait surproduction dûment contrôlée par le conseil du blé, blé qui devrait alors être employé à des programmes spéciaux d'alimentation définis par la F.A.O. Ces blés ne pourraient être revendus sur le marché ordinaire, et les pays qui profiteraient de ces importations à tarif réduit seraient obligés de continuer leurs importations normales, ce contingentement particulier devant être strictement réservé aux consommateurs que la F.A.O. a décidé d'avantager. L'idée est ingénieuse, sinon d'une réalisation très facile.


En ce qui concerne les stocks, les pays importateurs comme les pays exportateurs conviendraient du chiffre maximum et du chiffre minimum - des stocks à constituer par chacun d'eux, avec faculté de dépassement dans un sens ou dans l'autre en cas de surproduction ou de déficit reconnus. Chaque membre s'engagerait à conserver une partie de ses stocks comme réserve internationale contre la famine. D'autre part seraient, sur ces stocks, également prévues des réserves spéciales pour la stabilisation des prix. Lorsque les prix internationaux atteindraient le minimum dont nous avons parlé plus haut, ces réserves seraient constituées, tandis qu'elles seraient mises en vente quand les prix internationaux atteindraient le plafond. De plus les pays importateurs, dans le cas où les prix arriveraient au maximum, puiseraient dans leurs réserves de stabilisation avant de recourir à l'importation. En outre le conseil du blé serait constamment tenu au courant du montant des réserves de chaque pays afin d'avoir toujours un état de la situation et de pouvoir en conséquence faire des recommandations circonstanciées.


Au sujet du tonnage des exportations effectives le conseil du blé se réserverait le droit de modifier le programme normal lorsque au cours d'une campagne il apparaîtrait des risques de surproduction ou de déficit. En période de pénurie les exportations seraient rationnées; en période de surproduction on contingenterait les exportations; le contingent global d'exportation serait réparti entre les déférents pays exportateurs suivant un pourcentage à déterminer. Au surplus il y aurait lieu de prévoir des mesures pour les transactions avec les États non-membres; c'est ainsi que les pays exportateurs membres, en période de pénurie, ne seraient pas autorisés à vendre aux pays non-membres avant que les besoins normaux des nations importatrices membres soient satisfaits : de même, en période de surproduction, les importateurs membres devraient d'abord acheter leurs importations normales à leurs fournisseurs membres.


On voit qu'il s'agit là d'une belle conception théorique qui, sur le papier, ne manque pas d'être séduisante. Toute la question se résume toujours de savoir dans quelle mesure les États sont disposés à abandonner leur souveraineté pour souscrire et surtout pour respecter de tels engagements. Il convient, pensons-nous, d'être quelque peu sceptique.


Il faut toutefois féliciter les organisateurs de la prochaine conférence de Paris d'avoir mis à l'ordre du jour la question des céréales secondaires. Le problème mondial du blé ne saurait en effet être posé seul, et la méconnaissance de cette nécessité est sans doute une des raisons de l'échec de la conférence du blé de Londres. Si, en particulier, en pleine pénurie, on oblige aujourd'hui la France à acheter du maïs alors qu'elle préférerait du blé, il conviendrait qui l'avenir nous nous réservions le droit d'acheter des céréales secondaires à la place du blé qu'on voudra alors nous fournir. En effet, si nous sommes, à une échéance plus ou moins prochaine, capables de subvenir à nos besoins en blé - voire d'être exportateurs, - en revanche notre déficit en céréales secondaire sera beaucoup plus long, sinon chronique, pendant bien des années au moins. A l'opposé, et à l'échelle mondiale, la surproduction en céréales secondaires est plus prochaine que celle du blé, et des engagements d'importation alternative seraient, semble-t-il, à l'avantage réciproque des vendeurs comme des acheteurs.


C'est peut-être dans ce sens qu'il conviendrait d'orienter les conversations internationales afin que leurs résultats ne soient pas toujours négatifs.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_009003.xml</field>
    <field name="pdate">1947-07-07T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
