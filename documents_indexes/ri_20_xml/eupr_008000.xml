<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Pourquoi pas un porte-avions franco-européen ?</field>
    <field name="author">Cyrille Schott</field>
    <field name="id">eupr_008000.xml</field>
    <field name="description"><![CDATA[



« Le Charles-de-Gaulle aura besoin d'un successeur »,

souligna Florence Parly, la ministre des Armées, au salon Euronaval en octobre 2018, en lançant une phase d'études pour la construction d'un nouveau porte-avions, qui pourrait entrer en service vers 2030-2035. Ce successeur sera-t-il isolé ? Ou en couple, comme le furent naguère le

Clemenceau

et le

Foch

? Cette solution serait militairement préférable, permettant à la France de toujours disposer d'un bâtiment opérationnel, tandis que l'autre serait en période d'entretien ou en refonte. Elle aurait toutefois un coût élevé, celui d'un seul navire étant estimé à 4,5 milliards d'euros.


Le porte-avions offre une capacité militaire majeure à une marine. Son déploiement, lors d'une crise, représente un signal politique fort. Dans sa mission

Clemenceau

, entre mars et juillet derniers, le groupe aéronaval, constitué par le

Charles de Gaulle

avec ses bâtiments d'accompagnement, a participé à l'opération

Chammal

contre Daech au Moyen-Orient, puis a rejoint la région indo-pacifique pour une série d'exercices avec les marines de l'Inde, des États-Unis, de l'

Australie

, de la Malaisie et du Japon, et celle de l'Egypte au retour. Combat contre le terrorisme et coopération avec nos alliés ont ainsi conjugué guerre et diplomatie.


Dans sa réponse aux propositions d'Emmanuel Macron en vue d'une réforme de l'Union européenne, Annegret Kramp-Karrenbauer, alors successeure d'Angela Merkel à la présidence de l'Union chrétienne-démocrate d'Allemagne et désormais ministre fédérale de la Défense, a écrit en mars de cette année :



« Dès à présent, l'Allemagne et la France travaillent ensemble au projet d'un futur avion de combat européen... La prochaine étape pourrait consister en un projet hautement symbolique, la construction d'un porte-avions européen commun, pour souligner le rôle de l'Union européenne dans le monde en tant que puissance garante de sécurité et de paix. »



Macron et Merkel ont plusieurs fois exprimé l'ambition d'une « armée européenne. » Si l'Europe veut tenir sa place dans le concert mondial, tel qu'il se dessine avec des puissances-continent comme les États-Unis, la Chine, la Russie ou l'Inde, elle doit se doter d'une capacité militaire d'action. Des progrès indéniables sont intervenus depuis 2017, notamment grâce à la coopération structurée permanente, le fonds européen de défense, l'initiative européenne d'intervention, mais l'Union européenne reste loin de disposer d'une véritable armée.



Un porte-avions aux couleurs de l'Europe ?



L'apparition d'un porte-avions aux couleurs de l'Europe serait certes « hautement symbolique », comme l'écrit la responsable allemande, et ce navire pourrait montrer le drapeau de l'Union sur les mers du globe et aux approches des continents. Cependant, le symbole ne suffit pas dans les questions de défense. Un porte-avions est un navire de guerre et, s'il peut remplir des missions diplomatiques de présence, il faut, pour que celles-ci soient crédibles, qu'il soit aussi capable d'intervenir militairement, qu'il sache effectivement combattre.


À cet égard, la réalisation d'un porte-avions peut paraître prématurée à ce stade de l'intégration européenne. Florence Parly a, en effet répondu, sur les ondes de RMC en mai dernier qu'on

« n'en est pas encore tout à fait là »

, en évoquant les conditions d'emploi d'un tel navire. Il ne suffit pas de construire un porte-avions, encore faut-il être capable de l'employer, certes pour des missions de présence, mais également, si besoin, pour un engagement armé dans une crise ou un conflit. Or, l'on n'en est pas encore là. La brigade franco-allemande est déployée au Sahel, mais seules ses composantes françaises se battent contre les djihadistes, la partie allemande intervenant dans le cadre d'un mandat de l'Union européenne pour la formation de l'armée malienne ou dans celui de la force onusienne

Minusma.




Conjuguer les besoins de la France et ceux de l'Europe ?



Faut-il alors abandonner l'idée d'un porte-avion

s

européen ? Ne pourrait-on conjuguer les besoins de la France et ceux de l'Europe ? Un second porte-avions serait utile à la marine française. Un porte-avions européen signifierait une étape considérable dans l'affirmation militaire de l'Union, qui est en chemin. Pourquoi la France ne partagerait-elle pas un porte-avions ? Pourquoi ne pas engager la construction de deux porte-avions, le premier français, le second franco-européen. Celui-ci naviguerait sous le pavillon national, celui-là naviguerait généralement sous le pavillon européen et arborerait le pavillon français, quand l'autre serait indisponible. Le premier serait financé uniquement par la France, le second le serait à parité par la France et l'Union européenne.


Budgétairement, l'opération serait rentable : la France disposerait toujours d'un porte-avions opérationnel pour un coût probablement inférieur à celui d'un bâtiment et demi, une série de deux s'avérant à l'unité moins couteuse que la construction d'un seul ; l'Union européenne acquérait, de même, un porte-avions pour un budget inférieur sans doute à la moitié du coût d'un navire isolé.



Mettre en oeuvre un groupe aérien de qualité



Des questions sensibles seraient à résoudre, l'une des premières concernant le groupe aérien. En effet, la puissance d'un porte-avions réside dans sa capacité mettre en oeuvre un groupe aérien de qualité. La France est le seul pays européen à utiliser, comme les Etats-Unis, des catapultes. La Grande-Bretagne, l'Espagne et l'Italie déploient des porte-aéronefs avec des avions à décollage court ou vertical, aux capacités moindres. Le groupe aérien, qui réunirait des appareils capables d'apponter sur les nouveaux porte-avions, serait logiquement composé du futur avion de combat européen, dont le projet a été lancé par la France et l'Allemagne, rejointes par l'Espagne. Des évolutions devraient intervenir chez nos amis, soit, comme en Allemagne, pour reconstituer une aviation embarquée, soit, comme en Italie, pour choisir un avion européen. Des décisions significatives seraient nécessaires, mais ces pays, voire d'autres, pourraient vouloir développer une composante aéronavale moderne, dès lors que le coût budgétaire, né de la coopération européenne, serait raisonnable. Rien, techniquement, n'interdirait de créer des flottilles européennes d'avions pouvant apponter sur ces porte-avions.


Un autre sujet délicat est celui de l'équipage. Celui-ci rend opérationnel le porte-avions, qui accueille des flottilles d'avions et l'état-major du groupe aéronaval. Il doit être en phase avec son navire. Quand le bâtiment franco-européen naviguerait sous pavillon français, son équipage devrait, au besoin, pouvoir être engagé au combat, y compris avec ses membres non français.



Développer la participation, étendre le périmètre de discussion



S'agissant du groupe aéronaval, d'ores et déjà l'habitude est prise que des navires européens intègrent celui du

Charles-de-Gaulle

, y compris dans des missions d'engagement armé. Ainsi, chaque fois que ce bâtiment a été déployé pour frapper Daech, il a été accompagné par de tels navires (allemand, belge, britannique et italien). L'existence d'un porte-avions européen développerait la participation des marines européennes, en contribuant à leur excellence.


L'accord devrait intervenir également sur le système de propulsion, nucléaire ou non, et sur le partage des tâches entre les industries navales, la France étant la seule, à ce jour, à disposer des compétences pour construire des porte-avions dotés de catapultes. D'autres sujets seraient à traiter, comme le port d'attache, la formation et l'entrainement, la chaîne de commandement...


La résolution de certaines questions serait indéniablement délicate, mais possible en présence d'une vraie volonté politique. Cette volonté témoignerait d'un pas nouveau et significatif de l'Europe pour sa défense et de son rayonnement dans le monde.


____


NOTE DE L'AUTEUR



Cet article est le fruit des réflexions d'un groupe de travail constitué au sein d'EuroDéfense France et composé de l'ingénieur général de 1re classe de l'armement (2S) Patrick Bellouard, président d'EuroDéfense France, du préfet (h.) de région Cyrille Schott, animateur du groupe et ancien directeur de l'Institut national des hautes études de la sécurité et de la justice, du vice-amiral d'escadre (2S) Patrick Hébrard, ancien commandant du porte-avions Clemenceau, du vice-amiral (2S) Gilles Combarieu, de l'ancien ambassadeur Jean-Loup Kuhn Delforge, du général (2S) Patrice Mompeyssin, secrétaire général d'EuroDéfense France et du groupe de travail.



Cet article est paru dans La Tribune (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008000.xml</field>
    <field name="pdate">2019-10-25T00:00:00Z</field>
    <field name="pubname">La Tribune (site web)</field>
    <field name="pubname_short">La Tribune (site web)</field>
    <field name="pubname_long">La Tribune (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">TRIBUNES</field>
  </doc>
</add>
