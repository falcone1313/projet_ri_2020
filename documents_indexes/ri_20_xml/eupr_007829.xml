<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">LE SYSTÈME FRANÇAIS EST-IL VRAIMENT LE MEILLEUR DU MONDE?</field>
    <field name="author">CAPUCINE COQUAND</field>
    <field name="id">eupr_007829.xml</field>
    <field name="description"><![CDATA[


La réforme des retraites programmée par le gouvernement semble indispensable pour adopter notre système à l'allongement de l'espérance de vie et aux nouvelles formes de travail. Pourtant elle inquiète certains qui craignent la destruction du meilleur des modèles. Vraiment ? Notre système est le meilleur au monde », répète depuis plusieurs mois le leader de la CGT, Philippe Martinez. Vent debout depuis la publication en juillet 2019 de la série de préconisations pour transformer notre régime actuel, rédigée par le désormais ex-haut commissaire aux retraites, Jean-Paul Delevoye, le syndicaliste ne décolère pas. Et il n'est pas le seul. De nombreuses professions, inquiètes, refusent massivement le projet du gouvernement.


La France n'est pourtant pas tout à fait championne en matière de retraite. Selon le cabinet international Mercer qui, chaque année, compare les divers régimes de retraite en s'appuyant sur quarante indicateurs concrets, l'Hexagone se place en dix-huitième position sur trente-sept pays analysés. Pas de quoi pavoiser, donc, même si tout n'est pas négatif. La France fait même partie du peloton de tête sur plusieurs points: âge du départ à la retraite (la moyenne est de 65 ans pour les travailleurs des pays membres de l'OCDE, elle est de 61 ans pour les Français), montant des pensions ou encore transparence des régimes. C'est en revanche la viabilité de son système et sa capacité à rester en équilibre financier sur le long terme qui plombent le pays. Sur cette question, il occupe d'ailleurs la 28e place du classement. Une ombre au tableau que le gouvernement d'Édouard Philippe semble décidé à écarter en prenant exemple sur certains de nos voisins européens. Et plus particulièrement sur la Suède, dans laquelle Emmanuel Macron voit « une véritable source d'inspiration ».


LE MODÈLE SUÉDOIS: BAISSE GLOBALE DES PENSIONS Le chef de l'État a, en effet, salué à plusieurs reprises l'efficacité d'un modèle reposant sur un régime universel par répartition par points et par capitalisation que la Suède a instauré au début des années 2010. Concrètement, les Suédois épargnent des points tout au long de leur vie professionnelle, quels que soient leur statut ou leur activité. L'intégralité de la carrière est ainsi prise en compte dans le calcul des pensions et non simplement les quinze meilleures années. L'âge légal du départ à la retraite se situe à 65 ans, avec plusieurs dérogations possibles pour partir dès 60 ans. Si le modèle est bien classé pour sa stabilité financière et qu'il est reconnu pour sa viabilité sur le long terme, il ne fait pourtant pas tout à fait l'unanimité. D'abord parce qu'il a engendré une baisse du montant des pensions. Celles-ci correspondaient à 53, 4 % du salaire en fin de carrière en 2018 contre 60 % en 2000, selon les chiffres de l'OCDE. Ensuite parce que la valeur du point est susceptible de fluctuer en fonction de l'espérance de vie, de la conjoncture économique et de l'âge de départ à la retraite. Celle-ci a d'ailleurs baissé en 2010 (- 3 %) et en 2011 (- 4 %) avant de remonter par la suite. Enfin, les carrières hachées sont pénalisées par ce type de système. Les retraitées suédoises, dont la plupart se sont arrêtées pour élever leurs enfants, sont ainsi perdantes. Elles gagnent chaque mois en moyenne 600 euros de moins que les hommes.


ÂGE DE DÉPART: POLÉMIQUE EN ALLEMAGNE Le régime allemand serait-il plus favorable? En 2012, nos voisins d'outre-Rhin ont troqué leur système de répartition en annuités (tel qu'il existe aujourd'hui en France) pour un système par points. En moyenne, avec ce régime, les pensions s'élèvent à 58 % du salaire perçu en fin de carrière. Une proportion qui, selon la loi, sera revue à la baisse à partir de 2030. Le système crée par ailleurs des conflits sur la question de l'âge du départ à la retraite. Si celui-ci est fixé à 65 ans depuis 2012, le texte indique qu'il augmentera progressivement jusqu'à atteindre 67 ans en 2030. Sur ce point, la Banque centrale allemande a, dans le cadre de son rapport annuel publié à la fin de l'année 2019, jeté un pavé dans la mare en préconisant de repousser l'âge de départ à 69 ans et 4 mois. « L'évolution démographique future constitue une menace pour le financement des retraites ( ). L'espérance de vie continue d'augmenter et les nombreux baby-boomers partiront à la retraite au milieu des années 2020 », souligne l'organisation qui suggère, pour compenser l'envolée des coûts du système de retraites par répartition, d'augmenter les cotisations salariales et de diminuer le montant des pensions. Un sujet particulièrement sensible sur le plan politique qui provoque l'ire de certains syndicats allemands. Et le débat n'est pas près de se conclure. Car, si la loi de financement des retraites adoptée en 2018 fixe le taux de cotisation et le niveau moyen des pensions, elle ne prévoit pas la suite.


DANEMARK, PAYS-BAS,

AUSTRALIE

: LES BONS ÉLÈVES Mais alors, qui est le meilleur élève en la matière? La tête du classement est aujourd'hui occupée par le Danemark qui possède un régime public de pension minimum, un régime de pension complémentaire en fonction du revenu, un régime de pension à capitalisation intégrale, des cotisations déterminées et des régimes professionnels obligatoires. Confronté, comme tous les pays européens, au vieillissement de sa population, le Danemark a également revu à la hausse l'âge de départ à la retraite. Celui-ci sera ainsi porté à 67 ans entre 2019 et 2022. Les Pays-Bas sont, eux aussi, bien classés. Et la particularité de leur régime, outre une pension professionnelle quasi obligatoire, réside dans la méthode de calcul de l'âge de départ à la retraite, qui prend en compte l'espérance de vie. Il pourrait toutefois atteindre 67 ans et 3 mois en 2022 et grimper jusqu'à 70 ans à partir de 2066. Un sujet déjà débattu en

Australie

qui occupe actuellement la troisième position du classement. Selon le cabinet Mercer, le géant d'Océanie pourrait servir d'exemple aux pays ayant besoin de réformer leur système de retraite.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_007829.xml</field>
    <field name="pdate">2020-01-01T00:00:00Z</field>
    <field name="pubname">Décideurs Magazine</field>
    <field name="pubname_short">Décideurs Magazine</field>
    <field name="pubname_long">Décideurs Magazine</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
