<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Charbon, colonisation, déni : tout comprendre à l'</field>
    <field name="author">L'Obs,</field>
    <field name="id">eupr_006427.xml</field>
    <field name="description"><![CDATA[


BibliObs. Les mégafeux qui ravagent l'

Australie

nous fascinent. Pourquoi ?
Fabrice Argounès. Il y a dans le feu une dimension eschatologique. C'est l'une des catastrophes les plus visibles, les flammes et les panaches de fumée suscitent beaucoup plus de photos de presse que de grandes inondations ou des sécheresses. Les clichés montrant des koalas brûlés ou sauvés des flammes par des pompiers ont fait le tour du monde, relayés sur les réseaux sociaux par de nombreuses stars dont les acteurs australiens vivant à Hollywood : Hugh Jackman, Nicole Kidman, Cate Blanchett. L'

Australie

est un pays important de l'industrie du spectacle et le feu est un spectacle particulièrement grandiose. Dernier élément : contrairement à la plupart des catastrophes, ces incendies touchent un pays industrialisé, anglo-saxon et blanc. Pour le dire en caricaturant : c'est une whitastrophe, et non pas une blackastrophe. Or, les catastrophes touchent plus notre conscience globale lorsqu'elles affectent un pays auxquels s'identifient les autres pays les plus riches. Ces incendies semblent d'une échelle sans précédent... L'

Australie

a connu de grands incendies. En 1851, un mégafeu a détruit cinq millions d'hectares. Plus récemment, en 1983, le « Mercredi des Cendres » a fait 75 morts et, en 2009, dans l'Etat de Victoria, on a compté 230 personnes tuées par les flammes. Cette année, le bilan total pourrait monter à plus de dix millions d'hectares, chiffre à rapporter aux 700 millions de la superficie du pays. On est donc à plus de 1 % du territoire parti en fumée. Si on le rapporte à la superficie des forêts australiennes, la proportion grimpe significativement. Quant à la faune, on parle d'un milliard d'animaux tués en Nouvelle-Galles du Sud, sans compter les batraciens et les insectes.
Mais la principale nouveauté, c'est que les flammes se voient depuis Sydney. Les gens portent des masques, leur vie quotidienne est affectée. Le feu frappe le coeur du pays : les deux états les plus affectés sont la Nouvelle-Galles du Sud et le Victoria, qui représentent les trois cinquièmes de la population. Explications « alternatives » On est tenté de relier les feux au réchauffement climatique, même s'il est très difficile d'établir des causalités certaines. De quels éléments dispose-t-on ? L'histoire longue de l'environnement australien nous montre que le pays est fragile, en particulier depuis sa très forte anthropisation par la politique impériale britannique et l'exploitation coloniale des terres. Dans les deux dernières décennies, les dégradations faites à l'environnement se sont accentuées. Comme vous le savez, le Premier ministre conservateur Scott Morrison et ses soutiens récusent cette explication. Ils ont longtemps affirmé que les départs de feux sont des actes volontaires et qu'ils ont été aggravés par le mauvais entretien des forêts, lui-même provoqué par le refus des écologistes d'exploiter le bois... Alan Jones, ancien sélectionneur de l'équipe australienne de rugby et figure locale de la droite réactionnaire, s'est fait une spécialité de ce genre d'explications « alternatives ». Et de nombreux Australiens touchés directement disent la même chose. A quoi on peut répondre en rappelant le contexte climatique du pays. D'abord, la très nette élévation de la température moyenne, notamment durant l'été austral, depuis vingt ans, avec une accélération pendant le mois de décembre 2019, au cours duquel la moyenne maximale a été battue à trois reprises. Ces chaleurs amènent des sécheresses tout aussi exceptionnelles, provoquant un important stress hydrique - dans certaines villes, des mesures de restrictions de l'usage de l'eau sont régulières. Il y a enfin des vents de plus en plus violents, qui gênent considérablement le travail des pompiers. On voit mal comment ces trois facteurs pourraient ne pas avoir favorisé les incendies... Cette fascination n'est-elle pas aussi politique ? L'

Australie

est connue pour ses positions climatosceptiques et voilà qu'elle semble subir les effets du réchauffement climatique. Il y aurait un côté « punition divine » pour reprendre le vocabulaire de l'évangéliste Scott Morrison... Depuis qu'il est devenu Premier ministre, en 2018, l'

Australie

a refusé d'appliquer les accords de Paris et freine les négociations internationales. De façon plus structurelle, le pays occupe le rang peu enviable de plus gros émetteur de CO2 par habitant de l'OCDE, juste devant le Canada et les Etats-Unis. L'

Australie

est au même niveau que Bahreïn et l'Arabie Saoudite. A la manière d'une monarchie pétrolière, son industrie est presque entièrement tournée vers l'extraction de matières premières polluantes. Charbon, fer, gaz, or et aluminium sont les principales exportations du pays, qui représentent plus de 200 milliards de dollars australiens. Les réserves sont immenses et le gouvernement a validé, il y a quelques mois ,un immense projet de mine à ciel ouvert de Carmichael, dans le Queensland. A distance de la société civile, les conglomérats miniers, comme Rio Tinto ou BHP sont néanmoins des pourvoyeurs de fonds pour les grands partis, avec le soutien indéfectible de Rupert Murdoch, le magnat de la presse - même si, pour ne pas perdre les lecteurs urbains, ses journaux ne sont pas toujours aussi radicalement anti-écologistes que leur propriétaire. « Terra nullius » L'

Australie

semble n'avoir aucun scrupule dans son rapport à la nature. D'où cela vient-il ? Il existe effectivement un rapport de prédation et de destruction qui s'explique par la matrice coloniale. Lorsque le premier navire anglais touche les côtes en 1788, les bagnards qui débarquent font face à un pays hors-norme, immense, loin de tout. Les aborigènes qui vivaient dans les forêts sont vite repoussés vers les zones semi-désertiques et l'exploitation de la terre commence aussitôt. Sur un plan juridique, l'appropriation se fait par une déclaration qui proclame que l'

Australie

est « terra nullius » : une terre qui n'appartient à personne. On peut comparer avec la Nouvelle-Zélande, où le traité de Waitangi signé en 1840 par les colons et les Maoris établit ces derniers comme des partenaires légitimes, si ce n'est des égaux. Les Maoris représentent aujourd'hui 16% de la population néozélandaise, contre 2% pour les aborigènes en

Australie

. Et c'est peut-être aussi pour cela la Nouvelle-Zélande est aujourd'hui en pointe dans la lutte écologique, et pas l'

Australie

... C'est également dû à la structure économique du pays. Les exportations néo-zélandaises, elles aussi centrales pour l'économie du pays, ne sont pas autant liées au secteur minier. Plus largement, la colonisation de l'

Australie

est portée par le mythe des grands espaces que l'on doit dompter, mais aussi par l'obsession de conserver des liens privilégiés avec des puissances lointaines. C'est d'abord la Grande-Bretagne, vers laquelle l'

Australie

va exporter du blé et des animaux d'élevage. WikiCommons Au début du XXe siècle, les Australiens sont parmi les habitants les plus riches du monde, mais déjà l'impact environnemental est énorme : la terre s'épuise, la faune locale disparaît au profit de la faune importée (lapins, moutons, dromadaires). A partir des années 1960, alors que la Grande-Bretagne se met à acheter son blé en Europe, l'

Australie

se tourne le Japon, puis la Chine. Aujourd'hui, le Japon, la Chine, l'Inde et la Corée représentent 60% de ses exportations. L'extraordinaire croissance australienne repose désormais sur les locomotives asiatiques particulièrement énergivores, faisant bondir la demande mondiale de charbon de 65% depuis une vingtaine d'années. Ce rapport à la nature est-il en train de changer ? La question environnementale commence-t-elle à exister dans le débat politique ? Elle est bien présente, mais parfois sous la forme d'un clivage entre les zones urbaines côtières et l'intérieur du pays. Dans les villes, il existe une solide tradition écologique et contestataire. Il y a eu un parti écologiste dès les années 1970, le groupe de rock « Midnight Oil » a obtenu des succès mondiaux grâce à ses chansons pour la défense de la nature et, en septembre dernier, la grande manifestation organisée par Extinction Rebellion a réuni plus de 300 000 personnes, ce qui est considérable. L'

Australie

a même été en pointe sur la protection de l'Antarctique et des baleines. A l'inverse, dans le « bush », les mineurs et les agriculteurs sont unis contre les écologistes. Mais cette ligne de front évolue. Les agriculteurs commencent à bouger, car la sécheresse a des effets ravageurs : la production de blé a chuté de 30% et, pour la première fois, l'

Australie

doit en importer. On peut également observer que les entreprises de service - banques, assurance, consulting - coupent leur lien avec l'industrie minière, sous la pression de leurs clients urbains. Premier putsch anti-écolo Et qu'en est-il de la politique gouvernementale ? De 1996 à 2007, le pays a été dirigé par le conservateur John Howard, grand défenseur de l'industrie minière, et qui avait refusé de ratifier le protocole de Kyoto. Le gouvernement travailliste qui lui a succédé n'a fait qu'un peu mieux, malgré la présence du chanteur de « Midnight Oil », Peter Garrett, comme ministre de l'Environnement. Mais en 2013, les conservateurs sont revenus aux affaires, d'abord avec Tony Abbott comme Premier ministre, puis Malcolm Turnbull, qui sans être un grand écologiste, a tout de même tenté d'inscrire dans la loi les engagements pris par l'

Australie

lors des accords de Paris - une réduction de 26% des émissions de gaz à effet de serre par rapport à leur niveau de 2005 d'ici 2030. Opposé à cette mesure, Scott Morrison a renversé Turnbull lors d'un vote interne au Parti libéral et est devenu Premier ministre. Cela s'est passé début 2018 et l'on peut dire qu'il s'agit du premier putsch anti-écolo (certes interne à un parti) de l'histoire politique mondiale. En août 2019, contre toute attente, Scott Morrison est réélu, en poursuivant sa campagne contre les accords de Paris.
Morrison apparaît comme un « bad guy », d'une droite décomplexé qu'il est facile de détester. Il a d'abord été le Hortefeux local : en 2013, il est ministre de l'Immigration et de la protection des frontières et mène l'opération « sovereign frontiers ». Les migrants tentant de gagner l'

Australie

ne sont plus conduits dans des camps sur les îles, mais directement renvoyés en Asie. En 2016, il devient ministre des Finances et s'oppose au mariage homosexuel défendu par son gouvernement. Enfin, en 2019, il a fait campagne contre les impôts et l'état providence - qui, en

Australie

, n'est déjà pas très élevé. Si l'on y ajoute qu'il est chrétien évangélique pentecôtiste et en parle à chaque discours, il coche toutes les cases pour être un bon repoussoir de la gauche australienne. James Ross/AP/SIPA Australian Prime Minister Scott Morrison, center, tours the Wildflower farm owned by Paul and Melissa Churchman in Sarsfield, Victoria, Friday, Jan. 3, 2020. Navy ships plucked hundreds of people from beaches and tens of thousands were urged to flee Friday before hot weather and strong winds in the forecast worsen Australia's already-devastating wildfires. (James Ross/Pool Photo via AP)/TKMY305/20003259589763/POOL PHOTO/2001030830 Et c'est peut-être pour cela qu'il a été élu... Est-ce un tribun démagogue, façon Trump ou Johnson ? Non, car il n'a que peu de charisme, et une empathie proche de zéro. Depuis son élection, il a multiplié les bourdes, avec ses vacances à Hawaï alors la forêt brûle, son refus d'aider les pompiers bénévoles - dont certains, chômeurs, perdaient leurs indemnités quand ils allaient combattre le feu - et reste apathique face aux Etats qui déclarent l'état de catastrophe naturelle, puisque nous sommes dans un pays fédéral. Du reste, l'opinion commence à se retourner. Il a perdu six à sept points dans les sondages et la presse australienne estime qu'il pourrait être victime à son tour d'un putsch interne, en faveur d'un politicien plus « libéral écolo », ou tout simplement moins éclaboussé. L'

Australie

peut-elle changer son modèle écologique ? Peut-elle réduire ses émissions de CO2 ? L'avantage comparatif de l'

Australie

se situe dans sa production intensive d'énergie et c'est un élément déterminant de la bonne santé de l'économie et de la société australienne, dont les villes occupent les premières places du classement des plus agréables au monde. De plus, le pays s'est construit sur ce rapport singulier à la nature et à l'espace et l'organisation territoriale qui en découle rend très compliquée la transition écologique. Vous ne pouvez pas dire à un habitant de Perth qui va à Sydney, sa capitale, de ne pas prendre l'avion : c'est à 3000 kilomètres, il n'a pas le choix. Dans le bush, les médecins se déplacent en petit avion. Partout, il est écrit au bord des routes : « Faites le plein » pour affronter les très longs trajets. On ne peut pas dire à tout un pays qu'ils sont les perdants de notre nouvelle ère de l'Anthropocène et qu'ils doivent changer tout leur rapport à leur environnement sans prendre en compte les spécificités de leur territoire. Les travaillistes travaillent à repenser l'

Australie

autrement, mais leur défaite surprise aux dernières élections les a échaudés. Fabrice Argounès, bio express Géographe et politiste, Fabrice Argounès enseigne à l'université de Rouen-Normandie INSPE. Il a publié « l'

Australie

et le monde » (2015). Il publie ce printemps « Premiers Méridiens. Mesurer, Partager, Dominer le monde (Ier-XXIe) » (Presse Universitaire de Provence) et fait partie des coordonnateurs du « Dictionnaire critique de l'Anthropocène » (CNRS Éditions, 2020).


Cet article est paru dans L'Obs (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006427.xml</field>
    <field name="pdate">2020-01-17T00:00:00Z</field>
    <field name="pubname">L'Obs (site web)</field>
    <field name="pubname_short">L'Obs (site web)</field>
    <field name="pubname_long">L'Obs (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Idées</field>
  </doc>
</add>
