<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Les écologistes bousculent la campagne des municipales, et pourraient gagner des grandes villes</field>
    <field name="author">Abel Mestre et Sylvia Zappi</field>
    <field name="id">eupr_004128.xml</field>
    <field name="description"><![CDATA[


C’est un rôle nouveau pour Europe Ecologie-Les Verts (EELV) : celui de favori. A deux mois des élections municipales, le parti écologiste est en position de ravir plusieurs villes de toute première importance : Rouen, Montpellier, Bordeaux, Besançon ou encore Nîmes, Perpignan, voire Lyon… De nombreux sondages placent les candidats EELV en situation de gagner ou de devenir la première force d’opposition. Une vague verte pressentie qui, depuis quelques semaines, indique que les candidats écologistes défient les partis traditionnels et sont en passe de devenir une troisième voie possible dans le pays au duel entre La République en marche (LRM) et le Rassemblement national (RN, ex-FN).


Si Grenoble avait été la seule métropole remportée par un maire EELV, Eric Piolle, lors des municipales de 2014, les écolos apparaissent désormais comme une alternative crédible pour gérer d’autres grandes villes. Les canicules et incendies à répétition à l’échelle mondiale, comme actuellement en

Australie

, les prévisions du Groupe d’experts intergouvernemental sur l’évolution du climat (GIEC) sonnant l’alarme de l’urgence climatique, la mobilisation massive des jeunes lors des marches climat : autant de signaux qui actent une prise de conscience durable. En France, celle-ci pourrait se traduire par un vote tendance ou un vote refuge en faveur des écologistes de la part d’électeurs de plus en plus déboussolés et dans un climat continu de dégagisme contre les autres forces politiques.


Alors, il leur faut changer de peau. Abandonner celle, confortable, de supplétifs du Parti socialiste. La mue écologiste a commencé dès le soir des résultats des élections européennes en mai 2019. Arrivée en troisième position (derrière le RN et LRM) avec 13,5 % des suffrages, la liste EELV emmenée par Yannick Jadot rebattait déjà les cartes à gauche, en distançant largement le PS et La France insoumise (LFI) qui recueillaient tous deux à peine 6,5 % des voix.



« Si vous voulez l’écologie, votez pour les écologistes »



Cette fois-ci, il s’agit aux municipales de confirmer que le leadership à gauche a pris la couleur verte. « Le changement se fait au niveau local. Il y a une vision qui préserve l’avenir, et une autre, obsolète, qui prône le développement tentaculaire, a estimé Julien Bayou, le nouveau secrétaire national d’EELV, lors de ses vœux, mercredi 15 janvier. Si vous voulez l’écologie, votez pour les écologistes. Il est temps de porter une alternative forte. Les gens ne veulent plus du macronisme et de sa politique brutale. »


Forts de leur nouveau poids politique, les candidats EELV affichent donc, non seulement de grandes ambitions, mais surtout une nouvelle méthode. Ils sont toujours pour le rassemblement de la gauche et des écologistes… à condition qu’il se fasse derrière eux. A de rares exceptions près (comme à Saint-Denis, en Seine-Saint-Denis, où les Verts soutiennent le maire communiste), ce sont les écolos qui mènent les listes. Parfois avec le Parti socialiste (à Perpignan), avec les communistes (à Rouen), ou avec toute la gauche (à Besançon). Et s’il leur arrive aussi de faire liste commune avec les « insoumis », c’est en règle générale dans le cadre de rassemblements « citoyens ».


Les leaders d’EELV l’ont bien compris, et en tout premier lieu Yannick Jadot. Le député européen, qui n’est pas encore officiellement candidat à l’élection présidentielle, pressent que le scrutin de mars aura valeur de test. Et qu’il pourrait se servir des villes conquises comme rampe de lancement pour sa candidature en 2022. Une « écologie par en bas », en somme, avec des villes laboratoires, comme à Grenoble, où Eric Piolle a de bonnes chances d’être réélu. En outre, faire tomber une « grande ville », voire plusieurs, acterait définitivement une nouvelle réalité : la recomposition politique autour de l’écologie.


La dynamique d’EELV est aussi due à des circonstances exceptionnelles pour la formation : « Si on additionne l’urgence climatique, le déclin irréversible du PS, la déception vis-à-vis de La France insoumise et la droitisation d’Emmanuel Macron, on a l’explication du vote vert », résume Jean-Yves Dormagen, professeur de sciences politiques et directeur de campagne de Clothilde Ollier, candidate EELV donnée favorite à Montpellier.



Vide à gauche



Il est vrai que les Verts profitent à plein du vide à gauche. La France insoumise, durablement affaiblie par son mauvais score aux européennes et par de multiples crises internes, a décidé d’enjamber les municipales et de privilégier les « initiatives citoyennes ». Dans certaines villes, comme à Perpignan ou à Montpellier, LFI se divise carrément : dans ces deux cas, les proches de François Ruffin soutiennent les candidates EELV quand les autres « insoumis » sont sur des listes « citoyennes ».


Le PS quant à lui, est condamné à voir s’éloigner son ancien partenaire privilégié. Les socialistes ont espéré tenter de les convaincre de s’allier. En vain. « On est dans un contexte d’éparpillement dans de nombreuses grandes villes au premier tour et la cartographie politique en est brouillée. C’est parfois incompréhensible quand on est sortant et qu’on a géré ensemble jusqu’alors ou face à la droite », regrette Sébastien Vincini, secrétaire national du PS aux territoires.


Les socialistes feignent de ne pas s’alarmer. Ils se disent certains de conserver de grandes villes et espèrent même gagner Nancy et d’autres villes moyennes. « Rappelons-nous que les écologistes assuraient avant l’été pouvoir gagner Paris, Nantes, Rennes ou Toulouse. On en est loin », remarque Olivier Faure. Le premier secrétaire du PS souligne que ses troupes n’ont pas hésité à se ranger derrière les écolos pour que la gauche fasse de nouvelles prises, citant les exemples de Besançon ou de Bordeaux. Et regrette que l’alliance n’ait pu se faire à Lyon ou à Marseille, laissant un autre duel se jouer.


Aux yeux du patron des socialistes, la compétition est préjudiciable pour la suite. Il a toujours en tête qu’à l’horizon de la présidentielle, les forces de gauche et écologistes puissent s’unir dans une grande confédération progressiste. « Nous devons faire apparaître qu’un troisième bloc est capable de damer le pion au duel Macron-Le Pen. La démonstration que l’alliance de la gauche et des écologistes peut être gagnante est possible », insiste le député de Seine-et-Marne. Les écologistes eux aussi en sont persuadés, mais avec un autre schéma en tête : eux devant, et non la « vieille gauche ».


Cet article est paru dans Le Monde (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004128.xml</field>
    <field name="pdate">2020-01-18T00:00:00Z</field>
    <field name="pubname">Le Monde (site web)</field>
    <field name="pubname_short">Le Monde (site web)</field>
    <field name="pubname_long">Le Monde (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">politique</field>
  </doc>
</add>
