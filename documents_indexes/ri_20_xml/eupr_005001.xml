<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Grasse fleurit de nouveau</field>
    <field name="author">-Propos recueillis parCourrier international</field>
    <field name="id">eupr_005001.xml</field>
    <field name="description"><![CDATA[


-The Daily Telegraph(extraits) Londres


Maurin Pisani, 34 ans, cueille les dernières fleurs de jasmin de l'année, puis les porte à son nez pour humer leur parfum délicat. Derrière lui : les Alpes-Maritimes; au-dessous : les collines de Grasse ondulant jusqu'à la mer.

"Leur parfum change tout le temps car elles sont vivantes. Elles sentent parfois la mangue, l'abricot et la banane, parfois les amandes et la noix de coco, et quand elles commencent à se flétrir, elles dégagent l'odeur animale de l'indole",

explique-t-il.

"Tu veux dire celle du pipi de chat !"

corrige en riant sa compagne, Anne Caluzio, 32 ans.


Ce couple est la preuve vivante que le

Flower Power

("pouvoir des fleurs") est de retour dans cette ville de la Côte d'Azur, renommée pour ses parfums. Le jeune couple s'est lancé en juin dernier dans la plantation de jasmin

grandiflo-rum,

un ingrédient de base des meilleurs parfums français tels que

J'adore-L'Or

ou

Chanel N 5

. C'est un travail éreintant car les minuscules fleurs doivent être récoltées une à une à la main durant trois mois. Mais si tout marche comme prévu, ils devraient devenir d'ici quatre ou cinq ans les deuxièmes plus gros producteurs en France de cette fleur très convoitée.


Après des dizaines d'années de marasme, le secteur du parfum renaît aujourd'hui à Grasse. La reconnaissance [en novembre 2018] par l'Unesco des savoir-faire liés au parfum en pays de Grasse en tant que trésor du patrimoine culturel immatériel de l'humanité a marqué une sorte d'apogée. La relance de la production locale de roses, jasmins et tubéreuses très renommés a joué un rôle central dans cette résurrection.


Grasse est connue comme la capitale mondiale du parfum depuis le xvi siècle. À l'époque, les substances parfumées étaient utilisées pour cacher l'odeur désagréable dégagée par l'industrie de tannage alors ezplein essor, et pour confectionner des gants embaumés pour le roi. Même si la floriculture a continué depuis à vivre de sa renommée, la mondialisation et l'émergence de modes de production moins chers dans des pays comme l'Inde ou la Tunisie lui ont porté un rude coup dans la région ces dernières décennies. Par ailleurs, l'envolée des prix du foncier sur la Côte d'Azur a poussé de nombreux agriculteurs à vendre leurs terres. Ainsi, dans les champs alentour, il ne reste plus aujourd'hui qu'une quarantaine d'hectares de plantations de fleurs sur les 2 000 cultivés au début du xx siècle. Mais un petit groupe de cultivateurs obstinés a refusé de


s'avouer vaincu. Parmi eux se trouvent Armelle Janody, une ancienne professeure de français de 49 ans, et son compagnon, ingénieur du son, Rémy Foltête, 58 ans, qui, avec leurs enfants, ont tout abandonné il y a sept ans pour devenir cultivateurs de roses.

"Les anciens ont vraiment souffert du déclin de cette activité. Un jour, mon voisin a détruit ses champs de fleurs tellement il était en colère de ne pas réussir à trouver de cueilleurs",

raconte Armelle, en taillant ses rosiers.

"C'est pour ça que, lorsque je suis arrivée, il m'a demandé pour qui je me prenais. Tout le monde ici avait parié que nous ne tiendrions pas plus de deux ans",

se souvient-elle. Mais les membres de son association Les fleurs d'exception du pays de Grasse, qui représente les cultivateurs locaux, ont réussi à inverser la tendance en changeant de modèle économique. Jadis à la merci des industriels locaux, qui transformaient leurs fleurs pour en extraire le parfum, et confrontés à une chute de la demande à la suite de l'explosion des arômes de synthèse, les producteurs ont décidé de se tourner vers les plus grands parfumeurs français.

"Cela a marché parce que le retour aux matières premières naturelles est dans l'air du temps",

explique la cultivatrice.


Au patrimoine de l'Unesco.Aujourd'hui, Armelle Janody et la plupart des producteurs de fleurs ont signé des accords d'exclusivité avec des géants du luxe comme Chanel, ou plus récemment LVMH, qui possède Dior et Louis Vuitton et qui a racheté un château dans la région en 2016 [le château de la Colle noire, ancienne propriété de Christian Dior, racheté en 2013 et dont la rénovation s'est achevée en 2016].


Les parfumeurs locaux indépendants se plaignent en revanche d'avoir été purement et simplement exclus du marché du fait des petites quantités produites.

"Grasse doit veiller à ne pas laisser les plus puissants continuer à contrôler la production florale, même si c'est grâce à eux que des nouveaux producteurs ont pu se lancer",

estime Jessica Buchanan, une parfumeuse canadienne installée dans le centre de Grasse. Cette situation pourrait toutefois changer car, face à la hausse de la demande, les pouvoirs publics locaux ont décidé en novembre de réserver 70 hectares supplémentaires de terrains à la culture des fleurs. C'est ainsi que le domaine de Maurin Pisani a été sauvé des griffes d'un promoteur immobilier.


Cette initiative a bénéficié du soutien du sénateur et ancien maire Jean-Pierre Leleux, qui a également été le fer de lance de la candidature de Grasse à l'Unesco. Pour lui, le classement à l'Unesco devrait permettre à Grasse de mieux préserver son savoir-faire, qui

"pourrait être perdu si on ne trouve pas des moyens de le transmettre aux générations futures".

Il n'y a plus que quelques personnes dans la région qui savent encore greffer les roses de Grasse et un seul souffleur de verre capable de fabriquer les anciens flacons.


L'Unesco a reconnu [en novembre 2018] ce que Jean-Pierre Leleux appelle la

"sainte trinité"

de Grasse, à savoir non seulement la culture des plantes à parfum, mais aussi la connaissance et la transformation des matières premières naturelles, et l'art de composer le parfum. Certaines entreprises de la ville sont les premières du monde en ce qui concerne l'extraction des parfums de matières premières en utilisant différentes méthodes et un savoir-faire séculaire. L'une de ces entreprises, la société Robertet, vient de réintroduire l'ancienne technique de l'enfleurage, qui consiste à piquer des fleurs sur de la graisse pour absorber leur odeur. Grasse est également célèbre pour ses nez, parmi lesquels Fabrice Pellegrin, 50 ans, un compositeur de senteurs dont le père était parfumeur et la mère cueilleuse de jasmin. Là où des nez non entraînés ne discerneraient qu'environ 15 odeurs, il est capable d'en trouver 4 000.


La prospérité de Grasse a longtemps reposé sur des secrets jalousement gardés en matière de parfums, transmis au sein de petites entités familiales, explique-t-il :

"Par exemple, j'ai essayé pendant des mois de reproduire l'odeur de la fleur fraîche de jasmin de Grasse avant que mon père me dise :

'C'est bien, mais si tu veux que l'odeur soit naturelle, ajoute du basilic !'

J'ai appliqué son conseil et il avait raison..."

Inquiet à l'idée de voir certains aînés emporter leurs secrets avec eux dans leur tombe, il a oeuvré pour l'ouverture d'une école supérieure du parfum à Grasse, dont l'enseignement porte aussi bien sur l'olfaction que sur les conditions d'approvisionnement ou le marketing.


Il n'y a pas de meilleur endroit que Grasse pour apprendre le métier selon lui :

"Nous avons la chance d'être coincés entre les montagnes et la mer avec un microclimat unique, idéal pour les fleurs à parfum."

Celles-ci ont beau être parfois des dizaines de fois plus chères que leurs concurrentes étrangères, elles sont uniques en leur genre, souligne-t-il.


De retour dans leurs champs, Maurin Pisani et Anne Caluzio commencent à étendre des voiles blancs sur leurs arbustes pour éviter que leur précieux jasmin ne gèle en hiver.

"Nous sommes arrivés à un moment où tout repart, et nous sommes conscients de notre chance",

observe Maurin.

"Il reste encore des terrains à saisir, et il y a de plus en plus de gens intéressés par la culture des fleurs à parfum,

explique Anne,

c'est juste le début !"-Henry Samuel

Publié le 30 décembre 2018


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_005001.xml</field>
    <field name="pdate">2019-07-25T00:00:00Z</field>
    <field name="pubname">Courrier international, no. 1499</field>
    <field name="pubname_short">Courrier international, no. 1499</field>
    <field name="pubname_long">Courrier international, no. 1499</field>
    <field name="other_source">Courrier International (site web)</field>
    <field name="source_date">13 août 2019</field>
    <field name="section">n pleIne RenAIssAnCe</field>
  </doc>
</add>
