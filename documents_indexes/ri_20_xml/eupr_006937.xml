<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">La Côte d'Azur, à l'ombre des mimosas en fleur</field>
    <field name="author">Paula Boyer, envoyée spéciale à Bormes-les-Mimosas (Var)</field>
    <field name="id">eupr_006937.xml</field>
    <field name="description"><![CDATA[


De janvier à mars, la côte méditerranéenne se pare de l'or des fleurs de cet arbuste originaire d'

Australie

. La douceur de la fin de l'hiver se prête bien à la découverte de la route dédiée au mimosa, de Bormes-les-Mimosas à Grasse.


En cette fin janvier, il fait doux. Partout, dans les bourgs, sur le bord des routes, dans les collines entre Bormes-les-Mimosas et Grasse et même au-delà, les mimosas changent de couleur. Sur les hauts de Bormes, dans les restanques abritées du vent où Valérie et Vincent Torres ont installé leur exploitation il y a une vingtaine d'années, les arbustes commencent aussi à se couvrir d'or. Les bourdons prennent d'assaut les glomérules (c'est le nom savant donné aux pompons jaunes des mimosas). La mer danse en contrebas.


Chez les Torres, le « coup de feu » a commencé. S'il faut, toute l'année, entretenir les arbres, les tailler, veiller à ce qu'ils n'aient pas soif l'été (d'où l'installation d'un goutte-à-goutte), c'est l'hiver que se joue l'essentiel?: il faut alors couper les tiges fleuries au sécateur, les effeuiller, les mettre en bouquet puis les livrer à des fleuristes ou les expédier par la poste aux nombreux particuliers qui passent commande. D'énormes bouquets à 25 €, port compris, pesant 1,5 à 2 kg de fleurs, puisque le mimosa est une fleur - la seule - qui se vend au poids.


En balade, dans le Tanneron


Mirandole, Bon Accueil et Les Gaulois Astier


« C'est un métier très physique », glisse Valérie Torres. Ce rythme infernal dure de fin décembre à début mars même si, pour étaler la charge de travail, les Torres ont pris soin de planter trois variétés?: ainsi, après les précoces Mirandole, viennent les mimosas Bon Accueil. Les Gaulois Astier ferment le bal.


Ce sont des cultivars, comme ceux dont la maison Cavatore, également à Bormes, s'est fait une spécialité, au côté des espèces botaniques. Ce pépiniériste possède la collection nationale de mimosa (200 espèces et variétés). « À l'origine, le mimosa vient d'

Australie

. À la fin du XVIIIe siècle, des Anglais l'ont rapporté d'abord chez eux, notamment au jardin botanique de Kew, puis, transplanté sur la Côte d'Azur, lorsqu'ils ont pris l'habitude d'y venir en villégiature l'hiver et d'y développer de somptueux jardins », raconte Julien Cavatore qui a repris en 2012 l'affaire créée par ses parents en 1981.


Le mimosa ne s'est cantonné ni aux jardins privés, ni à quelques domaines d'exception comme Le Rayol qui, juché sur la corniche des Maures, en face des îles d'Hyères, en possède une superbe collection. Ou comme le parc Gonzalez de Bormes dont la collection plus modeste, n'en est pas moins intéressante.


Valérie Torres, mimosiste installée sur les hauteurs de Bormes. / Paula Boyer


Dans le Tanneron, le mimosa ne cesse de progresser


Une fois installé sur la côte méditerranéenne, cet arbuste aux racines traçantes et aux graines baladeuses, a colonisé les collines dès que le sol s'y prêtait (le mimosa ne supporte pas le calcaire, sauf s'il est greffé). Dans l'Esterel, sa propagation semble contenue, mais dans le massif du Tanneron, où vit, à l'état sauvage, l'espèce Acacia dealbata, il ne cesse de progresser.


Les variétés cultivés chez les mimosistes (c'est le nom des horticulteurs spécialisés) ont des glomérules plus grosses, des feuilles plus vertes, bref tout ce qui a fait le succès de cette fleur en «?bouquetterie?». Cependant, même si 8 millions de bouquets sont encore envoyés dans le monde entier de janvier à mars, l'âge d'or du mimosa est passé.


Depuis une vingtaine d'années, cette fleur est moins à la mode, le MIN (marché d'intérêt national) de Nice a connu des déboires. Surtout, la frénésie des prix de l'immobilier rend impossible l'installation de jeunes, sauf s'ils succèdent à leurs parents, comme Emilie Oggero. Cette jeune femme qui fournit des grossistes aux Pays-Bas et en Italie, est installée au Capitou, le quartier des mimosistes à Mandelieu-la Napoule. Naguère, des dizaines de producteurs y vivaient. Il en reste... quatre. « Nous sommes très tributaires de la météo, dit-elle. C'est un métier très saisonnier, très physique, impossible à mécaniser. C'est beaucoup de travail pour un revenu pas formidable. »


Du parfum et des chocolats au Mimosa


Le mimosa n'en reste pas moins emblématique de la côte méditerranéenne, de Bormes à Grasse, où les parfumeurs l'utilisent dans leurs compositions, les chocolatiers - comme La Muscadine à Sainte-Maxime - dans leurs préparations et les traiteurs dans leurs plats, à l'image d'Yves Terrillon dont l'atelier est joliment baptisé « La cuisine des fleurs ».


Paula Boyer


Bormes et Grasse sont réunies par 130 km d'une route sinueuse mais extraordinairement parfumée et pittoresque, désormais estampillée « route du mimosa » par huit communes (quatre dans le Var, quatre dans les Alpes-Maritimes) soucieuses d'attirer des touristes « hors saison ». C'est avec délice que l'on succombe à l'invite. La Côte d'Azur, pénible à l'été à cause de la chaleur et des embouteillages, est si agréable dans la douceur hivernale. Et, ce qui ne gâte rien, on peut se garer presque partout et gratuitement.


Au départ, la découverte de Bormes plonge dans le ravissement. Au IVe siècle av. J.-C., les Grecs avaient installé, là, un comptoir pour commercer avec une tribu ligure, les Bormani. Après avoir fui pour échapper aux Sarrasins, les habitants sont revenus s'installer sur la colline. En février, ses rues tortueuses, ses passages couverts, ses maisons de pierre, ses placettes, et ses deux églises dont l'une dédiée à saint François de Paule qui a délivré le bourg de la peste en 1480, baignent littéralement dans l'or des mimosas.


Une histoire riche


Quarante-quatre kilomètres plus à l'est, Sainte-Maxime a les pieds dans l'eau. Là aussi, les Grecs avaient installé un comptoir, puis les Romains, une ville, Calidianisi. Une fois les invasions sarrasines repoussées par Guillaume de Provence, Sainte-Maxime a été créée. Au XIXe siècle, après l'arrivée du chemin de fer, elle sortira de sa léthargie grâce au tourisme hivernal des « fortunes françaises » à qui l'on doit les maisons Belle époque. Et aussi le château où « M. Gaumont faisait ses expériences cinématographiques ».


Plus à l'est encore, à l'embouchure de la Siagne, Mandelieu-la-Napoule est blottie autour de son château médiéval qu'un couple d'artistes américains fortunés, Henry et Marie Clews ont restauré il y a un siècle. On peut y voir l'atelier et les oeuvres d'Henry Clews, sculpteur. On peut s'offrir un thé sur la terrasse, l'une des plus belles de la Côte d'Azur. La mer clapote juste en contrebas...


Au creux de l'hiver, tous ces bourgs organisent des fêtes en hommage au mimosa. À Bormes, à l'expo-vente de plantes Mimosalia organisée les 26 et 27 janvier (thème 2019?: l'intelligence des arbres), succède le corso, le dernier week-end de février. à Sainte-Maxime, la fête a lieu début février, à Mandelieu, du 20 au 27 février... Histoire de donner un coup de pouce aux producteurs, les chars sont surtout habillés de mimosas.


---


EN PRATIQUE


?En savoir plus sur la route du mimosa?: routedumimosa.com


?Se renseigner. Office de tourisme de la Côte d'Azur (

www.cotedazurfrance.fr

),du Var (

www.visitvar.fr

), de Bormes-les-mimosas (

www.bormeslesmimosas.com

), de Sainte-Maxime (

www.sainte-maxime.com

), de Mandelieu-la-Napoule (

www.ot-mandelieu.fr

).


?Boire un thé au château de la Napoule?:

www.chateau-lanapoule.com



?Acheter des chocolats. La Muscadine, 7, rue du commerce à Sainte-Maxime. Tél.?:


04.94.96.46.12.


?Se faire livrer du mimosa. Valérie Torres, 870, chemin du Landon à Bormes. Tél.?: 04.94.71.89.94

brindesoleil83@gmail.com



?Pour des ateliers de cuisine et une carte traiteur originale. L'atelier « La cuisine des fleurs » d'Yves Terrillon.

www.la-cuisine-des-fleurs.com.

Tél. 04.92.95.13.32. Jardins du Rayol au Rayol-Canadel-sur-Mer?:

www.domainedurayol.org



Cet article est paru dans La Croix (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006937.xml</field>
    <field name="pdate">2019-01-25T00:00:00Z</field>
    <field name="pubname">La Croix (site web)</field>
    <field name="pubname_short">La Croix (site web)</field>
    <field name="pubname_long">La Croix (site web)</field>
    <field name="other_source">La Croix</field>
    <field name="source_date">26 janvier 2019</field>
    <field name="section">Culture Art-de-vivre</field>
  </doc>
</add>
