<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">PRÉSENT ET AVENIR DE BORDEAUX</field>
    <field name="author">Edmond DELAGE</field>
    <field name="id">eupr_009080.xml</field>
    <field name="description"><![CDATA[


Le lancement aux Chantiers de la Gironde du paquebot Le Moyne-d'Iberville, construit à Bordeaux par l'ingénieur de Verdière sous la haute direction de M. Nepveu, s'est déroulé la semaine dernière sans incidents, dans la Joie populaire. Événement fréquent cette année qu'un lancement de cargo.


La marine marchande française se reconstitue vigoureusement. C'est en dix mois que fut construit le beau bâtiment d'un port en lourd de 9 100 tonnes, actionné par un appareil moteur Diesel-Sulzer de 8 000 chevaux qui lui assurera la vitesse de 16 noeuds, avec un rayon d'action de 10 000 milles. Il partira dès août prochain à la suite de son frère le Cavelier-de-La-Salle, que nous visitâmes, fin prêt, à son appointement dans le même chantier. M. Jean Marie, président de la Compagnie générale transatlantique à qui sont destinés tes deux navires, évoqua la carrière, trop peu connue des Français, de ce Le Moyne d'Iberville; elle est pourtant au moins aussi glorieuse que celle de Cavelier. Elle la complète. Le premier organisa la navigation sur les grands lacs, explora le Texas, trouva la mort en cherchant par mer l'embouchure du Mississipi. Le Moyne, issu d'un petit fief normand, Iberville, troisième de onze frères, fait campagne dès 1682 dans l'immense contrée de la baie d'Hudson, qu'il donne i la France. Avec deux frères, Sainte-Hélène et Maricourt, il pénètre l'épée à la main dans le fort de Moutsipi. En 1696 il attaque l'Acadie; peu après il enlève Saint-John's-Harbour de Terre-Neuve. Après la paix de Ryswick il reprend le projet de remonter par mer l'embouchure du Mississipi. En décembre 1698 il découvre la Floride et las passes du grand fleuve, qu'il franchit en 1700. Avec l'appui de Vauban il conçoit le plan de chasser les Anglais de New-York et de Boston. En 1706 il va partir des Antilles contre la Barbade et la Jamaïque, la Caroline et la Virginie. Le " mal de Siam " terrasse à quarante-huit ans, en rade de La Havane, sur le juste, le " Cid canadien ".


C'est à la ligne du " Golfe " que sont affectés les deux cargos neufs. En 1938 la Transatlantique y transporta 261 000 tonnes de fret; le tonnage atteignit 415 000 tonnes dès l'an dernier. Il comporta des matières essentielles à notre économie : bois, zinc, asphalte, soufre, plomb, coton. En son récent périple dans toute l'Amérique centrale M. Jean Marie a jeté les bases d'échanges plus intenses encore avec ces pays si riches, où la France est attendue. Deux grands paquebots, Flandre et Antilles, les relieront à nos ports en 1950.


A la chambre de commerce M. Colin, ministre de la marine marchande, souligna l'ampleur de l'effort de construction déjà réalisé sous l'impulsion d'un grand commis, l'ingénieur général Courau. Tout ne sera pas terminé quand seront, bientôt, remplacées les unités détruites par la guerre. Il restera notamment à reconstruire 400 000 tonnes de navires hors d'âge. Les nombreux chantiers français sont dès aujourd'hui rééquipés. Les Chantiers de la Gironde fournissent un bon exemple de cette reconstitution. Ils mettront aussitôt sur cale deux autres cargos long-courriers de 8 300 tonnes. Leur personnel, de 1 700 hommes, leur matériel, en grande partie modernisé, sont capables d'un rendement accru. Tous les autres chantiers, récemment visités par nous à propos de fêtes analogues, sont dans une situation apparemment aussi brillante. Leur prospérité est-elle durable ? Les finances et le crédit public pourront-ils leur garantir des commandes suffisantes pour un laps de temps assez long ? Problème qui, dès maintenant, commence à se poser.


La même question hante l'esprit d'un ami de Bordeaux, qui a profité de cette occasion pour visiter les installations de ce port historique, sous la conduite de son directeur général Grange et de son adjoint l'ingénieur Aubriot.


Le port avait en 1938 maintenu une prospérité honorable - sans plus, - mais son trafic général avait plutôt diminué depuis 1913. Il se chiffra cette année-là par 4 606 456 tonnes; il ne fut plus que de 4 115 347 tonnes en 1938 - ceci en dépit de travaux magnifiques et coûteux, comme l'organisation des passes extérieures de la Gironde, ou la création d'un chef-d'oeuvre unique dans la construction portuaire : le Verdon. La guerre toucha assez durement Bordeaux. Ses ravages y apparurent moins brutalement qu'ailleurs; c'est que le port, en la ville même, avait conservé ses 3 000 mètres de quais à peu près intacts; ses bassins, d'ailleurs désuets, avaient peu souffert. Mais, avec un véritable génie du mal, les techniciens allemands, aussi experts en l'art de détruire que dans celui d'édifier (la gigantesque base sous-marine à onze alvéoles, comme celles de Lorient et Saint-Nazare, en fait foi), comprirent que pour ruiner Bordeaux avant leur fuite il suffisait d'étrangler son chenal. Ils choisirent le lieudit Lagrange, non loin des passes de Grattequina qu'ils obstruèrent en coulant dans le courant limoneux et rapide dix-huit puissants navires d'environ chacun 150 mètres de longueur. La tâche qui s'imposait à nos ingénieurs paraissait presque impossible. L'opacité des eaux jaunâtres de la Gironde, la violence de son flot ne permettaient ni exploration par scaphandre, ni relevages. Après bien des tâtonnements les ingénieurs des Grands travaux de Marseille et ceux des Remorqueurs de l'Océan appliquèrent la seule méthode praticable : le découpage des épaves par chalumeaux et explosifs, et leur effondrement dans le fleuve, à l'écart du chenal. C'est là que gît désormais cette véritable mine de fer sous-marine. Mais dès février 1946 le chenal était libre : c'était l'essentiel.


En attendant ce résultat - aussi brillant en son genre par son originalité technique que celui de la Telindière, récemment admiré en Loire maritime - l'ingénieur Grange avait préconisé et fait immédiatement construire, à environ 4 kilomètres en aval du barrage, les appontements provisoires, en bois de sapin, en eau profonde, du Marquis, accessibles à des Liberty-ships par 60 % des marées, et qui rendent aujourd'hui encore les plus grands services au trafic charbonnier. Les quais de Bordeaux-Bassens furent remis en état. L'appareillage, dont les Allemands avaient expédié le quart le plus moderne en Europe centrale, fut rapidement complété par dix-sept grues modernes Wellman, expédiées sur plans français par les États-Unis. Les passes extérieures de la Gironde furent dégagées, balisées. Le chenal de 120 kilomètres - le gros handicap de Bordeaux - a été dragué de toute urgence par un matériel puissant (la grande drague P.-Lefort est attendue le mois prochain à Saint-Nazaire). D'intéressantes et prometteuses études de remontée de la Gironde grâce au " radar de chenalage " - précis à petite distance - sont en cours.


Il n'en reste pas moins vrai que le port de Bordeaux reste soumis à ses lourdes servitudes géographiques. Si, dès août 1948, le trafic total depuis janvier s'éleva à près de 2 millions de tonnes, dont 1 413 836 tonnes aux importations, si Bordeaux reste la tête de ligne d'un commerce actif avec les Antilles, le Maroc, l'A.-O. F., Madagascar, l'

Australie

, les Indes, si son aviation de Mérignac lui apporte déjà un appui fort efficace, le problème primordial de la remontée du fleuve grève lourdement ses usagers. Ses pilotes ont enfin compris, sous la menace de la faillite de leur corporation, la nécessité de simplifier leur organisation et de réduire leurs prétentions. Les dirigeants du port sentent vivement la menace de la concurrence d'un port voisin comme La Pallice, doué d'un accès direct en eau profonde sur l'Océan. Ils se préparent à équiper - à défaut du Verdon, inexploitable pour le moment


- un véritable avant-port, avec postes à quai, à Pauillac, bien raccordé au réseau ferroviaire général et à la ville. Ses dockers - et surtout leurs cadres


- paraissent - après une grève dure et vaine - depuis peu assagis. L'exportation des nectars, tels que " châteaulafite " ou " la suffelière ", semble menacée d'une éclipse par la raréfaction, espérons passagère, de la clientèle étrangère, mais l'importation des hydrocarbures se développe. Puisque Ambès n'est accessible qu'aux tankers de 16 000 tonnes, un pipe-line le reliera, dit-on, bientôt à Pauillac. Les Landes dévastées, mais, souhaitons-le, bientôt ranimées, envoient encore nombre de poteaux aux mines polonaises, a défaut des britanniques. De vastes perspectives industrielles s'ouvriront à la ville et à sa région, avec l'adduction récente du gaz de Saint-Gaudens, et, plus tard, grâce à l'utilisation de la force hydraulique de la Dordogne et des Pyrénées. D'autres travaux devront être promptement effectués à Bordeaux même pour la traversée plus aisée du fleuve et pour le remplacement ou le doublement de l'unique pont, si vétuste. Malgré des nuages qui obscurcissent aujourd'hui un peu le blason de la charmante et fastueuse cité, l'avenir de son port - le port du Croissant, - et de ses vignobles, et de ses industries, reste malgré tout rassurant, si elle sait prendre les initiatives nécessaires.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_009080.xml</field>
    <field name="pdate">1949-01-31T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
