<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Le vêtement et la chaussure</field>
    <field name="author">Camille Brinie.</field>
    <field name="id">eupr_006156.xml</field>
    <field name="description"><![CDATA[


Dans l'industrie de la chaussure, et plus encore dans celle du vêlement, le plan de fabrication d'articles d'utilité sociale semble moins poussé que dans la lingerie.


Si en effet on trouve à acheter facilement des souliers " d'utilité sociale ", en revanche le Parisien ou la Parisienne ne peuvent guère trouver que " par relations " un complet, une robe ou un manteau à un prix raisonnable.


J'ai eu la bonne fortune de découvrir dans un magasin de l'avenue de l'Opéra des complets pour homme deux pièces, de coupe à la mode, veston long, pour le prix vraiment modique de 2.690 francs. Le vendeur m'a dit recruter sa clientèle parmi les petits fonctionnaires ou employés.


J'ai été également surpris de voir dans la vitrine d'un grand magasin de la rue de Rivoli des manteaux de femme en lainage à partir de 2.930 francs.


Mais je dois avouer que ce ne sont là que d'heureuses exceptions, et aucune pancarte n'attire l'attention du client. En réalité l'initiative de ces fabrications est due à l'office professionnel de l'habillement et du travail des étoffes, et les vêtements ont été confectionnés au début de l'année, bien avant par conséquent les articles d'utilité sociale, à la suite d'un arrivage de tissus anglais. Aujourd'hui ces vêtements peuvent être incorporés dans le plan d'articles d'utilité sociale, ils en ont presque toutes les caractéristiques.


Le scandale du " marché noir " dans les lainages


L'industrie lainière a été un peu moins touchée par la guerre que notre industrie cotonnière, par suite de la présence dans la métropole et dans nos colonies d'importants troupeaux de moutons. Cependant notre production était tombée, en 1944, à 15 % de celle de 1938. Actuellement elle s'est nettement relevée grâce à nos importations de laine d'

Australie

et de Nouvelle-Zélande et se trouve en tête de nos industries de fibres naturelles avec le coefficient 74 par rapport à 1938.


" Mais où va donc la laine ? " vous demanderez-vous. Dans le Nord, avant la guerre, 25 % de la production du textile étaient livrés au négoce, 75 % allaient à la confection. Actuellement les proportions sont inversées, 75 % des tissus vont au négoce et 25 % à la confection. On s'explique donc pourquoi on ne trouve presque plus de complets, de manteaux ou de tailleurs tout faits. Et si vous voulez obtenir les trois mètres de tissu nécessaires pour vous faire confectionner un costume, il vous faudra passer par un grand nombre d'intermédiaires (grossiste, demi-grossiste) qui, chacun, prélève, comme l'on pense, un bénéfice.


Aujourd'hui, grâce à la loi du 11 mai 1946, 50 % de la production des lainages sont réservés aux confectionneurs. Grâce aux divers protocoles qui viennent d'être signés avec les principales fédérations de fabricants de France on espère pouvoir mettre sur le marché une assez grande quantité d'articles de première nécessité.


La confection masculine


Un protocole signé le 7 juin entre la fédération de la confection masculine et la production industrielle prévoit la fourniture mensuelle à partir de fin septembre de 150.000 costumes deux pièces pour hommes, 50.000 complets garçonnets, 100.000 pantalons hommes et garçonnets et 50.000 culottes garçonnets. Les tissus sont de trois qualités différentes; les prix aux détaillants s'échelonnent pour le complet homme entre 1.950 fr. et 2.400 francs, pour le complet garçonnet entre 1.200 fr. et 1.450 francs.


Un autre protocole prévoit la livraison mensuelle de 60.000 vareuses de drap (620 francs) ou de coutil (475 francs) et de 75.000 pantalons de drap (600 francs) ou de coutil (400 francs) à l'usage de nos agriculteurs. En outre 70.000 vêtements imperméables doivent être mis sur le marché au prix de 1.200 francs pour le " modèle court " et 1.300 francs pour le " raglan ".


La confection féminine


Si la mode masculine est sujette à peu de variations et si les hommes se contentent d'un petit nombre de qualités de tissus, il n'en est pas de même de la mode féminine, beaucoup plus capricieuse et beaucoup plus variée. Les accords qui ont été signés ont tenu compte de cette caractéristique.


C'est ainsi que le protocole du 21 juin signé entre la production industrielle et la fédération française de l'industrie des vêtements féminins envisager une plus grande gamme de tissus et de couleurs. Le protocole prévoit la fourniture fin septembre aux détaillants de 120.000 manteaux à des prix variant entre 2.139 francs et 3.701 francs, de 20.000 tailleurs de 2.131 fr. à 3.801 francs, de 100 000 robes de 1.276 à 2.901 francs.


Tous les prix qui ont été fournis sont des prix de vente au détaillant; pour obtenir le prix de vente au consommateur il suffit d'ajouter à ces chiffres, le taux de marque du commerçant détaillant, qui n'est pas encore fixé, mais qui doit être de l'ordre de 21 à 28 0/0.


Plusieurs confectionneurs, qui m'ont reçu, ne m'ont pas caché les difficultés qu'ils éprouvaient à tenir leurs engagements par suite de la livraison en trop faible quantité de doublure et de toile tailleur. En effet, pour le mois d'août, sur 133.000 mètres de drap livrés à la confection féminine, il n'y a eu que 13.000 mètres de doublure et 5.000 mètres de toile tailleur, quantité nettement insuffisante. Il serait bon de remédier à cet état de choses une obliger les confectionneurs à utiliser des toiles à sac comme certains m'ont dit être contrainte de le faire pour remplacer la toile tailleur.


La chaussure


Si l'industrie du cuir a eu moins à souffrir d'une façon directe du marché noir que l'industrie vestimentaire, c'est d'une façon indirecte qu'elle a été touchée par suite du détournement du marché normal d'une grande quantité de cuir du à l'abattage clandestin. On estime en effet de 40 à 50 % la production du cuir qui échappe au commerce régulier, soit que les peaux servent à alimenter d'une façon clandestine des petits artisans qui fabriquent des objets en cuir (sacs, descentes de lit, etc), soit que de trop nombreuses peaux soient enfouies par les abatteurs clandestins. Enfin, les cuirs livrés à la collecte sont bien souvent abîmés et parfois même inutilisables par suite de trop grands déchets.


Néanmoins une seule entreprise a mis en vente dans la région parisienne 150.000 paires de chaussures usage ville tout cuir, a des prix s'échelonnant pour les chaussures d'hommes de 490 à 540 francs, et de femmes de 415 à 565 francs avec une réduction du taux de marque de la 0/0. Les ventes doivent s'étendre d'ici peu au Nord et à la Seine-Inférieure.


Un protocole est, sur le point d'être signé entre les fabricants de chaussure: et la production industrielle portant sur un programme mensuel de 1.200.000 paires. D'autre part il est question de mettre en vente libre un contingent de 1.400.000 paires en provenance d'Allemagne.


Avant la guerre la production mensuelle était d'environ 1.600.000 paires pour hommes et de plus de 2.000.000 de paires pour femmes. Nous sommes encore assez loin du compte, mais n'oublions pas qu'a côté du secteur " utilitaire " existe aussi un secteur libre.


Le plan d'articles d'utilité sociale, " dirigé " à la base, mais entièrement libre aux autres stades de la fabrication, doit réussir à condition que l'on ne se contente pas de fournir les matières essentielles, mais aussi les accessoires, les doublures, les toiles tailleur, le fil, les machines à coudre et les pièces de rechange de ces dernières.


Il ne faut pas non plus que l'appellation " utilité sociale " se confonde dans l'esprit des consommateurs avec les costumes et chaussures " nationaux " de certaines expériences de l'autre après-guerre. Les promoteurs du plan ont eu le souci de respecter l'individualisme français et savent l'horreur que nos compatriotes ont de l'uniforme. Il ne s'agissait pas non plus de normaliser ou de standardiser notre production, ce qui aurait entraîné à une transformation complète de notre équipement industriel. Le mot d'ordre donné aux fabricants a été : " Faites ce que vous avez l'habitude de faire. "


Non seulement au point de vue coupe ou au point de vue mode, mais aussi au point de vue qualité, les articles que j'ai vus ne sont pas des articles de grand luxe, mais tous peuvent soutenir la comparaison avec ceux d'avant guerre.


Espérons que ce vaste programme, qui ne se limite pas à l'Industrie textile et à l'industrie du cuir, mais louche déjà à la quincaillerie, aux voitures d'enfants, etc., sera encore développé, selon le voeu unanime de la conférence nationale économique des salaires et des prix et contribuera à détourner le consommateur du " marché noir ".


(1) Voir le Monde du 10 août


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_006156.xml</field>
    <field name="pdate">1946-08-17T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
