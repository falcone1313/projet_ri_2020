<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">En Nouvelle-Zélande, savoir bavarder est essentiel</field>
    <field name="author">The Guardian</field>
    <field name="id">eupr_004305.xml</field>
    <field name="description"><![CDATA[


En Nouvelle-Zélande, refuser de faire la conversation avec des inconnus est considéré comme grossier. Et bavarder ne se limite pas à échanger des banalités.


C'était le jour le plus chaud de la canicule en Europe. J'étais invitée à un dîner à Roelofarendsveen, un village tranquille des Pays-Bas, à 30 kilomètres au sud d'Amsterdam. J'avais pris mon vélo pour faire des courses au supermarché et, au moment de payer mes achats, je me suis mise à bavarder avec le caissier, comme nous le faisons naturellement en Nouvelle-Zélande. "Vous avez passé une bonne journée?? Ah?! là, là, qu'est-ce qu'il fait chaud?! Quand pensez-vous que les températures vont baisser??"


Le silence qui a suivi m'a fait lever les yeux de mes sacs : le caissier me considérait en plissant les yeux, interloqué. Mon compagnon, en riant, lui a expliqué : "Ne faites pas attention, elle est néozélandaise." Nous adressant un sourire gêné, le caissier nous a fait au revoir d'un signe de tête.


Ces pays où l'on n'aime pas bavarder


Pendant un mois, j'enchaînai les gaffes. Serrée dans les rames suffocantes du métro londonien, je souriais à mes voisins, qui détournaient aussitôt le regard. Après avoir commandé un café dans le quartier de Kensington, je demandai au barista s'il avait apprécié le double mixte Serena Williams-Andy Murray ou le dernier match de cricket, et si la canicule n'était pas trop dure. "Oui, je ne sais pas", me répondait-on généralement.


Déconcertée, je me retrouvai avec mon rédacteur en chef à Kings Place [siège du Guardian], après un nouveau trajet silencieux en métro. "Je commence à comprendre que les Britanniques n'aiment pas papoter, lui ai-je dit. J'agace les gens." Récemment rentré d'un séjour de quelques années en

Australie

, il a tout de suite compris ce que je voulais dire : "Il m'a fallu des mois pour réapprendre à tenir ma langue."


Il m'a gentiment énoncé sa théorie : dans les grandes villes surpeuplées, de 10 millions d'âmes et plus, les gens s'accrochent au petit espace privé qu'il leur reste, ils sont dans leur bulle, seuls avec leurs pensées.


Si j'ai bien compris, le bavardage futile n'est pas considéré comme quelque chose de sympathique, mais comme un dérangement injustifié. N'allez pas vous imaginer que vous avez des droits sur le temps, l'espace et la conversation des gens. Quelle grossièreté de la part d'une inconnue?! J'étais un peu découragée, mais le message était passé.


Le bavardage, c'est beaucoup plus que du bavardage


Pourtant, même en connaissant les règles, j'avais du mal à me retenir. En Nouvelle-Zélande, on s'habitue à ce badinage permanent, on y participe sans y penser. Et souvent cela ne se limite pas à des échanges de banalités. Si vous demandez familièrement à une personne au supermarché comment s'est passée sa journée, elle prendra certainement le temps de vous raconter qu'elle a placé sa mère en maison de retraite, que son bébé ne fait pas ses nuits ou que son mari est en poste à l'étranger depuis trop longtemps.


Inutile d'essayer de lire vos e-mails dans un taxi, car le chauffeur voudra savoir qui vous êtes, d'où vous venez, ce que vous faites, pourquoi vous êtes là. Quand quelqu'un refuse de faire la conversation, cela me met mal à l'aise. C'est considéré comme grossier, cela revient à montrer à l'autre qu'il n'est pas digne de votre temps et de votre attention.


En Nouvelle-Zélande, on a du temps à consacrer à tout le monde. Ici, on vit à petite échelle, la population n'est pas très importante. L'isolement et la solitude sont des questions plus pressantes que la surpopulation. D'après le dernier recensement, les gens vivant seuls sont plus nombreux que jamais et les jeunes restent célibataires plus longtemps qu'autrefois.


En Nouvelle-Zélande, le bavardage n'est pas que du bavardage, c'est aussi un moment de socialisation, parfois même le seul contact social de toute une journée dans le cas de personnes isolées, au chômage ou seules. Je me suis retrouvée dans ces situations, et ces échanges venaient chaque fois me rappeler que j'étais encore un être humain, digne de se voir adresser la parole, alors que mon coeur et mon esprit semblaient me dire le contraire.


En Nouvelle-Zélande, on a du temps à consacrer aux autres


Les premiers immigrants arrivés en

Australie

et en Nouvelle-Zélande étaient souvent attirés par la perspective d'une société plus égalitaire, sans classes - une société où le Premier ministre boit une tasse de thé avec le plombier. (Le cas de Jacinda Ardern étant un peu différent car elle s'occupe de sa plomberie elle-même.) Il serait faux de dire que la Nouvelle-Zélande a atteint cet idéal. Mais cet égalitarisme est sans doute plus souvent une réalité ici que chez certains de nos partenaires occidentaux.


Après un mois en Europe, nous sommes rentrés en Nouvelle-Zélande. Nous allions y retrouver notre travail, nos habitudes de couche-tôt et nos animaux domestiques. Tandis que je parcourais une boutique de duty free à la recherche d'une bouteille de whisky, un paysan rougeaud de l'Otago [région de Nouvelle-Zélande] s'est avancé vers moi d'un pas tranquille. "Pfff?! m'a-t-il lancé. Vous croyez que la neige va tenir??" J'ai eu un moment d'hésitation. Est-ce que je connaissais cet homme, cet agriculteur entre deux âges, en chemise rayée, coiffé d'une casquette de base-ball froissée?? Est-ce que j'aurais oublié son visage??


Tandis que nous continuions à bavarder, j'ai compris que nous étions l'un pour l'autre des inconnus. Des inconnus qui s'entretenaient de la fonte des glaciers sur la côte ouest. Des inconnus se plaignant des repas affreux qu'on leur avait servis à bord. Des inconnus qui avaient du temps à se consacrer mutuellement, qui n'étaient pas pressés - qui étaient chez eux.


Finalement, l'homme s'est éloigné, pour rejoindre sa femme qui avait fini son shopping. Mon compagnon est revenu vers moi, l'air amusé. "Tu le connaissais, alors??" "Non, ai-je répondu en souriant. Je ne l'avais jamais rencontré."


Lire l'article original AuteurEleanor Ainge Roy


Auteure et journaliste free lance, Eleanor Ainge Roy vit à Dunedin, en Nouvelle-Zélande. Elle collabore notamment au quotidien britannique The Guardian.


SourceThe Guardian


Londres

www.theguardian.com

L'indépendance et la qualité caractérisent ce titre né en 1821, qui abrite certains des chroniqueurs les plus respectés du pays. The Guardian est le journal de référence de l'intelligentsia, des enseignants et des syndicalistes. Orienté au[...]


Lire la suite


Sélection de la rédactionNouvelle-Zélande Destination. Nouvelle-Zélande : tout ce que vous devez savoir avant de partir


Nouvelle-Zélande. "J'ai tout plaqué pour créer une ferme bio à l"autre bout du monde"


Enquête. La Nouvelle-Zélande, ultime refuge des ultra-riches


Nouvelle-Zélande. Entreprises : bientôt la semaine de quatre jours??


Cet article est paru dans Courrier International (site web)




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004305.xml</field>
    <field name="pdate">2019-10-25T00:00:00Z</field>
    <field name="pubname">Courrier International (site web)</field>
    <field name="pubname_short">Courrier International (site web)</field>
    <field name="pubname_long">Courrier International (site web)</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Lifestyle</field>
  </doc>
</add>
