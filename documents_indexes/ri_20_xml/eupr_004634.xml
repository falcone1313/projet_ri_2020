<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Voici à quoi ressemblera le cinéma dans 5 ans</field>
    <field name="author">Patrick Laurent</field>
    <field name="id">eupr_004634.xml</field>
    <field name="description"><![CDATA[


ÀHollywood, l'improvisation est un mot banni du vocabulaire. Chaque évolution de la société est scrutée, analysée, "statistisée", transformée en nouvelle règle absolue inscrite en lettres d'or dans la Bible des blockbusters. Le public aime se goinfrer de corn-flakes ou de nachos tout en sirotant un seau de soda ? Pas de souci : les scènes d'action sont rallongées et les (rares) explications indispensables à la compréhension du récit répétées à l'envi. Ainsi, le spectateur hédoniste peut sortir de la salle pour se remplir l'estomac et revenir l'esprit tranquille, il n'a rien manqué.


Problème résolu. Mais aujourd'hui, un autre phénomène réclame toute l'attention des grands studios. Habitués à consulter plusieurs écrans en même temps, les spectateurs souffrent de plus en plus de déficiences de la concentration. Plusieurs enquêtes réalisées aux quatre coins du monde ont mis en évidence ce constat : 66 % des Américains et des Canadiens perdent complètement le fil de ce qu'ils suivent à cause de leur tablette ou de leur smartphone. En

Australie

, 68 % des cinéphiles sont touchés. Et en Angleterre, cela grimpe à 74 % de la population, selon un sondage réalisé par RadioTimes auprès de 12 500 personnes.


Même si cela concerne principalement les films et séries visionnés à la maison, les pontes du 7e art américain ont déjà commencé à intégrer cette évolution dans leurs productions, puisque désormais, les fictions sont destinées à une multitude d'écrans. "L'impact des nouvelles technologies et de cette distraction sur la manière de filmer est bien visible, confirme le Dr. Sandra Rothenberger, professeur de marketing à l'ULB. À l'époque des westerns spaghetti, lors des duels, les protagonistes étaient tous les deux au bord de l'écran. Et on utilisait énormément de plans de vastes paysages. Aujourd'hui, à cause des petits écrans, il faut filmer en gros plan. Et les écriteaux sont anormalement grands aussi, pour être lisibles. On tourne plus vite désormais, sans ajouter ces détails qui faisaient la richesse des grands films. L'attention, on l'attire avec des couleurs, des tailles, des effets spéciaux, pas avec les détails qu'il faut chercher et qui en disent long. Prenez la trilogie Sissi . Les décors, les paysages, les robes, c'étaient des oeuvres d'art, même si l'histoire était kitsch. Et ça, dans chaque scène. Aujourd'hui, seule l'action compte. Et le volume sonore a fortement augmenté. On crie, la musique va fort, c'est très agressif. Ce sont des stimuli pour garder le spectateur en alerte et concentré, du moins artificiellement."


Mais c'est très compliqué de rester concentré pendant une heure et demie ou deux heures...


C'est impossible. D'ailleurs, une des sommités d'Hollywood, le patron de la future plateforme de streaming Quibi, Jeffrey Katzenberg, estime que les films ne dépasseront plus une heure, pour ne pas ennuyer le spectateur. Ou alors, ils seront découpés en plusieurs épisodes de sept à dix minutes. Comme des séries. Regardez le succès des séries : il ne peut qu'influencer le cinéma. Les films vont se réduire car les séries, elles, permettront de vivre des histoires beaucoup plus longues chez soi. D'ailleurs, souvent, à la sortie d'une salle, on entend dire qu'un film est trop long. Alors que chez soi, devant Netflix, la durée n'est pas un problème à condition de tout découper en petites tranches. Un film ne dure pas plus longtemps, mais avec une série, on peut s'arrêter quand on veut. Hollywood l'a bien compris. Sex and the City, le film, n'a pas rencontré le même succès que la série. Parce que c'est le type d'histoire qu'on regarde quelques minutes chez soi ou pendant une demi-heure. Aujourd'hui, on aime compartimenter sa consommation médiatique. Pour les longs métrages, mais aussi pour les informations. On n'est plus capable de rester assis. Avant, on estimait la capacité d'attention à 18 secondes. Pour la génération suivante, c'était 8 secondes. Et pour l'actuelle, c'est trois secondes ! C'est pire que les poissons rouges. Des études de neurosciences le démontrent. Chaque jour, on est soumis à 5 000 stimuli. C'est énorme. Avant Internet, on parlait de 500 stimuli. Cela explique aussi pourquoi les gens sont si nerveux. Ils courent toujours derrière quelque chose, le temps ou le travail par exemple. Il faudrait oser perdre du temps, ce qui n'est plus du tout accepté à l'heure actuelle.


Les spectateurs de 15 à 35 ans sont ceux qui vont le plus en salle. Mais ce sont aussi les cibles prioritaires des plateformes de streaming. Cela ne risque-t-il pas aussi d'avoir un impact sur le cinéma ?


L'évolution a déjà commencé. Les jeunes préfèrent regarder leur tablette à la maison. Regardez les courbes démographiques : il y a plus de "vieux" que de jeunes dans les salles. On parle de Silver Society. Une étude menée par UGC montre d'ailleurs ce vieillissement des spectateurs en salle par rapport à voici 10 dans. C'est flagrant. Dans les cinémas, les spectateurs ont souvent plus de 30-40 ans. Désormais, les cinéphiles, ce sont les baby-boomers et la fin de la génération X (les spectateurs nés avant les années 80, NdlR) .


Quelles en seront les conséquences ?


Les blockbusters sortiront toujours en salle, parce qu'on en prend pleine les yeux avec ses proches, sans être perturbé tout le temps par le téléphone. Les studios ont aussi besoin de créer des événements culturels populaires pour augmenter leur attrait, ensuite, sur les plateformes. Le cinéma d'auteur pourrait aussi en profiter. Les plateformes visent le grand public, alors que les films d'auteur sont destinés aux cinéphiles. Donc, ces oeuvres culturelles vont sans doute être mieux mises en valeur dans les cinémas à l'avenir. Ce n'est pas un hasard si le festival des cinémas au Bozar est sold-out. On y présente des films déjà montrés au festival de Venise ou de Cannes, mais c'est rempli. Une partie du public assimile encore le cinéma à la culture, à condition que ce soit un peu plus sophistiqué, profond. Les fans de divertissements rapides, d'effets spéciaux, d'explosions de stimuli, eux, se rabattront plutôt sur les plateformes. Les oeuvres d'art auront plus leur place dans les salles. On verra sans doute plus de films centrés sur les problèmes sociaux. Comme Girl, qui a rencontré un énorme succès, alors que le sujet est un peu "lourd". Il faut rester concentré pour l'apprécier. Et il y a un public pour la culture. Parce qu'après, on peut en parler. Alors qu'après un divertissement, la plupart du temps, on ne discute pas. On n'en a rien retiré. Avec les grands films, le cinéma peut redevenir un événement de consommation lente, pas rapide.


Dans combien de temps ?


C'est impossible à savoir avec précision, mais à mon avis, dans les 5 à 10 prochaines années, il va y avoir un changement énorme. Les petits cinémas peinent à survivre et vont viser les publics de niches, tandis que les multiplexes risquent de perdre une partie du grand public et, donc, de la pub. C'est pour cela qu'on voit les multiplexes créer des événements, avec des séances spéciales pour les femmes, les enfants, les amateurs d'opéra ou même de football. Mais ce sont des "coups". L'exclusivité, qui rend le cinéma si attractif pour l'instant, risque de disparaître à terme. D'ailleurs, la fréquentation ne cesse de chuter. Donc, le cinéma n'aura pas d'autre choix que de se réinventer.


Un casse-tête sur lequel planchent déjà toutes les têtes pensantes d'Hollywood.


Patrick Laurent


Dans un futur proche, les films ne dépasseront sans doute plus une heure, pour ne pas ennuyer le spectateur.


Aujourd'hui, on regarde souvent sa tablette en même temps que le film. Sans rien suivre attentivement...


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_004634.xml</field>
    <field name="pdate">2019-10-19T00:00:00Z</field>
    <field name="pubname">La Libre Belgique</field>
    <field name="pubname_short">La Libre Belgique</field>
    <field name="pubname_long">La Libre Belgique</field>
    <field name="other_source">La Dernière Heure - Les Sports</field>
    <field name="source_date">19 octobre 2019</field>
    <field name="section">INFORMATIONS GENERALES</field>
  </doc>
</add>
