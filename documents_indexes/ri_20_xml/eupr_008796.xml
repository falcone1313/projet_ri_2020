<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Mashrou' Leila, de vrais musiciens d'abord</field>
    <field name="author">Carla Henoud</field>
    <field name="id">eupr_008796.xml</field>
    <field name="description"><![CDATA[


Festival International de Byblos Le 21 juillet 2008, Hamed Sinno, Firas Abou Fakher, Haig Papazian, Carl Gerges et, à l'époque, Omaya Malaeb, André Chedid et Ibrahim Badr, se produisaient pour la première fois sur la scène de l'AUB. 10 ans plus tard, ils ne sont plus que quatre à sillonner le monde avec leur talent. Retour sur un groupe qui se produit désormais partout et n'a envie de parler que d'une seule chose : de musique, en attendant, en espérant qu'il puisse la partager, comme prévu, sur scène, ce 9 août au Festival de Byblos.


On s'était (presque) dit rendez-vous dans dix ans. Mais nul ne pensait qu'un tel déluge de haine allait accompagner cet anniversaire et le concert de Mashrou' Leila, prévu le 9 août à Byblos, et remettre tant de choses en question.


C'est dans le café où la première entrevue avait eu lieu, en avril 2009, que nous avions rendez-vous avec Carl Gerges, une semaine avant que la rage, l'homophobie, l'intolérance et la mauvaise lecture des chansons du groupe ne s'emparent de certaines personnes. L'enseigne a peut-être changé, le décor, le menu, nos souvenirs, de même que la situation d'un pays qui perd le respect des différences et le sens de la liberté.


Mais ce qui n'a clairement pas changé, c'est l'aversion qu'éprouve le reste du groupe pour les interviews, et notamment Hamed Sinno, qui en est pourtant la « voix ». Une voix qui a choisi de se taire à défaut d'être bien comprise. Ainsi que l'émotion encore intacte de l'architecte-batteur-musicien pour tous ces moments vécus et l'apprentissage qu'ils en ont tiré. Parfois inattendus, parfois intenses, les arrêts, les parenthèses, souvent imposées, ont appris au groupe à transformer un Mashrou' d'un soir en carrière internationale, jusqu'à faire la une des Inrocks, de Libération, de Rolling Stones, du Guardian ou encore du New York Times. À devenir, que leurs détracteurs le veuillent ou pas, un groupe phare du Moyen-Orient, avec ce mélange unique de rock alternatif et de poésie arabe, imbibé par le violon magique de Haig Papazian et la voix ensorcelante de Hamed Sinno. Même si eux-mêmes ne s'y attendaient pas à ce point et qu'ils ressortent de ces 10 années comme d'un parcours de combattants malgré eux avec la nécessité d'élargir le(ur) monde et de mieux se faire comprendre.


« Les doutes sont toujours là, mais ils sont différents, confiait Carl Gerges. Nous avons vécu beaucoup d'événements stressants, des émotions, des interdictions, des chocs qui nous ont poussés à nous arrêter un moment et remettre en question certaines choses. » Pour revenir, chargés à présent de nouvelles interrogations. Aujourd'hui, plus que jamais, complètement dépassés et bouleversés par cette chasse aux sorcières déclenchée il y quelques jours, de nouvelles interrogations s'imposent. La première étant : et après ?


Flashback


Quand quelques étudiants en architecture et design à l'AUB, après une nuit blanche, accouchent d'un nom (étrange), celui d'un groupe qui devait durer une nuit (tous les fans le savent à présent), ils ne savaient pas qu'il allait durer dans le temps. Dans la foulée, sortait Ra'sit Leila qui, immédiatement, a ensorcelé un public surpris. C'était en 2008. Entre cette année-là et septembre 2017, où, après la Jordanie, ils sont interdits de concerts en Égypte - un (seul) drapeau arc-en-ciel brandi dans le public va provoquer 70 arrestations -, les quatre jeunes sentent le vent tourner. Porte-parole de causes qui vont de l'écologie à la liberté d'expression, certes, mais musiciens d'abord, ils préfèrent parler de collaborations artistiques, de sons nouveaux, de belles rencontres. « Le public égyptien était notre plus grande audience. Ils étaient 35 000 à ce concert, et les deux concerts supplémentaires étaient complets. Nous nous sommes sentis coupables des arrestations qui ont suivi. Depuis, tout a changé... », explique Carl Gerges. Et de poursuivre : « En réalisant qu'il restait très peu de pays où l'on pouvait chanter en arabe, nous avons pensé qu'il valait peut-être mieux s'arrêter. Nous avons failli le faire... Nous sommes passés par une très violente remise en question. Mais elle nous a permis de réaliser la chance que nous avions de faire ce métier. »


Mashrou' Leila, 10 ans de grandes émotions. Photo DR


Nouvelle énergie


Après l'Égypte et quelques mois de réflexion, Mashrou' Leila enchaîne avec des concerts dans des lieux mythiques, participe à des tables rondes, donne des conférences, se concentre plus sur l'Europe, les États-Unis, l'

Australie

, l'Asie et l'Amérique latine, et lance aussi quelques morceaux en langue anglaise. « Rien ne s'est passé du jour au lendemain dans notre carrière. Tout s'est fait progressivement. Au début, nous étions un groupe de jeunes étudiants révoltés. La manière de communiquer nos messages a changé. Nous avons beaucoup évolué musicalement et personnellement. À présent, nous maîtrisons beaucoup mieux la composition, la production et les instruments qui sont devenus une langue... » poursuit Carl Gerges. La sortie de leur album The Beirut School est venue confirmer cette maturité. « C'est pour nous, souligne le jeune homme, une manière de clôturer cette période et de rendre hommage à nos 10 ans. L'album comprend un best of de nos cinq albums ainsi que trois nouvelles chansons. » Mashrou' Leila, qui a également décidé de « profiter des rencontres et d'artistes qui ont une vision plus large », ose la légèreté, une forme de spontanéité dans un style plus accessible, plus électronique. Ils ont voulu « sortir de notre zone de confort », et ils l'ont fait sans jamais être dans la provocation, en donnant de nombreux concerts dans des lieux mythiques, dont l'Olympia, puis en prenant part, début juillet, à l'installation sonore Vessel Orchestra, d'Oliver Beer, au Met Breur à New York. « C'est la première installation de ce genre organisée au Met, explique Gerges. Beer a créé un instrument en utilisant les vases, sculptures et objets décoratifs existant au musée, certains datent de la Mésopotamie et de la Grèce Antique, d'autres sont plus modernes. Il nous a donné carte blanche pour faire ce que nous voulions et adapter notre musique à cet instrument et à ce lieu, sur la voix de Nour el-Houda et les mots de Rabih Alameddine. C'était magnifique. »


En attendant un nouveau rendez-vous - confidences, dans dix ans peut-être -, Mashrou' Leila sera-t-il dans le magnifique cadre de Byblos dont il est familier, avec cette nouvelle énergie mêlée au son mélancolique et sincère du groupe ? « Nous voulons juste célébrer nos 10 ans avec le public, précise Carl Gerges, avec un grand nombre d'invités, dont Hercules & Love Affair, un groupe avec qui nous avons déjà collaboré. La scène que l'on a prévue devrait être très spéciale et impressionnante, comme à chaque fois que nous avons été en concert à Byblos, avec des écrans et des visuels. Et surtout, et c'est le plus important pour moi, nous espérons dévoiler sept nouvelles chansons qui n'ont jamais été jouées auparavant. »


En attendant que se calme cette tempête malsaine qui ne fait que diviser encore plus les esprits et les convictions, en priant (chacun son Dieu) que ce faux débat cesse et que la musique de Mashrou' Leila retrouve sa place, le groupe a publié sur son compte Instagram ces quelques précisions : « Nous sommes quatre Libanais de différentes religions et milieux socioculturels. Notre objectif est et a toujours été de nous épanouir en temps qu'artistes et d'utiliser les espaces qui nous sont offerts pour essayer de mettre la lumière sur les problèmes du monde qui nous entoure, tout en essayant de rendre les personnes autour de nous fières. Ni plus ni moins.»


10 COMME 10 ANS


DISCOGRAPHIE


Mashrou' Leila (2008)


El-Hal romancy EP (2011)


Raasuk (2013)


Ibn el-leil (2015) - numéro 11 au classement international de Billboard


Numéro un sur iTunes MENA du 1er au 26 décembre


The Beirut School (2019).


Leurs 10 titres préférés


Aeode


Ashabi


Cavalry


Roman


Radio Romance


Lil watan


Fasateen


Ra'sit leila


Shim el-yasmine


Taxi


10 (lieux et) concerts, 10 moments forts


Mashrou' Leila en concert à Byblos en 2016. Photo DR


Égypte


Festival international de Byblos (2010 et 2016)


Festival international de Baalbeck (2012)


Round House - London


Barbican Headline Show à Londres (2015)


Music Hall of Williamsburg Headling Show (2016)


Inauguration du musée St Laurent à Marrakech (2017)


All Points East Festival - London (2018)


Olympia Paris (2019)


Met New York (2019).


À la une
« Nasrallah ne peut pas décider du cours de la justice », martèle Joumblatt


Yara ABI AKL


Trois mois d'horreur dans la province d'Idleb


Caroline HAYEK


Coup de tonnerre de Ghada Aoun : les carrières des Fattouche mises sous scellés


Sandra NOUJEIM


Inscrivez-vous à la Newsletter
JE M'INSCRIS Inscription en cours.. Vous êtes déjà inscrit à la newsletter de L'Orient-Le Jour Inscription réussie Veuillez vérifier votre saisie et réessayer Introduisez votre email Merci pour votre inscription Retour à la page "Culture"


Cet article est paru dans L'Orient-Le Jour




]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008796.xml</field>
    <field name="pdate">2019-07-27T00:00:00Z</field>
    <field name="pubname">L'Orient-Le Jour</field>
    <field name="pubname_short">L'Orient-Le Jour</field>
    <field name="pubname_long">L'Orient-Le Jour</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Culture</field>
  </doc>
</add>
