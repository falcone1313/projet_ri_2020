<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">La désobéissance civile devient nécessaire selon des militants écologistes</field>
    <field name="author">Ugo Giguère</field>
    <field name="id">eupr_008908.xml</field>
    <field name="description"><![CDATA[


MONTRÉAL - Les militants écologistes qui luttent contre le changement climatique veulent augmenter la pression sur les gouvernements qu'ils accusent d'inaction face à l'urgence mondiale décriée par les scientifiques de la planète. La prochaine étape pourrait bien être la désobéissance civile et un expert estime que leur argument est de plus en plus solide.


Le 13 juillet dernier, 25 militants associés au groupe Extinction Rébellion Québec ont été volontairement arrêtés, sans aucune résistance, au terme d'un « sit-in » qui aura duré près de cinq heures sur la rue Sherbrooke au centre-ville de Montréal.


Il pourrait s'agir du premier exemple d'une nouvelle tendance alors que l'organisation internationale Extinction Rébellion, qui s'identifie par les initiales « XR » , prône carrément la désobéissance civile dans ses campagnes de mobilisation.


Rencontrée lors de la manifestation, l'initiatrice de la branche québécoise du mouvement fondé au Royaume-Uni, Elza Kephart, soulignait que « de grands mouvements de désobéissance civile ont fait changer la société drastiquement » . Elle faisait notamment référence au mouvement pour les droits civils des Afro-Américains ainsi qu'aux suffragettes qui réclamaient le droit de vote des femmes.


« On est rendu à un tel point où seulement un changement drastique de notre système peut sauver l'humanité de l'extinction. Ce n'est pas mon imagination, mais la science qui le dit » , avait-elle déclaré.


La définition de la désobéissance civile que propose l'encyclopédie Universalis sur son site web parle de citoyens portés par des motivations éthiques qui « transgressent délibérément, de manière publique, concertée et non violente, une loi en vigueur, pour exercer une pression visant à faire abroger ou amender ladite loi par le législateur ou à faire changer une décision politique prise par le pouvoir exécutif » .


Mouvement mondial


Tout récemment, du 15 au 19 juillet, XR a tenu une semaine de désobéissance civile dans plusieurs villes du Royaume-Uni. Ses militants ont notamment bloqué des routes, érigé des campements sur des ponts et interrompu des chantiers de construction sans jamais avoir recours à la violence.


Les villes de Londres, Bristol, Leeds et Cardiff, en Angleterre, ainsi que Glasgow, en Écosse, ont fait l'objet de cette opération de « perturbation pacifique des activités quotidiennes » , comme le résume l'organisation qui a pris la peine d'offrir ses excuses à la population pour les désagréments occasionnés.


Dans le même ton, quelques minutes avant le début de la manifestation du 13 juillet à Montréal, le formateur en désobéissance civile chez XR Québec, François Léger-Boyer, tenait à insister sur l'aspect « civisme » de la démarche.


« Dans l'expression "désobéissance civile", il y a le mot "civil", donc il faut le faire avec civisme. C'est sûr que le mot "désobéissance" prend toute la place, mais dans le fond, on désobéit seulement quand le gouvernement n'assure pas la dignité, la sécurité ou la survie des générations futures » , s'est-il défendu.


Des médias internationaux rapportent d'autres activités du genre à New York, Paris, Bordeaux et à Santiago au Chili. Le Guardian de Londres a recensé des actions militantes de XR au Japon, en Nouvelle-Zélande, en Colombie, au Ghana, en Inde, en

Australie

, en Afrique du Sud et en Belgique.


Utiliser la justice


Au Royaume-Uni, où XR prend ses racines, certains militants arrêtés et traduits en justice ont voulu profiter de leur comparution devant le juge pour faire entendre leurs revendications et faire valoir ce que les juristes appellent une défense de nécessité en s'appuyant sur les preuves scientifiques de l'urgence climatique.


Du côté de la branche québécoise, l'organisation créée en janvier dernier affirme ne pas avoir déterminé de stratégie en ce sens et surtout ne pas avoir les ressources financières pour entreprendre ce genre de débat judiciaire très onéreux.


Pour la coordonnatrice du comité légal d'Extinction Rébellion Québec, Me May Chiu, la décision de convenir d'une stratégie de défense revient à chacune des personnes accusées et à leur avocat. Pour le moment, l'organisme a choisi de se mobiliser pour faciliter la libération rapide des militants et pour minimiser les conséquences de leur arrestation.


« Tout ce que je peux vous dire, c'est que l'on va fournir la meilleure défense possible à ces gens » , a commenté Me Chiu en entrevue téléphonique.


Dès l'arrestation de ses manifestants, XR Québec leur a offert du soutien en leur fournissant une liste d'avocats favorables aux mouvements progressistes. Ces juristes se seraient engagés à défendre les militants admissibles à l'aide juridique et à offrir un tarif « militantisme » aux autres personnes arrêtées.


Selon May Chiu, les manifestants qui en ont les moyens vont s'acquitter eux-mêmes du coût de leur défense. Pour ceux qui ont un revenu plus modeste, une campagne de sociofinancement a été mise en ligne sur la plateforme The Action Network.


Toutes les personnes arrêtées au terme du « sit-in » du 13 juillet ont été libérées sous promesse de comparaître à une date ultérieure. Pour le moment, elles sont toutes accusées d'entrave au travail des policiers.


Les comparutions devant le juge sont prévues à la fin du mois de septembre.


De plus en plus justifiable


La défense de nécessité en lien avec un geste de désobéissance civile représente tout un défi devant le tribunal puisque selon le professeur à la faculté de droit de l'Université de Montréal Hugo Tremblay, spécialisé en ressources naturelles et environnement, elle n'aurait jamais été accueillie par un verdict favorable dans une cause écologiste au Canada.


Le professeur Tremblay explique que cette défense est applicable en matière de droit pénal et criminel et qu'elle permet de démontrer que lorsqu'on se trouve dans une situation de nécessité, on peut déroger à la loi, ou violer certaines normes juridiques, afin d'éviter un mal plus grand que celui que l'on choisit de causer.


Celui-ci croit que des groupes militants écologistes pourraient avoir une cause défendable en ce sens.


« L'état du droit actuellement, dans la société occidentale et au Canada en particulier, est que l'on peut détruire l'environnement et en arriver de manière tout à fait légale à un écosystème invivable. Face à ce constat-là, oui, on peut s'attendre à ce que des groupes de protection de l'environnement en viennent à utiliser des moyens de résistance qui les mènent à poser des gestes illégaux auxquels ils pourraient opposer une défense de nécessité » , observe l'universitaire.


D'après le Groupe d'experts intergouvernemental sur l'évolution du climat (GIEC), au rythme actuel où la production de gaz à effet de serre (GES) s'accumule, la planète devrait s'être réchauffée de 1,5 degré Celsius par rapport à l'ère préindustrielle entre 2030 et 2052.


Ces experts estiment que la planète a déjà gagné un degré par rapport à l'ère préindustrielle. De plus, le GIEC indique dans son plus récent rapport que le réchauffement s'accélère actuellement.


Dans ce même document, ils sonnent l'alerte afin de renverser drastiquement la tendance. Si les conséquences d'un réchauffement de 1,5 degré s'annoncent déjà dramatiques, les scientifiques préviennent qu'elles pourraient être catastrophiques si le réchauffement atteint deux degrés tel que le scénario le prévoit d'ici 2100.


Extinction Rébellion Québec a déjà annoncé qu'elle participerait à la semaine de grève contre le changement climatique, du 22 au 27 septembre prochain, puis à la semaine d'action organisée par leurs collègues du Royaume-Uni, le 7 octobre prochain.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008908.xml</field>
    <field name="pdate">2019-07-30T00:00:00Z</field>
    <field name="pubname">La Presse Canadienne - Le fil radio</field>
    <field name="pubname_short">La Presse Canadienne - Le fil radio</field>
    <field name="pubname_long">La Presse Canadienne - Le fil radio</field>
    <field name="other_source">Le Devoir (site web)</field>
    <field name="source_date">30 juillet 2019</field>
    <field name="section">Nouvelles Générales</field>
  </doc>
</add>
