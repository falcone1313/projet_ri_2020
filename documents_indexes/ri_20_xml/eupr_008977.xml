<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">IV. - La colonisation en Somalie italienne</field>
    <field name="author">Yves MONVAL.</field>
    <field name="id">eupr_008977.xml</field>
    <field name="description"><![CDATA[


Commencé en 1921, sur l'initiative du duc des Abruzzes, le défrichement de terres arables à une centaine de kilomètres de la côte, et le long d'un oued qui tient cette gageure en ce pays d'avoir de l'eau, se poursuivit sans arrêt, selon des principes formels établis au départ. Deux bases étaient nécessaires : assurer une irrigation régulière et dresser des boucliers contre le vent. La mousson est passionnée et très souvent violente. De l'océan Indien elle court sur des sables qu'elle a dévêtus à plaisir, et sur lesquels, rabrougris, misérables, sans qu'aucun d'eux n'ose s'élever au-dessus des autres, se dressent de temps eu temps des genres de baobabs difformes, ainsi que des épineux de toute nature, des mimosées, des buissons morts et vifs. Contre ces vents, maîtres absolus du terrain, il fallait, comme en Provence nos maraichers de Châteaurenard, protéger avant toute chose la lente et paisible croissance des tentatives humaines.


Ainsi ont été tracés des lots d'une moyenne de 7 hectares - les " aziendas " - enfermés dans des barrières sans fissures. L'arbre est en général une sorte de tamaris importé d'

Australie

. Il répond fidèlement au rôle qui lui a été assigné.


La Societa agricola italo-somala avait été constituée à Gênes en novembre 1920. Son capital initial de 24 millions de lire fut rapidement porté, en 1923-1924. à 35 millions, avec des parties prenantes intéressées aux réalisations de l'entreprise : cotonniers et " sucriers ", indépendamment des personnes privées et des établissements bancaires. La concession était de 12.000 hectares. La moitié fut mise en chantier, soit six domaines de 1.000 hectares, confiés chacun à un chef de culture, naturellement ingénieur agronome, appuyé par deux " ouvriers " italiens Au début, ce fut la charge de 2.000 hectares qui fut ainsi remise à chacune de ces équipes, avec la tâche de prendre la brousse à pleins bras, et de s'en rendre maître. Le travail fut rude. Il fallut tout raser, mettre la terre a nu, l'ouvrir, la retourner, la faire respirer, creuser les canaux, construire les routes et, sur celles-ci, installer la voie ferrée de 0 m. 50, puis édifier les bâtiments individuels afin que les chefs d'îlots et leurs collaborateurs vivent sur leurs terres, et non pas groupés au " village ", en marge du domaine.


Les cultures entreprises furent la canne à sucre, le coton, l'arachide. Des plantations de cocotiers furent essayées. Une sucrerie fut édifiée en 1926. L'huilerie fabrique également du savon. L'oeuvre fut ainsi poursuivie sans hésitation, avec une sûreté de vues, une émulation, une énergie, qui sont les marques essentielles de toute colonisation, telles que nous les avons relevées chez les Italiens et chez les Belges, soutenus par leur gouvernement, comme chez les Français, trop souvent combattus, eux, par celui-ci.


Le résultat est aujourd'hui sous mes yeux. Il se matérialise tout d'abord par la tombe du duc des Abruzzes, enterré en 1933 au milieu des terres auxquelles il a donné sa vie comme il a donné leur propre vie à celles-ci. Et dans ce petit cimetière, lui aussi défendu contre la profanation sans retenue du vent, une vingtaine de tombes forment le premier groupe des morts du " Villagio duca degli Abruzzi " - " Villabruzzi " en abrégé - où, plus communément, du " Villagio ". Le dernier message des morts, pour les vivants qui ont encore du respect au coeur, demeure le legs qu'ils font de leur nom à l'oeuvre qu'ils ont édifiée.


Villabruzzi, paysage des Flandres


Villabruzzi compte encore cent cinquante Italiens, avec femmes et enfants. Comme des naufragés sur leur radeau, dans l'incertitude, et au milieu des courants contraires qui portent le désastre en eux. les colons de Villabruzzi poursuivent leur oeuvre. Le directeur technique, M. Cotturi, qui travailla à Douai, me permet de dresser le bilan. Tout d'abord de l'équipement : de 70 à 80 kilomètres de route, 40 kilomètres de Decauville. Mêmes chiffres environ pour les canaux d'irrigation, qui s'alimentent à un bassin central armé de ses écluses, contre l'oued Chebeli. En exploitation : 1.000 hectares de canne à sucre, de 400 a 6C0 d'arachides, de 400 à 600 également de maïs. Il est à noter que la Somalie italienne est l'un des rares pays au monde à effectuer deux campagnes de sucre par an, grâce à la rotation de ses cultures (la canne est bonne à couper après seize mois).


La production de sucre était avant la guerre de 5.000 tonnes. Elle n'est plus aujourd'hui que de 3.500, et cela pour une cause unique : le manque d'engrais. Le coton est aujourd'hui stocké et bloqué sur ordre des autorités d'occupation. L'huilerie, dressée au milieu de bordures touffues de lauriers-roses, est stoppée. Elle cuit seulement un peu de savon pour la consommation locale. Le rhum n'est de même produit que pour les besoins locaux. D'ailleurs, l'alcool de la canne avait été surtout distillé pour alimenter tracteurs et voitures, tous les moteurs ayant été durant la guerre transformés à cet usage.


Nous courons sur les routes de la concession. Le canal d'un côté, le Decauville de l'autre. Des tracteurs remorquent vers l'usine leurs wagonnets de cannes. Des travailleurs regagnent leurs cases : 16 heures, leur journée est terminée. Nous croisons l'auto du docteur, occupé à faire sa visite des malades légers, les deux hôpitaux, européen et indigène, étant au village, sous la dépendance de trois soeurs, dont le casque est bizarrement posé sur la cornette blanche. À perte de vue, les aziendas s'alignent, encadrées par leurs hauts arbres. Le ciel, bourré de nuages, s'affale sur elles et met ainsi la dernière main à l'image surprenante d'un paysage des Flandres. Quand nous passons près d'une habitation nous saluons du klaxon. Ici, un blanc répare un tracteur, et là s'affaire parmi des travailleurs. La vie marche au ralenti, mais marche tout de même, grâce au coeur qui ne s'arrête pas de battre.


Mais que restera-t-il de tout cela ? Que restera-t-il de ces petites villes nées dans l'enthousiasme, et qui meurent lentement sans avoir atteint la maturité ? Que deviendront les quatre cent cinquante colons répartis entre Ghisimaio, Merca, Vittorio-d'Africa, Brava qui croyaient qu'il suffit de travailler pour vivre, et qui s'aperçoivent que le courage, pour eux, ne sert plus désormais qu'à supporter le poids du malheur.


Merca, port de la banane


La route de Mogadiscio à Ghisimaio ne longe pas la mer, mais s'allonge, morne, dans un désert plat, un désert plat hanté de toute la gamme de la sauvage végétation africaine, à une dizaine de kilomètres en moyenne du rivage. Il faut donc bifurquer pour atteindre Merca, comme on bifurque pour atteindre Brava. La bifurcation de Merca s'appelle Victorio-d'Africa, en bordure de la station agricole de la Somalie italienne, fondée en 1912 par Romolo Onor, dont la tombe est à Merca, mais qui mourut en 1918 à Genale, chef-lieu, si l'on peut dire, de cette immense ferme expérimentale de 407 hectares, sur laquelle furent installés cent vingt colons avec leurs familles et vingt ouvriers spécialistes. C'est à Genale que furent étudiées toutes les cultures pouvant être entreprises dans la colonie : coton, manioc, ricin, tabac, arachide, ramie, de même que banane, pamplemousse, papaye, mangue, noix de coco. À côté de la station agricole proprement dite, se sont créées, comme à Villabruzzi - comme d'ailleurs dans les marais Pontins - les aziendas dont la route longe pendant des kilomètres les terres encore cultivées en dépit du destin qui les condamne cependant à retourner en friche. On comptait avant la guerre 25.000 hectares irrigués, divisés en cent sept concessions. La culture principale était la banane. Vittorio-d'Africa était son lieu de concentration et d'emballage, Merca son port d'embarquement. À Vittorio-d'Africa, l'immeuble du " Consorzio " achève de se délabrer, et les dizaines de milliers de panneaux démontés de caisses d'emballage de se pourrir, comme à Merca le wharf de se désagréger. Avec ses 12.000 habitants, ses 210 Italiens, et ce petit wharf à double Decauville, à doubles grues et à doubles glissières de chargement, bien abrité du vent d'est qui pousse devant lui des lames rageuses, ce port suffisait à ce trafic. Il était le second de la colonie, et était appelé " le port de la banane ". C'est de Merca que l'Italie recevait en effet la majeure partie de ces fruits. L'hôtel porte d'ailleurs un joli nom - Hôtel de la Banane d'Or " Albergho Banana d'Oro ". C'est encore à Merca que se trouve l'institut de recherches sur les maladies du bétail, fondé en 1912, et dont les études sur la peste bovine font autorité, de même que celles sur les maladies des chameaux. Le laboratoire de recherches bactériologiques et parasitologiques est tout à fait moderne et s'appuie sur des autoclaves de désinfection et des frigorifiques pour la conservation des sérums.


Ainsi l'Europe, personnifiée ici par l'Italie, a-t-elle apporté à ce groupement de maisons primitives la marque tangible de sa présence, et les constructions qu'elle a édifiées ont-elles donné à cette petite cité, tassée sur elle-même, délabrée, abandonnée à toutes les sordides fantaisies de la mer, des vents et de la limaille du sable, un caractère d'agrément, de grâce, de propreté, qui en fait une résidence coquette et facile à vivre. Une huilerie presse modestement ses arachides, sans autre ambition que celle de répondre aux besoins locaux. Et, dans les sables, planté avec orgueil, s'érige un bateau, le " Sant'-Emilio ", de toutes pièces construit sur place et qui pourra, avec ses 75 chevaux porter environ 150 tonnes de marchandises. Pitchpin au carré et dans les deux cabines, plats-bords en chêne, tôles marquées par l'usage, deux hélices qui ont déjà battu la mer, tout est du matériel de récupération. Le tracteur s'affaire déjà pour préparer le lancement sans cale, le lendemain à marée haute. En repassant à Merca, j'ai vu le " Sant'-Emilio " à son mouillage : il viendra appuyer le cabotage auquel se livre déjà un autre modeste bateau, le " San-Domenico ", de Massaoua à Mogadiscio - unique liaison, maritime, terrestre ou aérienne, qui existe entre les deux ex-colonies italiennes...


A Brava, ville cimetière


C'est au théâtre que pénètre le voyageur qui arrive à Brava. Du haut des dunes d'un rouge presque sonore, sur l'étroite bande de sable que constitue le rivage, dans le poudroiement de cette fin de journée chauffée dès l'aurore par un soleil sans pitié, un troupeau de cubes ternes apparaît. De sa tête s'élance une Jetée qui atteint un îlot sombre, frangé d'écume, sur lequel se dresse un phare bas, trapu et carré. A droite, en une ligne interrompue, rongés au cours des siècles par l'Océan, les rochers maintenant presque en poussière, continuent à irriter le flot et à se noyer dans son écume. Cette barricade madréporique, très étroite, longue d'environ deux kilomètres, se termine par un îlot plus important sur lequel se dresse la " Torre Mnara ", que les Portugais avaient construite, et sur le sommet de laquelle ils allumaient un feu lorsqu'un navire était signalé.


Comme en toutes choses, la réalité vue de près bafoue le rêve : Brava est le ramassis habituel des maisons arabes, dépenaillées, avec leurs façades souillées par le temps, et qui se soucient bien peu d'effacer ces souillures. Les ruines, les démolitions faute d'entretien, comme à l'accoutumée se mêlent aux bâtisses qui tiennent encore debout. Les unes soutiennent évidemment les autres. Le tout est entassé au hasard, sans voies tracées, avec des cocotiers, quand il y a un peu de place, dont les plumets sont décharnés, desséchés pour la plupart, et qui, fatigués d'être sur terre, semblent maudire leur célibat. Quelques maisons s'ornent de la véranda fermée surplombant l'entrée et appuyée sur deux épaisses colonnes hexagonales. On retrouve là l'empreinte portugaise. C'est que Brava est riche d'un passé actif. Elle possède vraisemblablement la plus ancienne mosquée de toute la côte, qui fut construite en l'an 1100. Mais surtout elle est l'unique point d'eau naturelle, d'une rare pureté, à laquelle tous les navigateurs ont eu recours. Brava fut ainsi longtemps le point le plus fréquenté de toute la côte du Bénadir. Il ne demeure plus rien, aujourd'hui, de tous ces chocs de l'histoire. L'Italie avait songé à faire de Brava le chef-lieu de sa colonie. L'air y est léger. Une brise fraîche et constante restitue au corps le bonheur de vivre, aux poumons celui de respirer pleinement. Et surtout cette eau pure en abondance retenait l'attention. Ailleurs, il faut distiller l'eau de mer. Ici elle est naturelle, et à ce point inépuisable que lors de la préparation de la campagne d'Éthiopie les 16.000 hommes de la division Graziani purent camper pendant des mois à Brava sans jamais manquer d'eau. En renonçant cependant à ses intentions, l'Italie s'est contentée d'édifier le bâtiment de la résidence. C'est virtuellement l'unique bâtiment moderne. D'ailleurs, contre les 9.000 habitants supposés, les Italiens ne se comptent que 25 au maximum, dont 4 s'affairent à la tannerie. Celle-ci, créée en 1919, demeure en activité. Ses deux moteurs, de 50 et de 35 chevaux, qui marchent alternativement, ont l'obligeance de donner la lumière à quelques maisons en ville, de 18 à 23 heures. Ils permettent à la fabrique de traiter 1.000 quintaux de cuir par mois, boeuf, chèvre, dig-dig (sorte de gazelle en réduction, guère plus grosse qu'un chat), et de sortir annuellement une moyenne de 15.000 paires de chaussures et sandales. Les extraits tannants (mimosa) sont achetés au Kenya, les colorants en Union sud-africaine. Au total, 150 indigènes travaillent dans cette tannerie, unique industrie de cette si modeste cité qui semble, elle aussi renoncer à vivre.


Il faut presque avoir le pied marin pour atteindre l'extrémité de la jetée, au pied du phare. Moitié digue, moitié wharf, l'une ne vaut pas mieux que l'autre. Le Decauville est littéralement rongé, le ciment à la lèpre, le platelage en bois est pourri. Quant au phare, sans lanterne ni lentille, sa porte est entr'ouverte et semble battre au vent. Ce qui était les palans de chargement n'est plus que deux moignons rouillés et rongés, qui se dressent à l'extrémité de ce wharf comme deux mausolées. C'est bien un pèlerinage. Quatre ou cinq boutres, tirés à terre, lamentablement couchés sur leurs flancs, semblent être eux aussi voués à la démolition. Sur tout cela, une seule rumeur celle de la mer. Mais nul indice de vie. Bien au contraire, l'atmosphère lourde des cimetières. On se prend à ironiser sur ce nom de " Brava " que porte encore cette cité malheureuse, aujourd'hui rejetée hors de la civilisation.


Nous abandonnerons demain cette cité résignée, qui semble porter son deuil. Le soir, à l'auberge, pour ne rien boire d'ailleurs parce qu'il n'y a rien à boire - pas même du vin ! - tous les résidants italiens se retrouvent. Leur maigre poignée n'est pas bruyante comme la petite colonie d'Hargeisa qui, tant bien que mal, s'affaire à ses camions, ce qui lui donne encore l'illusion de vivre. Ici, résignée, mais abattue, elle parle de la patrie, et du fameux " Toscana ". Brava est une leçon de cafard.


L'écriteau anglais, au carrefour, porte, en milles " Gélib 100 - Pangheni 135 - Ghisimaio 172. " Tout ceci, c'est encore la Somalie ex-italienne, terre interdite.


Mais il porte aussi " Nairobi 654 ". Et cela, c'est la capitale de celle qui va donner sa réplique à l'Union sud-africaine, patiemment édifiée depuis 1919 par les Britanniques, l'Union est-africaine - la terre qui se lève.


(1) Voir le Mande des 17, 18, 28-29, 31 juillet, 19, 20 et 21 novembre 1946.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_008977.xml</field>
    <field name="pdate">1946-11-23T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
