<?xml version='1.0' encoding='utf8'?>
<add>
  <doc>
    <field name="title">Cinquante questions importantes figurent à l'ordre du jour de rassemblée des Nations unies</field>
    <field name="author">G.-Jean REYBAZ.</field>
    <field name="id">eupr_009060.xml</field>
    <field name="description"><![CDATA[


Les délégués à l'assemblée générale des cinquante-huit notions membres de l'O.N.U. ne seront pas seuls présents au palais de Chaillot dès la troisième décade de septembre : c'est l'Organisation presque entière qui, venant de Long-Island, s'y sera transportée. Le conseil de sécurité y siégera, - pour poursuivre notamment l'élude des problèmes palestinien et indonésien. De même le conseil économique et social, le conseil de tutelle et la plupart des autres organes de l'O.N.U. ainsi que des représentations substantielles des " organismes Spécialisés " rattachés à quelque titre à l'Organisation.


Le rapport du secrétaire général, dont " le Monde " a publié les grandes lignes et qu'un récent bulletin a commenté, indique suffisamment les difficultés auxquelles aura à faire face le Parlement mendiai lors de sa troisième session ordinaire. M. Trygve Lie les a résumées à son départ pour Paris, en observant que le succès de cette session dépendrait de l'entente entre les cinq grandes nations...


Le seul énoncé des principales questions figurant au rapport du conseil de sécurité permet de se faire une idée des débats dont le sobre et harmonieux édifice de Chaillot répercutera les échos : on y trouve les questions grecque, égyptienne, indonésienne, palestinienne, tchécoslovaque (l'affaire coréenne est du ressort de l'assemblée), le différend Inde-Pakistan, la nomination d'un gouverneur du Territoire libre de Trieste, le contrôle de l'énergie atomique, l'admission de nouveaux membres, la procédure de vote au conseil de sécurité, la carence du comité militaire... La liberté de l'information, l'un des points du rapport du conseil économique et social, fournira sans doute à M. Vychinski ou à M. Arutiunian l'occasion de citer en exemple son pays, où, comme on sait, la situation des journalistes ne rappelle en rien celle des nouvellistes dans l'Espagne de Figaro : " On peut écrire sur tout, à condition... " Les Arabes joueront du pétrole, et les Israéliens, pour peu qu'une décision leur soit favorable, remarqueront avec ironie que l'on ne saurait accuser l'O.N.U. de se laisser influencer par la dense et puissante population Juive de New-York. Les nombreux " observateurs " venus de " tras los montes " s'efforceront dans la coulisse d'émousser le sentiment antifranquiste, encore vivace au sein de l'O.N.U... La question espagnole sera du reste évoquée à rassemblée par diverses nations de l'Amérique latine.


L'élection de trois membres du conseil de sécurité et de six du conseil économique et social donnera lieu à de vives compétitions, et le rapport de ce dernier organisme sur l'administration britannique du Tanganyika et belge du Ruanda-Urundi permettra aux représentants des peuples libres d'au delà le rideau de fer de dénoncer une fois de plus les méfaits du colonialisme...


Le veto, l'énergie atomique, les admissions...


La liste des questions figurant à l'ordre du jour - soixante, dont cinquante sont d'une égale importance, a déclaré M. Trygve Lie - peut d'ailleurs s'allonger au cours de la session. Mais les principales, celles qui polariseront l'attention des délégués, seront, selon toute vraisemblance, les questions du veto, de l'énergie atomique, et l'admission de nouveaux membres.


Le veto, à vrai dire, n'existe pas à l'O.N.U. Ce que l'on y désigne généralement par ce terme est le vote négatif de l'une des cinq grandes puissances au conseil de sécurité dans une question de fond, qui entraine automatiquement le rejet d'une proposition si ladite puissance n'a pas spécifié (procédure non prévue par la charte mais qui tend à s'instituer) que son vote n'aurait point une telle conséquence.


À l'assemblée intérimaire, ou " petite assemblée ", dont le mandat vient à expiration mais qui a de grandes chances, sinon de devenir définitif, du moins d'être prolongé pour une année, - seules l'Argentine et la Nouvelle-Zélande persistent à réclamer des amendements à la charte à l'effet de modifier la procédure de vote au conseil. Les efforts, constructifs, de la commission intérimaire tendent plutôt à établir une série de règles destinées à guider les membres du conseil. Sur les quatre-vingt-dix-huit décisions possibles de ce dernier, allant de l'élection du président à l'imposition de sanctions et à l'utilisation de forces armées, la " petite assemblée " recommande que trente-six soient considérées comme " de procédure " et non sujettes, par suite, au veto, et que vingt et une relatives à des questions ressortissant à un règlement pacifique soient tranchées par le vote de sept quelconques des onze membres du conseil.


La commission de contrôle de l'énergie atomique a. on le sait, remis à l'assemblée générale le mandat qu'elle en avait reçu. Les positions, apparemment irréductibles, prises de part et d'autre et qui l'ont amenée à cette décision, sont connues de nos lecteurs. L'U.R.S.S. ne veut à aucun prix d'un contrôle s'étendant, par la voie d'inspections, à son territoire. Or à défaut de telles inspections le contrôle serait inopérant.


La question de l'admission de nouveaux membres est celle qui a suscité le plus grand nombre de votes négatifs au conseil de la part de grandes puissances. L'assemblée aura, pour la guider en ce domaine, un avis de la Cour internationale de justice. L'Union soviétique a constamment insisté pour que les deux groupes d'États candidats soient admis, ou aucun. La Cour, par neuf voix contre six, a estimé que le seul critère pour les admissions doit être celui qu'indique la charte : être un État pacifique, qui accepte les obligations de cette dernière et, au jugement de l'Organisation, est capable de les remplir et disposé à le faire. Elle a spécifié en outre qu'un État membre ne peut faire dépendre son vote affirmatif de la condition qu'un autre État candidat sera admis concurremment.


Vers le meilleur ou vers le pire


Telles sont les pièces des divers procès qu'aura à juger l'assemblée. Les délégués de l'U.R.S.S. et des pays soviétiques viendront-ils à Paris animés du seul désir de faire du palais de Chaillot un amplificateur, d'une puissance sans égale, pour leur propagande ? Les perspectives seraient alors fort sombres. La course aux armements se poursuivrait et s'intensifierait, aggravant les menaces à la paix.


La tournure que prendront les négociations des Quatre à Moscou et à Berlin, ailleurs éventuellement, l'élection présidentielle américaine en novembre, influeront certainement sur le cours des débats. Du choix du président de l'assemblée (les noms le plus généralement cités sont ceux de M. Arce, l'Argentin qui a présidé la session spéciale de l'assemblée sur la Palestine au printemps dernier, du docteur Evatt, ministre des affaires étrangères d'

Australie

, et de M. Georges Bidault, ancien ministre des affaires étrangères), du tact et de l'énergie dont lui et les présidents des grandes commissions feront preuve dépendra d'autre part, dans une large mesure, le succès ou l'insuccès de la session parisienne de l'assemblée générale des Nations unies.


]]></field>
    <field name="url">http://localhost:8983/solr/eufr/browse?q=id:eupr_009060.xml</field>
    <field name="pdate">1948-09-04T00:00:00Z</field>
    <field name="pubname">Le Monde</field>
    <field name="pubname_short">Le Monde</field>
    <field name="pubname_long">Le Monde</field>
    <field name="other_source">Inconnu</field>
    <field name="source_date">Inconnu</field>
    <field name="section">Inconnu</field>
  </doc>
</add>
