Dossier recherche d'informations - M2 TDL

Noms de dossiers :
```
documents_html : documents provenant du site Europresse
    └───europresse_all_documents.html : résultat de la fusion des documents
    └───documents_collectes : documents originaux collectés sur Europresse
```

```
documents_indexes : documents prêts à être indexés dans Solr
    └───europresse_ner_utf8_2 : documents txt ner
    └───europresse_txt : documents en format txt
    └───ri_20_xml : premiers fichiers xml résultant du script euro2solr.py
    └───ri_20_xml_ents : fichiers xml avec les entités nommées
    └───ri_20_xml_postag : fichiers xml avec les annotations en parties du discours
```

    
```
scripts_python : scripts python utilisés
    └───ri_20_xml_ents
        |
        └───conf
            └───managed-schema
            └───solrconfig.xml
            |
            └───velocity : fichiers de configuration Velocity pour les entités nommées
        
     └───ri_20_xml_postag
        |
        └───conf
            └───managed-schema
            └───solrconfig.xml
            |
            └───velocity : fichiers de configuration Velocity pour les parties du discours
```

            
            
`    SADOUN_recherche_d_informations.docx : rapport du dossier`
