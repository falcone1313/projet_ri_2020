import spacy
import os
import re
import glob

nlp = spacy.load("fr")    
os.chdir(r"C:\Users\User\Desktop\R\europresse_txt")
for file in glob.glob("*.txt"):

    with open(file, encoding='utf-8') as f:
        first_line_a = f.readline()
        first_line = first_line_a.replace('<', '&#60;').replace('>', '&#62;').replace('&', '&#38;').replace('%', '&#37;').replace("'", "&#39;").replace('(', '&#40;').replace(')', '&#41;').replace('*', '&#42;').replace('+', '&#43;').replace('/', '&#47;').replace('=', '&#61;').replace('?', '&#63;').replace('@', '&#64;').replace('[', '&#91;').replace('\'', '&#92;').replace(']', '&#93;')
    doc = nlp(first_line)
    newxml = file[:-4] + "_postag.xml"
    outF = open(newxml, "w", encoding='utf-8')
        
    outF.writelines("<?xml version='1.0' encoding='utf8'?>\n<add>\n\t<doc>\n\t\t<field name=\"id\">" + file[:-4] + ".xml</field>\n\t\t<field name=\"description\">" + first_line + "</field>\n\t\t<field name=\"url\">http://localhost:8983/solr/ri20_xml_postag/browse?q=id:" + file[:-4] + "_postag.xml</field>\n")
    
    for token in doc:
        
        outF.writelines("\t\t<field name=\"{1}\">{0}</field>\n".format(
                token.text,
                token.pos_,
            ))
    outF.writelines("\t</doc>\n</add>")
    print("File: " + newxml + " done")
    
    outF.close()