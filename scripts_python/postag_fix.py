import re
import os
import re
import glob
from xml.etree import ElementTree as et

os.chdir(r"C:\Users\User\Desktop\R\europresse_txt")
for file in glob.glob("*.xml"):
    xmlnew = file[:-7] + ".xml"
    xmlout = open(xmlnew, 'w', encoding='utf-8')
    
    for line in open(file, 'r', encoding='utf-8'):
        output_line = line
        output_line = line.replace('&<', '&#38;<').replace('&#41', '&#41;').replace('&#60', '&#60;').replace('&#62', '&#62;').replace('&#38', '&#38;').replace('&#37', '&#37;').replace("&#39", "&#39;").replace('&#40', '&#40;').replace('&#41', '&#41;').replace('&#42', '&#42;').replace('&#43', '&#43;').replace('&#47', '&#47;').replace('&#61', '&#61;').replace('&#63', '&#63;').replace('&#64', '&#64;').replace('&#91', '&#91;').replace('&#92', '&#92;').replace('&#93', '&#93;')
        xmlout.writelines(output_line)
    print("file: " + xmlnew + " done !")