"""
Create Solr update XML from Europresse HTML
@author: ruizfabo
"""

from bs4 import BeautifulSoup as bs
# https://dateparser.readthedocs.io/en/latest/
import dateparser 
from datetime import datetime as dt
from lxml import etree
import os
import re


# IO ---------------------------------------------------------------------------

indir = r"C:\Users\User\Desktop\R\HTML_europresse"
oudir = indir + "_newaera"
if not os.path.exists(oudir):
    os.makedirs(oudir)


# Constants --------------------------------------------------------------------
# regular expression for date parsing
secre = re.compile("(.+), +(.+)", re.DOTALL)
# name of the index
index_name = "eufr"
# number to start file numbering with
start_from = 3500

# Processing -------------------------------------------------------------------

# create the HTML parser
parser = etree.HTMLParser()


def extract_section_date_metadata(st, rx):
    """
    Extract the date for the article
    @param rx: regular expression
    """
    esti_date = False
    st = re.sub(" - [^,]+", "", st)
    st = re.sub(" [0-9]+ mots", "", st)
    st = re.sub(", p\. [^\n]+$", "", st) # mardi 18 septembre 2018, p. 27
    if "," in st:
        has_md = re.match(rx, st)
        if has_md:
            sec = has_md.group(1)
            datestr = has_md.group(2)
            dob = dateparser.parse(datestr.strip())
        else:
            #print("BAD", st)
            pass
    else:
        #print("SBDATE", st)
        dob = dateparser.parse(st)
        if dob is None:
            sec = st
            dob = dateparser.parse("2018")
            esti_date = True
        else:
            sec = "Inconnu"
    #print("##", sec, dob.strftime("%Y-%m-%dT00:00:00Z"), esti_date, "\n\n")
    return sec.strip(), dob.strftime("%Y-%m-%dT00:00:00Z"), esti_date


def create_xml(idir, odir, start_nbr=start_from):
    """
    Create XML in Solr update format based on Europresse HTML export
    (See U{https://lucene.apache.org/solr/guide/7_3/uploading-data-with-index-handlers.html#adding-documents})
    @param idir: input dir containing HTML
    @param odir: output dir for the XML to generate
    @param start_nbr: number to start file numbering with
    """
    dones = start_nbr
    for fn in os.listdir(idir):
        ffn = os.path.join(idir, fn)
        tree = etree.parse(ffn, parser)
        articles = tree.xpath("//article")
        for idx, art in enumerate(articles):
            # publication name
            try:
                pubname_short = art.xpath("header//span[@class='DocPublicationName']/text()")[0].strip()
                pubname_long = re.sub(
                    "\s{2,}", " ",
                    " ".join(art.xpath("header//span[@class='DocPublicationName']/text()")).strip())
                #print(pubname.strip())
            except IndexError:
                #print("BAD: ", fn)
                pubname_long = "Inconnu"
                pubname_short = "Inconnu"
                
            #other sources
            try:
                other_source = art.xpath(
                    "header//*[contains(@class, 'source-name-APD')]/text()")[0].strip()
            except IndexError:
                #print("BAD: ", fn)
                other_source = "Inconnu"
                
            #source date
            try:
                source_date = art.xpath(
                    "header//*[contains(@class, 'apd-sources-date')]/text()")[0].strip()
            except IndexError:
                #print("BAD: ", fn)
                source_date = "Inconnu"

    
                
            # title
            try:
                title = art.xpath(
                    "header//*[contains(@class, 'titreArticleVisu')]/text()")[0].strip()
            except IndexError:
                # Skip articls without title
                print("! Skip no title {}, ID {}".format(fn, idx))
                continue
            
            # author
            try:
                author = art.xpath(
                    "header//*[contains(@class, 'docAuthors')]/text()")[0].strip()
            except IndexError:
                author = "Inconnu"
                continue
            
            
            # section and date
            try:
                secspan = art.xpath("header//*[@class='DocHeader']/text()")[0].strip()
                sec, solrdate, date_status = extract_section_date_metadata(secspan, secre)
                if sec == "Inconnu":
                    #print(">>>", title)
                    pass
            except IndexError:
                # Skip articles without section or date
                print("! Skip no sec, date {}".format(fn, idx))
                continue
            text = " ".join(art.xpath("section/div[contains(@class, 'DocText')]//text()"))
            text = re.sub(r"[\n ]{2,}", "\n", text)
            #text = "\n".join(text.split("\n"))

            # create Solr XML
            fid = "eupr_{}.xml".format(str(dones+1).zfill(6))
            root = etree.Element("add")
            doc = etree.SubElement(root, "doc")
            title_e = etree.SubElement(doc, "field", name="title")
            author_e = etree.SubElement(doc, "field", name="author")
            docid = etree.SubElement(doc, "field", name="id")
            text_e = etree.SubElement(doc, "field", name="description")
            url = etree.SubElement(doc, "field", name="url") #url for Velocity
            #original_url = etree.SubElement(doc, "field", name="original_url")
            date = etree.SubElement(doc, "field", name="pdate")
            pubname_e = etree.SubElement(doc, "field", name="pubname")
            pubname_short_e = etree.SubElement(doc, "field", name="pubname_short")
            pubname_long_e = etree.SubElement(doc, "field", name="pubname_long")
            other_source_e = etree.SubElement(doc, "field", name="other_source")
            source_date_e = etree.SubElement(doc, "field", name="source_date")
            section_e = etree.SubElement(doc, "field", name="section")
            # set values
            docid.text = fid
            title_e.text = title
            author_e.text = author
            try:
                text_e.text = etree.CDATA(text)
            except ValueError :
                print("§§ CDATA error skip §§")
                continue
            url.text = "http://localhost:8983/solr/{}/browse?q=id:{}".format(
                index_name, fid)
            date.text = solrdate
            pubname_e.text = pubname_long
            pubname_short_e.text = pubname_short
            pubname_long_e.text = pubname_long
            other_source_e.text = other_source
            source_date_e.text = source_date
            
            section_e.text = sec
            # serialize
            sertree = etree.tostring(root, xml_declaration=True,
                                     encoding="utf8", pretty_print=True)
            ofn = os.path.join(odir, fid)
            # wb https://stackoverflow.com/questions/5512811
            with open(ofn, "wb") as ofh:
                print("=> {}".format(os.path.basename(ofn)))
                ofh.write(sertree)
            #print(dones)
            dones += 1


if __name__ == "__main__":
    create_xml(indir, oudir)
