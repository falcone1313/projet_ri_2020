#coding=utf8

"""
Applique l'outil NERC-FR pour annoter des entités en français.
Dans la salle 4s04, à éxécuter depuis C:\Program Files (x86)\Java\jre1.8.0_211\bin>
si la commande "java" n'est pas reconnue
"""


import os
import re
import sys


# IO ---------------------------------------------------------------------------
#   xmls d'entrée
xmldir = r"C:\Users\DelLupo\Desktop\R\THE HTMLS_pretty_newaera"
#   dossier pour le texte extrait
txtdir = r"C:\Users\DelLupo\Desktop\R\europresse_txt"
#   dossier pour les textes étiquetés
nerdir = r"C:/Users\DelLupo\Desktop\R\europresse_ner_utf8_2"
#   dossier pour le XML après ajout d'étiquettes NER
newxml = xmldir + "_ents_utf8_2"

for dirname in xmldir, txtdir, nerdir, newxml:
    if not os.path.exists(dirname):
        os.makedirs(dirname)


# Config du NER ----------------------------------------------------------------
# NER tool infos
#   chemin à l'outil
ner_tool = r"C:\Users\DelLupo\Desktop\R\annotation\nerc-fr\opennlp\lib\opennlp-tools-custom-0.0.1.jar"
#   chemin au modèle
ner_model = r"C:\Users\DelLupo\Desktop\R\annotation\nerc-fr\models\nerc\nerc-fr.bin"
#   commande d'étiquetage
ner_command_str = "java -Dfile.encoding=UTF8 -jar {jar} TokenNameFinder {model} < {infile} > {outfile}"

# Regex pour sortir entités étiqueteés
#   on suppose qu'aucune entité ne contient "<"
#   autrement faire .+? pour le groupe
ner_re = re.compile(r"<START:([^>]+)>([^<]+)<END>")


# Traitement -------------------------------------------------------------------

def extract_text_lines(fn):
    """
    Extraction de lignes du texte des XML. Une librairie de traitement
    XML a été évité pour éviter pb de config dans la salle de cours.
    Normalement à faire avec lxml ou semblable.
    @param fn: nom du fichier xml
    """
    lines = open(fn, mode="r", encoding="utf8").readlines()
    try:
        start_line = [line for line in lines
                      if "name=\"description\"" in line][0]
    except IndexError:
        print("ERROR: Fichier {} n'a pas le format prévu".format(fn))
        return ""
    try:
        end_line = [line for line in lines if line.strip() == "]]></field>"][0]
    except IndexError:
        print("ERROR: Fichier {} n'a pas le format prévu".format(fn))
        return ""
    start_idx = lines.index(start_line)
    end_idx = lines.index(end_line)
    txt = " ".join([line.strip() for line in lines[start_idx+1:end_idx]])
    return txt


def write_text(fn, txt):
    """
    Écriture du texte extrait des XML
    @param fn: nom du fichier de sortie
    @txt: chaîne avec le texte
    """
    with open(fn, mode="w", encoding="utf8") as ofd:
        ofd.write(txt)


def tag_ner(fn, ofn):
    """
    Étiquetage des entités avec outil Java
    @param fn: nom du fichier d'entrée
    @param ofn: nom du fichier de sortie (étiqueté)
    """
    os.system(ner_command_str.format(jar=ner_tool, model=ner_model, infile=fn, outfile=ofn))
    

def add_ner_to_xml(nerf, xmlin, xmlout):
    """
    Ajout des entités au XML. On a évité les librairies XML pour éviter pb
    de config dans la salle de cours.
    Normalement à faire avec lxml ou semblable.
    @param nerf: path au fichier txt avec étiquettes
    @param xmlin: path au fichier XML original
    @param xmlout: path au fichier XML avec étiquettes
    """
    with open(nerf, mode="r", encoding="utf8") as nfd, open(xmlin, mode="r", encoding="utf8") as xfd, open(xmlout, mode="w", encoding="utf8") as xout:
        txt = nfd.read()
        ents = re.finditer(ner_re, txt)
        ent_elements = []
        for ent in ents:
            etype = ent.group(1)
            estr = ent.group(2).strip()
            estr = re.sub("[.,;]\s*$", "", estr)
            xml_ele = "\n    <field name=\"enti_{}\">{}</field>".format(
                etype.lower(), estr.strip())
            ent_elements.append(xml_ele)
        xmltxt = xfd.read()
        if len(ent_elements) > 0:
            newxml = xmltxt.replace(
                "  </doc>",
                "".join(set(ent_elements)) + "</doc>")
            xout.write(newxml)
        else:
            xout.write(xmltxt)

        
def main():
    """Run"""
    for fn in sorted(os.listdir(xmldir)):
        print("+ {}\n  - Extract".format(fn))
        ffn = os.path.join(xmldir, fn)
        txt = extract_text_lines(ffn)
        txt_fn = "".join((os.path.splitext(fn)[0], ".txt"))
        txt_ffn = os.path.join(txtdir, txt_fn)
        write_text(txt_ffn, txt)
        ner_ffn = os.path.join(nerdir, txt_fn)
        print("  - Tag NER")
        tag_ner(txt_ffn, ner_ffn)
        print("  - New XML")
        new_xml_ffn = os.path.join(newxml, fn)
        add_ner_to_xml(ner_ffn, ffn, new_xml_ffn)
        

if __name__ == "__main__":
    main()      

    
